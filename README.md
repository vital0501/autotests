# README #

## Описание ##

Проект для автоматического smoke и регрессионного тестирования.

## Запуск ##

## Локальный запуск ##

```
#!python

python run.py --suite %suite_name%|include_testcase=testcase1,testcase2|exclude_testcase=testcase3|url=%url%|data=%data_collection_name% --browser %browser_name%
```

## Параметры:##
**suite** - Id тестсьюта, обязательны параметр.

**testcase**: Id тесткейса, задается если из всего тестсьюта необходимо выполнить только несколько тесткейсов

**exclude_testcase**: Id тесткейсов, которые необходимо исключить при запуске.

**data**: Id коллекции данных для тестов

**browser**: Браузер для запуска. В данный момент поддерживается: firefox, chrome

##Дополнительные параметры:##

### Отчеты и нотификации: ###
**--testrail_notification** - Включает интеграцию с TestRail.

**--email_notification** - Включает email нотификация при провале тестов. По умолчанию: development@vsemayki.ru. При необходимости можно поменять, см --default_email.

**--zabbix_notification** - Включает интеграция с Zabbix.

**--telegram** - Включает интеграцию с телеграмм. 

### Настройки для отчетов и нотификаций: ###

**--default_email %email%** - Изменить default_email

**--append_emails %email1,email2%** - Дополнительные email для отправки нотификиции

**--testrail_run_id %RunId%** - Указывает Id для тестрана в TestRail для репорта результатов тестов

**--callback_url** - Указывает url на который после проведения тестов будет проведен GET запрос.


##Добавление тестов в Jenkins ##

Для добавления тестов в Jenkins необходимо в настройки сборки, добавить этап "Выполнить команду shell" с тектом:

```
#!bash

cd /home/vsemayki/projects/autotests/current
export DISPLAY=":99"
echo env
whoami
python run.py #Параметры запуска# --task_id ${JOB_NAME}-${BUILD_NUMBER}
```