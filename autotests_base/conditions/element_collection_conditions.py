from selenium.common.exceptions import StaleElementReferenceException


class ElementsCollectionCondition(object):
    def __init__(self, elements_collection):
        """

        :type elements_collection: autotests_base.elements.waiting_elements_collection.WaitingElementCollection
        """
        self.elements_collection = elements_collection

    @property
    def error_msg_postfix(self):
        raise NotImplementedError


class len_less(ElementsCollectionCondition):
    def __init__(self, elements_collection, count):
        """

        :type count: int
        :type elements_collection: autotests_base.elements.waiting_elements_collection.WaitingElementCollection
        """
        super().__init__(elements_collection)
        self.count = count

    def __call__(self, *args, **kwargs):
        try:
            return len(self.elements_collection) < self.count
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' contains {0} elements' .format(len(self.elements_collection))


class len_not_equal(ElementsCollectionCondition):
    def __init__(self, elements_collection, count):
        """

        :type count: int
        :type elements_collection: autotests_base.elements.waiting_elements_collection.WaitingElementCollection
        """
        super().__init__(elements_collection)
        self.count = count

    def __call__(self, *args, **kwargs):
        try:
            return len(self.elements_collection) != self.count
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' contains {0} elements'.format(len(self.elements_collection))


class len_equal(ElementsCollectionCondition):
    def __init__(self, elements_collection, count):
        """

        :type count: int
        :type elements_collection: autotests_base.elements.waiting_elements_collection.WaitingElementCollection
        """
        super().__init__(elements_collection)
        self.count = count

    def __call__(self, *args, **kwargs):
        try:
            return len(self.elements_collection) == self.count
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' contains {0} elements'.format(len(self.elements_collection))


class len_gte(ElementsCollectionCondition):
    def __init__(self, elements_collection, count):
        """

        :type count: int
        :type elements_collection: autotests_base.elements.waiting_elements_collection.WaitingElementCollection
        """
        super().__init__(elements_collection)
        self.count = count

    def __call__(self, *args, **kwargs):
        try:
            return len(self.elements_collection) >= self.count
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' contains {0} elements' .format(len(self.elements_collection))


class len_gt(ElementsCollectionCondition):
    def __init__(self, elements_collection, count):
        """

        :type count: int
        :type elements_collection: autotests_base.elements.waiting_elements_collection.WaitingElementCollection
        """
        super().__init__(elements_collection)
        self.count = count

    def __call__(self, *args, **kwargs):
        try:
            return len(self.elements_collection) > self.count
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' contains {0} elements' .format(len(self.elements_collection))


class len_more(ElementsCollectionCondition):
    def __init__(self, elements_collection, count):
        """

        :type count: int
        :type elements_collection: autotests_base.elements.waiting_elements_collection.WaitingElementCollection
        """
        super().__init__(elements_collection)
        self.count = count

    def __call__(self, *args, **kwargs):
        try:
            return len(self.elements_collection) > self.count
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' contains {0} elements' .format(len(self.elements_collection))


class len_change(ElementsCollectionCondition):
    def __init__(self, elements_collection, count):
        """

        :type count: int
        :type elements_collection: autotests_base.elements.waiting_elements_collection.WaitingElementCollection
        """
        super().__init__(elements_collection)
        self.count = count

    def __call__(self, *args, **kwargs):
        try:
            return len(self.elements_collection) != self.count
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' contains {0} elements' .format(len(self.elements_collection))