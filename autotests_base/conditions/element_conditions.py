from selenium.common.exceptions import StaleElementReferenceException, WebDriverException


class WaitingElementCondition(object):
    def __init__(self, element):
        """
        :type element: autotests_base.elements.waiting_elements.WaitingElement
        """
        self.element = element

    @property
    def error_msg_postfix(self):
        raise NotImplementedError

    def __call__(self, *args, **kwargs):
        raise NotImplementedError


class click(WaitingElementCondition):
    @property
    def error_msg_postfix(self) -> str:
        return 'not clicked!'

    def __call__(self, *args, **kwargs):
        try:
            try:
                self.element.click()
            except WebDriverException:
                return False
            return True
        except StaleElementReferenceException:
            return False


class exist(WaitingElementCondition):
    @property
    def error_msg_postfix(self) -> str:
        return 'not found!'

    def __call__(self, *args, **kwargs):
        try:
            return self.element.exist()
        except StaleElementReferenceException:
            return False


class not_exist(WaitingElementCondition):
    @property
    def error_msg_postfix(self) -> str:
        return ' found!'

    def __call__(self, *args, **kwargs):
        try:
            return not self.element.exist()
        except StaleElementReferenceException:
            return False


class text(WaitingElementCondition):
    def __init__(self, element, element_text):
        """

        :type element_text: str
        :type element: autotests_base.elements.waiting_elements.WaitingElement
        """
        super().__init__(element)
        self.element_text = element_text

    def __call__(self, *args, **kwargs):
        try:
            return self.element.text == self.element_text
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return 'text not equal. Expect: {0}. Actually: {1}'.format(self.element_text, self.element.text)


class text_in(WaitingElementCondition):
    def __init__(self, element, elements_text):
        """

        :type element_text: str
        :type element: autotests_base.elements.waiting_elements.WaitingElement
        """
        super().__init__(element)
        self.elements_text = elements_text

    def __call__(self, *args, **kwargs):
        try:
            return self.element.text in self.elements_text
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return 'text not equal. Expect: {0}. Actually: {1}'.format(self.elements_text, self.element.text)


class text_not_none(WaitingElementCondition):
    def __call__(self, *args, **kwargs):
        try:
            return bool(self.element.text) and self.element.text != "None"
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' text is None'


class input_type(WaitingElementCondition):
    def __init__(self, element, obj_type):
        """

        :type obj_type: str
        :type element: autotests_base.elements.waiting_elements.WaitingElement
        """
        super().__init__(element)
        self.obj_type = obj_type

    def __call__(self, *args, **kwargs):
        try:
            return self.element.get_attribute("type") == self.obj_type
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' input_type not {0}'.format(str(self.obj_type))


class is_enabled(WaitingElementCondition):
    def __call__(self, *args, **kwargs):
        try:
            return bool(self.element.is_enabled())
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' not enabled'


class is_displayed(WaitingElementCondition):
    def __call__(self, *args, **kwargs):
        try:
            return bool(self.element.is_displayed())
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' not displayed'


class is_not_displayed(WaitingElementCondition):
    def __call__(self, *args, **kwargs):
        try:
            return not bool(self.element.is_displayed())
        except StaleElementReferenceException:
            return True

    @property
    def error_msg_postfix(self) -> str:
        return ' is displayed'


class name_not_none(WaitingElementCondition):
    def __call__(self, *args, **kwargs):
        try:
            return bool(self.element.get_name())
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' is none'


class text_not_0(WaitingElementCondition):
    def __call__(self, *args, **kwargs):
        try:
            return int(self.element.text) != 0
        except (StaleElementReferenceException, ValueError):
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' text is 0'


class text_changed(WaitingElementCondition):
    def __init__(self, element, old_text):
        """

        :type obj_type: str
        :type element: autotests_base.elements.waiting_elements.WaitingElement
        """
        super().__init__(element)
        self.old_text = old_text

    def __call__(self, *args, **kwargs):
        try:
            return self.element.text != str(self.old_text)
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' text is 0'


class attribute_changed(WaitingElementCondition):
    def __init__(self, element, attribute, old_attribute_value):
        """

        :type obj_type: str
        :type element: autotests_base.elements.waiting_elements.WaitingElement
        """
        super().__init__(element)
        self.attribute = attribute
        self.old_attribute_value = old_attribute_value

    def __call__(self, *args, **kwargs):
        try:
            try:
                return getattr(self.element, self.attribute)() != self.old_attribute_value
            except AttributeError:
                return False
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return ' text is 0'


class attribute_equal(WaitingElementCondition):
    def __init__(self, element, attribute, attribute_value):
        """

        :type element_text: str
        :type element: autotests_base.elements.waiting_elements.WaitingElement
        """
        super().__init__(element)
        self.attribute_value = attribute_value
        self.attribute = attribute

    def __call__(self, *args, **kwargs):
        try:
            try:
                return getattr(self.element, self.attribute)() == self.attribute_value
            except AttributeError:
                return False
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        try:
            return 'attribute "{0}" not equal. Expect: {1}. Actually: {2}'.format(self.attribute,
                                                                                  self.attribute_value,
                                                                                  getattr(self.element,
                                                                                          self.attribute)())
        except AttributeError:
            return 'Object has no attribute "{0}"'.format(self.attribute)


class attribute_not_equal(WaitingElementCondition):
    def __init__(self, element, attribute, attribute_value):
        """

        :type element_text: str
        :type element: autotests_base.elements.waiting_elements.WaitingElement
        """
        super().__init__(element)
        self.attribute_value = attribute_value
        self.attribute = attribute

    def __call__(self, *args, **kwargs):
        try:
            try:
                return getattr(self.element, self.attribute)() != self.attribute_value
            except AttributeError:
                return False
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        try:
            return 'attribute "{0}" equal.'.format(self.attribute)
        except AttributeError:
            return 'Object has no attribute "{0}"'.format(self.attribute)


class property_equal(WaitingElementCondition):
    def __init__(self, element, property, property_value):
        """

        :type element_text: str
        :type element: autotests_base.elements.waiting_elements.WaitingElement
        """
        super().__init__(element)
        self.property_value = property_value
        self.property = property

    def __call__(self, *args, **kwargs):
        try:
            try:
                return self.element.get_attribute(self.property) == self.property_value
            except AttributeError:
                return False
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return 'property "{0}" not equal. Expect: {1}. Actually: {2}'.format(self.property,
                                                                             self.property_value,
                                                                             self.element.get_attribute(self.property))


class property_not_exist(WaitingElementCondition):
    def __init__(self, element, property):
        """

        :type element_text: str
        :type element: autotests_base.elements.waiting_elements.WaitingElement
        """
        super().__init__(element)
        self.property = property

    def __call__(self, *args, **kwargs):
        try:
            try:

                return True if self.element.get_attribute(self.property) is None else False
            except AttributeError:
                return False
        except StaleElementReferenceException:
            return False

    @property
    def error_msg_postfix(self) -> str:
        return 'Element has property "{0}". Expect: Element has not property.'.format(self.property,
                                                                             self.element.get_attribute(self.property))
