import time
from selenium.common.exceptions import NoSuchElementException, TimeoutException, WebDriverException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from autotests_base.conditions.element_conditions import WaitingElementCondition
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection


class BaseWaitingElement(object):
    def __init__(self, driver, selector, selector_type=By.XPATH, timeout=5, is_collection_item=False):
        self._selector_type = selector_type
        self._selector = selector
        self._driver = driver
        self._timeout = timeout
        self._is_collection_item = is_collection_item
        self._update = True
        self._element = None

    def get_attribute(self, attr_name):
        return self._finder().get_attribute(attr_name)

    def _finder(self):
        return self._driver.find_element(by=self._selector_type, value=self._selector)

    def __getattr__(self, item):
        return getattr(self._finder(), item)

    def assure(self, condition_class, *args, **kwargs):
        """
        :rtype: WaitingElement
        :type condition_class: WaitingElementCondition
        """
        timeout = self._timeout if 'timeout' not in kwargs.keys() else kwargs['timeout']
        condition = condition_class(self, *args)
        try:
            WebDriverWait(self._driver, timeout).until(condition, str(self) + condition.error_msg_postfix)
        except TimeoutException:
            WebDriverWait(self._driver, 1).until(condition, str(self) + condition.error_msg_postfix)
        return self

    def exist(self) -> bool:
        try:
            self._finder()
        except NoSuchElementException:
            return False
        return True

    def get_index(self) -> int:
        if self._is_collection_item:
            return int(self._selector.split('[')[-1][:-1]) - 1
        else:
            return None

    def __str__(self):
        return "{0} [selector_type={1}, selector={2}, timeout={3}]".format(self.__class__.__name__,
                                                                           self._selector_type,
                                                                           self._selector,
                                                                           self._timeout)

    def scroll_to_element(self):
        raise NotImplementedError


class WaitingElement(BaseWaitingElement):
    def scroll_to_element(self):
        location = self.location
        self._driver.execute_script("window.scrollTo({0}, {1});".format(str(int(location['x']) - 10), str(int(location['y']) - 10)))
        return self

    def mouse_over_element(self, coordinates=None):
        if coordinates is not None:
            hov = ActionChains(self._driver).move_to_element_with_offset (self._finder(),
                                                                          xoffset=coordinates['x'],
                                                                          yoffset=coordinates['y'])
        else:
            hov = ActionChains(self._driver).move_to_element(self._finder())

        hov.perform()
        return self

    def click(self):
        try:
            return self._finder().click()
        except Exception as e:
            self.scroll_to_element()
            self.mouse_over_element()
            try:
                return self._finder().click()
            except WebDriverException:
                self.mouse_over_element(coordinates={'x': 15, 'y': 15})
                return self._finder().click()


class AndroidWaitingElement(BaseWaitingElement):
    def scroll_to_element(self):
        pass

    def __init__(self, driver, selector, selector_type=By.ID, timeout=5, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item)


class SubWaitingElement(WaitingElement):
    def __init__(self, waiting_element, sub_selector, element_type=None, is_collection_item=False):
        """

        :type sub_selector: str
        :type waiting_element: WaitingElement
        """
        super().__init__(waiting_element._driver,
                         waiting_element._selector + sub_selector,
                         waiting_element._selector_type,
                         waiting_element._timeout,
                         is_collection_item=is_collection_item)

        if element_type:
            self.__class__ = element_type
            element_type.__init__(self, waiting_element._driver,
                                  waiting_element._selector + sub_selector,
                                  waiting_element._selector_type,
                                  waiting_element._timeout,
                                  is_collection_item=is_collection_item)


class SubWaitingElementCollection(WaitingElementCollection):
    def __init__(self, waiting_element, sub_selector, elements_type=WaitingElement, **kwargs):
        """
        :type sub_selector: str
        :type waiting_element: WaitingElement
        """
        super().__init__(waiting_element._driver,
                         waiting_element._selector +
                         sub_selector,
                         waiting_element._selector_type,
                         elements_type=elements_type,
                         **kwargs)
