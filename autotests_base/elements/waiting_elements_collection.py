from selenium.webdriver.support.wait import WebDriverWait


class WaitingElementCollection(object):
    def __init__(self, driver, selector, selector_type, elements_type=None, timeout=20, **kwargs):
        from autotests_base.elements.waiting_elements import WaitingElement
        self._driver = driver
        self._selector_type = selector_type

        self._elements_type = elements_type if elements_type is not None else WaitingElement
        self._selector = selector
        self._timeout = timeout

        self._selectors = None

        self._generate_selectors()
        self.current = 0
        self.kwargs = kwargs

    def _generate_selectors(self):
        join = lambda x: "({0})[{1}]".format(self._selector, x)
        self._selectors = [join(item + 1) for item in range(0, self._get_collection_len())]

    def _get_collection_len(self) -> int:
        return len(self._driver.find_elements(by=self._selector_type, value=self._selector))

    def _create_element(self, index: int):

        """

        :rtype: autotests_base.elements.waiting_elements.WaitingElement
        """
        return self._elements_type(self._driver,
                                   self._selectors[index],
                                   self._selector_type,
                                   self._timeout,
                                   is_collection_item=True, **self.kwargs)

    def __getitem__(self, index):
        """

        :type index: int
        """
        self._generate_selectors()
        if isinstance(index, slice):
            stop = self.__len__() if index.stop is None else index.stop
            return [self._create_element(i) for i in range(index.start, stop)]
        return self._create_element(index)

    def __len__(self) -> int:
        self._generate_selectors()
        return len(self._selectors)

    def __iter__(self):
        """

        :rtype: WaitingElementCollection
        """
        self.current = 0
        return self

    def __next__(self):
        if self.current == self.__len__():
            raise StopIteration
        s = self[self.current]
        self.current += 1
        return s

    def assure(self, condition_class, *args, **kwargs):
        self._generate_selectors()
        timeout = self._timeout if 'timeout' not in kwargs.keys() else kwargs['timeout']
        condition = condition_class(self, *args)
        WebDriverWait(self._driver, timeout).until(condition, str(self) + condition.error_msg_postfix)
        return self
