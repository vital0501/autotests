class AutotestBaseException(Exception): pass


class ConfirmPageException(AutotestBaseException): pass


class NameCatalogPageException(AutotestBaseException): pass


class RegistrationEmptyEmailException(AutotestBaseException): pass


class RegistrationEmailAlreadyExistException(AutotestBaseException):
    pass


class RegistrationIncorrectPasswordException(AutotestBaseException):
    pass


class RegistrationException(AutotestBaseException):
    pass


class NotImplementedMenuItemException(AutotestBaseException):
    pass


class ExternalProjectPageException(AutotestBaseException):
    pass


class CatalogExistException(AutotestBaseException):
    pass


class CatalogExistException(AutotestBaseException):
    pass


class NotFoundSubCategory(AutotestBaseException):
    pass


class PageMenuItemNotImplementedException(AutotestBaseException):
    pass


class CityNotImplemented(AutotestBaseException):
    pass


class NotFoundCityInListException(AutotestBaseException):
    pass


class CityNotFoundException(AutotestBaseException):
    pass


class DeliveryMethodNotImplemented(AutotestBaseException):
    pass


class CountryNotFoundException(AutotestBaseException):
    pass


class DeliveryNotFoundException(AutotestBaseException):
    pass


class PaymentNotFoundException(AutotestBaseException):
    pass


class DeliveryMethodsNotFoundException(AutotestBaseException):
    pass


class CatalogLoaderLoadingTimeException(AutotestBaseException):
    pass

