import time

from appium.webdriver.common.touch_action import TouchAction

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

import settings
from autotests_base.conditions.element_conditions import exist
from autotests_base.exception import ConfirmPageException
from autotests_base.elements.waiting_elements import WaitingElement, AndroidWaitingElement
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection


class BasePageObject(object):
    def __init__(self, driver, main_href, timeout, confirm=True, init=False, base_init=False, structute=None):
        self.base_init = base_init
        self._driver = driver
        self._main_href = main_href
        self._timeout = timeout
        self.structure = self._load_structure() if not structute else structute
        self.page_load_timeout = self._get_page_load_timeout()

        if init:
            self.open_main_page()

        if confirm:
            self._confirm()

    def mouse_move_by_offset(self, x, y):
        hov = ActionChains(self._driver).move_by_offset(x, y)
        hov.perform()
        time.sleep(1)

    def go_to_href(self, href, page_class, timeout=None, main_href=None):
        self._driver.get(href)
        return self._get_page_object(page_class, timeout=timeout, href=main_href)

    def get_timeout(self) -> int:
        return self._timeout

    def search(self, selector, selector_type=By.XPATH, element_type=WaitingElement, timeout=None):
        """

        :rtype: WaitingElement
        """
        timeout = timeout if timeout else self._timeout
        return element_type(self._driver, selector, selector_type, timeout=timeout)

    def search_all(self, selector, selector_type=By.XPATH, elements_type=WaitingElement):
        """

        :rtype: WaitingElementCollection
        """
        return WaitingElementCollection(self._driver, selector, selector_type, elements_type)

    def _confirm(self, template_element=None):
        try:
            self.set_timeout(self._get_page_load_timeout())
            if template_element is None:
                self.search(self.structure.CONFIRM).assure(exist)
            else:
                self.search(self.structure.CONFIRM .format(template_element)).assure(exist)
            self.set_timeout(self._timeout)
        except TimeoutException as e:
            raise ConfirmPageException(
                "Не получилось открыть \"{0}\" за {1} сек.".format(self._error_page_name(), self._get_page_load_timeout()))

    def _load_structure(self):
        if not self.base_init:
            raise NotImplementedError

    def set_timeout(self, value: int) -> None:
        self._timeout = value

    def _get_page_load_timeout(self):
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _error_page_name(self):
        return "BasePageObjectError"

    def _get_page_object(self, page_class, timeout=None, href=None, **kwargs):
        href = href if href else self._main_href
        timeout = self._timeout if timeout is None else timeout
        return page_class(self._driver, href, timeout, **kwargs)

    def open_main_page(self):
        self._driver.get(self._main_href)

    def is_alert_present(self) -> bool:
        try:
            WebDriverWait(self._driver, self.get_timeout()).until(EC.alert_is_present())
            return True
        except TimeoutException:
            return False

    def get_alert(self) -> Alert:
        return self._driver.switch_to.alert

    def back(self, page_class, timeout=None, **kwargs):
        self._driver.back()
        timeout = self._timeout if timeout is None else timeout
        return page_class(self._driver, self._main_href, timeout, **kwargs)

    def input_standard_field(self, xpath, value, scroll=True, clear=True):
        element = self.search(xpath)
        if scroll: element.scroll_to_element()
        if clear: element.clear()
        element.send_keys(value)


class BaseAndroidPageObject(object):
    def __init__(self, driver, timeout, confirm=True):
        self.driver = driver
        self.timeout = timeout
        self.structure = self._load_structure()
        if confirm:
            self._confirm()

    def search(self, selector, element_type=AndroidWaitingElement, timeout=None):
        """

        :rtype: AndroidWaitingElement
        """
        selector_type = By.XPATH if selector.startswith('//') else By.ID
        timeout = timeout if timeout else self.timeout
        return element_type(self.driver, selector, selector_type, timeout=timeout)

    def search_all(self, selector, selector_type=By.ID, elements_type=WaitingElement):
        """

        :rtype: WaitingElementCollection
        """
        return WaitingElementCollection(self.driver, selector, selector_type, elements_type)

    def _load_structure(self):
        raise NotImplementedError

    def _get_page_object(self, page_class, timeout=None, **kwargs):
        timeout = self.timeout if timeout is None else timeout
        return page_class(self.driver, timeout, **kwargs)

    def scroll_screen(self):
        action = TouchAction(self.driver)
        x = self.driver.get_window_size()['width']
        y = self.driver.get_window_size()['height']
        action.press(x=x/2, y=y - 1).move_to(x=0, y=-y+2).release().perform()

    def _mini_scroll(self):
        action = TouchAction(self.driver)
        x = self.driver.get_window_size()['width']
        y = self.driver.get_window_size()['height']
        action.press(x=x / 2, y=y - 1).move_to(x=0, y=int(-y*0.019)).release().perform()
        time.sleep(2)

    def _get_page_load_timeout(self) -> int:
        raise NotImplementedError

    def _error_page_name(self) -> str:
        raise NotImplementedError

    def _confirm(self):
        try:
            self.search(self.structure.CONFIRM, timeout=self._get_page_load_timeout()).assure(exist)
        except TimeoutException:
            raise ConfirmPageException(
                "Не получилось открыть \"{0}\" за {1} сек.".format(self._error_page_name(), self._get_page_load_timeout()))
