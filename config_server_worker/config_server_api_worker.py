import json
import requests

from config_server_worker.utils import get_response_data
from core.exception import NotConfiguredValueException


class TestConfigApiWorker(object):
    def __init__(self, server, api_version):
        self.server = server
        self.api_version = api_version

        self.api_url = self.server + 'api/v{0}/'.format(self.api_version)
        self.test_rail_url = self.server + 'service/testrail/'
        self.jenkins_url = self.server + ''

    @staticmethod
    def _send_post(url, **kwargs):
        return requests.post(url, json=json.dumps(kwargs))

    @staticmethod
    def _send_get(url):
        return requests.get(url)

    @staticmethod
    def _send_patch(url, **kwargs):
        return requests.patch(url, json=json.dumps(kwargs))

    @staticmethod
    def _send_put(uri, **kwargs):
        return requests.put(uri, json=json.dumps(kwargs))

    @staticmethod
    def get_response_data(response):
        return get_response_data(response)

    @staticmethod
    def _generate_query_string(**kwargs):
        result = {}
        for key in kwargs.keys():
            if kwargs[key]:
                result[key] = kwargs[key]
        return '?' + '&'.join(["{0}={1}".format(item, kwargs[item]) for item in result.keys()])

    def _send_patch_api_request(self, request, testrail_send=False, **kwargs):

        if len(kwargs) > 0 and testrail_send:
            return self._send_patch(self.api_url + request + self._generate_query_string(**kwargs))
        else:
            return self._send_patch(self.api_url + request, **kwargs)

    def _send_get_api_request(self, request, **kwargs):
        if len(kwargs) > 0:
            return self._send_get(self.api_url + request + self._generate_query_string(**kwargs))
        else:
            return self._send_get(self.api_url + request)

    def _send_put_api_request(self, request, **kwargs):
        return self._send_put(self.api_url + request, **kwargs)

    def _send_get_service_api_request(self, request, **kwargs):
        if len(kwargs) > 0:
            return self._send_get(self.test_rail_url + request + self._generate_query_string(**kwargs))
        else:
            return self._send_get(self.test_rail_url + request)

    def _send_put_service_api_request(self, request, **kwargs):
        if len(kwargs) > 0:
            return self._send_put(self.api_url + request + self._generate_query_string(**kwargs))
        else:
            return self._send_put(self.api_url + request)

    def _send_post_api_request(self, request, **kwargs):
        return self._send_post(self.api_url + request, **kwargs)

    def new_server_testrun(self, **kwargs):
        return self._send_post_api_request('testrun/', **kwargs)

    def get_server_testrun_data(self, server_testrun_id):
        return get_response_data(self._send_get_api_request('testrun/{0}/'.format(server_testrun_id)))

    def get_server_test_run_suites(self, server_testrun_id):
        return self.get_server_testrun_data(server_testrun_id=server_testrun_id)['suites']

    def search_testrun(self, **kwargs):
        return get_response_data(self._send_get_api_request('testrun/', **kwargs))

    def server_testrun_log(self, server_test_run_id, **kwargs):
        return self._send_patch_api_request('testrun/{0}/'.format(server_test_run_id), **kwargs)

    def get_testcase(self, testcase_id):
        return self.get_response_data(self._send_get_api_request('testcase', testcase_id=testcase_id))

    def get_test_run(self, testrun_id):
        return self.get_response_data(self._send_get_api_request('testrun/{0}/'.format(testrun_id)), )

    def get_last_android_apk_id(self):
        return self.get_response_data(self._send_get_api_request('last_apk_file/'))['id']

    def send_warning(self, testcase, testsuite, warning_class, warning_message, testrun, added_data=None,
                     warning_type=None, task_name=None):
        if task_name is not None:
            self._send_post_api_request('warning/', testcase=testcase, testsuite=testsuite, warning_class=warning_class,
                                        warning_message=warning_message, testrun=testrun, added_data=added_data,
                                        warning_type=warning_type)

    def find_testcases(self, task_name):
        return self.get_response_data(self._send_get_api_request('testrun', jenkins_task_name=task_name))


class TestCaseDataObject(object):
    def __init__(self, data, url=None):
        self._data = data
        self.skipped = data['skip']
        self.skip_reason = 'Skipped by configuration' if not data['skip_reason'] else data['skip_reason']
        self.url = url

        self.data = DataCollectionObject(data['data_objects'])

    def get_main_href(self):
        return self.url

    def get_skipped(self):
        return self.skipped

    def get_skip_reason(self):
        return self.skip_reason


class DataCollectionObject(object):
    def __init__(self, data):
        self._collection = data

    def __getattr__(self, item):
        try:
            return self._collection[item]
        except KeyError:
            raise NotConfiguredValueException('Не определены все переменные для тесткейса')

    def __str__(self):
        return str(self._collection)
