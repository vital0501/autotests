import json


def get_response_data(response):
    return json.loads(response.text)['response_data'] if response.text else {}


def get_response_message(response):
    return json.loads(response.text)['response_message']


def get_response_code(response):
    return json.loads(response.text)['response_code']