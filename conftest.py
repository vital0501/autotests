import pytest


def pytest_addoption(parser):
    parser.addoption("--suite", action="store", help="Set suite")
    parser.addoption("--flow", action="store", help="Set flow")
    parser.addoption("--basedir", action="store", help="Set basedir")
    parser.addoption("--browser", action="store", help="Set browser")
    parser.addoption("--testcase", action="store", help="Set testcase")
    parser.addoption("--exclude_testcase", action="store", help="Set exclude testcase")
    parser.addoption("--url", action="store", help="Set url")
    parser.addoption("--server_task_id", action="store", help="Set server_task_id")
    parser.addoption("--task_name", action="store", help="Set task_name")
    parser.addoption("--build_number", action="store", help="Set build_number")
    parser.addoption("--mobile", action="store", help="Set mobile")
    parser.addoption("--apk_id", action="store", help="Set apk_id")
    parser.addoption("--device_name", action="store", help="Set device_name")
    parser.addoption("--android_unicode_keyboard", action="store", help="Set android_unicode_keyboard")
    parser.addoption("--server_test_run_id", action="store", help="Set server_test_run_id")
    parser.addoption("--headless", action="store", help="Set headless")
    parser.addoption("--local_run", action="store_true", help="Set local_run")


@pytest.fixture(scope='class')
def headless(request):
    return request.config.getoption("--headless")


@pytest.fixture(scope='class')
def flow(request):
    return request.config.getoption("--flow")


@pytest.fixture(scope='class')
def local_run(request):
    return request.config.getoption("--local_run")


@pytest.fixture(scope='class')
def suite(request):
    return request.config.getoption("--suite")


@pytest.fixture(scope='class')
def suite(request):
    return request.config.getoption("--suite")


@pytest.fixture(scope='class')
def android_unicode_keyboard(request):
    return request.config.getoption("--android_unicode_keyboard")


@pytest.fixture(scope='class')
def device_name(request):
    return request.config.getoption("--device_name")


@pytest.fixture(scope='class')
def mobile(request):
    return request.config.getoption("--mobile") not in ['None', 'False']


@pytest.fixture(scope='class')
def apk_id(request):
    return None if request.config.getoption("--apk_id") is not None else int(request.config.getoption("--apk_id"))


@pytest.fixture(scope='class')
def server_test_run_id(request):
    return request.config.getoption("--server_test_run_id")


@pytest.fixture(scope='class')
def server_task_id(request):
    return request.config.getoption("--server_task_id")


@pytest.fixture(scope='class')
def task_name(request):
    return request.config.getoption("--task_name")


@pytest.fixture(scope='class')
def build_number(request):
    return request.config.getoption("--build_number")


@pytest.fixture(scope='class')
def url(request):
    return request.config.getoption("--url")


@pytest.fixture(scope='class')
def basedir(request):
    return request.config.getoption("--basedir")


@pytest.fixture(scope='class')
def browser(request):
    return request.config.getoption("--browser")


@pytest.fixture(scope='class')
def testcase(request):
    return request.config.getoption("--testcase")


@pytest.fixture(scope='class')
def exclude_testcase(request):
    return request.config.getoption("--exclude_testcase")
