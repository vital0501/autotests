import json
import os
import random
import traceback
from typing import Union

import allure
import pytest
import time
import settings

from selenium import webdriver
from config_server_worker.config_server_api_worker import TestConfigApiWorker, TestCaseDataObject
from module_projects.base.exception import BaseModuleException
from core.decorators import deprecated
from module_projects.base.exception import WebDriverSessionException
from run import BASE_DIR
from appium import webdriver as android_webdriver

testConfigApiWorker = TestConfigApiWorker(server=settings.CONFIG_SERVER_URL, api_version=settings.API_VER)
SUPPORT_BROWSER = {'firefox': webdriver.Firefox, 'chrome': webdriver.Chrome}


class BaseTest(object):
    flow = None
    driver = None
    main_page = None
    active_test = None
    _config_name = None
    _test_rail_id = None
    test_case = None
    browser = None
    headless = True
    mobile = False
    step_data = {}
    _testcase_name = None
    _testsuite_name = None
    _server_test_run_id = None
    _task_name = None

    local_run = False

    def open_page_by_url(self, url, cls, init=True, confirm=True):
        _page = cls(driver=self.driver,
                    url=url,
                    project_url=self.main_page,
                    timeout=settings.TIMEOUT,
                    confirm=confirm,
                    init=init)
        return _page

    def step(self, repeat_func, clean_func, step_test, step_page, **kwargs):
        error_text = ''
        for try_num in range(0, 3):
            try:
                with pytest.allure.step("{0} - Шаг номер {1}".format(repeat_func.__name__, try_num)):
                    print('Start {0}'.format(repeat_func.__name__))
                    return repeat_func(_test=step_test, _page=step_page, **kwargs)
            except Exception as e_step_error:
                with pytest.allure.step("Обнаружена ошибка при выполении теста."):
                    traceback.print_tb(e_step_error.__traceback__)
                    error_text = ' '.join([str(e_step_error.__class__.__name__), str(e_step_error)])

                    self.log_text(log_message="Ошибка: {0}" .format(error_text))
                    try:
                        self.log_screenshot()
                    except Exception:
                        self.log_text("Ошибка снятия скриншота")
                    self.log_txt_data(log_data=traceback.format_exc(), log_message="Stacktrace")

                    testConfigApiWorker.send_warning(testcase=self._testcase_name, testsuite=self._testsuite_name,
                                                     warning_class=e_step_error.__class__.__name__,
                                                     warning_message=str(e_step_error),
                                                     testrun=self._server_test_run_id, warning_type='step',
                                                     added_data=repeat_func.__name__, task_name=self._task_name)
                    if e_step_error.__class__ == WebDriverSessionException.__class__:
                        pytest.fail("Ошибка сессии веб драйвера при открытии страницы.")

                if try_num != 2:

                    with pytest.allure.step("Чистим изменения после шага."):
                        print('Start {0}'.format(clean_func.__name__))
                        try:
                            clean_func(_test=step_test, _page=step_page, **kwargs)
                        except Exception as clean_step_error:
                            traceback.print_tb(clean_step_error.__traceback__)
                            step_error_text = ' '.join([str(clean_step_error.__class__.__name__), str(clean_step_error)])

                            step_test = BaseTest()

                            self.log_screenshot()
                            self.log_txt_data(log_message="Stacktrace", log_data=traceback.format_exc())
                            self.log_text(log_message="Ошибка: {0}".format(step_error_text))

                            testConfigApiWorker.send_warning(testcase=self._testcase_name, testsuite=self._testsuite_name,
                                                             warning_class=clean_step_error.__class__.__name__,
                                                             warning_message=str(clean_step_error),
                                                             testrun=self._server_test_run_id, warning_type='clean',
                                                             added_data=clean_func.__name__, task_name=self._task_name)
                            raise clean_step_error
        else:
            with pytest.allure.step("Шаг завершился ошибкой! Тест провален."):
                pytest.fail(error_text)

    @pytest.yield_fixture(autouse=True)
    def init_and_teardown(self, request, suite, browser, url, testcase, task_name, exclude_testcase, server_test_run_id, mobile, headless, local_run, flow):

        testConfigApiWorker.server_testrun_log(server_test_run_id, status=6)
        self.browser = browser
        self.flow = int(flow)
        self.mobile = mobile
        self.headless = False if headless in ['False', 'false'] else True
        self._testcase_name = request.function.__name__
        self._testsuite_name = request.fspath.purebasename
        self._server_test_run_id = server_test_run_id
        self._task_name = task_name
        self.local_run = local_run

        self.log_text("RunID = {0}" .format(self._server_test_run_id))
        self.log_text("Task: {0}" .format(task_name))

        with pytest.allure.step('Подготовка'):
            with pytest.allure.step('Получение данных с сервера'):
                print("\nStart '{0}'" .format(request.function.__name__))

                if testcase != 'None':
                    if request.function.__name__ not in testcase.split('|'):
                        print('Skip because test not included in in-line parameter')
                        pytest.skip('Skip because test not included in in-line parameter')

                if exclude_testcase != 'None':
                    if request.function.__name__ in exclude_testcase.split('|'):
                        print('Skip test by in-line parameter')
                        pytest.skip('Skip test by in-line parameter')

                self.test_case = testConfigApiWorker.get_testcase(request.function.__name__)

                if self.test_case is None:
                    print('Testcase not found in test config server')
                    pytest.skip('Testcase not found in test config server')

                self._config_name = suite
                self.log_text('suite_name: {0}'.format(suite))
                self.active_test = TestCaseDataObject(data=self.test_case, url=url)

                if self.active_test.get_skipped():
                    print(self.active_test.skip_reason)
                    pytest.skip(self.active_test.skip_reason)

                self.log_text("TRCase = {0}" .format(str(self.test_case['testrail_cases'])))
                self._test_rail_id = self.test_case['testrail_cases']
                self.main_page = url
                self.log_text('Главная страница: {0}'.format(self.main_page))

            with pytest.allure.step('Открытие браузера'):
                self.driver = self.get_new_browser(request.function.__name__)

        # allure.environment(browser=browser)

        yield  # everything after 'yield' is executed on tear-down

        with pytest.allure.step('Закрытие браузера'):
            try:
                self.log_screenshot()
                pass
            except Exception as e:
                self.log_text('Ошибка снятия скриншота')
                self.log_text(str(e))

            try:
                self.driver.quit()
            except Exception as e:
                self.log_text("Ошибка закрытия драйвера")

            time.sleep(2)

    @deprecated(deprecated_in='No info', removed_in='No info')
    def log(self, log_message=None, screenshot=False, txt_data=None, json_data=None, data=None, **kwargs):
        log_message = log_message if log_message is not None else log_message
        if screenshot:
            self.log_screenshot()
        elif txt_data is None and json_data is None and data is None and log_message is not None:
            self.log_text(log_message=log_message)
        elif (txt_data is not None) or (data is not None):
            txt_data = txt_data if txt_data is not None else data
            self.log_txt_data(log_data=str(txt_data), log_message=log_message)
        elif json_data is not None and (isinstance(json_data, list) or isinstance(json_data, dict)):
            self.log_json_data(log_data=json_data, log_message=log_message)
        elif json_data is not None and not (isinstance(json_data, list) or isinstance(json_data, dict)):
            self.log_json_data(log_data=json.loads(json_data), log_message=log_message)

    @staticmethod
    def log_text(log_message):
        with pytest.allure.step(str(log_message)):
            pass

    def log_screenshot(self, name=None):
        name = name if name else 'Скриншот'
        allure.attach(self.driver.get_screenshot_as_png(), name, attachment_type=allure.attachment_type.PNG)

    @staticmethod
    def log_txt_data(log_data, log_message=None, allure_step=True, step_name=None):
        log_message = log_message if log_message is not None else "Данные шага"
        step_name = log_message if step_name is None else step_name

        def _log(_log_data, _log_message):
            allure.attach(str(_log_data), name=_log_message, attachment_type=allure.attachment_type.TEXT)

        if allure_step:
            with pytest.allure.step(step_name):
                _log(_log_data=log_data, _log_message=log_message)
        else:
            _log(_log_data=log_data, _log_message="Данные шага")

    def log_json_data(self, log_data: Union[list, dict], log_message=None, allure_step=True, step_name=None):
        log_message = log_message if log_message is not None else "Данные шага"
        step_name = log_message if step_name is None else step_name

        def _log(_log_data, _log_message):
            allure.attach(json.dumps(_log_data), name=_log_message, attachment_type=allure.attachment_type.JSON)

        if allure_step:
            with pytest.allure.step(step_name):
                _log(_log_data=log_data, _log_message=log_message)
        else:
            _log(_log_data=log_data, _log_message="Данные шага")
        pass

    @staticmethod
    def get_random_values(start, end):
        random_values = [item for item in range(start, end)]
        random.shuffle(random_values)
        return random_values

    def set_test_rail_url(self, param):
        self.log_text("[TESTRAIL] {0}".format(param))

    def subtest_start(self, tr_id):
        self.log_text("[TESTRAIL]{0}[START]".format(tr_id))
        self.log_text("Starting testrail test \"{0}\" ({1})".format(self._test_rail_id[str(tr_id)]['name'], tr_id))

    def subtest_pass(self, tr_id: object):
        self.log_text("[TESTRAIL]{0}[PASS]".format(tr_id))

    def subtest_fail(self, tr_id: int, message: str = '') -> None:
        self.log_text("[TESTRAIL]{0}[FAIL]{1}".format(tr_id, message))

    def get_new_browser(self, test_name='undefined'):
        from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

        if not self.mobile:
            if not self.local_run:
                print('Starting "{0}" browser' .format(self.browser))
                capabilities = {
                    "browserName": self.browser,
                    "version": "",
                    "name": test_name,
                }

                driver = webdriver.Remote(
                    command_executor="http://testserver1.vsemayki.pro:4444/wd/hub",
                    desired_capabilities=capabilities)
                driver.set_window_size(1920, 1080)
            else:

                if self.browser == 'firefox':
                    print('Starting firefox browser')
                    firefox_capabilities = DesiredCapabilities.FIREFOX
                    firefox_capabilities['loggingPrefs'] = {'browser': 'ALL'}

                    driver = SUPPORT_BROWSER[self.browser](
                        executable_path=os.path.join(BASE_DIR, 'drivers', settings.gecko_driver_name),
                        capabilities=firefox_capabilities)
                    driver.maximize_window()

                else:
                    pass

                    options = webdriver.ChromeOptions()

                    if self.headless:
                        options.add_argument('headless')
                        options.add_argument('disable-gpu')

                    options.add_argument("--window-size=1920,1080")

                    driver = SUPPORT_BROWSER[self.browser](
                        executable_path=os.path.join(BASE_DIR, 'drivers', settings.chrome_driver_name),
                        chrome_options=options)

            driver.set_page_load_timeout(180)

        else:
            print('Starting android browser')
            cp = DesiredCapabilities.ANDROID
            cp.update({'platformName': 'android'})
            cp.update({'deviceName': '63af9708'})
            cp.update({"unicodeKeyboard": 'true'})

            if 'android_web' in self._testsuite_name:
                cp.update({'browserName': 'Chrome'})
            else:
                cp.update({'app': 'http://testserver1.vsemayki.pro/apk/{0}' .format(testConfigApiWorker.get_last_android_apk_id())})

            driver = android_webdriver.Remote(command_executor=settings.APPIUM_SERVER, desired_capabilities=cp)

        return driver

    def log_cookies(self):
        self.log_json_data(log_data=self.driver.get_cookies(), log_message='Json')
        self.log_txt_data(log_data="\n".join(["{0} = {1}".format(item['name'], item['value']) for item in self.driver.get_cookies()]),
                          log_message='TXT')

    def init_page_by_url(self, cls, url, project_url, timeout, confirm=True):
        return cls(driver=self.driver,
                   url=url,
                   project_url=project_url,
                   timeout=timeout,
                   confirm=confirm,
                   init=True)

    def wait_tab(self, tab_num, change_focus, timeout=4):
        for item in range(timeout * 5):
            if len(self.driver.window_handles) >= tab_num:
                if change_focus:
                    self.driver.switch_to.window(self.driver.window_handles[tab_num - 1])
                    break
            else:
                time.sleep(0.2)
        else:
            raise BaseModuleException("Не открылась новая вкладка")