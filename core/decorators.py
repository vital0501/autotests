from functools import wraps
import warnings


def deprecated(alternative="an alternative", deprecated_in='No info', removed_in='No info'):
    """Decorator for marking a function or method as deprecated."""

    def decorator(function):
        @wraps(function)
        def deprecated_function(*args, **kwargs):
            message = "{0}() is deprecated in {1}. Will removed in {2} Please use {3} instead.".format(
                function.__name__,
                deprecated_in,
                removed_in,
                alternative)
            warnings.warn(message, PendingDeprecationWarning, stacklevel=2)
            return function(*args, **kwargs)
        return deprecated_function
    return decorator
