class AutotestCoreException(Exception):
    pass


class AutotestDictLengthException(AutotestCoreException):
    pass


class AutotestCollectionException(AutotestCoreException):
    pass


class AutotestConfigLoaderException(AutotestCoreException):
    pass


class ConfigNotExistException(AutotestCoreException):
    pass


class AutotestTestCaseMultiplyImplementedError(AutotestCoreException):
    pass


class NotConfiguredTestCaseException(AutotestCoreException):
    pass


class NotConfiguredValueException(AutotestCoreException):
    pass


class LoginException(AutotestCoreException):
    pass


class LoginValidationException(AutotestCoreException):
    pass


class RegistrationEmailAlreadyExistException(AutotestCoreException):
    pass


class PaginationPageNumNotFound(AutotestCoreException):
    pass


class RegistrationEmptyEmailException(AutotestCoreException):
    pass


class RegistrationIncorrectPasswordException(AutotestCoreException):
    pass


class IncorrectClassValueException(AutotestCoreException):
    pass


class NotFoundMenuItemException(AutotestCoreException):
    pass


class InformationBlockNotFound(AutotestCoreException):
    pass


class ConfigServerException(AutotestCoreException):
    pass


class NotAuthorizedException(AutotestCoreException):
    pass


class NotImplementedInputName(AutotestCoreException):
    pass


class AlreadyAddedException(AutotestCoreException):
    pass


class NotAddedException(AutotestCoreException):
    pass


class AutotestPriceNotCorrectException(AutotestCoreException):
    pass
