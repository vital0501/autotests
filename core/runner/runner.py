import os

import pytest
import settings

from config_server_worker.config_server_api_worker import TestConfigApiWorker

testConfigApiWorker = TestConfigApiWorker(server=settings.CONFIG_SERVER_URL, api_version=settings.API_VER)


class Runner3(object):
    def __init__(self, base_dir, suite_id, browser, url, report_dir, testcase=None, exclude_testcase=None,
                 server_test_run_id=None, apk_id=None, mobile=False, device_name=None, android_unicode_keyboard=True,
                 headless=True, task_name='Local_run', build_number=None, flow=3, local_run=False):
        self.base_dir = base_dir
        self.android_unicode_keyboard = False if android_unicode_keyboard == 'False' else True
        self.suite_id = suite_id
        self.browser = browser
        self.url = url
        self.report_dir = report_dir
        self.testcase = testcase
        self.exclude_testcase = exclude_testcase
        self.server_test_run_id = server_test_run_id
        self.mobile = mobile
        self.apk_id = apk_id
        self.device_name = device_name
        self.headless = False if headless in ['False', 'false'] else True
        self.task_name = task_name
        self.build_number = build_number
        self.flow = flow
        self.local_run = local_run

    def run(self):
        return pytest.main(self._get_run_command())

    def _get_run_command(self):
        py_file = os.path.normpath(os.path.join(self.base_dir, 'tests/{0}.py'.format(self.suite_id)))

        command = ['-s',
                   # '--cache-clear',
                   py_file.replace('\\', '\\\\'),
                   "--json={0}" .format(
                       str(os.path.join(self.report_dir, str(self.server_test_run_id), "report.json")).replace('\\', '\\\\')),
                   '--suite={0}'.format(self.suite_id),
                   '--alluredir={0}'.format(
                       str(os.path.join(self.report_dir, str(self.server_test_run_id))).replace('\\', '\\\\')),
                   '--browser={0}'.format(self.browser),
                   '--basedir={0}'.format(self.base_dir.replace('\\', '\\\\')),
                   '--url={0}'.format(self.url),
                   '--testcase={0}'.format(self.testcase),
                   '--mobile={0}'.format(self.mobile),
                   '--apk_id={0}'.format(self.apk_id),
                   '--exclude_testcase={0}'.format(self.exclude_testcase),
                   '--server_test_run_id={0}'.format(self.server_test_run_id),
                   '--device_name={0}'.format(self.device_name),
                   '--android_unicode_keyboard={0}'.format(self.android_unicode_keyboard),
                   '--headless={0}'.format(self.headless),
                   "--task_name={0}".format(self.task_name),
                   "--build_number={0}" .format(self.build_number),
                   "--flow={0}".format(self.flow)
                   ]
        if self.flow not in [1, '1']:
            command.insert(1, '-n{0}'.format(self.flow))
        if self.local_run:
            command.append("--local_run")
        print(command)
        return command
