# from data_layer.exceptions import DataLayerException, DataLayerSectionNotFound


class DataLayer(object):

    def __init__(self, driver=None):
        self.data = None
        self.driver = driver

    def _get_data_from_page(self):
        data = self.driver.execute_script('return dataLayer')
        for item in data:
            if 'gtm.element' in item.keys():
                item['gtm.element'] = {"name": "gtm.element"}
        self.data = data

    @classmethod
    def from_page(cls, page):
        _cls = cls(driver=page.driver)
        _cls._get_data_from_page()
        return _cls

    def find_element_in_list(self, section_key, parameter, value):
        for item in self.get_value(section_key):
            if item[parameter] == value:
                return item

    def get_value(self, key, section_sub_key=None, section_add_key=None):
        return self.get_section(key=key, section_sub_key=section_sub_key, section_add_key=section_add_key)[key.split('.')[-1]]

    def get_section(self, key, _step=1, _data=None, section_sub_key=None, section_add_key=None):
        _key_split = key.split('.')
        _key = _key_split[_step-1]
        _data = _data if _data else self.data
        result = None

        for item in _data:
            _work_data = item if isinstance(_data, list) else _data
            if _key in _work_data.keys():
                if _step == len(_key_split):
                    if section_sub_key or section_add_key:
                        _filter = section_sub_key if section_sub_key else section_add_key
                        _keys = _work_data.keys() if section_add_key else _work_data[_key].keys()
                        if _filter in _keys:
                            result = _work_data
                    else:
                        result = _work_data
                else:
                    result = self.get_section(key=key, _step=_step+1, _data=_work_data[_key], section_sub_key=section_sub_key)

            if result:
                return result

            # Если данные словарь - то достаточно 1 прохода по циклу поиска
            if isinstance(_data, dict):
                break
