class DataLayerException(Exception):
    pass


class DataLayerSectionNotFound(DataLayerException):
    pass

