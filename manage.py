from optparse import OptionParser

import os

from manage.exceptions import ManageProjectNameNotFound, ManagePageAlreadyExist

__version__ = '0.1'
BASE_DIR = os.path.normpath(os.path.dirname(os.path.realpath(__file__)))
print(BASE_DIR)


def main():
    parser = OptionParser(version=__version__, description='Manage service')

    parser.add_option('--createpage',
                      action='store_true',
                      dest='create_page',
                      help='Create PageObject',
                      default=None)

    parser.add_option('--project_name',
                      action='store',
                      dest='project_name',
                      help='Project name',
                      default=None)

    parser.add_option('--page_name',
                      action='store',
                      dest='page_name',
                      help='Page name',
                      default=None)

    (options, args) = parser.parse_args()

    # Создание страницы
    if options.create_page:
        if not options.project_name or not options.page_name:
            raise ManageProjectNameNotFound('project_name must be implemented!')


        # Ищем проект
        project_path = os.path.join(BASE_DIR, 'module_projects', options.project_name)
        print(project_path)

        if not os.path.exists(project_path):
            raise ManageProjectNameNotFound('Project "{0}" not found!' .format(options.project_name))

        # Создаем папку страницы
        page_name_template = "{0}Page" .format(options.page_name.capitalize())
        page_path = os.path.join(project_path, "pages", page_name_template)

        if os.path.exists(page_path):
            raise ManagePageAlreadyExist('Page "{0}" already exist' .format(page_name_template))

        os.mkdir(page_path)

        open(os.path.join(page_path, '__init__.py'), 'a', encoding="UTF-8").close()

        page_template_module = getattr(__import__('manage'), 'template').page

        for page_name in ("check.py", "exception.py", "page.py", "structure.py", "waiting_elements.py"):

            with open(os.path.join(page_path, page_name), 'w', encoding="UTF-8") as fp:
                page_template = getattr(page_template_module, page_name.replace(".py", "_page").upper())
                fp.write(page_template .format(options.page_name.capitalize(), options.project_name.upper(), options.project_name.lower()))


if __name__ == '__main__':
    main()
