class ManageException(Exception):
    pass


class ManageProjectNameNotFound(ManageException):
    pass


class ManagePageAlreadyExist(ManageException):
    pass

