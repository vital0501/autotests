CHECK_PAGE = """from module_projects.base.check import BaseCheck


class {0}Check(BaseCheck):
    def __init__(self, page):
        \"\"\"

        :type page: module_projects.{2}.pages.{0}Page.page.{1}{0}Page
        \"\"\"
        super().__init__(page)
        self.page = page
"""


EXCEPTION_PAGE = """from module_projects.{2}.pages.BasePage.exception import {1}BasePageException


class {0}Exception({1}BasePageException):
    pass
"""

PAGE_PAGE = """from typing import Union

from selenium import webdriver

from module_projects.{2}.pages.BasePage.page import {1}BasePage
from module_projects.{2}.pages.{0}Page.check import {0}Check
from module_projects.{2}.pages.{0}Page.structure import {1}{0}PageStructure


class {1}{0}Page({1}BasePage):
    page_name = '{1}{0}'

    @staticmethod
    def get_structure():
        return {1}{0}PageStructure()

    def _get_check(self):
        \"\"\"

        :rtype: {0}Check
        \"\"\"
        return {0}Check(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()
"""

STRUCTURE_PAGE = """from module_projects.base.structure import BasePageStructure


class {1}{0}PageStructure(BasePageStructure):
    CONFIRM = ""
"""

WAITING_ELEMENTS_PAGE = """from autotests_base.elements.waiting_elements import WaitingElement


class Base{0}WE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)
"""
