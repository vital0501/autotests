import html

from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_collection_conditions import len_equal, len_more
from autotests_base.conditions.element_conditions import exist
from autotests_base.elements.waiting_elements import WaitingElement
from core.exception import AutotestCollectionException, AutotestDictLengthException
from module_projects.base.exception import ParameterException, DictValueException
from module_projects.vmr.pages.BasePage.exception import ValueException


class BaseCheck(object):
    def __init__(self, page):
        """

        :type page: module_projects.base.page.BasePage
        """
        self.page = page

    def check_element_exist(self, xpath: str = None, element: WaitingElement = None, raise_error=False, error_text=None, error_class=None, timeout=4):
        error_class = TimeoutException if error_class is None else error_class
        error = error_class(error_text)

        if xpath:
            check_element = self.page.search(xpath)
        elif element:
            check_element = element
        else:
            raise ParameterException("Для проверки требуется задать параметр \"xpath\" или \"element\"")

        try:
            check_element.assure(exist, timeout=timeout)
            result = True
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise error

        return result

    def check_length_collection_eq(self, collection, value, collection_name=None, raise_error=False, error_class=None, timeout=10):
        error_text = 'Length collection {0} is not equal {1}' .format(collection_name if collection_name else '', value)
        return self._check_collection(len_equal, collection, value, error_class, error_text, raise_error, timeout)

    def check_length_collection_gt(self, collection, value, collection_name=None, raise_error=False, error_class=None, timeout=10):
        error_text = 'Length collection {0} is not more that {1}' .format(collection_name if collection_name else '', value)
        return self._check_collection(len_more, collection, value, error_class, error_text, raise_error, timeout)

    @staticmethod
    def _check_collection(condition, collection, value, error_class, error_text, raise_error=False, timeout=10):
        error_class = AutotestCollectionException if error_class is None else error_class
        error = error_class(error_text)

        try:
            collection.assure(condition, value, timeout=timeout)
        except TimeoutException:
            if raise_error:
                raise error
            else:
                return False
        return True

    @staticmethod
    def check_length_list(dict_obj, value, dict_obj_name, error_class=None, raise_error=False):
        result = len(dict_obj) == value

        if not result and raise_error:
            error_class = error_class if error_class else AutotestDictLengthException
            raise error_class("Incorrect length \"{2}\". Expect: {0}, Current: {1}"
                              .format(value, len(dict_obj), dict_obj_name))
        return result

    def check_list_of_dict_eq(self, obj_1, obj_2, obj_2_name='obj_2', error_class=None, raise_error=False, parameter=None):

        def find_element(list_obj, parameter, value):
            for item in list_obj:
                if item[parameter] == value:
                    return item
        element_error = []
        parameter_error = []
        data_error = []

        for item in obj_1:
            if item not in obj_2:
                if parameter is not None:
                    if not self.check_element_with_parameter_in_list(list_obj=obj_2, value=item['name'], parameter='name', error_class=error_class):
                        parameter_error.append(item[parameter])
                    else:
                        _item = find_element(obj_2, parameter, item[parameter])
                        for key in item.keys():

                            if isinstance(item[key], str):
                                item[key] = html.unescape(item[key])
                                _item[key] = html.unescape(_item[key])

                            if item[key] != _item[key]:
                                data_error.append([item[parameter], key, item[key], _item[key]])
                else:
                    element_error.append(item)

        result = not (bool(element_error) or bool(parameter_error) or bool(data_error))

        err = ["Element with parameter \"{0}\"=\"{1}\" not exist in {2}".format(parameter, item, obj_2_name)
               for item in parameter_error]

        err.extend(["Element with parameter \"{0}\"=\"{1}\" in {2} has incorrect data parameter {3}. "
                    "Current: {4}, Expect: {5}".format(parameter, item[0], obj_2_name, item[1], item[2], item[3]) for item in
                    data_error])

        err.extend(["Element {0} not found in {1}".format(item, obj_2_name) for item in element_error])

        err_text = '\n'.join(err)

        if not result and raise_error:
            raise error_class(err_text)
        return result

    @staticmethod
    def check_element_with_parameter_in_list(list_obj, parameter, value, error_class=None, raise_error=False):
        for item in list_obj:
            if item[parameter] == value:
                result = True
                break
        else:
            result = False

        if not result and raise_error:
            raise error_class("Element with \"{0}\"=\"{1}\" not found.")
        return result

    @staticmethod
    def check_dict_eq(dict_1: dict, dict_2: dict, error_class=None, dict_name=None, raise_error=False):
        error_class = error_class if error_class else DictValueException
        dict_name = dict_name if dict_name else 'Dictionary'

        result = dict_1 == dict_2

        err_msg = []
        if not result:
            for key in dict_1.keys():
                if dict_1[key] != dict_2[key]:
                    err_msg.append("\tValue of key \"{0}\" not equal. D1={1}, D2={2}"
                                   .format(key, dict_1[key], dict_2[key]))

        if not result and raise_error:
            raise error_class("EqualException in \"{0}\"!\n".format(dict_name) + "\n".join(err_msg))
        return result

    @staticmethod
    def check_value_gt_0(value, raise_error=False):
        result = int(value) > 0

        if not result and raise_error:
            raise ValueException('Value is not great 0.')
        return result

    @staticmethod
    def check_number_value_e(value: int, number: int, raise_error=False):
        result = int(value) == number

        if not result and raise_error:
            raise ValueException('Value {0} is not equal {1}' .format(value, number))
        return result

    @staticmethod
    def check_simple_list_ne(list_1, list_2, raise_error=False):
        result = not sorted(list_1).__eq__(sorted(list_2))

        if not result and raise_error:
            raise ValueException('Lists is equal!')
        return result
