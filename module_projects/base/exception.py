class BaseModuleException(Exception):
    pass


class WebDriverSessionException(BaseException):
    pass


class ElementNotFoundException(BaseModuleException):
    pass


class ConfirmException(ElementNotFoundException):
    pass


class ParameterException(BaseModuleException):
    pass


class ProductExistException(BaseModuleException):
    pass


class BaseCheckException(BaseModuleException):
    pass


class DictValueException(BaseCheckException):
    pass


class DataLayerSectionException(BaseCheckException):
    pass
