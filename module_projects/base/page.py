from typing import Union

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.alert import Alert

from autotests_base.conditions.element_conditions import exist
from autotests_base.elements.waiting_elements import WaitingElement
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from data_layer.data_layer import DataLayer
from module_projects.base.check import BaseCheck
from module_projects.base.exception import ConfirmException, WebDriverSessionException
from module_projects.base.structure import BasePageStructure


class BasePage(object):
    page_name = 'BasePage'

    @staticmethod
    def norm_href(value: str):
        if isinstance(value, str):
            value = value[0:-1] if value.endswith('/') else value
            value = value[1:] if value.startswith("/") else value
        return value

    def norm_url_value(self, value, project_url):
        if isinstance(value, str):
            project_url = self.norm_href(project_url)
            value = self.norm_href(value)
            return "{0}/{1}" .format(project_url, value) if value.find(project_url) == -1 else value
        else:
            return value

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome],
                 url, project_url, timeout, confirm=True, init=False):
        self.driver = driver

        self.url = self.norm_url_value(url if url is not None else '', project_url)
        self.project_url = self.norm_href(project_url)
        self.timeout = timeout
        self.confirm = confirm
        self.init = init
        self.structure = self.get_structure()
        self.CHECK = self._get_check()
        self.data_layer = DataLayer(None)

        if init:
            self.init_page(self.url)

        if confirm:
            self._confirm()

    def init_page(self, url):
        try:
            return self.driver.get(url)
        except TimeoutException:
            raise WebDriverSessionException

    def _confirm(self):
        try:
            self.search(self.structure.CONFIRM).assure(exist, timeout=self.timeout)
        except TimeoutException:
            raise ConfirmException("Не удалось открыть \"{0}\"" .format(self.page_name))

    @staticmethod
    def get_structure():
        return BasePageStructure()

    def search(self, selector, selector_type=By.XPATH, element_type=WaitingElement, timeout=4, **kwargs):
        """

        :rtype: WaitingElement
        """
        return element_type(self.driver, selector, selector_type, timeout=timeout, **kwargs)

    def search_all(self, selector, selector_type=By.XPATH, elements_type=WaitingElement, timeout=4, **kwargs):
        """

        :rtype: WaitingElementCollection
        """
        return WaitingElementCollection(self.driver, selector, selector_type, elements_type, timeout=timeout, **kwargs)

    def _get_check(self):
        return BaseCheck(self)

    def open_by_url(self, url, cls, timeout=10, confirm=True):
        return cls(driver=self.driver,
                   url=url,
                   project_url=self.project_url,
                   timeout=timeout,
                   confirm=confirm,
                   init=True)

    def create_page_object(self, cls, url=None, project_url=None, timeout=10, confirm=True, init=False):
        return cls(driver=self.driver,
                   url=url,
                   project_url=self.project_url if project_url is None else project_url,
                   timeout=timeout,
                   confirm=confirm,
                   init=init)

    def input_standard_field(self, xpath, value, scroll=True, clear=True):
        element = self.search(xpath)
        if scroll:
            element.scroll_to_element()
        if clear:
            element.clear()
        element.send_keys(value)

    def init_data_layer(self):
        self.data_layer = DataLayer.from_page(self)

    def is_alert_present(self) -> bool:
        try:
            WebDriverWait(self.driver, self.get_timeout()).until(EC.alert_is_present())
            return True
        except TimeoutException:
            return False

    def get_alert(self) -> Alert:
        return self.driver.switch_to.alert

    def get_timeout(self) -> int:
        return self.timeout

    def back(self, cls, timeout=None, **kwargs):
        self.driver.back()
        timeout = self.timeout if timeout is None else timeout
        return self.create_page_object(cls, timeout=timeout if timeout else self.timeout, **kwargs)

    def scroll_up(self):
        self.driver.execute_script("window.scrollTo(0, 0);")
