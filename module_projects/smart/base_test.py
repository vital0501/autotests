import pytest

import settings
from autotests_base.exception import ConfirmPageException
from core.baseTest import BaseTest
from module_projects.smart.pages.MainPage.page import SMARTMainPage


class ProjectBaseTest(BaseTest):
    def open_main_page(self, init=True, confirm=True) -> SMARTMainPage:
        with pytest.allure.step('Открытие главной страницы'):
                try:
                    if self.mobile:
                        self.driver.switch_to.context("NATIVE_APP")
                        self.driver.find_element_by_xpath("//android.widget.RadioButton[@text='Google']").click()
                        self.driver.find_element_by_xpath("//android.widget.Button[@text='OK']").click()
                        self.driver.find_element_by_accessibility_id("Ещё").click()
                        self.driver.find_element_by_id("com.android.chrome:id/checkbox").click()
                        self.driver.switch_to.context("CHROMIUM")

                    page = SMARTMainPage(driver=self.driver,
                                         project_url=self.main_page,
                                         url=self.main_page,
                                         timeout=settings.TIMEOUT,
                                         init=init,
                                         confirm=confirm)

                    return page
                except ConfirmPageException as e:
                    pytest.fail(e.args)

