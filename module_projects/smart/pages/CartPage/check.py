from module_projects.base.check import BaseCheck


class CartCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.smart.pages.CartPage.page.SMARTCartPage
        """
        super().__init__(page)
        self.page = page
