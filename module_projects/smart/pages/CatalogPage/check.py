from module_projects.base.check import BaseCheck


class CatalogCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.smart.pages.CatalogPage.page.SMARTCatalogPage
        """
        super().__init__(page)
        self.page = page
