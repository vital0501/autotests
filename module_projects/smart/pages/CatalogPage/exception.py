from module_projects.smart.pages.BasePage.exception import SMARTBasePageException


class CatalogException(SMARTBasePageException):
    pass
