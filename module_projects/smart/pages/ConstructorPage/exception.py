from module_projects.smart.pages.BasePage.exception import SMARTBasePageException


class ConstructorException(SMARTBasePageException):
    pass
