import random
from typing import Union

from selenium import webdriver
from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_conditions import is_displayed, is_not_displayed
from module_projects.smart.pages.BasePage.page import SMARTBasePage
from module_projects.smart.pages.ConstructorPage.check import ConstructorCheck
from module_projects.smart.pages.ConstructorPage.exception import ConstructorException
from module_projects.smart.pages.ConstructorPage.structure import SMARTConstructorPageStructure
from module_projects.smart.pages.ConstructorPage.waiting_elements import SMARTConstructorPopUpWE


class SMARTConstructorPage(SMARTBasePage):
    page_name = 'SMARTConstructor'

    @staticmethod
    def get_structure():
        return SMARTConstructorPageStructure()

    def _get_check(self):
        """

        :rtype: ConstructorCheck
        """
        return ConstructorCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=20, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()
        self.wait_loader()

    def wait_loader(self):
        try:
            self.search(self.structure.LOADER).assure(is_displayed, timeout=2)
        except TimeoutException:
            pass

        try:
            self.search(self.structure.LOADER).assure(is_not_displayed, timeout=20)
        except TimeoutException:
            raise ConstructorException("Долгая загрузка лодера")

    def get_price(self):
        return int(self.get_price_element().text)

    def get_price_element(self):
        return self.search(self.structure.PRICE)

    def set_random_type(self):
        return random.choice(self.get_product_type_elements()).click()

    def get_product_type_elements(self):
        return self.search_all(self.structure.PRODUCT_TYPES)

    def set_random_color(self):
        return random.choice(self.get_color_elements()).click()

    def get_color_elements(self):
        return self.search_all(self.structure.COLORS)

    def set_random_size(self):
        return random.choice(self.get_size_elements()).click()

    def get_size_select_element(self):
        return self.search(self.structure.SIZES_SELECT)

    def get_size_elements(self):
        return self.search_all(self.structure.SIZES)

    def add_product_to_cart(self):
        self.get_add_button_element().click()
        pop_up = self.get_pop_up_element(timeout=20)
        pop_up.assure(is_displayed)
        pop_up.close()

    def get_add_button_element(self):
        return self.search(self.structure.ADD_BUTTON)

    def get_pop_up_element(self, timeout=4):
        return self.search(self.structure.POP_UP, element_type=SMARTConstructorPopUpWE, timeout=timeout)

    def get_random_type_id(self):
        return random.randint(0, len(self.get_product_type_elements()) - 1)

    def get_random_color_id(self):
        return random.randint(0, len(self.get_color_elements()) - 1)

    def get_random_size_id(self):
        return random.randint(0, len(self.get_size_elements()) - 1)

    def set_type_by_id(self, value):
        self.get_product_type_elements()[value].click()

    def set_color_by_id(self, value):
        self.get_color_elements()[value].click()

    def set_size_by_id(self, value):
        self.get_size_elements()[value].click()
