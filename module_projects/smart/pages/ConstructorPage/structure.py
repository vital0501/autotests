from module_projects.base.structure import BasePageStructure


class SMARTConstructorPageStructure(BasePageStructure):

    PRICE = '//*[@id="price"]'

    CONFIRM = "//section[@class='constructor-holder']"
    LOADER = "//*[@class='main-loader newloader']"

    PRODUCT_TYPES = "//*[@id='poducttypes-preview']/div/div"
    COLORS = "//*[@id='colors-preview']/div/div"
    SIZES_SELECT = "//select[@id='size']"
    SIZES = "//select[@id='size']/option[@data-tkey]"

    ADD_BUTTON = '//*[@id="add-to-cart"]'

    POP_UP = '//*[@id="success_popup"]'
