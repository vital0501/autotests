from typing import Union

from selenium import webdriver
from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_collection_conditions import len_gt
from autotests_base.conditions.element_conditions import is_displayed
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.smart.pages.BasePage.page import SMARTBasePage
from module_projects.smart.pages.OrderPage.check import OrderCheck
from module_projects.smart.pages.OrderPage.exception import PopUpCityNotFoundException, NotFoundAutocompliteCity
from module_projects.smart.pages.OrderPage.structure import SMARTOrderPageStructure
from module_projects.smart.pages.OrderPage.waiting_elements import OrderPagePopUpCityWE, CourierDeliveryWE, PaymentWE


class SMARTOrderPage(SMARTBasePage):
    page_name = 'SMARTOrder'

    @staticmethod
    def get_structure():
        return SMARTOrderPageStructure()

    def _get_check(self):
        """

        :rtype: OrderCheck
        """
        return OrderCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def fill_data(self, name=None, phone=None, email=None, city=None, region=None, address=None, comment=None):
        if phone:
            self.fill_phone(value=phone)

        if email:
            self.fill_email(value=email)

        if address:
            self.fill_address(address)

        if comment:
            self.fill_comment(comment)

        if city:
            self.fill_city(city=city, region=region)

        if name:
            self.fill_name(value=name)

    def fill_city(self, city, region=None):
        # Вводим город
        self.input_standard_field(self.structure.CITY, city)

        # Определяем что элементы показались
        try:
            # Ждем всплывающие элементы
            city_elements = self._get_pop_up_city_elements().assure(len_gt, 0)
            city_elements[0].assure(is_displayed)
        except TimeoutException:
            raise PopUpCityNotFoundException('Не показались всплывающие подсказки городов')

        for item in city_elements:
            if item.get_city() == city:
                if region:
                    if region == item.get_region():
                        item.click()
                        break
                    else:
                        continue
                item.click()
                break
        else:
            err_text = "Не появился город \"{0}\"".format(city)
            raise NotFoundAutocompliteCity(err_text if not region else err_text + " регионе \"{0}\"" .format(region))

    def fill_email(self, value):
        self.input_standard_field(xpath=self.structure.EMAIL, value=value)

    def fill_comment(self, value):
        self.input_standard_field(xpath=self.structure.COMMENT, value=value)

    def fill_phone(self, value):
        element = self.search(self.structure.PHONE)
        element.click()
        element.send_keys(value)

    def fill_name(self, value):
        self.input_standard_field(xpath=self.structure.NAME, value=value)

    def fill_address(self, value):
        self.input_standard_field(xpath=self.structure.ADDRESS, value=value)

    def _get_pop_up_city_elements(self):

        """

        :rtype: WaitingElementCollection[OrderPagePopUpCityWE]
        """
        return self.search_all(self.structure.CITY_ELEMENTS, elements_type=OrderPagePopUpCityWE)

    def get_delivery_elements(self):
        return self.search_all(self.structure.DELIVERY)

    def get_courier_delivery_elements(self):
        """

        :rtype: WaitingElementCollection[CourierDeliveryWE]
        """
        return self.search_all(self.structure.COURIER_DELIVERY, elements_type=CourierDeliveryWE)

    def get_active_payments(self):
        """

        :rtype: PaymentWE
        """
        return self.search(self.structure.ACTIVE_PAYMENT, element_type=PaymentWE)

    def create_order(self):
        """

        :rtype: module_projects.smart.pages.SuccessfulOrderPage.page.SMARTSuccessfulOrderPage
        """
        self.get_create_order_button_element().click()
        from module_projects.smart.pages.SuccessfulOrderPage.page import SMARTSuccessfulOrderPage
        return self.create_page_object(SMARTSuccessfulOrderPage, timeout=20)

    def get_create_order_button_element(self):
        return self.search(self.structure.CREATE_BUTTON)
