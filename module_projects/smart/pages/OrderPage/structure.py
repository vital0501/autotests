from module_projects.base.structure import BasePageStructure


class SMARTOrderPageStructure(BasePageStructure):

    ACTIVE_PAYMENT = '//*[@class="payments"]/div[contains(@class, "active")]'

    COURIER_DELIVERY = '//*[@data-id="COURIER"]/div'
    DELIVERY = '//*[contains(@class, "delivery-item")]'

    CREATE_BUTTON = '//*[@id="order_button"]'

    CITY_ELEMENTS = "//*[@id='ui-id-1']/li"

    CITY = '//*[@name="fulladdress"]'
    ADDRESS = '//*[@name="address"]'
    NAME = '//*[@name="fio"]'
    PHONE = '//*[@name="phone"]'
    COMMENT = '//*[@name="comment"]'
    EMAIL = '//*[@name="email"]'

    CONFIRM = '//*[@id="order_form"]'
