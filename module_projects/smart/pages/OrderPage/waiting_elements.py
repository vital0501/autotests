import re

import time

from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseOrderWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class OrderPagePopUpCityWE(BaseOrderWE):
    def get_city(self):
        return SubWaitingElement(self, '//b').text

    def get_region(self):
        return re.findall('[^\(\)]*?', self.text)[0]


class CourierDeliveryWE(BaseOrderWE):
    def is_active(self):
        return 'active' in self.get_attribute('class')

    def set_active(self):
        for i in range(0, 5):
            SubWaitingElement(self, '//input').click()

            for j in range(0, 10):
                if self.is_active():
                    break
                else:
                    time.sleep(1)

            if self.is_active():
                break


class PaymentWE(BaseOrderWE):
    def get_type(self):
        return self.get_attribute('data-type')
