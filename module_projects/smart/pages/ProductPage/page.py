import random
from typing import Union

import time
from selenium import webdriver

from module_projects.smart.pages.BasePage.page import SMARTBasePage
from module_projects.smart.pages.ProductPage.check import ProductCheck
from module_projects.smart.pages.ProductPage.structure import SMARTProductPageStructure


class SMARTProductPage(SMARTBasePage):
    page_name = 'SMARTProduct'

    @staticmethod
    def get_structure():
        return SMARTProductPageStructure()

    def _get_check(self):
        """

        :rtype: ProductCheck
        """
        return ProductCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def set_random_size(self):
        for item in range(0, 20):
            time.sleep(0.1)
            random.choice(self.get_available_sizes_elements()).click()
            time.sleep(0.1)
            if self.get_active_size_element() is not None:
                break

    def set_random_color(self):
        for item in range(0, 20):
            time.sleep(0.1)
            random.choice(self.get_available_colors_elements()).click()
            time.sleep(0.1)
            if self.get_active_color_element() is not None:
                break

    def add_product_to_cart(self):
        self.get_add_product_button_element().click()

    def get_add_product_button_element(self):
        return self.search(self.structure.ADD_BUTTON)

    def get_available_sizes_elements(self):
        return self.search_all(self.structure.SIZES)

    def get_active_size_element(self):
        return self.search(self.structure.ACTIVE_SIZE)

    def get_available_colors_elements(self):
        return self.search_all(self.structure.COLORS)

    def get_active_color_element(self):
        return self.search(self.structure.ACTIVE_COLOR)

    def get_name(self):
        return self.get_product_name_element().text

    def get_product_name_element(self):
        return self.search(self.structure.PRODUCT_NAME)
