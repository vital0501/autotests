from typing import Union

import re
from selenium import webdriver

from module_projects.smart.pages.BasePage.page import SMARTBasePage
from module_projects.smart.pages.SuccessfulOrderPage.check import SuccessfulOrderCheck
from module_projects.smart.pages.SuccessfulOrderPage.structure import SMARTSuccessfulOrderPageStructure


class SMARTSuccessfulOrderPage(SMARTBasePage):
    page_name = 'SMARTSuccessfulOrder'

    @staticmethod
    def get_structure():
        return SMARTSuccessfulOrderPageStructure()

    def _get_check(self):
        """

        :rtype: SuccessfulOrderCheck
        """
        return SuccessfulOrderCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_order_num(self):
        return re.findall('(\d+)', self.get_order_num_element().text)[0]

    def get_order_num_element(self):
        return self.search(self.structure.ORDER_NUM)
