from module_projects.base.check import BaseCheck


class HeaderCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.smart.parts.SMARTHeader.page.SMARTHeader
        """
        super().__init__(page)
        self.page = page