from module_projects.smart.pages.BasePage.exception import SMARTBasePageException


class HeaderException(SMARTBasePageException):
    pass
