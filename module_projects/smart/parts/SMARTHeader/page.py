from typing import Union

import time
from selenium import webdriver
from selenium.common.exceptions import WebDriverException

from autotests_base.conditions.element_conditions import is_displayed
from module_projects.base.page import BasePage
from module_projects.smart.parts.SMARTHeader.check import HeaderCheck
from module_projects.smart.parts.SMARTHeader.exception import HeaderException
from module_projects.smart.parts.SMARTHeader.structure import SMARTHeaderStructure


class SMARTHeader(BasePage):
    page_name = 'SMARTHeader'

    @staticmethod
    def get_structure():
        return SMARTHeaderStructure()

    def _get_check(self):
        """

        :rtype: HeaderCheck
        """
        return HeaderCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome],
                 project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def go_to_cart(self):
        """

        :rtype: module_projects.smart.pages.CartPage.page.SMARTCartPage
        """
        try:
            self.get_cart_element().click()
        except WebDriverException:
            if self.get_sub_cart_button().exist():
                self.get_sub_cart_button().assure(is_displayed).click()

        from module_projects.smart.pages.CartPage.page import SMARTCartPage
        return self.create_page_object(SMARTCartPage)

    def get_cart_element(self):
        return self.search(self.structure.CART_ELEMENT)

    def get_cart_count_element(self):
        return self.search(self.structure.CART_COUNT)

    def get_cart_count(self):
        _text = self.get_cart_count_element().text
        return 0 if _text == '' else int(_text)

    def wait_cart_count_change(self, number, timeout=5):
        for i in range(0, timeout * 5):
            if self.get_cart_count() == number:
                break
            else:
                time.sleep(0.2)
        else:
            raise HeaderException('Значение корзины не изменилось. Ожидаем: {0}. Видим: {1}'
                                  .format(number, self.get_cart_count()))

    def get_sub_cart_button(self):
        return self.search(self.structure.SUB_CART_BUTTON)
