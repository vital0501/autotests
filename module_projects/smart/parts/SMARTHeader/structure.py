from module_projects.base.structure import BasePageStructure


class SMARTHeaderStructure(BasePageStructure):

    SUB_CART_BUTTON = "//div[contains(@class, 'topcart')]/a[not(child::span)]"
    CART_COUNT = '//*[@id="cart_count"]'
    CART_ELEMENT = "//div[contains(@class, 'topcart')]/a[child::span]"
