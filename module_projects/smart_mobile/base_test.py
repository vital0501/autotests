import pytest
from selenium.common.exceptions import NoSuchElementException

import settings
from autotests_base.exception import ConfirmPageException
from core.baseTest import BaseTest
from module_projects.smart_mobile.pages.MainPage.page import SMARTMainPage


class ProjectBaseTest(BaseTest):
    def open_main_page(self, init=True, confirm=True) -> SMARTMainPage:
        with pytest.allure.step('Открытие главной страницы'):
                try:
                    if self.mobile:
                        self.driver.switch_to.context("NATIVE_APP")

                        try:
                            self.driver.find_element_by_xpath("//android.widget.RadioButton[@text='Google']")
                            self.driver.find_element_by_xpath("//android.widget.RadioButton[@text='Google']").click()
                            self.driver.find_element_by_xpath("//android.widget.Button[@text='OK']").click()
                        except NoSuchElementException:
                            pass
                        self.driver.switch_to.context("CHROMIUM")

                    page = SMARTMainPage(driver=self.driver,
                                         project_url=self.main_page,
                                         url=self.main_page,
                                         timeout=settings.TIMEOUT,
                                         init=init,
                                         confirm=confirm)

                    return page
                except ConfirmPageException as e:
                    pytest.fail(e.args)

