from module_projects.base.exception import BaseModuleException


class SMARTBasePageException(BaseModuleException):
    pass
