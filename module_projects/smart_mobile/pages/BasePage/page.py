from typing import Union

from selenium import webdriver

from module_projects.base.page import BasePage
from module_projects.smart_mobile.pages.BasePage.structure import SmartBasePageStructure
from module_projects.smart_mobile.parts.SMARTHeader.page import SMARTHeader


class SMARTBasePage(BasePage):
    page_name = 'SmartBasePage'

    @staticmethod
    def get_structure():
        return SmartBasePageStructure()

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome],
                 url, project_url, timeout, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm, init)

        self.HEADER = SMARTHeader(self.driver, project_url, timeout)
