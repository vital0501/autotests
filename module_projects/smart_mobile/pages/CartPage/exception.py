from module_projects.smart_mobile.pages.BasePage.exception import SMARTBasePageException


class CartException(SMARTBasePageException):
    pass
