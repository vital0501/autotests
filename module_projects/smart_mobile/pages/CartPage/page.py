from typing import Union

import time
from selenium import webdriver

from autotests_base.conditions.element_collection_conditions import len_gte
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.smart_mobile.pages.BasePage.page import SMARTBasePage
from module_projects.smart_mobile.pages.CartPage.check import CartCheck
from module_projects.smart_mobile.pages.CartPage.structure import SMARTCartPageStructure
from module_projects.smart_mobile.pages.CartPage.waiting_elements import CartPageProductsWE


class SMARTCartPage(SMARTBasePage):
    page_name = 'SMARTCart'

    @staticmethod
    def get_structure():
        return SMARTCartPageStructure()

    def _get_check(self):
        """

        :rtype: CartCheck
        """
        return CartCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def clean_cart(self):
        cart_count = self.HEADER.get_cart_count()
        product_in_cart_count = len(self.get_products_elements())

        for product_index in range(0, product_in_cart_count):
            self.get_products_elements()[0].del_from_cart()

            for i in range(0, 5):
                if cart_count - 1 == self.HEADER.get_cart_count():
                    cart_count -= 1
                    break
                time.sleep(0.2)

    def get_products_elements(self):
        """

        :rtype: WaitingElementCollection[CartPageProductsWE]
        """
        return self.search_all(self.structure.PRODUCTS, elements_type=CartPageProductsWE).assure(len_gte, 0)

    def go_to_order_page(self):
        """

        :rtype: module_projects.smart.pages.OrderPage.page.SMARTOrderPage
        """
        self.get_go_to_order_button_element().click()
        from module_projects.smart_mobile.pages.OrderPage.page import SMARTOrderPage
        return self.create_page_object(SMARTOrderPage)

    def get_go_to_order_button_element(self):
        return self.search(self.structure.GO_TO_ORDER_BUTTON)
