from module_projects.base.structure import BasePageStructure


class SMARTCartPageStructure(BasePageStructure):

    GO_TO_ORDER_BUTTON = '//*[@id="order_button"]'

    CONFIRM = "//*[@class='headingmain']"
    PRODUCTS = '//*[@id="new_cart"]/div'
