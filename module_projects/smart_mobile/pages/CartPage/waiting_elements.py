from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseCartWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class CartPageProductsWE(BaseCartWE):

    def del_from_cart(self):
        SubWaitingElement(self, '//a[@class="remove-item"]').click()
