from module_projects.smart_mobile.pages.BasePage.exception import SMARTBasePageException


class CatalogException(SMARTBasePageException):
    pass
