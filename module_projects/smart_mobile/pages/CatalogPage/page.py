from typing import Union

from selenium import webdriver

from module_projects.smart_mobile.pages.CatalogPage.check import CatalogCheck
from module_projects.smart_mobile.pages.CatalogPage.structure import SMARTCatalogPageStructure
from module_projects.smart_mobile.pages.MainPage.waiting_elements import BaseMainWE


class SMARTCatalogPage(BaseMainWE):
    page_name = 'SMARTCatalog'

    @staticmethod
    def get_structure():
        return SMARTCatalogPageStructure()

    def _get_check(self):
        """

        :rtype: CatalogCheck
        """
        return CatalogCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()
