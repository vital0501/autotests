from selenium.common.exceptions import TimeoutException

from module_projects.base.check import BaseCheck
from module_projects.smart_mobile.pages.ConstructorPage.exception import ConstructorException


class ConstructorCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.smart.pages.ConstructorPage.page.SMARTConstructorPage
        """
        super().__init__(page)
        self.page = page

    def check_price_gte(self, value, raise_exception=False):
        try:
            result = self.page.get_price() >= value
        except TimeoutException:
            result = False

        if not result and raise_exception:
            raise ConstructorException('Цена < {0}' .format(value))
