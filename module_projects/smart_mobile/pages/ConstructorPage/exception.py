from module_projects.smart_mobile.pages.BasePage.exception import SMARTBasePageException


class ConstructorException(SMARTBasePageException):
    pass
