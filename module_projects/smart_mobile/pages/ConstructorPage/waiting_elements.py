from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseConstructorWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class SMARTConstructorPopUpWE(BaseConstructorWE):
    def close(self):
        SubWaitingElement(self, '//button').click()
