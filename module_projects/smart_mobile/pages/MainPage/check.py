from module_projects.base.check import BaseCheck


class MainCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.smart.pages.MainPage.page.SMARTMainPage
        """
        super().__init__(page)
        self.page = page
