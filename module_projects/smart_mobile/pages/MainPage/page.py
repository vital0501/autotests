from typing import Union

from selenium import webdriver

from module_projects.smart_mobile.pages.BasePage.page import SMARTBasePage
from module_projects.smart_mobile.pages.ConstructorPage.page import SMARTConstructorPage
from module_projects.smart_mobile.pages.MainPage.check import MainCheck
from module_projects.smart_mobile.pages.MainPage.structure import SMARTMainPageStructure
from module_projects.smart_mobile.pages.MainPage.waiting_elements import SmartProductWE
from module_projects.smart_mobile.pages.ProductPage.page import SMARTProductPage


class SMARTMainPage(SMARTBasePage):
    page_name = 'SMARTMain'

    @staticmethod
    def get_structure():
        return SMARTMainPageStructure()

    def _get_check(self):
        """

        :rtype: MainCheck
        """
        return MainCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def go_to_product_by_element(self, element: SmartProductWE):
        element.click()
        return self.create_page_object(SMARTProductPage)

    def get_products_elements(self):
        return self.search_all(self.structure.PRODUCTS, elements_type=SmartProductWE)

    def go_to_constructor(self):
        """

        :rtype: SMARTConstructorPage
        """
        self.search(self.structure.CONSTRUCTOR_MENU).click()
        return self.create_page_object(SMARTConstructorPage)
