from module_projects.base.structure import BasePageStructure


class SMARTMainPageStructure(BasePageStructure):
    CONFIRM = "//*[@id='main-container']"
    PRODUCTS = '//div[@class="row catalog-products"]/div'
    LOGO = '//*[@class="logotype"]'
    CONSTRUCTOR_MENU = '//li[@class="constructor"]'
