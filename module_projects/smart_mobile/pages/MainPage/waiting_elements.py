import time

from autotests_base.conditions.element_conditions import exist, is_displayed
from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseMainWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class SmartProductWE(BaseMainWE):
    def click(self):
        SubWaitingElement(self, '/a').click()

    def get_name_element(self):
        return SubWaitingElement(self, '//*[@class="productname"]')

    def get_name(self):
        return self.get_name_element().text