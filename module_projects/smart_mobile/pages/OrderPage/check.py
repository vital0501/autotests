import time
from selenium.common.exceptions import TimeoutException

from module_projects.base.check import BaseCheck
from module_projects.smart_mobile.pages.OrderPage.exception import PostalPaymentNotActive


class OrderCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.smart.pages.OrderPage.page.SMARTOrderPage
        """
        super().__init__(page)
        self.page = page

    def check_payments_postal_active(self, raise_error=False):
        result = None
        try:
            for i in range(0, 5):
                if self.page.get_active_payments().get_type() == 'postal':
                    result = True
                    break
                else:
                    result = False
                    time.sleep(1)
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise PostalPaymentNotActive
