from module_projects.smart_mobile.pages.BasePage.exception import SMARTBasePageException


class OrderException(SMARTBasePageException):
    pass


class PopUpCityNotFoundException(OrderException):
    pass


class NotFoundAutocompliteCity(OrderException):
    pass


class PostalPaymentNotActive(OrderException):
    pass