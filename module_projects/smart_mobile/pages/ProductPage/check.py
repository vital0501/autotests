from module_projects.base.check import BaseCheck


class ProductCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.smart.pages.ProductPage.page.SMARTProductPage
        """
        super().__init__(page)
        self.page = page
