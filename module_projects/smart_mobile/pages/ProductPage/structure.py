from module_projects.base.structure import BasePageStructure


class SMARTProductPageStructure(BasePageStructure):

    PRICE_ELEMENT = '//*[@id="product_price"]'

    SIZE_TABLE = '//*[@id="sizes_table_link"]'

    PRODUCT_NAME = '//*[@class="productname"]'

    CONFIRM = "//*[@class='productarticle']"

    COLORS = '//*[@id="product_color"]/div'
    ACTIVE_COLOR = COLORS + '[child::input[@checked]]'

    SIZES = '//*[@id="product_size"]/div'
    ACTIVE_SIZE = SIZES + '[child::input[@checked]]'

    ADD_BUTTON = '//*[@id="cart_add_button"]'
