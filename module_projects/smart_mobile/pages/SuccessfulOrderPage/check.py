from module_projects.base.check import BaseCheck


class SuccessfulOrderCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.smart.pages.SuccessfulOrderPage.page.SMARTSuccessfulOrderPage
        """
        super().__init__(page)
        self.page = page
