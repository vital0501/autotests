from module_projects.smart_mobile.pages.BasePage.exception import SMARTBasePageException


class SuccessfulOrderException(SMARTBasePageException):
    pass
