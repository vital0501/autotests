from module_projects.base.structure import BasePageStructure


class SMARTSuccessfulOrderPageStructure(BasePageStructure):

    CONFIRM = "//h1[contains(text(), 'Заказ оформлен. Номер заказа')]"
    ORDER_NUM = CONFIRM
