from module_projects.base.structure import BasePageStructure


class SMARTHeaderStructure(BasePageStructure):

    SUB_CART_BUTTON = "//div[contains(@class, 'topcart')]/a[not(child::span)]"
    CART_COUNT = '//*[@class="mobile-menu-cart__counter"]'
    CART_ELEMENT = '//*[@class="cart-mobile"]'
