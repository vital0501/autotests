import pytest

import settings
from autotests_base.exception import ConfirmPageException
from core.baseTest import BaseTest
from module_projects.vmr.pages.MainPage.page import VMRMainPage


class ProjectBaseTest(BaseTest):
    def open_main_page(self, init=True, confirm=True) -> VMRMainPage:
        with pytest.allure.step('Открытие главной страницы'):
                try:
                    page = VMRMainPage(driver=self.driver,
                                       project_url=self.main_page,
                                       timeout=settings.TIMEOUT,
                                       init=init, confirm=confirm)
                    return page
                except ConfirmPageException as e:
                    pytest.fail(e.args)

