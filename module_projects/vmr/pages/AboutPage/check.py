from selenium.common.exceptions import TimeoutException

from module_projects.base.check import BaseCheck
from module_projects.vmr.pages.AboutPage.exception import AboutCompanyNotFoundPhotoException


class AboutCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.AboutPage.page.VMRAboutPage
        """
        super().__init__(page)
        self.page = page

    def check_href_photos(self, value, photo, raise_error=False):
        try:
            result = value == photo
        except TimeoutException:
            result = False
        if not result and raise_error:
            raise AboutCompanyNotFoundPhotoException("Не совпадают href фото. Ожидаем: {0}. Видим: {1}".format(value, photo))
        return result
