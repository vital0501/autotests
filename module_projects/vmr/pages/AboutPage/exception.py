from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class AboutException(VMRBasePageException):
    pass

class AboutCompanyNotFoundPhotoException(AboutException):
    pass
