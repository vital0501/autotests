from typing import Union

from selenium import webdriver

from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.AboutPage.check import AboutCheck
from module_projects.vmr.pages.AboutPage.structure import VMRAboutPageStructure
from module_projects.vmr.pages.AboutPage.waiting_elements import AboutCompanyPagePhotoWE, AboutCompanyPageOpenPhotoWE


class VMRAboutPage(VMRBasePage):
    page_name = 'VMRAbout'

    @staticmethod
    def get_structure():
        return VMRAboutPageStructure()

    def _get_check(self):
        """

        :rtype: AboutCheck
        """
        return AboutCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_photos_elements(self):
        """

        :rtype: WaitingElementCollection[AboutCompanyPagePhotoWE]
        """
        return self.search_all(self.structure.PHOTOS, elements_type=AboutCompanyPagePhotoWE)

    def open_photo(self, photo: AboutCompanyPagePhotoWE):
        """

        :rtype: AboutCompanyPageOpenPhotoWE
        """
        photo.scroll_to_element()
        photo.click()
        return self.search(self.structure.PHOTO_AREA, element_type=AboutCompanyPageOpenPhotoWE)

    def get_photo_element(self):
        """

        :rtype: AboutCompanyPageOpenPhotoWE
        """
        return self.search(self.structure.PHOTO_AREA, element_type=AboutCompanyPageOpenPhotoWE)
