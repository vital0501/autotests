from module_projects.base.structure import BasePageStructure


class VMRAboutPageStructure(BasePageStructure):
    CONFIRM = "//h3[text()='О компании']"

    PHOTOS = "//div[@class='photo-block']//li"
    PHOTO_AREA ="//div[@class='fancybox-skin']"
