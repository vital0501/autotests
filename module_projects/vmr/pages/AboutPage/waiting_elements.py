import time

from autotests_base.conditions.element_conditions import exist
from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseAboutWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class AboutCompanyPagePhotoWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_href(self):
        return SubWaitingElement(self, '//img').assure(exist).get_attribute('src')


class AboutCompanyPageOpenPhotoWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def next_photo(self):
        SubWaitingElement(self, "//a[@class='fancybox-nav fancybox-next']").assure(exist).click()
        time.sleep(1)

    def prevision_photo(self):
        SubWaitingElement(self, "//a[@class='fancybox-nav fancybox-prev']").assure(exist).click()
        time.sleep(1)

    def get_href(self):
        return SubWaitingElement(self, '//img').assure(exist).get_attribute('src')

    def close(self):
        SubWaitingElement(self, "//a[@class='fancybox-item fancybox-close']").assure(exist).click()
