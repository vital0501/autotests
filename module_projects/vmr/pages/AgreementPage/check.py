from module_projects.base.check import BaseCheck


class AgreementCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.AgreementPage.page.VMRAgreementPage
        """
        super().__init__(page)
        self.page = page
