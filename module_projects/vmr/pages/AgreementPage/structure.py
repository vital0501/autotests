from module_projects.base.structure import BasePageStructure


class VMRAgreementPageStructure(BasePageStructure):
    CONFIRM = "//div[@class='policy__title'][contains(text(),'Правила использования Сервиса онлайн-печати vsemayki.ru (Оферта)')]"
