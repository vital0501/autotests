from core.exception import AutotestDictLengthException
from module_projects.base.check import BaseCheck
from module_projects.vmr.pages.BasePage.exception import DataLayerSectionNotExistException, DataLayerValueException, \
    DataLayerProductDataException


class VMRBaseCheck(BaseCheck):
    pass


class DataLayerCheck(BaseCheck):
    def check_data_layer_value(self, key, value, error_class=None, section_sub_key=None, section_add_key=None,
                               raise_error=False, section_key=None):

        section_key = section_key if section_key else key
        error_class = error_class if error_class else DataLayerValueException

        _value = self.page.data_layer.get_section(section_key,
                                                  section_sub_key=section_sub_key,
                                                  section_add_key=section_add_key)

        _value = _value[key.split('.')[-1]]

        if not _value == value and raise_error:
            raise error_class("Values not equal. Expect: {0}, Current: {1}" .format(value, _value))
        return _value == value

    def check_data_layer_section_exist(self, section_key, error_class=None, section_sub_key=None,
                                       section_add_key=None, raise_error=False):

        error_class = error_class if error_class else DataLayerSectionNotExistException
        result = bool(self.page.data_layer.get_section(section_key,
                                                       section_sub_key=section_sub_key,
                                                       section_add_key=section_add_key))

        if not result and raise_error:
            err_text = "Not found section with key \"{0}\"" .format(section_key)
            if section_add_key or section_sub_key:
                sec_key = section_add_key if section_add_key else section_sub_key
                err_text += ' and {0}={1}' .format('section_add_key' if section_add_key else 'section_sub_key', sec_key)
            raise error_class(err_text)
        return result

    def check_data_layer_length_impressions_eq(self, value, data, dict_name='Dict', error_class=None,
                                               raise_error=False):

        _error_class = error_class if error_class else AutotestDictLengthException
        return self.check_length_list(data, value, dict_name, _error_class, raise_error)

    def check_data_layer_impressions_data_with_page_eq(self, value, data, obj_2_name,
                                                       parameter=None, error_class=None, raise_error=False):
        return self.check_list_of_dict_eq(data, value,
                                          obj_2_name=obj_2_name,
                                          error_class=error_class,
                                          raise_error=raise_error,
                                          parameter=parameter)

    def check_page_data_impressions_with_data_layer_eq(self, value, data, obj_2_name,
                                                       parameter=None, error_class=None, raise_error=False):
        return self.check_list_of_dict_eq(value, data,
                                          obj_2_name=obj_2_name,
                                          error_class=error_class,
                                          raise_error=raise_error,
                                          parameter=parameter)

    @staticmethod
    def check_product_data_data_layer_eq(product_data, data_layer_data, error_class=None):
        error_class = error_class if error_class else DataLayerProductDataException
        dl_data = {}

        for item in product_data.keys():
            product_data[item] = str(product_data[item])
            dl_data[item] = str(data_layer_data[item])

        error = []

        if dl_data != product_data:
            for item in product_data.keys():
                if dl_data[item] != product_data[item]:
                    error.append("Параметр '{0}' на странцие не совпадает с параметром в DataLayer. "
                                 "Ожидаем: {1}, Видим: {2}".format(item, product_data[item], dl_data[item]))

        if error:
            raise error_class("Некорректные данные продукта: {0}".format('\n'.join(error)))
