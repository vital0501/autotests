from module_projects.base.exception import BaseModuleException


class VMRBasePageException(BaseModuleException):
    pass


class DataLayerException(VMRBasePageException):
    pass


class DataLayerSectionNotExistException(DataLayerException):
    pass


class DataLayerValueException(DataLayerException):
    pass


class DataLayerProductDataException(DataLayerException):
    pass


class ValueException(BaseModuleException):
    pass


class DeliveryNotEquals(BaseModuleException):
    pass
