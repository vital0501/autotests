from typing import Union

from selenium import webdriver

from module_projects.base.page import BasePage
from module_projects.vmr.pages.BasePage.structure import VMRBasePageStructure
from module_projects.vmr.parts.VMRCatalogMenu.page import VMRCatalogMenu
from module_projects.vmr.parts.VMRFooter.page import VMRFooter
from module_projects.vmr.parts.VMRHeader.page import VMRHeader


class VMRBasePage(BasePage):
    page_name = 'VMRBasePage'

    @staticmethod
    def get_structure():
        return VMRBasePageStructure()

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome],
                 url, project_url, timeout, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm, init)
        self.HEADER = VMRHeader(driver, project_url, timeout)
        self.CATALOG_MENU = VMRCatalogMenu(driver, project_url, timeout)
        self.FOOTER = VMRFooter(driver, project_url, timeout)
