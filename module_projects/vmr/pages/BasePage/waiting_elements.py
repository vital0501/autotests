import abc
import ast

from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class VMRBaseWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class VMRInitDataWE(WaitingElement):
    __metaclass__ = abc.ABCMeta

    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)
        self.init_data = self.get_init_data()

    def get_init_data(self):
        return ast.literal_eval(SubWaitingElement(self, self.get_data_sub_path()).get_attribute('value'))

    @abc.abstractmethod
    def get_data_sub_path(self) -> str:
        return ''
