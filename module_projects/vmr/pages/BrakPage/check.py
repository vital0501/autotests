from module_projects.base.check import BaseCheck


class BrakCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.BrakPage.page.VMRBrakPage
        """
        super().__init__(page)
        self.page = page
