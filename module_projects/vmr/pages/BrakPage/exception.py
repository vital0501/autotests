from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class BrakException(VMRBasePageException):
    pass
