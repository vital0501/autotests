from typing import Union

from selenium import webdriver

from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.BrakPage.check import BrakCheck
from module_projects.vmr.pages.BrakPage.structure import VMRBrakPageStructure


class VMRBrakPage(VMRBasePage):
    page_name = 'VMRBrak'

    @staticmethod
    def get_structure():
        return VMRBrakPageStructure()

    def _get_check(self):
        """

        :rtype: BrakCheck
        """
        return BrakCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()
