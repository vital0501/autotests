from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_collection_conditions import len_equal
from module_projects.vmr.pages.BasePage.check import VMRBaseCheck, DataLayerCheck
from module_projects.vmr.pages.CartPage.exception import CartLenProductsException, VMRCartPageLengthSectionException, \
    VMRCartPageDataLayerSectionException, CartPageLenProductException, CartOrderPriceException, \
    CartPageLenRussianSizeException, CartPageLenProductException, CartOrderPriceException, \
    VMRCartPageFavoritesAddNotExistException, CartPromoCodeTextException, CartProductPriceException


class CartCheck(VMRBaseCheck, DataLayerCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.CartPage.page.VMRCartPage
        """
        super().__init__(page)
        self.page = page

    def check_products_length_qe(self, num, raise_error=False, timeout=10):
        collection = self.page.get_product_elements()

        return self.check_length_collection_eq(collection, num,
                                               'Продукты на странице корзины',
                                               raise_error,
                                               CartLenProductsException,
                                               timeout)

    def check_data_layer_length_impressions_eq(self, value, data=None, dict_name='Impressions', error_class=None,
                                               raise_error=False):

        error_class = error_class if error_class else VMRCartPageLengthSectionException
        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        super().check_data_layer_length_impressions_eq(value, data, dict_name, error_class, raise_error)

    def check_page_data_impressions_with_data_layer_eq(self, value, data=None, obj_2_name='', parameter=None,
                                                       error_class=None, raise_error=False):

        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        error_class = VMRCartPageDataLayerSectionException
        super().check_data_layer_impressions_data_with_page_eq(value, data, "DataLayer", 'name', error_class, True)

    def check_data_layer_impressions_data_with_page_eq(self, value, data=None, obj_2_name="", parameter=None,
                                                       error_class=None, raise_error=False):

        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        error_class = VMRCartPageDataLayerSectionException
        super().check_data_layer_impressions_data_with_page_eq(value, data,
                                                               "Данные страницы", 'name', error_class, True)

    def check_len_products_eq(self, value, raise_error=False):

        try:
            self.page.get_product_elements().assure(len_equal, value)
            result = True
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise CartPageLenProductException("Некорректное количество продуктов в корзине. Ожидаем: {0}, Видим: {1}"
                                              .format(value, len(self.page.get_product_elements())))

    def check_order_price_eq(self, value, raise_error=False):
        result = self.page.get_order_price() == value

        if not result and raise_error:
            raise CartOrderPriceException("Стоимость заказа {0} не совпадает с {1}"
                                          .format(self.page.get_order_price(), value))
        return result

    def check_favorites_active(self, raise_error=False):
        try:
            self.page.get_add_favorites_add_element()
            result = True
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise VMRCartPageFavoritesAddNotExistException("Не найден элемент 'добавить в избраноое' {0}"
                                                           .format(self.page.get_add_favorites_add_element()))
        return result

    def check_len_russian_size_gte(self, len_size, raise_error=False):
        try:
            result = len(self.page.get_product_element(index=0).get_rus_size_str()) >= len_size
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise CartPageLenRussianSizeException("Некорректный русский размер")

    def check_promocode_text_eg(self, value, raise_error=False):
        try:
            result = value == self.page.get_promo_code_text()
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise CartPromoCodeTextException("Текст не совпадает. Ожидаем: {0}. Видим: {1}"
                                             .format(value, self.page.get_promo_code_text()))
        return result

    def check_order_price_element_eq(self, value, index, raise_error=False):
        result = self.page.get_product_element(index).get_price() == value

        if not result and raise_error:
            raise CartProductPriceException("В товаре '{0}' цена {1} не совпадает с ожидаемой {2}"
                                          .format(self.page.get_product_element(index).get_name(),
                                                  self.page.get_product_element(index).get_price(), value))
        return result
