from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class CartException(VMRBasePageException):
    pass

class CartEndedProductException(VMRBasePageException):
    pass

class CartLoaderException(CartException):
    pass


class CartProductDeleteException(CartException):
    pass


class CartLenProductsException(CartException):
    pass


class CartIntrestingProductException(CartException):
    pass


class VMRCartPageLengthSectionException(CartException):
    pass


class VMRCartPageDataLayerSectionException(CartException):
    pass


class VMRCartPageDataLayerSectionExistException(CartException):
    pass


class VMRCartPageFavoritesAddNotExistException(CartException):
    pass


class VMRCartDataLayerException(CartException):
    pass


class CartPageLenProductException(CartException):
    pass


class CartValueException(CartException):
    pass


class CartPageImageGenerationException(CartException):
    pass


class CartOrderPriceException(CartException):
    pass


class CartPageLenRussianSizeException(CartException):
    pass


class CartPromoCodeTextException(CartException):
    pass


class CartProductPriceException(CartException):
    pass


class CartNotEmptyException(CartException):
    pass


class CartNotFoundRecommendProductException(CartException):
    pass


class CartNotRefreshRecommendProductException(CartException):
    pass


class CartProductNotFoundException(CartException):
    pass


class CartBigImageException(CartException):
    pass