from typing import Union

import time
from selenium import webdriver
from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_collection_conditions import len_equal, len_more
from autotests_base.conditions.element_conditions import is_displayed, is_not_displayed, exist, text_not_none
from autotests_base.elements.waiting_elements import WaitingElement
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.base.exception import ConfirmException
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.CartPage.check import CartCheck
from module_projects.vmr.pages.CartPage.exception import CartLoaderException, CartProductDeleteException, \
    CartIntrestingProductException, CartOrderPriceException, CartEndedProductException
from module_projects.vmr.pages.CartPage.structure import VMRCartPageStructure
from module_projects.vmr.pages.CartPage.waiting_elements import CartInterestingProductWaitingElement, CartProductWE


class VMRCartPage(VMRBasePage):
    page_name = 'VMRCart'

    @staticmethod
    def get_structure():
        return VMRCartPageStructure()

    def _get_check(self):
        """

        :rtype: CartCheck
        """
        return CartCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        url = 'cart' if url is None else url
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        try:
            self.search(self.structure.CART_LOADER).assure(is_not_displayed, timeout=10)
        except TimeoutException:
            raise CartLoaderException("Долгая загрузка лодера на странице корзины.")

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def clean_cart(self):
        i = self.HEADER.get_cart_count()

        if i == 0:
            return None

        for item in range(0, i):
            self.get_product_elements()[0].delete()
            self.get_product_elements().assure(len_equal, i - item)

    def get_product_elements(self):
        """

        :rtype: WaitingElementCollection[CartProductWE]
        """
        return self.search_all(self.structure.PRODUCTS, elements_type=CartProductWE)

    def get_product_element(self, index):
        """

        :rtype: CartProductWE
        """
        return self.get_product_elements()[index]

    def go_to_order_page(self):
        """

        :rtype: module_projects.vmr.pages.OrderPage.page.VMROrderPage
        """
        self.get_confirm_order_button_element().click()

        from module_projects.vmr.pages.OrderPage.page import VMROrderPage
        try:
            return self.create_page_object(VMROrderPage)
        except (TimeoutException, ConfirmException) as e:
            if self.has_ended_product():
                raise CartEndedProductException('Заказ содержит закончившиеся продукты')
            else:
                raise e

    def get_confirm_order_button_element(self):
        return self.search(self.structure.CONFIRM_ORDER_BUTTON)

    def get_interesting_products(self):
        """

        :rtype: WaitingElementCollection[CartInterestingProductWaitingElement]
        """
        try:
            return self.search_all(self.structure.INTERESTING_PRODUCT,
                                   elements_type=CartInterestingProductWaitingElement).assure(len_more, 0)
        except TimeoutException:
            raise CartIntrestingProductException("Отсутствуют рекомендованные товары.")

    def refresh_interesting_products(self):
        self.search(self.structure.CART_REFRESH).assure(exist).assure(is_displayed).click()

    def delete_product_by_index(self, index):
        self.get_product_elements()[index].delete()

    def get_order_price(self):
        try:
            return int(self.get_order_price_element().text.split(' ')[0])
        except ValueError:
            raise CartOrderPriceException("Некорректная стоимость заказа")

    def get_order_price_element(self):
        return self.search(self.structure.ORDER_PRICE)

    def has_ended_product(self):
        return self.get_ended_message_element().is_displayed()

    def get_ended_message_element(self):
        return self.search(self.structure.ENDED_PRODUCT_MESSAGE)

    def add_favorites(self):
        self.get_add_favorites_add_element().click()

    def get_add_favorites_add_element(self):
        return self.search(self.structure.FAVORITES_ADD).assure(exist).assure(is_displayed)

    def del_favorites(self):
        self.search(self.structure.FAVORITES_DEL).assure(exist).assure(is_displayed).click()

    def set_promo_code(self, promo_code):
        element = self.get_promo_code_input_element()
        element.clear()
        element.send_keys(promo_code)
        self.apply_promo_code()

    def get_promo_code_input_element(self):
        return self.search(self.structure.PROMO_CODE).assure(exist).assure(is_displayed)

    def apply_promo_code(self):
        self.get_apply_promo_button_element().click()

    def get_apply_promo_button_element(self):
        return self.search(self.structure.APPLY_PROMO_CODE).assure(exist)

    def get_promo_code_text(self):
        try:
            return self.get_promo_code_text_element().assure(exist).assure(text_not_none).text
        except TimeoutException:
            return None

    def get_promo_code_text_element(self):
        return self.search(self.structure.PROMO_CODE_TEXT)

    def is_empty(self) -> bool:
        try:
            self.search(self.structure.EMPTY).assure(exist).assure(is_displayed)
        except TimeoutException:
            return False
        else:
            return True

    def set_choice_in_shop(self):
        """

        :rtype: module_projects.vmr.pages.MainPage.page
        """
        self.get_shop_button_element().click()
        from module_projects.vmr.pages.MainPage.page import VMRMainPage
        return self.create_page_object(VMRMainPage)

    def get_shop_button_element(self) -> WaitingElement:
        return self.search(self.structure.GET_SHOP_BUTTON).assure(exist).assure(is_displayed)

    def create_product(self):
        """

        :rtype: module_projects.vmr.pages.ConstructorPage
        """
        self.search(self.structure.CREATE_PRODUCT).assure(is_displayed).click()
        from module_projects.vmr.pages.ConstructorPage.page import VMRConstructorPage
        return self.create_page_object(VMRConstructorPage)

    def get_products_elements(self):
        """

        :rtype: WaitingElementCollection[CartProductWE]
        """
        return self.search_all(self.structure.PRODUCTS, elements_type=CartProductWE)

    def next_pop_up_image(self):
        self.search(self.structure.POP_UP_NEXT_IMAGE).assure(exist).assure(is_displayed).click()

    def get_pop_up_img_href(self) -> str:
        return self.get_pop_up_element().get_attribute('src')

    def get_pop_up_element(self) -> WaitingElement:
        return self.search(self.structure.POP_UP_IMG).assure(exist).assure(is_displayed)

    def close_pop_up(self):
        self.search(self.structure.POP_UP_CLOSE).assure(exist).assure(is_displayed).click()

    def is_pop_up(self):
        try:
            return self.search(self.structure.POP_UP_IMG).assure(is_displayed)
        except TimeoutException:
            return False

    def return_main_page(self):
        """

        :rtype:  module_projects.vmr.pages.MainPage.page
        """
        self.search(self.structure.RETURN_BUTTON).assure(exist).assure(is_displayed).click()
        from module_projects.vmr.pages.MainPage.page import VMRMainPage
        return self.create_page_object(VMRMainPage)

    def go_to_product_page(self, index: int):
        self.get_products_elements().assure(len_more, 0)[index].get_product_href().click()
        from module_projects.vmr.pages.ProductPage.page import VMRProductPage
        return self.create_page_object(VMRProductPage)

