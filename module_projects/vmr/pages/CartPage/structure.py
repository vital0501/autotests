from module_projects.base.structure import BasePageStructure


class VMRCartPageStructure(BasePageStructure):

    ENDED_PRODUCT_MESSAGE = '//*[@class="row errors"]'

    CONFIRM = '//*[@id="cart"]'
    CART_LOADER = '//*[contains(@class,"newloader")]'

    PRODUCTS = "//*[@id='cart_items_table']/div"
    CONFIRM_ORDER_BUTTON = "//*[@id='to_delivery']"

    INTERESTING_PRODUCT = '//*[@class="row related-item"]'

    ORDER_PRICE = "//*[@id='cart_sum']/b"

    FAVORITES_DEL = '//*[@class="to-favorites hidden-xs hidden-sm"]//*[@class="fa fa-heart"]'
    FAVORITES_ADD = '//*[@class="to-favorites hidden-xs hidden-sm"]//*[@class="fa fa-heart-o"]'

    PROMO_CODE = "//*[@id='code']"
    APPLY_PROMO_CODE = "//*[@id='apply_discount']"
    PROMO_CODE_TEXT = "//p[@class='help-block error']|//*[@class='help-block'][not(ancestor::div[@id='cart_sum'])]"

    EMPTY = "//*[@class='empty-cart']"
    GET_SHOP_BUTTON = EMPTY + "//a[text()='Выбрать в магазине']"
    CREATE_PRODUCT = "//*[text()='Создать товар']"
    CART_REFRESH = "//*[@class='reload text-center']/span"
    POP_UP_NEXT_IMAGE = "//*[@class='fancybox-nav fancybox-next']"
    POP_UP_IMG = "//*[@class='fancybox-image']"
    POP_UP_CLOSE = "//*[@class='fancybox-item fancybox-close']"
    RETURN_BUTTON = "//*[text()='< Вернуться к выбору']"