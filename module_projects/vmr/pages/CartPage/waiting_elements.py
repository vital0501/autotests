import requests
import time

from selenium.webdriver.support.select import Select

from autotests_base.conditions.element_conditions import exist, text_not_none, is_displayed
from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement
from module_projects.vmr.pages.BasePage.waiting_elements import VMRInitDataWE
from module_projects.vmr.pages.CartPage.exception import CartPageImageGenerationException


class BaseCartWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class CartProductWE(BaseCartWE, VMRInitDataWE):
    def is_ended(self):
        return SubWaitingElement(self, '//*[@class="sorry"]').is_displayed()

    def get_data_sub_path(self) -> str:
        return '/input'

    def delete(self):
        SubWaitingElement(self, "//*[@class='delete']").click()

    def get_total_price_element(self):
        return SubWaitingElement(self, "//p[@class='total']")

    def get_price(self) -> int:
        return int(self.get_total_price_element().assure(exist).assure(text_not_none).text.replace(' руб.', ''))

    def _get_name_element(self):
        return SubWaitingElement(self, "//span[@class='design-name']")

    def get_full_name(self) -> str:
        return SubWaitingElement(self._get_name_element(), "/parent::*").assure(exist).text

    def get_name(self) -> str:
        return self._get_name_element().assure(exist).text

    def get_size_element(self):
        return SubWaitingElement(self, "//*[contains(@id, 'size_select')]")

    def get_size_str(self) -> str:
        return Select(self.get_size_element().assure(exist)).first_selected_option.text.split('(')[0].strip()

    def get_rus_size_str(self) -> str:
        return Select(self.get_size_element().assure(exist)).first_selected_option.text.split('(')[1].strip().split(')')[0].strip()

    def get_size_key(self) -> str:
        return Select(self.get_size_element().assure(exist)).first_selected_option.get_attribute('value')

    def get_type(self):
        return self.init_data['type']

    def get_type_name(self):
        return self.get_full_name().split("«")[0].strip()

    def get_full_img_href(self) -> str:
        return SubWaitingElement(self, "//a[descendant::img]").get_attribute('href')

    def wait_to_generate_img(self):
        for i in range(0, 10):
            a = requests.get(self.get_full_img_href())
            if a.status_code == 200:
                break
            time.sleep(2)
        else:
            raise CartPageImageGenerationException("Не сгенерировалась картинка продукта за 20 сек")

    def open_img_pop_up(self):
        self.get_img().click()

    def get_img(self):
        return SubWaitingElement(self,  "//a/img")

    def del_from_cart(self):
        SubWaitingElement(self,  "//span[@class='delete']").click()

    def get_product_href(self):
        return SubWaitingElement(self,  "//a[@class ='name']")

class CartInterestingProductWaitingElement(BaseCartWE, VMRInitDataWE):
    def get_data_sub_path(self) -> str:
        return '/input'

    def add_to_cart(self):
        SubWaitingElement(self, "//a[@class='add btn btn-vm']").assure(is_displayed).click()

    def get_name(self):
        return SubWaitingElement(self, "//h5/a").assure(exist).assure(text_not_none).text

    def get_init_product_id(self):
        return self.init_data['article']

    def get_price(self):
        return self.init_data['price']

    @staticmethod
    def get_init_product_list():
        return "Recommend_Basket"

    def get_product_type(self):
        return self.init_data['productName']

    def variant(self):
        return self.init_data['productTkey']