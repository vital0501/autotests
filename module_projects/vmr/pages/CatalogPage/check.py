from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_collection_conditions import len_more
from module_projects.vmr.pages.BasePage.check import VMRBaseCheck, DataLayerCheck
from module_projects.vmr.pages.BasePage.exception import ValueException
from module_projects.vmr.pages.CatalogPage.exception import CatalogLenProductException, \
    VMRCatalogDataLayerSectionException, VMRCatalogLengthSectionException,\
    CatalogNullPriceException, EmptyCatalogPageException


class CatalogCheck(VMRBaseCheck, DataLayerCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.CatalogPage.page.VMRCatalogPage
        """
        super().__init__(page)
        self.page = page

    def check_products_ge(self, value, raise_error=False, timeout=10):
        collection = self.page.get_products_elements()

        return self.check_length_collection_gt(collection,
                                               value,
                                               'Продукты на странице каталога',
                                               raise_error,
                                               CatalogLenProductException,
                                               timeout)

    def check_data_layer_length_impressions_eq(self, value, data=None, dict_name='Impressions', error_class=None, raise_error=False):
        error_class = error_class if error_class else VMRCatalogLengthSectionException
        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        super().check_data_layer_length_impressions_eq(value, data, dict_name, error_class, raise_error)

    def check_data_layer_impressions_data_with_page_eq(self, value, data=None, obj_2_name="", parameter=None, error_class=None, raise_error=False):
        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        error_class = VMRCatalogDataLayerSectionException
        super().check_data_layer_impressions_data_with_page_eq(value, data, "DataLayer", 'name', error_class, True)

    def check_page_data_impressions_with_data_layer_eq(self, value, data=None, obj_2_name='', parameter=None, error_class=None, raise_error=False):
        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        error_class = VMRCatalogDataLayerSectionException
        super().check_page_data_impressions_with_data_layer_eq(value, data, "Page", 'name', error_class, True)

    def check_product_null_price(self, value, raise_error=False):
        try:
            result = self.check_value_gt_0(value=value, raise_error=raise_error)
        except ValueException:
            result = False

        if not result and raise_error:
            raise CatalogNullPriceException("Не обнаружена или нулевая цена у продукта")
        return result

    def check_not_empty_catalog(self, catalog_name, raise_error=False):
        result = len(self.page.get_products_elements()) > 0
        if not result and raise_error:
            raise EmptyCatalogPageException(('Обнаружен пустой каталог "{0}"'.format(catalog_name)))
        return result

    def check_not_empty_page(self, raise_error=False) -> bool:
        try:
            self.page.get_products_elements().assure(len_more, 0, timeout=10)
            result = True
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise EmptyCatalogPageException('Каталог пуст')
        return result
