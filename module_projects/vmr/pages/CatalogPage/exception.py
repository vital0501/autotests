from module_projects.base.exception import DataLayerSectionException
from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class VMRCatalogPageException(VMRBasePageException):
    pass


class CatalogLenProductException(VMRCatalogPageException):
    pass


class CatalogPageProductException(VMRBasePageException):
    pass


class VMRCatalogDataLayerSectionException(DataLayerSectionException, VMRCatalogPageException):
    pass


class VMRCatalogDataLayerSectionExistException(DataLayerSectionException, VMRCatalogPageException):
    pass


class VMRCatalogLengthSectionException(DataLayerSectionException, VMRCatalogPageException):
    pass


class CatalogNullPriceException(VMRCatalogPageException):
    pass


class CatalogPageLoaderException(VMRCatalogPageException):
    pass


class CatalogPaginationException(VMRCatalogPageException):
    pass

class CatalogPageCatalogMenuException(VMRCatalogPageException):
    pass

class EmptyCatalogPageException(VMRCatalogPageException):
    pass