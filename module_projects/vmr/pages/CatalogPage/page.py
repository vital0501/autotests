from typing import Union


from selenium import webdriver
from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_conditions import is_displayed, is_not_displayed
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection

from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.CatalogPage.check import CatalogCheck
from module_projects.vmr.pages.CatalogPage.exception import CatalogPageLoaderException, CatalogPageCatalogMenuException
from module_projects.vmr.pages.CatalogPage.structure import VMRCatalogPageStructure
from module_projects.vmr.pages.CatalogPage.waiting_elements import CatalogProductWE


class VMRCatalogPage(VMRBasePage):
    page_name = 'CatalogPage'

    @staticmethod
    def get_structure():
        return VMRCatalogPageStructure()

    def _get_check(self):
        """

        :rtype: CatalogCheck
        """
        return CatalogCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout, confirm, init):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_products_elements(self):
        """

        :rtype: WaitingElementCollection[CatalogProductWE]
        """
        return self.search_all(self.structure.PRODUCTS, elements_type=CatalogProductWE)

    def get_str_id(self) -> str:
        return self.driver.current_url.split('/')[-1]

    def next_pagination(self):
        self.get_next_pagination_button_element().click()

        try:
            self.search(self.structure.CATALOG_LOADER).assure(is_displayed, timeout=2)
        except TimeoutException:
            pass

        try:
            self.search(self.structure.CATALOG_LOADER).assure(is_not_displayed, timeout=10)
        except TimeoutException:
            raise CatalogPageLoaderException("Долгая загрузка лодера на странице каталога.")

    def get_next_pagination_button_element(self):
        return self.search(self.structure.NEXT_PAGINATION_BUTTON)

    def go_to_product_by_product(self, product_element):
        """

        :rtype: module_projects.vmr.pages.ProductPage.page.VMRProductPage
        """
        product_element.click()
        from module_projects.vmr.pages.ProductPage.page import VMRProductPage
        return self.create_page_object(VMRProductPage)

    def go_to_product_by_index(self, index):
        """

        :rtype: module_projects.vmr.pages.ProductPage.page.VMRProductPage
        """
        self.get_products_elements()[index].click()
        from module_projects.vmr.pages.ProductPage.page import VMRProductPage
        return self.create_page_object(VMRProductPage)
