from module_projects.base.structure import BasePageStructure


class VMRCatalogPageStructure(BasePageStructure):
    CONFIRM = "//*[@id='filter']"
    PRODUCTS = '//*[@data-autotest-id="product"]'
    CATALOG_LOADER = "//*[@class='catalog-loader']"

    NEXT_PAGINATION_BUTTON = "//a[@title='Следующая страница']"

    PRODUCT_ADD_FAVORITES = "//*[@class='favorites-label__icon']"