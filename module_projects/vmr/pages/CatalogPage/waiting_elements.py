import html

from selenium.common.exceptions import NoSuchElementException

from autotests_base.conditions.element_conditions import exist
from autotests_base.elements.waiting_elements import SubWaitingElement
from module_projects.vmr.pages.BasePage.waiting_elements import VMRInitDataWE
from module_projects.vmr.pages.CatalogPage.exception import CatalogPageProductException


class CatalogProductWE(VMRInitDataWE):
    def get_init_category(self) -> str:
        return self.init_data['full_category']

    def get_price(self) -> int:
        try:
            return int(SubWaitingElement(self, '//i[@class="list-item__price-value"]').text)
        except NoSuchElementException:
            self.scroll_to_element()
            raise CatalogPageProductException("Не обнаружена цена у продукта {0}" .format(self.get_name()))

    def get_old_price(self)-> int:
        element = SubWaitingElement(self, '//i[@class="list-item__price-value list-item__price-value--old"]')

        if element.exist():
            return int(element.text)
        else:
            return None

    def get_product_type(self):
        return SubWaitingElement(self, '/div/a').get_attribute('href').split('/')[4]

    def get_init_product_id(self) -> int:
        return int(self.init_data['id'])

    def get_name(self) -> str:
        return html.unescape(SubWaitingElement(self, "//span[@itemprop='name']").assure(exist).text).strip()

    def get_init_product_list(self) -> str:
        return self.init_data['list']

    def get_data_sub_path(self):
        return '/input[@name="productInit"]'

    def __init__(self, driver, selector, selector_type, timeout, is_collection_item):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def add_favorites(self):
        SubWaitingElement(self, "//*[@class='favorites-label__icon']").assure(exist).click()