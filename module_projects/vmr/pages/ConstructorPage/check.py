from autotests_base.conditions.element_conditions import exist, is_displayed
from module_projects.vmr.pages.BasePage.check import VMRBaseCheck, DataLayerCheck
from module_projects.vmr.pages.ConstructorPage.exception import ConstructorPriceException, \
    ConstructorPageNullPriceException, ConstructorPageLoaderPriceException, ConstructorPageTableException


class ConstructorCheck(VMRBaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.ConstructorPage.page.VMRConstructorPage
        """
        super().__init__(page)
        self.page = page

    def check_price_eq(self, value, raise_error=False):

        try:
            result = self.page.get_price() == value
            _error_text = "Цена продукта не равна {0}".format(value)
        except (ConstructorPageNullPriceException, ConstructorPageLoaderPriceException) as e:
            result = False
            _error_text = str(e)

        if not result and raise_error:
            raise ConstructorPriceException(_error_text)
        return result

    def check_price_not_eq(self, value, raise_error=False):

        try:
            result = self.page.get_price() != value
            _error_text = "Цена продукта равна {0}".format(value)
        except (ConstructorPageNullPriceException, ConstructorPageLoaderPriceException) as e:
            result = False
            _error_text = str(e)

        if not result and raise_error:
            raise ConstructorPriceException(_error_text)
        return result

    def is_exist(self, _table_element, raise_error=False):
        _error_text = ''
        try:
            _table_element.assure(exist).assure(is_displayed)
            result = True
        except ConstructorPageTableException as e:
            _error_text = str(e)
            result = False

        if not result and raise_error:
            raise ConstructorPageTableException(_error_text)
        return result
