from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class ConstructorException(VMRBasePageException):
    pass


class ConstructorPageLoaderException(ConstructorException):
    pass


class ConstructorPageLoaderPriceException(ConstructorException):
    pass


class ConstructorPageNullPriceException(ConstructorException):
    pass


class ConstructorPriceException(ConstructorException):
    pass


class ConstructorPopUpNotFoundException(ConstructorException):
    pass


class ConstructorPageSetSizeException(ConstructorException):
    pass


class ConstructorPageSetColorException(ConstructorException):
    pass


class ConstructorPageTableException(ConstructorException):
    pass
