import datetime
import random
from typing import Union

import time
from selenium import webdriver
from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_collection_conditions import len_more, len_gte
from autotests_base.conditions.element_conditions import is_displayed, is_not_displayed, text_not_0, exist
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.ConstructorPage.check import ConstructorCheck
from module_projects.vmr.pages.ConstructorPage.exception import ConstructorPageLoaderException, \
    ConstructorPageLoaderPriceException, ConstructorPopUpNotFoundException, \
    ConstructorPageSetSizeException, ConstructorPageSetColorException
from module_projects.vmr.pages.ConstructorPage.structure import VMRConstructorPageStructure
from module_projects.vmr.pages.ConstructorPage.waiting_elements import ConstructorSizeWE, ConstructorProductTypeWE, \
    ConstructorPopUpWE, ConstructorColorWE, ProductTypeMenuWE, ConstructorProductTypeGroupWE


class VMRConstructorPage(VMRBasePage):
    page_name = 'VMRConstructor'

    @staticmethod
    def get_structure():
        return VMRConstructorPageStructure()

    def _get_check(self):
        """

        :rtype: ConstructorCheck
        """
        return ConstructorCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()
        self.wait_loader()

        self.PRODUCT_TYPES_WORKER = self.get_product_type_worker_element()

    def wait_loader(self):
        try:
            self.search(self.structure.CONSTRUCTOR_LOADING).assure(is_displayed, timeout=0.5)
        except TimeoutException:
            pass

        try:
            self.search(self.structure.CONSTRUCTOR_LOADING).assure(is_not_displayed, timeout=20)
        except TimeoutException:
            raise ConstructorPageLoaderException("Долгая загрузка лодера на странице конструтора")

    def wait_price_loader(self):
        try:
            self.get_price_loader().assure(is_displayed, timeout=0.5)
        except TimeoutException:
            pass

        try:
            self.get_price_loader().assure(is_not_displayed, timeout=5)
        except TimeoutException:
            raise ConstructorPageLoaderPriceException("Долгая загрузка цены")

    def get_price(self):
        try:
            result = int(self.search(self.structure.PRICE).text)
        except (TimeoutException, ValueError):
            result = 0
        return result

    def get_price_loader(self):
        return self.search(self.structure.PRICE_LOADER)

    def get_active_size_element(self):
        """

        :rtype: ConstructorSizeWE
        """
        return self.search(self.structure.ACTIVE_SIZE_ELEMENT, element_type=ConstructorSizeWE).assure(exist)

    def get_active_color_element(self):
        """

        :rtype: ConstructorColorWE
        """
        return self.search(self.structure.ACTIVE_COLOR_ELEMENT, element_type=ConstructorColorWE)

    def set_random_color(self):
        element = random.choice(self.get_colors_elements())

        for i in range(0, 3):
            element.click()
            try:
                self.get_active_color_element().assure(exist)
                time.sleep(3)
                break
            except TimeoutException:
                time.sleep(1)
        else:
            raise ConstructorPageSetColorException("Не получилось установить случайный цвет")

    def get_size_elements(self):
        return self.search_all(self.structure.SIZES).assure(len_more, 0)

    def set_random_size(self):
        element = random.choice(self.get_size_elements())

        for i in range(0, 3):
            element.click()
            try:
                self.get_active_size_element().assure(exist)
                time.sleep(3)
                break
            except TimeoutException:
                time.sleep(1)
        else:
            raise ConstructorPageSetSizeException("Не получилось установить случайный размер")

    def create_product(self):

        for item in range(0, 10):
            if self.search(self.structure.CREATE_PRODUCT_LOADER).is_displayed():
                break
            else:
                time.sleep(2)
            self.get_add_button_element().click()

    def get_created_pop_up(self, timeout=10):
        """

        :rtype: ConstructorPopUpWE
        """
        try:
            return self.search(self.structure.POP_UP, element_type=ConstructorPopUpWE, timeout=timeout).assure(exist)\
                .assure(is_displayed)
        except TimeoutException:
            raise ConstructorPopUpNotFoundException("Не появился поп-ап с сообщением о добавлении продукта в корзину")

    def get_colors_elements(self):
        return self.search_all(self.structure.COLORS, elements_type=ConstructorColorWE)

    def get_sizes_elements(self):
        return self.search_all(self.structure.SIZES, elements_type=ConstructorSizeWE)

    def set_size_by_index(self, index):
        self.get_sizes_elements()[index].click()
        self.wait_price_loader()

    def set_color_by_index(self, index):
        self.get_colors_elements()[index].click()
        self.wait_price_loader()

    def add_product_to_cart(self):
        self.get_add_button_element().click()

    def get_add_button_element(self):
        return self.search(self.structure.ADD_BUTTON)

    def get_add_product_pop_up(self):
        return self.search(self.structure.ADD_PRODUCT_POP_UP, element_type=ConstructorPopUpWE)

    def get_product_type_worker_element(self):
        """

        :rtype: ProductTypeMenuWE
        """
        return self.search(self.structure.PRODUCT_TYPE_MENU, element_type=ProductTypeMenuWE)

    def set_product_type_by_name(self, name, group_name):
        self.PRODUCT_TYPES_WORKER.set_product_type_by_name(name, group_name)
        self.wait_price_loader()

    def get_available_color_elements(self):
        """

        :rtype: WaitingElementCollection[ConstructorColorWE]
        """
        return self.search_all(self.structure.COLORS, elements_type=ConstructorColorWE).assure(len_gte, 0)

    def get_available_sizes_elements(self):
        """

        :rtype: WaitingElementCollection[ConstructorSizeWE]
        """
        return self.search_all(self.structure.SIZES, elements_type=ConstructorSizeWE).assure(len_gte, 0)

    def get_product_type_groups(self):
        """

        :rtype: WaitingElementCollection[ConstructorProductTypeGroupWE]
        """
        return self.search_all(self.structure.PRODUCT_TYPE_GROUP, elements_type=ConstructorProductTypeGroupWE)

    def set_product_type_group_by_index(self, index):
        self.PRODUCT_TYPES_WORKER.set_product_type_group_by_index(index=index)

    def get_active_product_types_elements(self):
        """

        :rtype: WaitingElementCollection[ConstructorProductTypeWE]
        """
        return self.search_all(self.structure.ACTIVE_PRODUCT_TYPES, elements_type=ConstructorProductTypeWE)

    def set_product_type_by_index(self, index):
        self.PRODUCT_TYPES_WORKER.set_product_type_by_index(index=index)

    def get_active_product_type(self):
        return self.search(self.structure.ACTIVE_PRODUCT_TYPE, element_type=ConstructorProductTypeWE)

    def open_about_tshirt(self):
        self.search(self.structure.ABOUT_TSHIRT_LINK).assure(exist).assure(is_displayed).click()

    def get_about_product_element(self):
        return self.search(self.structure.ABOUT_PRODUCT_TABLE)

    def close_about_product(self):
        return self.search(self.structure.CLOSE_ABOUT_PRODUCT_TABLE).assure(exist).assure(is_displayed).click()

    def open_sizes_table(self):
        self.search(self.structure.SIZES_TABLE_LINK).assure(exist).assure(is_displayed).click()

    def get_sizes_table_element(self):
        return self.search(self.structure.SIZES_TABLE)

    def close_sizes_table(self):
        return self.search(self.structure.CLOSE_SIZES_TABLE).assure(exist).assure(is_displayed).click()
