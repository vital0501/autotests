from module_projects.base.structure import BasePageStructure


class VMRConstructorPageStructure(BasePageStructure):

    CREATE_PRODUCT_LOADER = '//*[@id="product-creation-loader"]'

    PRODUCT_TYPE_MENU = '//*[@id="tool_products"]'

    CONFIRM = "//*[@class='main-constructor-holder']"
    CONSTRUCTOR_LOADING = '//*[@class="newloader mainloader text-center"]'

    PRODUCT_TYPES = "//*[@id='producttypes_preview']/div[contains(@class, 'active')]/div"
    ACTIVE_PRODUCT_TYPE = PRODUCT_TYPES + "[contains(@class, 'active')]"

    COLORS = '//*[@class="product-constructor__color-list"]/div'
    SIZES = '//*[@class="product-constructor__sizes-list"]/li'

    PRODUCT_TYPE_GROUP = '//*[@id="producttype-filter"]/div'
    ACTIVE_PRODUCT_TYPE_GROUP = PRODUCT_TYPE_GROUP + '[contains(@class, "active")]'
    PRODUCT_BY_GROUP_TEMPLATE = '//*[@id="producttypes_preview"]/div[@data-alias="{0}"]/div'
    ACTIVE_PRODUCT_TYPES = '//*[@class="product-constructor__groups-holder"]/div[contains(@class, "active")]/div'
    ACTIVE_SIZE_ELEMENT = SIZES + "[contains(@class, 'active')]"
    ACTIVE_COLOR_ELEMENT = COLORS + "[contains(@class, 'active')]"

    ADD_BUTTON = '//*[@id="add-to-basket"]'
    ADD_PRODUCT_POP_UP = '//*[@id="success_popup"]'

    PRICE = "//*[@id='price']"
    PRICE_LOADER = "//*[@id='price-loading']"

    POP_UP = "//*[@id='success_popup']"

    ABOUT_TSHIRT_LINK = "//*[@id='color_link']"
    ABOUT_PRODUCT_TABLE = "//*[@id='colors_tableLabel']"
    CLOSE_ABOUT_PRODUCT_TABLE = "//*[@id='colors_table']//button"

    SIZES_TABLE_LINK = "//*[@id='sizes_table_link']"
    SIZES_TABLE = "//*[@id='sizes_tableLabel']"
    CLOSE_SIZES_TABLE = "//*[@id='sizes_table']//button"
