import time

from autotests_base.conditions.element_conditions import exist, is_not_displayed, is_displayed
from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement
import lxml.html


class StructureElement(object):
    def __init__(self, name, group_name, group_key):
        self.name = name
        self.group_name = group_name
        self.group_key = group_key

    def to_json(self):
        return {
            'name': self.name,
            'group_name': self.group_name,
            'group_key': self.group_key,
        }

    def __str__(self):
        return str(self.to_json())


class BaseConstructorWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class ConstructorProductTypeWE(BaseConstructorWE):
    def get_name(self):
        return SubWaitingElement(self, '/p').assure(exist, timeout=10).text


class ConstructorProductTypeGroupWE(BaseConstructorWE):
    def get_name(self):
        return self.text

    def get_key(self):
        return self.get_attribute('data-tkey')


class ConstructorColorWE(BaseConstructorWE):
    def get_name(self):
        return self.get_attribute('title')


class ConstructorSizeWE(BaseConstructorWE):
    def get_name(self):
        return SubWaitingElement(self, '/span').text

    def get_id(self):
        return self.get_attribute("data-key")


class ConstructorPopUpWE(BaseConstructorWE):
    def close(self):
        SubWaitingElement(self, '//*[@aria-label="Close"]').click()


class ProductTypeMenuWE(BaseConstructorWE):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item)
        self._group_structure = {}
        self._menu_structure = self._init_menu_structure()

    def get_menu_structure(self):
        """

        :rtype: list[StructureElement]
        """
        return self._menu_structure

    def set_product_type_by_name(self, name, group_name):
        self.set_product_type_by_element(self.find_element(name=name, group_name=group_name))

    def set_product_type_by_element(self, element):
        SubWaitingElement(self, '//*[@id="producttype-filter"]/div[@data-tkey="{0}"]'.format(element.group_key)).assure(
            is_displayed).click()
        SubWaitingElement(self, '//*[@id="producttypes_preview"]/div[@data-alias="{0}"]/div/p[text()="{1}"]'.format(
            element.group_key, element.name)).assure(is_displayed).click()

    def find_element(self, **kwargs) -> StructureElement:
        for item in self._menu_structure:
            for key in kwargs:
                if not hasattr(item, key):
                    break
                elif getattr(item, key) != kwargs[key]:
                    break
            else:
                return item
        return None

    def _init_menu_structure(self):
        result = []

        lxml_text = self.get_attribute('innerHTML').replace("\n", '')
        main_lxml_element = lxml.html.fromstring(lxml_text)

        group_elements = main_lxml_element.findall('.//*[@id="producttype-filter"]/div')

        for index, group_element in enumerate(group_elements):
            # group_element = lxml.html.HtmlElement()
            g_name = group_element.text
            g_key = group_element.attrib['data-tkey']
            self._group_structure.update({index: {"name": g_name, "key": g_key}})

            elements = main_lxml_element.findall('.//*[@id="producttypes_preview"]/div[@data-alias="{0}"]/div'.format(g_key))
            for element in elements:
                e_name = element.find('./p').text
                result.append(StructureElement(group_name=g_name, group_key=g_key, name=e_name))

        return result

    def set_product_type_group_by_index(self, index):
        SubWaitingElement(self, '//*[@id="producttype-filter"]/div[@data-tkey="{0}"]'.format(self._group_structure[int(index)]['key'])).assure(is_displayed).click()

    def get_active_product_type_group(self):
        """

        :rtype: ConstructorProductTypeGroupWE
        """
        return SubWaitingElement(self, '//*[@id="producttype-filter"]/div[contains(@class, "active")]', element_type=ConstructorProductTypeGroupWE)

    def set_product_type_by_index(self, index):
        SubWaitingElement(self, '//*[@id="producttypes_preview"]//div[contains(@class, "active")]/div[{0}]'.format(index + 1)).assure(is_displayed).click()



