from module_projects.base.check import BaseCheck


class ContactsCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.ContactsPage.page.VMRContactsPage
        """
        super().__init__(page)
        self.page = page
