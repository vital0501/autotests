from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class ContactsException(VMRBasePageException):
    pass


class ContactsSendMsgException(ContactsException):
    pass
