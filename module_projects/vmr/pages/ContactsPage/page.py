from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import exist, is_displayed
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.ContactsPage.check import ContactsCheck
from module_projects.vmr.pages.ContactsPage.structure import VMRContactsPageStructure
from module_projects.vmr.pages.ContactsPage.waiting_elements import ContactFormWA
from module_projects.vmr.parts.VMRLeftMenuInfo.page import VMRLeftMenuInfo


class VMRContactsPage(VMRBasePage):
    page_name = 'VMRContacts'

    @staticmethod
    def get_structure():
        return VMRContactsPageStructure()

    def _get_check(self):
        """

        :rtype: ContactsCheck
        """
        return ContactsCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

        self.LEFT_MENU = VMRLeftMenuInfo(driver, project_url, timeout)

    def get_form_element_element(self):
        return self.search(self.structure.FORM, element_type=ContactFormWA)

    def close_confirmation_sending_message(self):
        self.search(self.structure.CLOSE_CONFIRMATION_SENDING_MESSAGE).assure(exist).assure(is_displayed).click()
        return VMRContactsPageStructure
