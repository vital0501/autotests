from module_projects.base.structure import BasePageStructure


class VMRContactsPageStructure(BasePageStructure):
    CONFIRM = "//h1[text()='Контактная информация']"
    FORM = "//*[@id='contact_form']"
    CLOSE_CONFIRMATION_SENDING_MESSAGE = "//*[@id='empty']//button/span"
