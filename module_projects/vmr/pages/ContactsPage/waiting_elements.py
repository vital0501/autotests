from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_conditions import text_not_none
from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseContactsWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class ErrorTextWA(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class ContactFormWA(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def click_submit(self):
        SubWaitingElement(self, "//button[@type='submit']").click()

    def set_name(self, name):
        self.get_name_input_element().clear()
        self.get_name_input_element().send_keys(name)

    def get_name_input_element(self):
        return SubWaitingElement(self, "//*[@id='name']")

    def set_email(self, email):
        self.get_email_input_element().clear()
        self.get_email_input_element().send_keys(email)

    def get_email_input_element(self):
        return SubWaitingElement(self, "//*[@id='mail']")

    def set_text(self, text):
        self.get_text_input_element().clear()
        self.get_text_input_element().send_keys(text)

    def get_text_input_element(self):
        return SubWaitingElement(self, "//*[@id='text']")
