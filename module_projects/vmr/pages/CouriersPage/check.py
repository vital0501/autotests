from module_projects.base.check import BaseCheck


class CouriersCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.CouriersPage.page.VMRCouriersPage
        """
        super().__init__(page)
        self.page = page
