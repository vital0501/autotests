from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class CouriersException(VMRBasePageException):
    pass
