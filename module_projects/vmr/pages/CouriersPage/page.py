from typing import Union

from selenium import webdriver

from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.CouriersPage.check import CouriersCheck
from module_projects.vmr.pages.CouriersPage.structure import VMRCouriersPageStructure


class VMRCouriersPage(VMRBasePage):
    page_name = 'VMRCouriers'

    @staticmethod
    def get_structure():
        return VMRCouriersPageStructure()

    def _get_check(self):
        """

        :rtype: CouriersCheck
        """
        return CouriersCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()
