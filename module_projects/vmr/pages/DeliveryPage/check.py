from module_projects.base.check import BaseCheck
from module_projects.vmr.pages.DeliveryPage.exception import DeliveryPageCityNotFound


class DeliveryCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.DeliveryPage.page.VMRDeliveryPage
        """
        super().__init__(page)
        self.page = page

    def check_city_eq(self, city, raise_error=False):
        page_city = self.page.get_city()
        result = page_city == city
        if not result and raise_error:
            raise DeliveryPageCityNotFound("Город не совпадает. Ожидаем: {0}, Видим: {1}" .format(city, page_city))
        return result