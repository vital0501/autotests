from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class DeliveryException(VMRBasePageException):
    pass


class DeliveryPageCityNotFound(VMRBasePageException):
    pass
