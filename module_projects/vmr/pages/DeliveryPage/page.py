from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_collection_conditions import len_more
from autotests_base.conditions.element_conditions import text_not_none, exist, is_not_displayed, is_displayed, text
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.DeliveryPage.check import DeliveryCheck
from module_projects.vmr.pages.DeliveryPage.exception import DeliveryPageCityNotFound
from module_projects.vmr.pages.DeliveryPage.structure import VMRDeliveryPageStructure
from module_projects.vmr.pages.DeliveryPage.waiting_elements import DeliveryItemsWaitingElement, PopUpDescriptionElement


class VMRDeliveryPage(VMRBasePage):
    page_name = 'VMRDelivery'

    @staticmethod
    def get_structure():
        return VMRDeliveryPageStructure()

    def _get_check(self):
        """

        :rtype: DeliveryCheck
        """
        return DeliveryCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_city_list(self):
        self.open_city_list_pop_up()
        result = [item.assure(text_not_none).text for item in self.get_city_list_elements()]
        self.close_city_list_pop_up()
        return result

    def open_city_list_pop_up(self):
        self.search(self.structure.CITY).assure(exist).assure(is_displayed).click()
        self.search(self.structure.CITY_CHOICE_POP_UP).assure(exist).assure(is_displayed)

    def close_city_list_pop_up(self):
        element = self.search(self.structure.CLOSE_CITY_POP_UP)
        element.click()
        element.assure(is_not_displayed)

    def get_city_list_elements(self):
        return self.search_all(self.structure.LOCATION_CITY_LIST)

    def get_delivery_items_elements(self):
        """

        :rtype: WaitingElementCollection[DeliveryItemsWaitingElement]
        """
        return self.search_all(self.structure.DELIVERY_ITEMS, elements_type=DeliveryItemsWaitingElement)

    def set_list_city(self, city):
        self.open_city_list_pop_up()
        elements = self.get_city_list_elements().assure(len_more, 0)
        for _city in elements:
            if _city.text == city:
                _city.click()
                break
        else:
            raise DeliveryPageCityNotFound("Не найден город \"{0}\"" .format(city))

        self.search(self.structure.CITY).assure(text, city, timeout=10)

        if self.get_pop_up().exist():
            if self.get_pop_up().is_displayed():
                self.close_pop_up()

    def get_pop_up(self):
        """

        :rtype: PopUpDescriptionElement
        """
        return self.search(self.structure.POP_UP, element_type=PopUpDescriptionElement)

    def close_pop_up(self):
        self.search(self.structure.CLOSE_POP_UP).click()

    def get_city(self):
        return self.search(self.structure.CITY).assure(exist, timeout=8).assure(is_displayed).text
