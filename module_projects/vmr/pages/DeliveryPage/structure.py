from module_projects.base.structure import BasePageStructure


class VMRDeliveryPageStructure(BasePageStructure):
    CONFIRM = "//h3[text()='Доставка']"

    CITY_CHOICE_POP_UP = "//*[@id='city']"
    LOCATION_CITY_LIST = "//div[contains(@class, 'city-list')]//li/a"
    CITY = "//*[@id='city_name']"
    CLOSE_CITY_POP_UP = "//*[@id='city']//button"

    DELIVERY_ITEMS = "//div[contains(@class, 'one')]"

    CLOSE_POP_UP = "//a[@title='Закрыть']"
    POP_UP = "//*[@class='fancybox-skin']"