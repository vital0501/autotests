from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseDeliveryWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class DeliveryItemsWaitingElement(BaseDeliveryWE):
    def get_name(self) -> str:
        return SubWaitingElement(self, "/*[@class='name']").text

    def click_details(self):
        SubWaitingElement(self, "/div[@class='dates']/a").click()

    def get_price(self):
        _text = SubWaitingElement(self, '//span[@class="price"]').text
        _text_split = _text.split(' ')
        _price_text = _text_split[0] if len(_text_split) == 2 else _text_split[1]
        return int(_price_text) if _price_text != 'бесплатно' else 0

    def get_date(self):
        _text = SubWaitingElement(self, '//span[@class="date"]').text
        _text_split = _text.split(' ')
        _date_text = _text_split[0] if len(_text_split) == 2 else _text_split[1]
        return int(_date_text)

    def get_type(self):
        return str(SubWaitingElement(self, '/..').get_attribute("data-key")).lower()


class PopUpDescriptionElement(BaseDeliveryWE):
    def get_name(self):
        return SubWaitingElement(self, '//h3').text