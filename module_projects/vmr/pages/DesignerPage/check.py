from module_projects.vmr.pages.BasePage.check import VMRBaseCheck, DataLayerCheck
from module_projects.vmr.pages.DesignerPage.exception import DesignerProductVendorCodeException, \
    DesignerPageProductsNotFound


class DesignerCheck(VMRBaseCheck, DataLayerCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.DesignerPage.page.VMRDesignerPage
        """
        super().__init__(page)
        self.page = page

    def check_products_length_gt(self, value, raise_error=False, timeout=10):
        collection = self.page.get_products_elements()
        return self.check_length_collection_gt(collection,
                                               value,
                                        'Продукты на странице дизайнера',
                                               raise_error,
                                               DesignerPageProductsNotFound,
                                               timeout)

    def check_vendor_code_is_int(self, raise_error=False):
        product = self.page.get_random_product_element()

        try:
            result = product.get_vendor_code() > 0
        except ValueError:
            raise DesignerProductVendorCodeException(
                "Артикул товара {0} в адресной строке '{1}' не является числом".format(product.get_name(),
                                                                                    product.get_href()))

        if raise_error and not result:
            raise DesignerProductVendorCodeException(
                "Некорректный артикул у товара {0} в адресной строке '{1}'".format(product.get_name(),
                                                                                    product.get_href()))
        else:
            return result
