from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class DesignerException(VMRBasePageException):
    pass


class DesignerPageProductsNotFound(DesignerException):
    pass


class DesignerProductVendorCodeException(DesignerException):
    pass
