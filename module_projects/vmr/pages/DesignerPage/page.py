import random
from typing import Union

from selenium import webdriver

from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.vmr.pages.CatalogPage.page import VMRCatalogPage
from module_projects.vmr.pages.DesignerPage.check import DesignerCheck
from module_projects.vmr.pages.DesignerPage.structure import VMRDesignerPageStructure
from module_projects.vmr.pages.DesignerPage.waiting_elements import DesignerProductWE


class VMRDesignerPage(VMRCatalogPage):
    page_name = 'VMRDesigner'

    @staticmethod
    def get_structure():
        return VMRDesignerPageStructure()

    def _get_check(self):
        """

        :rtype: DesignerCheck
        """
        return DesignerCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, url=None, timeout=10, confirm=True, init=False, designer_name=None):
        url = "{0}/designer/{1}" .format(self.norm_href(project_url), designer_name) if designer_name is not None else url
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)
        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_products_elements(self):
        """

        :rtype: WaitingElementCollection[DesignerProductWE]
        """
        return self.search_all(self.structure.PRODUCTS, elements_type=DesignerProductWE)

    def get_product_element_by_index(self, index) -> DesignerProductWE:
        return self.get_products_elements()[index]

    def go_to_product_by_index(self, index):
        """

        :rtype: module_projects.vmr.pages.ProductPage.page.VMRProductPage
        """
        from module_projects.vmr.pages.ProductPage.page import VMRProductPage
        self.get_products_elements()[index].click()
        return self.create_page_object(VMRProductPage)

    def go_to_product_by_element(self, element: DesignerProductWE):
        """

        :rtype: module_projects.vmr.pages.ProductPage.page.VMRProductPage
        """
        from module_projects.vmr.pages.ProductPage.page import VMRProductPage
        element.click()
        return self.create_page_object(VMRProductPage)

    def go_to_random_product(self):
        return self.go_to_product_by_element(self.get_random_product_element())

    def get_random_product_element(self) -> DesignerProductWE:
        return random.choice(self.get_products_elements())
