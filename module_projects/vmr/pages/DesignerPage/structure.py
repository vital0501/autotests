from module_projects.vmr.pages.CatalogPage.structure import VMRCatalogPageStructure


class VMRDesignerPageStructure(VMRCatalogPageStructure):

    NAME = "//*[@class='nickname']/a"
    CONFIRM = NAME
    PRODUCTS = '//*[@data-autotest-id="product"]'
