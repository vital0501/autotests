from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseDesignerWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class DesignerProductWE(BaseDesignerWE):
    def get_vendor_code(self):
        return int(self.get_href().split('/')[-1].split('?')[0])

    def get_name(self):
        return SubWaitingElement(self, '//*[contains(@class, "list-item__name")]/span[2]').text

    def get_href(self):
        return SubWaitingElement(self, '//div[contains(@class,"holder")]/a').get_attribute('href')

    def get_product_type(self):
        return SubWaitingElement(self, '/div/a').get_attribute('href').split('/')[4]