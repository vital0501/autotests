from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_collection_conditions import len_equal
from module_projects.base.check import BaseCheck
from module_projects.vmr.pages.FavoritesPage.exception import FavoritesEmptyTextException, \
    FavoritesCoutProductsException, FavoritesNameProductsException


class FavoritesCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.FavoritesPage.page.VMRFavoritesPage
        """
        super().__init__(page)
        self.page = page

    def check_is_empty_text_favorites(self, value, raise_error=False):
        try:
            result = value == self.page.get_favorites_emty_text_element()
        except TimeoutException:
            result = False
        if not result and raise_error:
            raise FavoritesEmptyTextException("Некорректное собщение пустого избранного. Ожидаем: {0}. Видим: {1}"
                                                .format(value, self.page.get_favorites_emty_text_element()))
        return result

    def check_is_product_favorites_count(self, product_add, products_favorits, raise_error=False):

        try:
            result = products_favorits.assure(len_equal, len(product_add))
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise FavoritesCoutProductsException("Некорректное количество избранного. Ожидаем: '{0}'. Видим: '{1}'".format(
                    len(product_add), len(products_favorits)))
        return result

    def check_is_product_favorites_name(self, product_add, products_favorits, raise_error=False):
        result = True
        for i in range(0, len(products_favorits)):
            if products_favorits[i].get_name().replace('«', '').replace('»', '') != product_add[i].replace(
                    '«', '').replace('»', ''):
                result = False

        if not result and raise_error:
            raise FavoritesNameProductsException(("Некорректное имя товара. Ожидаем: '{0}'. Видим: '{1}'"
                                                  .format(product_add[i], products_favorits[i].get_name())))
        return result