from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class FavoritesException(VMRBasePageException):
    pass

class FavoritesEmptyTextException(VMRBasePageException):
    pass

class FavoritesCoutProductsException(VMRBasePageException):
    pass

class FavoritesNameProductsException(VMRBasePageException):
    pass