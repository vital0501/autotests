from typing import Union

from selenium import webdriver
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection

from autotests_base.conditions.element_conditions import exist, is_displayed
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.FavoritesPage.check import FavoritesCheck
from module_projects.vmr.pages.FavoritesPage.structure import VMRFavoritesPageStructure
from module_projects.vmr.pages.FavoritesPage.waiting_elements import FavoritesProductElement


class VMRFavoritesPage(VMRBasePage):
    page_name = 'VMRFavorites'

    @staticmethod
    def get_structure():
        return VMRFavoritesPageStructure()

    def _get_check(self):
        """

        :rtype: FavoritesCheck
        """
        return FavoritesCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_favorites_emty_text_element(self):
        return self.search(self.structure.EMPTY_MESSAGE).assure(exist).assure(is_displayed).text

    def get_products_elements(self) -> WaitingElementCollection:
        return self.search_all(self.structure.PRODUCTS, elements_type=FavoritesProductElement)

    def delete_product_from_favorites(self, index):
        product = self.get_products_elements()[index]
        product.mouse_over_element()
        product.delete_from_favorite()