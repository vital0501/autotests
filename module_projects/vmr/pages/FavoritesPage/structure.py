from module_projects.base.structure import BasePageStructure


class VMRFavoritesPageStructure(BasePageStructure):
    CONFIRM = "//h3[text()='Избранное']"
    EMPTY_MESSAGE = "//*[@id='favorites-root']//p"
    PRODUCTS = "//*[@class='favorites-list']/div"