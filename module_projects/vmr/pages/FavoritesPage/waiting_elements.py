from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseFavoritesWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

class FavoritesProductElement(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_name(self) -> str:
        return SubWaitingElement(self, "//a/b").text

    def delete_from_favorite(self):
        SubWaitingElement(self, "//*[@title='Удалить из избранного']").click()