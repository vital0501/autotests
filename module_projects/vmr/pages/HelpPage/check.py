from module_projects.base.check import BaseCheck


class HelpCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.HelpPage.page.VMRHelpPage
        """
        super().__init__(page)
        self.page = page
