from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class HelpException(VMRBasePageException):
    pass

class HelpNotInfoException(VMRBasePageException):
    pass