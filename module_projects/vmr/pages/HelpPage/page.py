from typing import Union

from selenium import webdriver
from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_conditions import exist
from core.exception import InformationBlockNotFound, NotFoundMenuItemException
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.HelpPage.check import HelpCheck
from module_projects.vmr.pages.HelpPage.structure import VMRHelpPageStructure


class VMRHelpPage(VMRBasePage):
    page_name = 'VMRHelp'

    @staticmethod
    def get_structure():
        return VMRHelpPageStructure()

    def _get_check(self):
        """

        :rtype: HelpCheck
        """
        return HelpCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def is_help_link_open(self, link_name):
        link = self.get_help_link_by_name(link_name)

        if "Как купить?" in link.text:
            return self._is_open(self.structure.HOW_TO_BAY)
        elif "Обмен и возврат" in link.text:
            return self._is_open(self.structure.EXCHANGE)
        elif "Как создать товар с индивидуальным дизайном?" in link.text:
            return self._is_open(self.structure.DESIGN)
        elif "Часто задаваемые вопросы" in link.text:
            return self._is_open(self.structure.FAQ)
        elif "Технологии печати" in link.text:
            return self._is_open(self.structure.TECHNOLOGIES)

    def get_help_link_by_name(self, link_name):
        for item in self.get_help_links():
            if link_name in item.text:
                return item
        else:
            raise NotFoundMenuItemException

    def _is_open(self, main_path,):
        try:
            item = self.search(main_path + self.structure.DISPLAY_BLOCK)
            if item.exist():
                return True
        except TimeoutException:
            try:
                item = self.search(main_path + self.structure.DISPLAY_NONE).assure(exist)
                if item.exist():
                    return False
            except TimeoutException:
                try:
                    item = self.search(main_path + self.structure.DISPLAY_WITH_OUT_STYLE).assure(exist)
                    if item.exist():
                        return False
                except TimeoutException:
                    raise InformationBlockNotFound

    def get_help_links(self):
        return self.search_all(self.structure.HELP_LINKS)

    def open_help_link_by_name(self, link_name):
        self.get_help_link_by_name(link_name=link_name).assure(exist).click()