from module_projects.base.structure import BasePageStructure


class VMRHelpPageStructure(BasePageStructure):
    CONFIRM = "//h1[text()='Помощь']"

    DISPLAY_BLOCK = "[ancestor::div[@style='display: block;']]"
    DISPLAY_NONE = "[ancestor::div[@style='display: none;']]"
    DISPLAY_WITH_OUT_STYLE = "[ancestor::div[not(@style)][@class=\"slide\"]]"

    HOW_TO_BAY = "//p[contains(text(), 'Купить товар')]"
    EXCHANGE = "//p[contains(text(), 'Мы приносим свои извинения и готовы обменять вам товар или вернуть деньги.')]"
    DESIGN = "//p[contains(text(), 'хотите')]"
    FAQ = "//p[contains(text(), 'Возврат денежных')]"
    TECHNOLOGIES = "//p[contains(text(), 'Каждый предмет')]"

    HELP_LINKS = '//h5[ancestor::div[@class="info-holder"]]/span'
