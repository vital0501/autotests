from module_projects.base.check import BaseCheck


class HowCreateOrderCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.HowCreateOrderPage.page.VMRHowCreateOrderPage
        """
        super().__init__(page)
        self.page = page
