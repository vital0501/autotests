from typing import Union

from selenium import webdriver

from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.HowCreateOrderPage.check import HowCreateOrderCheck
from module_projects.vmr.pages.HowCreateOrderPage.structure import VMRHowCreateOrderPageStructure


class VMRHowCreateOrderPage(VMRBasePage):
    page_name = 'VMRHowCreateOrder'

    @staticmethod
    def get_structure():
        return VMRHowCreateOrderPageStructure()

    def _get_check(self):
        """

        :rtype: HowCreateOrderCheck
        """
        return HowCreateOrderCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()
