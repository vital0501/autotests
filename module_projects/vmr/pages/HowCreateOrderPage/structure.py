from module_projects.base.structure import BasePageStructure


class VMRHowCreateOrderPageStructure(BasePageStructure):
    CONFIRM = "//span[text()='Как сделать заказ?']"
