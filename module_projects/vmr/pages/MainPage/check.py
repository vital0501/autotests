from selenium.common.exceptions import TimeoutException

from module_projects.vmr.pages.CatalogPage.check import CatalogCheck
from module_projects.vmr.pages.MainPage.exception import VMRMainPageDataLayerSectionException, \
    VMRMainPageLengthSectionException, VMRMainPageSqlIjectionException, VMRMainPageNewsCountException


class MainCheck(CatalogCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.MainPage.page.VMRMainPage
        """
        super().__init__(page)
        self.page = page

    def check_data_layer_length_impressions_eq(self, value, data=None, dict_name='Impressions', error_class=None, raise_error=False):
        error_class = error_class if error_class else VMRMainPageLengthSectionException
        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        super().check_data_layer_length_impressions_eq(value, data, dict_name, error_class, raise_error)

    def check_data_layer_impressions_data_with_page_eq(self, value, data=None, obj_2_name="", parameter=None, error_class=None, raise_error=False):
        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        error_class = VMRMainPageDataLayerSectionException
        super().check_data_layer_impressions_data_with_page_eq(value, data, "Данные страницы", 'name', error_class, True)

    def check_page_data_impressions_with_data_layer_eq(self, value, data=None, obj_2_name='', parameter=None, error_class=None, raise_error=False):
        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        error_class = VMRMainPageDataLayerSectionException
        super().check_data_layer_impressions_data_with_page_eq(value, data, "DataLayer", 'name', error_class, True)

    def check_page_with_sql_inject(self, _page, raise_error=False):
        if _page.is_alert_present():
            _page.get_alert().accept()
            result = False
        else:
            result = True

        if not result and raise_error:
            raise VMRMainPageSqlIjectionException('SQL инъекции проходят!')
        return result

    def check_count_news_eg(self, value, raise_error=False):
        try:
            result = len(self.page.get_all_news_elements()) == value
            text = len(self.page.get_all_news_elements())
        except TimeoutException:
            text = 'Не появились новости на главной странице'
            result = False
        if not result and raise_error:
            raise VMRMainPageNewsCountException("Количество новостей не совпадает с ожидаемым. Ожидаем: {0}. Видим: {1}"
                      .format(value, text))
        return result
