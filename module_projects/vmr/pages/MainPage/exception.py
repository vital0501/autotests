from module_projects.vmr.pages.CatalogPage.exception import VMRCatalogDataLayerSectionException


class VMRMainPageBaseException(Exception):
    pass


class VMRMainPageDataLayerSectionException(VMRCatalogDataLayerSectionException, VMRMainPageBaseException):
    pass


class VMRMainPageDataLayerSectionExistException(VMRCatalogDataLayerSectionException, VMRMainPageBaseException):
    pass


class VMRMainPageLengthSectionException(VMRCatalogDataLayerSectionException, VMRMainPageBaseException):
    pass


class VMRMainPageSqlIjectionException(VMRMainPageBaseException):
    pass


class VMRMainPageRedirectException(VMRMainPageBaseException):
    pass


class VMRMainPageNewsCountException(VMRMainPageBaseException):
    pass