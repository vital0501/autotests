from typing import Union


from selenium import webdriver

from autotests_base.conditions.element_conditions import exist, is_displayed
from module_projects.vmr.pages.CatalogPage.page import VMRCatalogPage
from module_projects.vmr.pages.MainPage.check import MainCheck
from module_projects.vmr.pages.MainPage.structure import VMRMainPageStructure


class VMRMainPage(VMRCatalogPage):
    page_name = 'MainPage'

    @staticmethod
    def get_structure() -> VMRMainPageStructure:
        return VMRMainPageStructure()

    def _get_check(self):
        """

        :rtype: MainCheck
        """
        return MainCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome],
                 project_url, timeout, confirm, init, url=None):
        url = url if url else project_url
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        version_pages = ["https://{0}.vsemayki.ru".format(item) for item in ['a', 'b', 'c']]
        self.project_url = "http://www.vsemayki.ru" if self.norm_href(project_url) in version_pages else project_url

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def go_to_constructor(self):
        """

        :rtype: module_projects.vmr.pages.ConstructorPage.page.VMRConstructorPage
        """
        self.get_constructor_element().click()
        from module_projects.vmr.pages.ConstructorPage.page import VMRConstructorPage
        return self.create_page_object(VMRConstructorPage)

    def get_constructor_element(self):
        return self.search(self.structure.CONSTRUCTOR)

    def go_to_reviews_page(self):
        """

        :rtype: module_projects.vmr.pages.ReviewsPage.page.ReviewsPage
        """
        self.search(self.structure.REVIEWS_BUTTON).assure(exist).assure(is_displayed).click()
        from module_projects.vmr.pages.ReviewsPage.page import VMRReviewsPage
        return self.create_page_object(VMRReviewsPage)
