from module_projects.vmr.pages.CatalogPage.structure import VMRCatalogPageStructure


class VMRMainPageStructure(VMRCatalogPageStructure):
    CONFIRM = "//*[@id='autotest_confirm']"

    CONSTRUCTOR = "//*[@data-autotest-id='constructor']"
    REVIEWS_BUTTON = '//*[@class="allreviews"]//*[@href="/reviews"]'
