from module_projects.base.check import BaseCheck


class ManufactureCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.ManufacturePage.page.VMRManufacturePage
        """
        super().__init__(page)
        self.page = page
