from typing import Union

from selenium import webdriver

from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.ManufacturePage.check import ManufactureCheck
from module_projects.vmr.pages.ManufacturePage.structure import VMRManufacturePageStructure


class VMRManufacturePage(VMRBasePage):
    page_name = 'VMRManufacture'

    @staticmethod
    def get_structure():
        return VMRManufacturePageStructure()

    def _get_check(self):
        """

        :rtype: ManufactureCheck
        """
        return ManufactureCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()
