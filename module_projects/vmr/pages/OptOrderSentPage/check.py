from module_projects.base.check import BaseCheck


class OptOrderSentCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.OptOrderSentPage.page.VMROptOrderSentPage
        """
        super().__init__(page)
        self.page = page
