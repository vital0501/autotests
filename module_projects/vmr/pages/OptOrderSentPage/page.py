from typing import Union

from selenium import webdriver

from module_projects.base.page import BasePage
from module_projects.vmr.pages.OptOrderSentPage.check import OptOrderSentCheck
from module_projects.vmr.pages.OptOrderSentPage.structure import VMROptOrderSentPageStructure


class VMROptOrderSentPage(BasePage):
    page_name = 'VMROptOrderSentPage'

    @staticmethod
    def get_structure():
        return VMROptOrderSentPageStructure()

    def _get_check(self):
        """

        :rtype: OptOrderSentCheck
        """
        return OptOrderSentCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()
