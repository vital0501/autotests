from module_projects.base.structure import BasePageStructure


class VMROptOrderSentPageStructure(BasePageStructure):
    CONFIRM = '//*[@class="order-sent"]'
