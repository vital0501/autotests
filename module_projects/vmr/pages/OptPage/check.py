from module_projects.base.check import BaseCheck
from module_projects.vmr.pages.OptPage.exception import PageErrorTextExecption


class OptCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.OptPage.page.VMROptPage
        """
        super().__init__(page)
        self.page = page

    @staticmethod
    def check_value_text_error(expected_text, page_error_text, raise_error=False):
        result = expected_text == page_error_text
        if not result and raise_error:
            raise PageErrorTextExecption('Нет или некорректное значение "{}"'.format(expected_text))
        return result
