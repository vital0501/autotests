from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class OptException(VMRBasePageException):
    pass

class PageErrorTextExecption(VMRBasePageException):
    pass