from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import is_displayed, exist, text
from autotests_base.conditions.element_conditions import text_not_none

from module_projects.base.page import BasePage
from module_projects.vmr.pages.OptPage.check import OptCheck
from module_projects.vmr.pages.OptPage.structure import VMROptPageStructure
from module_projects.vmr.pages.OptOrderSentPage.page import VMROptOrderSentPage


class VMROptPage(BasePage):
    page_name = 'VMROpt'

    @staticmethod
    def get_structure():
        return VMROptPageStructure()

    def _get_check(self):
        """

        :rtype: OptCheck
        """
        return OptCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome],
                 url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def send_question(self, phone, name, text, go_OptOrderSent=False):
        self.get_question_phone_input_element().assure(exist).assure(is_displayed).clear()
        self.get_question_name_input_element().assure(exist).assure(is_displayed).clear()
        self.get_question_text_input_element().assure(exist).assure(is_displayed).clear()

        if phone:
            self.get_question_phone_input_element().assure(exist).assure(is_displayed).click()
            self.get_question_phone_input_element().assure(exist).assure(is_displayed).send_keys(phone)
        if name:
            self.get_question_name_input_element().assure(exist).assure(is_displayed).send_keys(name)
        if text:
            self.get_question_text_input_element().assure(exist).assure(is_displayed).send_keys(text)

        self.get_send_question_button_element().assure(exist).assure(is_displayed).click()

        if go_OptOrderSent:
            return self.create_page_object(VMROptOrderSentPage)

    def get_question_phone_error_text_element(self):
        return self.search(self.structure.QUESTION_PHONE_ERROR).assure(text_not_none).text

    def get_question_name_error_text_element(self):
        return self.search(self.structure.QUESTION_NAME_ERROR).assure(text_not_none).text

    def get_question_text_error_text_element(self):
        return self.search(self.structure.QUESTION_TEXT_ERROR).assure(text_not_none).text

    def send_request(self, phone, name, go_OptOrderSent=False):
        self.get_phone_input_element().assure(exist).assure(is_displayed).clear()
        self.get_name_input_element().assure(exist).assure(is_displayed).clear()

        if phone:
            self.get_phone_input_element().assure(exist).assure(is_displayed).click()
            for item in phone:
                self.get_phone_input_element().assure(exist).assure(is_displayed).send_keys(item)
        if name:
            self.get_name_input_element().assure(exist).assure(is_displayed).send_keys(name)

        self.get_send_request_button_element().assure(exist).assure(is_displayed).click()

        if go_OptOrderSent:
            return self.create_page_object(VMROptOrderSentPage)

    def get_phone_input_element(self):
        return self.search(self.structure.PHONE)

    def get_name_input_element(self):
        return self.search(self.structure.NAME)

    def get_send_request_button_element(self):
        return self.search(self.structure.SEND_REQUEST_BUTTON)

    def get_phone_error_text_element(self):
        element = self.get_phone_error_element().assure(exist).assure(is_displayed)
        return element.text

    def get_name_error_text_element(self):
        element = self.get_name_error_element().assure(exist).assure(is_displayed)
        return element.text

    def get_phone_error_element(self):
        return self.search(self.structure.PHONE_ERROR)

    def get_name_error_element(self):
        return self.search(self.structure.NAME_ERROR)

    def get_question_phone_input_element(self):
        return self.search(self.structure.QUESTION_PHONE)

    def get_question_name_input_element(self):
        return self.search(self.structure.QUESTION_NAME)

    def get_question_text_input_element(self):
        return self.search(self.structure.QUESTION_TEXT)

    def get_send_question_button_element(self):
        return self.search(self.structure.QUESTION_BUTTON)