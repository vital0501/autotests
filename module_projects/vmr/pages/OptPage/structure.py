from module_projects.base.structure import BasePageStructure


class VMROptPageStructure(BasePageStructure):
    CONFIRM = '//*[@id="top"]//a/img'

    PHONE = "//*[@id='fullprint_banner_phone']"
    NAME = "//*[@id='fullprint_banner_name']"
    SEND_REQUEST_BUTTON = '//*[@id="form_submit"]'
    PHONE_ERROR = "//*[descendant::*[@id='fullprint_banner_phone']]/span"
    NAME_ERROR = "//*[descendant::*[@id='fullprint_banner_name']]/span"

    QUESTION_PHONE = "//*[@id='question_form_phone']"
    QUESTION_NAME = "//*[@id='question_form_name']"
    QUESTION_TEXT = "//*[@id='question_form_message']"
    QUESTION_BUTTON = "//*[@id='question_section_form_submit']"
    QUESTION_PHONE_ERROR = '//*[@id="question_section_form"]//label[@for="question_form_phone"]/span'
    QUESTION_NAME_ERROR = '//*[@id="question_section_form"]//label[@for="question_form_name"]/span'
    QUESTION_TEXT_ERROR = '//*[@id="question_section_form"]//label[@for="question_form_message"]/span'