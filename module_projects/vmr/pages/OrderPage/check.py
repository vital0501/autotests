import time

from selenium.common.exceptions import TimeoutException

from module_projects.vmr.pages.BasePage.check import VMRBaseCheck, DataLayerCheck
from module_projects.vmr.pages.BasePage.exception import DeliveryNotEquals
from module_projects.vmr.pages.OrderPage.exception import OrderPageTotalDeliveryPriceException, \
    OrderPageTotalPriceException, OrderNotFoundRussianSize, OrderDeliveryNotFoundException, \
    OrderPageNotPriceDeliveryException, OrderPagePaymentMetodException


class OrderCheck(VMRBaseCheck, DataLayerCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.OrderPage.page.VMROrderPage
        """
        super().__init__(page)
        self.page = page

    def check_order_delivery_price_eq(self, value, timeout=4, raise_error=False):
        for item in range(0, timeout * 5):
            result = value == self.page.get_total_delivery_price()
            if result:
                break
            time.sleep(0.2)
        else:
            result = False

        if not result and raise_error:
            raise OrderPageTotalDeliveryPriceException("Не совпадают стоимость доставки. Ожидаем: {0}, Видим: {1}"
                                                       .format(value, self.page.get_total_delivery_price()))
        return result

    def check_order_price_eq(self, value, timeout=4, raise_error=False):
        for item in range(0, timeout * 5):
            result = value == self.page.get_total_amount_price()
            if result:
                break
            time.sleep(0.2)
        else:
            result = False

        if not result and raise_error:
            self.page.get_total_price_element().scroll_to_element()
            raise OrderPageTotalPriceException("Некорректная общая стоимость заказа. Ожидаем: {0}, Видим: {1}"
                                               .format(value, self.page.get_total_amount_price()))
        return result

    @staticmethod
    def check_delivery_city_list(page_dict: dict, order_dict: dict, raise_error=False):
        errors = []

        if page_dict == order_dict:
            return True

        # Проверяем что совпадают города города.
        _del_city = {'expect': [], 'actual': []}

        for _city in page_dict.keys():
            if _city not in order_dict.keys():
                errors.append("На странице заказа отстутствует город \"{0}\"".format(_city))
                _del_city['expect'].append(_city)

        for _city in order_dict.keys():
            if _city not in page_dict.keys():
                errors.append("На странице доставок отстутствует город \"{0}\"".format(_city))
                _del_city['actual'].append(_city)

        # Удаляем ненужные города из результатов
        for item in _del_city.keys():
            if item == 'expect':
                for _city in _del_city['expect']:
                    page_dict.pop(_city, None)
            else:
                for _city in _del_city['actual']:
                    order_dict.pop(_city, None)

        # Проверяем типы доставок
        _del_type = {'expect': {}, 'actual': {}}

        for _city in order_dict.keys():
            for _type in order_dict[_city]:
                if _type not in page_dict[_city].keys():
                    errors.append("В городе \"{0}\" на странице доставок отсутствует тип доставок \"{1}\"".format(
                        _city, _type
                    ))
                    if _city not in _del_type['actual'].keys():
                        _del_type['actual'][_city] = []
                    _del_type['actual'][_city].append(_type)

        # Удаляем ненужные типы из результатов
        for item in _del_type.keys():
            if item == 'expect':
                for _city in _del_type[item].keys():
                    for _type in _del_type[item][_city]:
                        page_dict[_city].pop(_type, None)
            else:
                for _city in _del_type[item].keys():
                    for _type in _del_type[item][_city]:
                        order_dict[_city].pop(_type, None)

        # Проверяем имена доставок
        for _city in order_dict.keys():
            for _type in order_dict[_city]:
                for _data in order_dict[_city][_type]:
                    if _data not in page_dict[_city][_type]:
                        errors.append("В городе \"{0}\" на странице доставок отсутствует доставка \"{1}\", "
                                      "с ценой \"{2}\" и временем доставки \"{3}\""
                                      .format(_city, _data['name'], _data['price'], _data['date']))

        if errors and raise_error:
            raise DeliveryNotEquals("\nНе совпадение доставок:\n\t" + "\n\t".join(errors))

    def check_len_russian_size_gte(self, len_size, raise_error=False):
        try:
            result = len(self.page.get_size_element().split('(')[1].strip().split(')')[0].strip()) >= len_size
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise OrderNotFoundRussianSize("Некорректный русский размер")

    def check_len_delivery_element_eg(self, len_size, raise_error=False):
        try:
            result = len(self.page.get_delivery_block_list_elements()) == len_size
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise OrderDeliveryNotFoundException("Некорректное количество блоков доставки")

    def check_name_delivery_block_eg(self, name, raise_error=False):
        try:
            result = self.page.get_delivery_block_list_elements()[0].get_name() == name
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise OrderDeliveryNotFoundException("Некорректное имя блока доставки. Ожидаем: \"Почта России\"")

    def check_name_delivery_element_eg(self, name, raise_error=False):
        try:
            result = self.page.get_delivery_block_list_elements()[0].get_delivery_methods()[0].get_name() == name
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise OrderDeliveryNotFoundException("Некорректное имя доставки. Ожидаем: \"Почта России 1 класса\"")

    def check_delivery_price(self, value, raise_error=False):
        try:
            result = (self.page.get_delivery_block_list_elements()[0].get_delivery_methods()[0].get_price()) > value
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise OrderPageNotPriceDeliveryException(
                "Некорректная цена для \"{0}\"".format(
                    self.page.get_delivery_block_list_elements()[0].get_delivery_methods()[0].get_name()))

    def check_name_payment_element_eg(self, name, raise_error=False):
        try:
            result = self.page.get_all_payment_methods_elements()[0].get_name() == name
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise OrderPagePaymentMetodException("Некорректный вариант оплаты. \"Ожидаем: Оплата онлайн:\"")
