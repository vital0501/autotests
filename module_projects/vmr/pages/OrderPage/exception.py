from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class OrderException(VMRBasePageException):
    pass


class OrderDeliveryLoaderException(OrderException):
    pass


class OrderPagePopUpCityNotFoundException(OrderException):
    pass


class OrderNotFoundAutocompleteCity(OrderException):
    pass


class OrderDeliveryLoaderException(OrderException):
    pass


class OrderDeliveryNotFoundException(OrderException):
    pass


class OrderSetCourierDeliveryException(OrderException):
    pass


class OrderEmptyCartException(OrderException):
    pass


class OrderCourierDeliveryNotFoundException(OrderException):
    pass


class OrderPageDataLayerSectionExistException(OrderException):
    pass


class OrderNotFoundRussianSize(OrderException):
    pass


class OrderPageDataLayerSectionException(OrderException):
    pass


class OrderDataLayerException(OrderException):
    pass


class OrderPageTotalDeliveryPriceException(OrderException):
    pass


class OrderPageTotalPriceException(OrderException):
    pass


class OrderPageNotPriceDeliveryException(OrderException):
    pass


class OrderPagePaymentMetodException(OrderException):
    pass