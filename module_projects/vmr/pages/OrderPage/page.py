from typing import Union

from selenium import webdriver
from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_collection_conditions import len_more, len_gte
from autotests_base.conditions.element_conditions import is_displayed, exist, is_not_displayed, click
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.OrderPage.check import OrderCheck
from module_projects.vmr.pages.OrderPage.exception import OrderPagePopUpCityNotFoundException, \
    OrderDeliveryNotFoundException, OrderSetCourierDeliveryException, OrderNotFoundAutocompleteCity, \
    OrderCourierDeliveryNotFoundException, OrderDeliveryLoaderException
from module_projects.vmr.pages.OrderPage.structure import VMROrderPageStructure
from module_projects.vmr.pages.OrderPage.waiting_elements import DeliveryBlockWA
from module_projects.vmr.pages.OrderPage.waiting_elements import CityPopUpWE, OrderDeliveryMethodsWE, OrderPaymentMethodsWE


class VMROrderPage(VMRBasePage):
    page_name = 'VMROrder'

    @staticmethod
    def get_structure():
        return VMROrderPageStructure()

    def _get_check(self):
        """

        :rtype: OrderCheck
        """
        return OrderCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome],
                 url, project_url,
                 timeout=10,
                 confirm=True,
                 init=False):
        url = 'cart/delivery' if url is None else url
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def set_data(self, email=None, phone=None, name=None, comment=None, city=None, region=None, address=None):
        if phone:
            self.set_phone(phone)
        if email:
            self.set_email(email)
        if name:
            self.set_name(name)
        if comment:
            self.set_comment(comment)
        if address:
            self.set_address(address)
        if city:
            self.set_city(city, region)

    def _get_pop_up_city_elements(self):
        """

        :rtype: WaitingElementCollection[CityPopUpWE]
        """
        elements = self.search_all(self.structure.CITY_POP_UP_ELEMENTS, elements_type=CityPopUpWE)
        try:
            elements.assure(len_more, 0)
        except TimeoutException:
            raise OrderNotFoundAutocompleteCity("Не найдены всплывающие подсказки для городов.")
        return elements

    def set_city(self, city, region=None):
        # Вводим город
        self.input_standard_field(self.structure.CITY, city)

        # Ждем всплывающие элементы
        city_elements = self._get_pop_up_city_elements()

        # Определяем что элементы показались
        try:
            city_elements[0].assure(is_displayed)
        except TimeoutException:
            raise OrderPagePopUpCityNotFoundException('Не показались всплывающие подсказки городов')

        for item in city_elements:
            if item.get_city() == city:
                if region:
                    if region == item.get_region():
                        item.click()
                        break
                    else:
                        continue
                item.click()
                break
        else:
            err_text = "Не появился город \"{0}\"".format(city)
            raise OrderNotFoundAutocompleteCity(err_text + " регионе \"{0}\"" .format(region)) if region else err_text

    def set_email(self, value):
        self.input_standard_field(xpath=self.structure.EMAIL, value=value)

    def set_comment(self, value):
        self.input_standard_field(xpath=self.structure.COMMENT, value=value)

    def set_phone(self, value):
        element = self.search(self.structure.PHONE)
        element.click()
        element.send_keys(value)

    def set_name(self, value):
        self.input_standard_field(xpath=self.structure.NAME, value=value)

    def set_address(self, value):
        self.input_standard_field(xpath=self.structure.ADDRESS, value=value)

    def get_re_loader_delivery_spinner(self):
        return self.search(self.structure.RE_DELIVERY_LOADER)

    def get_all_delivery_elements(self, num_gte=0, timeout=10):
        """

        :rtype: WaitingElementCollection[OrderDeliveryMethodsWE]
        """
        elements = self.search_all(self.structure.ALL_DELIVERY, elements_type=OrderDeliveryMethodsWE)

        try:
            self.get_re_loader_delivery_spinner().assure(is_not_displayed)
        except TimeoutException:
            raise OrderDeliveryLoaderException("Долгая загрузка лодера")

        try:
            self.get_delivery_spinner_element().assure(is_displayed)
        except TimeoutException:
            pass

        try:
            self.get_delivery_spinner_element().assure(exist).assure(is_not_displayed, timeout=10)
        except TimeoutException:
            raise OrderDeliveryLoaderException("Долгая загрузка лодера при получении доставок")

        try:
            return self.search_all(self.structure.ALL_DELIVERY, elements_type=OrderDeliveryMethodsWE)\
                .assure(len_gte, num_gte, timeout=timeout)
        except TimeoutException:
            raise OrderDeliveryNotFoundException("Не найдены или некорректеные доставки")

    def get_all_courier_delivery_elements(self, num_gte=1, timeout=10):
        """

        :rtype: WaitingElementCollection[OrderDeliveryMethodsWE]
        """
        try:
            return self.search_all(self.structure.DELIVERY_COURIER, elements_type=OrderDeliveryMethodsWE)\
                .assure(len_gte, num_gte, timeout=timeout)
        except TimeoutException:
            raise OrderCourierDeliveryNotFoundException("Не найдены курьерские доставки")

    def set_courier_delivery_data(self, trying: int = 10, index=0) -> bool:
        if trying > 0:
            try:
                self.get_all_courier_delivery_elements(num_gte=1, timeout=10)[index].scroll_to_element()
                self.get_all_courier_delivery_elements(num_gte=1, timeout=10)[index].click()
                self.search(self.structure.DELIVERY_COURIER + "[@class='type active']").assure(exist)
            except TimeoutException:
                return self.set_courier_delivery_data(trying=trying - 1)
        else:
            raise OrderSetCourierDeliveryException("Не смогли установить доставку курьером")

    def go_to_create_order(self):
        """

        :rtype: module_projects.vmr.pages.SuccessfulOrderPage.page.VMRSuccessfulOrderPage
        """
        self.search(self.structure.CREATE_ORDER).assure(exist).assure(is_displayed).assure(click)

        from module_projects.vmr.pages.SuccessfulOrderPage.page import VMRSuccessfulOrderPage
        return self.create_page_object(VMRSuccessfulOrderPage)
    
    def get_total_delivery_price(self):
        if not self.get_total_delivery_placeholder_element().exist():
            return int(self.get_total_delivery_price_element().text.split(' ')[0])
        return None

    def get_total_delivery_price_element(self):
        return self.search(self.structure.TOTAL_DELIVERY_PRICE)

    def get_total_delivery_placeholder_element(self):
        return self.search(self.structure.TOTAL_DELIVERY_PRICE_PLACEHOLDER)

    def is_delivery_set(self):
        return not self.get_total_delivery_placeholder_element().exist()

    def get_total_amount_price(self):
        return int(self.get_total_amount_price_element().text.split(' ')[0])

    def get_total_amount_price_element(self):
        return self.search(self.structure.TOTAL_AMOUNT_PRICE)
    
    def get_total_price(self):
        return int(self.get_total_price_element().text.split(' ')[0])

    def get_total_price_element(self):
        return self.search(self.structure.TOTAL_PRICE)

    def get_city_spinner_element(self):
        return self.search(self.structure.CITY_SPINNER)

    def get_all_payment_methods_elements(self):
        """

        :rtype: WaitingElementCollection[OrderPaymentMethodsWE]
        """
        return self.search_all(self.structure.ALL_PAYMENT, elements_type=OrderPaymentMethodsWE)

    def get_size_element(self):
        return self.search(self.structure.SIZE).assure(exist).assure(is_displayed).text

    def set_country(self, country):
        for item in self.get_countrys_list_elements():
            if item.text == country:
                item.assure(exist).assure(is_displayed).click()
                break

    def get_countrys_list_elements(self):
        return self.search_all(self.structure.COUNTRYS_LIST)

    def set_sng_city(self, city):
        self._get_city_element().clear()
        self._get_city_element().send_keys(city)

    def _get_city_element(self):
        return self.search(self.structure.CITY).assure(exist).assure(is_displayed)

    def get_delivery_block_list_elements(self):
        """

        :rtype: WaitingElementCollection[DeliveryBlockWA]
        """
        return self.search_all(self.structure.DELIVERY_BLOCK, elements_type=DeliveryBlockWA)

    def get_delivery_spinner_element(self):
        return self.search(self.structure.DELIVERY_SPINNER)