from module_projects.base.structure import BasePageStructure


class VMROrderPageStructure(BasePageStructure):

    RE_DELIVERY_LOADER = '//*[@class="newloader__title"]'

    CONFIRM = "//H4[text()='Ваши данные:']"

    PHONE = "//*[@id='phone']"
    EMAIL = "//*[@id='mail']"
    NAME = "//*[@id='name']"
    CITY = "//*[@id='city_name']"
    ADDRESS = "//*[@id='address']"
    COMMENT = "//*[@id='comment']"

    CITY_POP_UP_ELEMENTS = "//*[@class='autocomplete-holder']/ul/li"
    COUNTRYS_LIST = "//*[@id='country']/option"

    ALL_DELIVERY = "//*[@id='cart_address']//li/div"
    ALL_PAYMENT = "//*[@class='payment']//div[@class='type'][parent::div[not(contains(@style, 'display: none'))]]"
    DELIVERY_COURIER = "//*[@id='COURIER']/div"
    DELIVERY_SPINNER = "//*[@class='newloader delivery-loader']"

    CREATE_ORDER = "//*[@id='submit_address_button']"

    TOTAL_DELIVERY_PRICE = '//*[@class="total"]//strong[contains(@class, "total__delivery-price")]'
    TOTAL_DELIVERY_PRICE_PLACEHOLDER = '//*[@class="total"]//i[contains(text(), "Выберите способ доставки")]'
    TOTAL_AMOUNT_PRICE = '//*[@class="total"]//strong[@class="total__amount-price"]'
    TOTAL_PRICE = '//*[@class="total"]//strong[@class="total__price"]'

    CITY_SPINNER = '//*[@class="vm-spinner city-spinner"]'

    SIZE = '//*[@class="col-sm-7"]//p[contains(text(), "Размер")]'

    DELIVERY_BLOCK = "//*[@class='delivery']/ul/li"