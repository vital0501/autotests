from autotests_base.conditions.element_conditions import exist, text_not_none
from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement, SubWaitingElementCollection


class BaseOrderWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class CityPopUpWE(BaseOrderWE):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_city(self):
        return SubWaitingElement(self, "/b").text

    def get_region(self):
        return SubWaitingElement(self, "/span").text


class OrderDeliveryMethodsWE(BaseOrderWE):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def set_active(self):
        SubWaitingElement(self, '/input').assure(exist).scroll_to_element()
        SubWaitingElement(self, '/input').assure(exist).click()

    def get_day(self):
        _text_split = SubWaitingElement(self, '//b[@class="type-dates"]').text.split(' ')
        _day_text = _text_split[0] if len(_text_split) == 2 else _text_split[1]
        return int(_day_text) if '-' not in _day_text else int(_day_text.split('-')[0])

    def get_price(self):
        _text_split = SubWaitingElement(self, '//b[@class="type-price"]').text.split(' ')
        return int(_text_split[0]) if len(_text_split) == 2 else int(_text_split[1])

    def get_name(self):
        return SubWaitingElement(self, '//b/i').text

    def get_type(self):
        return SubWaitingElement(self, '/..').get_attribute("id").lower()


class OrderPaymentMethodsWE(BaseOrderWE):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_name(self) -> str:
        return SubWaitingElement(self, "//label/i[not(@class)]").text

    def set_active(self):
        SubWaitingElement(self, '/input').assure(exist).scroll_to_element()
        SubWaitingElement(self, '/input').assure(exist).click()


class DeliveryBlockWA(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_delivery_methods(self):
        """

        :rtype: WaitingElementCollection[OrderDeliveryMethodsWaitingElement]
        """
        return SubWaitingElementCollection(self, "/div", elements_type=OrderDeliveryMethodsWE)

    def get_name(self):
        return SubWaitingElement(self, '/h5').assure(text_not_none).text