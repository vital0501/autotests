from module_projects.base.check import BaseCheck


class PaymentInfoCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.PaymentInfoPage.page.VMRPaymentInfoPage
        """
        super().__init__(page)
        self.page = page
