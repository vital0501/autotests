from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class PaymentInfoException(VMRBasePageException):
    pass
