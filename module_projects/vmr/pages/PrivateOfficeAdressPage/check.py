from module_projects.base.check import BaseCheck


class PrivateOfficeAdressCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.PrivateOfficeAdressPage.page.VMRPrivateOfficeAdressPage
        """
        super().__init__(page)
        self.page = page