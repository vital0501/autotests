from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import exist, is_displayed
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.PrivateOfficeAdressPage.check import PrivateOfficeAdressCheck
from module_projects.vmr.pages.PrivateOfficeAdressPage.structure import VMRPrivateOfficeAdressPageStructure


class VMRPrivateOfficeAdressPage(VMRBasePage):
    page_name = 'VMRPrivateOfficeAdress'

    @staticmethod
    def get_structure():
        return VMRPrivateOfficeAdressPageStructure()

    def _get_check(self):
        """

        :rtype: PrivateOfficeAdressCheck
        """
        return PrivateOfficeAdressCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def add_adress(self):
        self.search(self.structure.ADRESS_ADD).assure(exist).assure(is_displayed).click()

    def set_country(self, country):
        self.search(self.structure.COUNTRY).assure(exist).assure(is_displayed).clear()
        self.search(self.structure.COUNTRY).assure(exist).assure(is_displayed).send_keys(country)

    def set_region(self, region):
        self.search(self.structure.REGION).assure(exist).assure(is_displayed).clear()
        self.search(self.structure.REGION).assure(exist).assure(is_displayed).send_keys(region)

    def set_city(self, city):
        self.search(self.structure.CITY).assure(exist).assure(is_displayed).clear()
        self.search(self.structure.CITY).assure(exist).assure(is_displayed).send_keys(city)

    def set_adress(self, address):
        self.search(self.structure.ADRESS).assure(exist).assure(is_displayed).clear()
        self.search(self.structure.ADRESS).assure(exist).assure(is_displayed).send_keys(address)

    def set_post_index(self, post_index):
        self.search(self.structure.POST_INDEX).assure(exist).assure(is_displayed).clear()
        self.search(self.structure.POST_INDEX).assure(exist).assure(is_displayed).send_keys(post_index)

    def save_form(self):
        self.search(self.structure.SAVE_BUTTON).assure(exist).click()

    def del_adress(self):
        self.search(self.structure.DEL_BUTTON).assure(exist).assure(is_displayed).click()

    def modify_adress(self):
        self.search(self.structure.ADRESS_ROW).assure(exist).assure(is_displayed).click()

    def go_to_support(self):
        """


        :rtype: module_projects.vmr.pages.PrivateOfficeSupportPage.page.VMRPrivateOfficeSupportPage
        """
        self.search(self.structure.SUPPORT).assure(exist).click()
        from module_projects.vmr.pages.PrivateOfficeSupportPage.page import VMRPrivateOfficeSupportPage
        return self.create_page_object(VMRPrivateOfficeSupportPage)