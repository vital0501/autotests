from module_projects.base.structure import BasePageStructure


class VMRPrivateOfficeAdressPageStructure(BasePageStructure):
    CONFIRM = "//*[text()='Мои адреса']"
    ADRESS_ADD = "//*[@class='address_popup btn btn-success']"
    COUNTRY = "//*[@id='simple_popup']//div[1]/div/input"
    REGION = "//*[@id='simple_popup']//div[2]/div/input"
    CITY = "//*[@id='simple_popup']//div[3]//input"
    ADRESS = "//*[@id='simple_popup']//div[4]//input"
    POST_INDEX = "//*[@id='simple_popup']//div[5]//input"
    SAVE_BUTTON = "//*[@id='simple_popup']//div[6]//input"
    DEL_BUTTON = "//section[1]//tr[1]/td[3]/a"
    ADRESS_ROW = "//*[@class='address_popup']"
    SUPPORT = "//a[contains(text(),'Служба поддержки')]"