import time

from module_projects.base.check import BaseCheck
from module_projects.vmr.pages.BasePage.check import VMRBaseCheck
from module_projects.vmr.pages.PrivateOfficePage.exception import PrivateOfficeOrderNotFoundException


class PrivateOfficeOrdersListCheck(VMRBaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.PrivateOfficeOrdersListPage.page.VMRPrivateOfficeOrdersListPage
        """
        super().__init__(page)
        self.page = page

    def check_order_exist(self, order_num, timeout=10, raise_error=False):

        for item in range(0, timeout * 5):
            if order_num not in self.page.get_order_list_num():
                time.sleep(0.2)
            else:
                result = True
                break
        else:
            result = False

        if not result and raise_error:
            raise PrivateOfficeOrderNotFoundException("Не найден заказ с номером \"{0}\"".format(order_num))
        return result
