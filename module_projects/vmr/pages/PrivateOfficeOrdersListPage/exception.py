from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class PrivateOfficeOrdersListException(VMRBasePageException):
    pass
