from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import exist
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.vmr.pages.PrivateOfficeOrdersListPage.check import PrivateOfficeOrdersListCheck
from module_projects.vmr.pages.PrivateOfficeOrdersListPage.structure import VMRPrivateOfficeOrdersListPageStructure
from module_projects.vmr.pages.PrivateOfficePage.page import VMRPrivateOfficePage
from module_projects.vmr.pages.PrivateOfficePage.waiting_elements import PROrderWE


class VMRPrivateOfficeOrdersListPage(VMRPrivateOfficePage):
    page_name = 'VMRPrivateOfficeOrdersListPage'

    @staticmethod
    def get_structure():
        return VMRPrivateOfficeOrdersListPageStructure()

    def _get_check(self):
        """

        :rtype: PrivateOfficeOrdersListCheck
        """
        return PrivateOfficeOrdersListCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_order_list_num(self):
        return [item.get_order_num() for item in self.get_orders_elements()]

    def get_orders_elements(self):
        """

        :rtype: WaitingElementCollection[PROrderWE]
        """
        return self.search_all(self.structure.ORDERS_ELEMENTS, elements_type=PROrderWE)

    def go_to_order_by_num(self, order_num):
        """

        :rtype: module_projects.vmr.pages.SuccessfulOrderPage.page.VMRSuccessfulOrderPage
        """
        self.search('//a[text()="{0}"]'.format(order_num)).assure(exist).click()

        from module_projects.vmr.pages.SuccessfulOrderPage.page import VMRSuccessfulOrderPage
        return self.create_page_object(VMRSuccessfulOrderPage)