from module_projects.base.structure import BasePageStructure


class VMRPrivateOfficeOrdersListPageStructure(BasePageStructure):
    CONFIRM = '//h4[text()="Мои заказы"]'
    ORDERS_ELEMENTS = '//*[@class="table table-striped table-hover"]/tbody/tr'
