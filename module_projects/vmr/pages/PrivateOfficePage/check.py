from selenium.common.exceptions import TimeoutException

from module_projects.vmr.pages.BasePage.check import VMRBaseCheck
from module_projects.vmr.pages.PrivateOfficePage.exception import PrivateOfficePhoneMsgException


class PrivateOfficeCheck(VMRBaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.PrivateOfficePage.page.VMRPrivateOfficePage
        """
        super().__init__(page)
        self.page = page

    def check_phone_error_eq(self, msg_error, raise_error=False):
        try:
            result = msg_error == self.page.get_phone_error_msg_element()
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise PrivateOfficePhoneMsgException("Некорректное сообщение. Ожидаем: {0}, Видим: {1}"
                                               .format(msg_error, self.page.get_phone_error_msg_element()))
        return result