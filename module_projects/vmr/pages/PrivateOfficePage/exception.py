from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class PrivateOfficeException(VMRBasePageException):
    pass


class PrivateOfficeOrderNotFoundException(PrivateOfficeException):
    pass

class PrivateOfficePhoneMsgException(PrivateOfficeException):
    pass