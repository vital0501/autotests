from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import is_displayed, exist, text_not_none
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.PrivateOfficePage.check import PrivateOfficeCheck
from module_projects.vmr.pages.PrivateOfficePage.structure import VMRPrivateOfficePageStructure
from module_projects.vmr.parts.VMRPRLeftMenu.page import VMRPRLeftMenu


class VMRPrivateOfficePage(VMRBasePage):
    page_name = 'VMRPrivateOffice'

    @staticmethod
    def get_structure():
        return VMRPrivateOfficePageStructure()

    def _get_check(self):
        """

        :rtype: PrivateOfficeCheck
        """
        return PrivateOfficeCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

        self.PR_LEFT_MENU = VMRPRLeftMenu(driver, project_url, timeout)

    def set_name(self, value):
        name_in = self.search(self.structure.NAME).assure(exist).assure(is_displayed)
        name_in.clear()
        name_in.send_keys(value)

    def set_phone(self, value):
        self.search(self.structure.PHONE_CLICK).assure(exist).assure(is_displayed).click()
        self.get_phone_element().click()
        self.get_phone_element().send_keys(value)
        self.search(self.structure.PHONE_BUTTON).assure(exist).assure(is_displayed).click()

    def close_phone_form(self):
        self.search(self.structure.PHONE_CLOSE).assure(exist).assure(is_displayed).click()

    def save_data(self):
        self.search(self.structure.SAVE_BUTTON).assure(exist).assure(is_displayed).click()

    def check_news(self):
        self.search(self.structure.CHECK_BOX).assure(exist).assure(is_displayed).click()

    def get_phone_error_msg_element(self):
        return str(self.search(self.structure.PHONE_ERROR).assure(exist).assure(is_displayed).assure(text_not_none).text)

    def get_phone_element(self):
        return self.search(self.structure.PHONE).assure(exist).assure(is_displayed)