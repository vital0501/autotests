from module_projects.base.structure import BasePageStructure


class VMRPrivateOfficePageStructure(BasePageStructure):
    CONFIRM = "//H4[text()='Мой профиль']"

    NAME = "//*[@class='form-control' and @name='form[fio]']"

    PHONE_CLICK ="//*[@id='current-phone']/a"
    PHONE = "//*[@id='phone']"
    PHONE_BUTTON = "//*[@class='but code']"
    PHONE_ERROR = "//*[@id='phone-number-error']"
    PHONE_CLOSE = "//*[@class='fancybox-item fancybox-close']"

    CHECK_BOX = "//*[@class='checkbox']//input"
    SAVE_BUTTON = "//*[@class='btn btn-vm' and @value='Сохранить информацию']"
