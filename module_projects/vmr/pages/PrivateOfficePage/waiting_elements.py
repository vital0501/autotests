from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BasePrivateOfficeWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class PROrderWE(BasePrivateOfficeWE):
    def get_order_num(self):
        return int(SubWaitingElement(self, '//a').text)