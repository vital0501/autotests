from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_conditions import exist, is_displayed
from module_projects.base.check import BaseCheck
from module_projects.vmr.pages.PrivateOfficeQuestionsPage.exception import PrivateOfficeQuestionsException


class PrivateOfficeQuestionsCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.PrivateOfficeQuestionsPage.page.VMRPrivateOfficeQuestionsPage
        """
        super().__init__(page)
        self.page = page


    def check_is_question_text_visible(self, index, raise_error=False):
        try:
            result = self.page.get_all_question_text_elements()[index].assure(exist).assure(is_displayed)
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise PrivateOfficeQuestionsException('Не показывается текст ответа')
        return result