from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import exist, is_displayed
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.PrivateOfficeQuestionsPage.check import PrivateOfficeQuestionsCheck
from module_projects.vmr.pages.PrivateOfficeQuestionsPage.structure import VMRPrivateOfficeQuestionsPageStructure


class VMRPrivateOfficeQuestionsPage(VMRBasePage):
    page_name = 'VMRPrivateOfficeQuestions'

    @staticmethod
    def get_structure():
        return VMRPrivateOfficeQuestionsPageStructure()

    def _get_check(self):
        """

        :rtype: PrivateOfficeQuestionsCheck
        """
        return PrivateOfficeQuestionsCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_all_question_elements(self):
        return self.search_all(self.structure.QUESTONS)

    def get_all_question_text_elements(self):
        return self.search_all(self.structure.QUESTONS_TEXT)

    def go_to_adress_delivery_page(self):
        """

        :rtype: module_projects.vmr.pages.PrivateOfficeAdressPage.page.VMRPrivateOfficeAdressPage
        """
        self.get_delivery_menu_element().click()
        from module_projects.vmr.pages.PrivateOfficeAdressPage.page import VMRPrivateOfficeAdressPage
        return self.create_page_object(VMRPrivateOfficeAdressPage)

    def get_delivery_menu_element(self):
        return self.search(self.structure.ADRESS_DELIVERY).assure(exist).assure(is_displayed)