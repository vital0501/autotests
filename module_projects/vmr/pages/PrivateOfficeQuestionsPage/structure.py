from module_projects.base.structure import BasePageStructure


class VMRPrivateOfficeQuestionsPageStructure(BasePageStructure):
    CONFIRM = "//H3[contains(text(),'Часто задаваемые вопросы')]"

    QUESTONS = "//*[@class='info-holder']//span"
    QUESTONS_TEXT = "//*[@class='info-holder']//div"

    ADRESS_DELIVERY = '//*[@href="/privateoffice/myAddresses"]'