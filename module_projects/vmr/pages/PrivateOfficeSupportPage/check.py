from selenium.common.exceptions import TimeoutException

from module_projects.base.check import BaseCheck
from module_projects.vmr.pages.PrivateOfficeSupportPage.exception import PrivateOfficeSupportSendMsgException


class PrivateOfficeSupportCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.PrivateOfficeSupportPage.page.VMRPrivateOfficeSupportPage
        """
        super().__init__(page)
        self.page = page

    def check_successful_message_eq(self, msg_1, msg_2, raise_error=False):
        try:
            result = self.page.get_successful_message_element() in [msg_1, msg_2]
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise PrivateOfficeSupportSendMsgException("Некорректное сообщение. Ожидаем: {0} или {1}, Видим: {2}"
                                               .format(msg_1, msg_2, self.page.get_successful_message_element()))
        return result