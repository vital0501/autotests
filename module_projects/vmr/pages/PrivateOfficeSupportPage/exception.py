from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class PrivateOfficeSupportException(VMRBasePageException):
    pass

class PrivateOfficeSupportSendMsgException(VMRBasePageException):
    pass
