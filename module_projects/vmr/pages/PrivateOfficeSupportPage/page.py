import random
import tempfile
import urllib
from typing import Union

import os
from selenium import webdriver

from autotests_base.conditions.element_conditions import is_displayed, exist, text_not_none
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.PrivateOfficeSupportPage.check import PrivateOfficeSupportCheck
from module_projects.vmr.pages.PrivateOfficeSupportPage.structure import VMRPrivateOfficeSupportPageStructure


class VMRPrivateOfficeSupportPage(VMRBasePage):
    page_name = 'VMRPrivateOfficeSupport'

    @staticmethod
    def get_structure():
        return VMRPrivateOfficeSupportPageStructure()

    def _get_check(self):
        """

        :rtype: PrivateOfficeSupportCheck
        """
        return PrivateOfficeSupportCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def close_pop_up(self):
        self.search(self.structure.CLOSE_POP_UP).click()

    def set_random_category(self):
        random.choice(self.get_all_category()).click()

    def get_all_category(self):
        return self.search_all(self.structure.CATEGORY)

    def set_email(self, email):
        self.search(self.structure.EMAIL).clear()
        self.search(self.structure.EMAIL).send_keys(email)

    def set_subject(self, subject):
        self.search(self.structure.SUBJECT).clear()
        self.search(self.structure.SUBJECT).send_keys(subject)

    def set_message(self, message):
        self.search(self.structure.MESSAGE).clear()
        self.search(self.structure.MESSAGE).send_keys(message)

    def set_file(self, file):

        testfile = urllib.request.URLopener()
        file_path = os.path.join(tempfile.gettempdir(), 'test.png')
        testfile.retrieve(file, file_path)
        self.search(self.structure.FILE_INPUT).send_keys(file_path)
        os.unlink(file_path)

    def send_message(self):
        self.search(self.structure.SEND_BUTTON).click()

    def get_successful_message_element(self):
        return str(self.search(self.structure.IS_SUCCESSFUL).assure(text_not_none).text)