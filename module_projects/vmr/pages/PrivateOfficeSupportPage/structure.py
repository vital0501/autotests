from module_projects.base.structure import BasePageStructure


class VMRPrivateOfficeSupportPageStructure(BasePageStructure):
    CONFIRM = "//h4[text()='Служба поддержки']"
    IS_SUCCESSFUL = "//*[@id='empty']//p"
    CLOSE_POP_UP = "//*[@id='empty']//button"
    CATEGORY = "//*[@name='form[category]']/option"
    EMAIL = "//*[@name='form[email]']"
    SUBJECT = "//*[@name='form[title]']"
    MESSAGE = "//*[@name='form[text]']"
    SEND_BUTTON = '//*[@value="Отправить"]'
    FILE_INPUT = "//*[@id='attach']"