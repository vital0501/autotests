from selenium.common.exceptions import TimeoutException

from selenium.common.exceptions import TimeoutException

from module_projects.vmr.pages.BasePage.check import DataLayerCheck, VMRBaseCheck
from module_projects.vmr.pages.ProductPage.exception import VMRProductPageDataLayerSectionException,\
    ProductException, VMRProductPageLengthSectionException, ProductPageAboutProductTableException, \
    ProductPageImageMoreException, ProductPage3dColorException, ProductPageSizeTableException


class ProductCheck(VMRBaseCheck, DataLayerCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.ProductPage.page.VMRProductPage
        """
        super().__init__(page)
        self.page = page

    def check_page_data_impressions_with_data_layer_eq(self, value, data=None, obj_2_name='', parameter=None, error_class=None, raise_error=False):
        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        error_class = VMRProductPageDataLayerSectionException
        super().check_data_layer_impressions_data_with_page_eq(value, data, "DataLayer", 'name', error_class, True)

    def check_data_layer_length_impressions_eq(self, value, data=None, dict_name='Impressions', error_class=None, raise_error=False):
        error_class = error_class if error_class else VMRProductPageLengthSectionException
        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        super().check_data_layer_length_impressions_eq(value, data, dict_name, error_class, raise_error)

    def check_data_layer_impressions_data_with_page_eq(self, value, data=None, obj_2_name="", parameter=None, error_class=None, raise_error=False):
        data = data if data else self.page.data_layer.get_value("ecommerce.impressions")
        error_class = VMRProductPageDataLayerSectionException
        super().check_data_layer_impressions_data_with_page_eq(value, data, "Данные страницы", 'name', error_class, True)

    def check_about_product_visible(self, msg, raise_error=False):
        try:
            result = msg ==self.page.get_about_product_element()
        except ProductPageAboutProductTableException():
            result = False

        if not result and raise_error:
            raise ProductPageAboutProductTableException("Некорректное сообщение. Ожидаем: {0}, Видим: {1}"
                                               .format(msg, self.page.get_about_product_element()))
        return result

    def check_image_more_visible(self, raise_error=False):
        try:
            result = self.page.get_image_more_product_element()
        except ProductPageImageMoreException():
            result = False

        if not result and raise_error:
            raise ProductPageImageMoreException("Не отобржается увеличенное фото товара")
        return result

    def check_color_change(self, old_name, raise_error=False):
        try:
            result = self.page.get_active_color_element().get_name() != old_name
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise ProductException("Не существует или не изменился активный цвет")
        return result

    def change_product_type_name(self, old_name, raise_error=False):
        try:
            result = self.page.get_product_type_name() != old_name
        except TimeoutException:
            result = False

        if not result and raise_error:
            raise ProductException("Продукт тип \"{0}\" не изменился." .format(old_name))
        return result

    def check_3D_color_available(self, raise_error=False):

        if '3D' in self.page.get_3d_color_text() and self.page.get_color_key() != 'white':
            return True

        if self.page.get_3d_color_text() == '3D печать' and self.page.get_color_key() == 'white':
            return True
        else:
            result = False

        if not result and raise_error:
            raise ProductPage3dColorException('Ошибка в товаре "{0}". Ожидаем "3D печать" или "3D {1}", видим "{1}"'
                                              .format(self.page.get_product_type_name(), self.page.get_3d_color_text()))

    def check_size_table_visible(self, value, raise_error=False):
        try:
            result = value == self.page.get_size_table_element()
        except TimeoutException():
            result = False

        if not result and raise_error:
            raise ProductPageSizeTableException("Не отображается таблица размеров. Ожидаем: {0}, Видим: {1}"
                                               .format(value, self.page.get_about_product_element()))
        return result

    def check_russian_size_visible(self, value, raise_error=False):
        try:
            result = value == self.page.get_size_russian_element()
        except TimeoutException():
            result = False

        if not result and raise_error:
            raise ProductPageSizeTableException("В таблице не отображается 'Российский'. Ожидаем: {0}, Видим: {1}}"
                                               .format(value, self.page.get_about_product_element()))
        return result

    def check_size_len(self, size_item,  raise_error=False):
        try:
            result = len(size_item.get_name_russian()) != 0
        except TimeoutException():
            result = False

        if not result and raise_error:
            raise ProductPageSizeTableException("В карточке товара некорректный российский размер в {0}}".format(size_item))
        return result