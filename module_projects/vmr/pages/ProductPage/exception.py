from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class ProductException(VMRBasePageException):
    pass


class ProductPageAddToCartException(ProductException):
    pass


class VMRProductPageDataLayerSectionException(ProductException):
    pass


class VMRProductPageDataLayerSectionExistException(ProductException):
    pass


class VMRProductPageLengthSectionException(ProductException):
    pass


class VMRProductDataLayerException(ProductException):
    pass


class ProductPageProductTypeNotFoundException(ProductException):
    pass


class ProductPageAboutProductTableException(ProductException):
    pass


class ProductPageImageMoreException(ProductException):
    pass


class ProductPageAboutProductTableException(ProductException):
    pass


class ProductPageImageMoreException(ProductException):
    pass


class ProductPage3dColorException(ProductException):
    pass


class ProductPageSizeTableException(ProductException):
    pass
