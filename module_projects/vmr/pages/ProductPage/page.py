import random
from typing import Union

import time
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException

from autotests_base.conditions.element_conditions import exist, is_displayed, text_not_none
from autotests_base.elements.waiting_elements import WaitingElement
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.ProductPage.check import ProductCheck
from module_projects.vmr.pages.ProductPage.exception import ProductPageAddToCartException
from module_projects.vmr.pages.ProductPage.structure import VMRProductPageStructure
from module_projects.vmr.pages.ProductPage.waiting_elements import ProductProductTypeWE, ProductThumbnailsWE, \
    ProductSocialNetworkShareWE
from module_projects.vmr.pages.ProductPage.waiting_elements import ColorWaitingElement,\
    SizeWaitingElement, TypeWaitingElement
from module_projects.vmr.parts.VMRBreadCrumbs.page import VMRBreadCrumbs
from module_projects.vmr.parts.VMRRecommended.page import VMRRecommendedProducts


class VMRProductPage(VMRBasePage):
    page_name = 'VMRProduct'

    @staticmethod
    def get_structure():
        return VMRProductPageStructure()

    def _get_check(self):
        """

        :rtype: ProductCheck
        """
        return ProductCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()
        self.BREADCRUMBS = VMRBreadCrumbs(driver, project_url, timeout)
        self.RECOMMENDED_PRODUCT = VMRRecommendedProducts(driver, project_url, timeout)

    def get_active_size_element(self):
        """

        :rtype: SizeWaitingElement
        """
        return self.search(self.structure.ACTIVE_SIZE, element_type=SizeWaitingElement)

    def get_active_size(self):
        _element = self.get_active_size_element()
        if _element.exist():
            return _element
        else:
            return None

    def set_random_size(self):
        for item in range(0, 20):
            time.sleep(0.1)
            random.choice(self.get_available_sizes_elements()).click()
            time.sleep(0.1)
            if self.get_active_size_element() is not None:
                break

    def get_active_color_element(self):
        """

        :rtype: ColorWaitingElement
        """
        return self.search(self.structure.ACTIVE_COLOR, element_type=ColorWaitingElement)

    def set_random_color(self):
        self.set_color_by_index(random.randint(0, len(self.get_available_color_elements()) -1))
        time.sleep(0.2)

    def set_color_by_index(self, index):
        self.get_available_color_elements()[index].assure(exist).assure(is_displayed).click()

    def get_active_color(self):
        _element = self.get_active_color_element()
        if _element.exist():
            return _element
        else:
            return None

    @staticmethod
    def set_type(product_type: TypeWaitingElement):

        if product_type.is_displayed():
            product_type.assure(is_displayed).click()
            time.sleep(10)

    def get_available_sizes_elements(self):
        """

        :rtype: WaitingElementCollection[SizeWaitingElement]
        """
        return self.search_all(self.structure.SIZES, elements_type=SizeWaitingElement)

    def get_available_color_elements(self):
        """

        :rtype: WaitingElementCollection[ColorWaitingElement]
        """
        return self.search_all(self.structure.COLORS, elements_type=ColorWaitingElement)

    def add_product_to_cart(self):
        self.get_add_to_cart_button_element().assure(exist).click()

        try:
            self.get_go_to_cart_button_element().assure(exist).assure(is_displayed)
        except TimeoutException:
            raise ProductPageAddToCartException("Не изменилось состоянии кнопки \"Добавить в корзину\"")

    def go_to_cart(self):
        """

        :rtype: module_projects.vmr.pages.CartPage.page.VMRCartPage
        """
        self.get_go_to_cart_button_element().click()
        from module_projects.vmr.pages.CartPage.page import VMRCartPage
        return self.create_page_object(VMRCartPage)

    def get_go_to_cart_button_element(self):
        return self.search(self.structure.GO_TO_CART_BUTTON)

    def get_add_to_cart_button_element(self):
        return self.search(self.structure.ADD_TO_CART_BUTTON)

    def get_product_type_name_element(self) -> WaitingElement:
        return self.search(self.structure.TYPE_NAME)

    def get_product_type_name(self) -> str:
        return self.get_product_type_name_element().text

    def get_product_type_eng(self):
        return self.driver.current_url.split('/')[-2]

    def get_product_int_id(self):
        return int(self.search(self.structure.PRODUCT_ID).get_attribute('innerHTML'))

    def get_full_name(self):
        return "{0} {1}" .format(self.get_product_type_name(), self.get_name())

    def get_name(self) -> str:
        return self.get_name_element().text.replace('«', '').replace('»', '')

    def get_name_element(self) -> WaitingElement:
        return self.search(self.structure.NAME)

    def get_price(self) -> int:
        return int(self.get_price_element().text)

    def get_price_element(self) -> WaitingElement:
        return self.search(self.structure.PRICE)

    def get_old_price(self) -> int:
        if self.is_actions():
            return int(self.get_old_price_element().text)
        else:
            return None

    def is_actions(self) -> bool:
        return self.get_old_price_element().exist()

    def get_old_price_element(self) -> WaitingElement:
        return self.search(self.structure.OLD_PRICE)

    def get_color_key(self) -> str:
        try:
            return self.search(self.structure.PRODUCT_COLOR, element_type=ColorWaitingElement).get_key()
        except (NoSuchElementException, TimeoutException):
            if self.search(self.structure.PRODUCT_FULLPRINT).exist():
                return 'white'

    def is_product_available(self):
        return not self.search(self.structure.PRODUCT_AVAILABLE).is_displayed()

    def open_about_product_table(self):
        self.search(self.structure.ABOUTPODUCT_LINK).assure(exist).assure(is_displayed).click()

    def get_about_product_element(self):
        return str(self.search(self.structure.ABOUTPRODUCT_TABLE).assure(exist).assure(is_displayed).assure(text_not_none).text)

    def close_about_product_table(self):
        self.search(self.structure.CLOSE_POPUP_ABOUTPRODUCT).assure(exist).assure(is_displayed).click()

    def open_product_img_href_element(self):
        self.search(self.structure.IMAGE_HREF).assure(exist).assure(is_displayed).click()

    def get_image_more_product_element(self):
        return self.search(self.structure.IMAGE_PRODUCT_MORE).assure(exist).assure(is_displayed)

    def close_image_more(self):
        self.search(self.structure.CLOSE_IMAGE_MORE).assure(exist).assure(is_displayed).click()

    def open_size_table(self):
        self.search(self.structure.SIZE_TABLE_LINK).assure(exist).assure(is_displayed).click()

    def get_size_table_element(self):
        return str(self.search(self.structure.SIZE_TABLE).assure(exist).assure(is_displayed).assure(text_not_none).text)

    def get_size_russian_element(self):
        return str(self.search(self.structure.SIZE_RUSSIAN).assure(exist).assure(is_displayed).assure(text_not_none).text)

    def close_size_table(self):
        self.search(self.structure.CLOSE_SIZE_TABLE).assure(exist).assure(is_displayed).click()

    def get_product_available_types(self):
        """

        :rtype: list[module_projects.vmr.pages.ProductPage.waiting_elements.TypeWaitingElement]
        """
        return [item for item in self.search_all(self.structure.TYPES, elements_type=TypeWaitingElement) if item.is_displayed()]

    def add_favorites(self):
        self.search(self.structure.FAVORITES_ADD).assure(exist).assure(is_displayed).click()

    def del_favorites(self):
        self.search(self.structure.FAVORITES_DEL).assure(exist).assure(is_displayed).click()

    def get_designer_name(self):
        try:
            return self.get_designer_element().text
        except TimeoutException:
            return None

    def get_designer_element(self):
        return self.search(self.structure.PRODUCT_DESIGNER).assure(exist).assure(is_displayed)

    def get_3d_color_text(self):
        return self.get_3d_color_element().text

    def get_3d_color_element(self):
        return self.search(self.structure.D3_COLOR).assure(exist).assure(is_displayed)

    def get_available_product_types_elements(self):

        """

        :rtype: WaitingElementCollection[ProductProductTypeWE]
        """
        return self.search_all(self.structure.PRODUCT_TYPES, elements_type=ProductProductTypeWE)

    def set_product_type_by_index(self, index):
        self.get_available_product_types_elements()[index].click()

    def get_photo_href(self):
        return self.get_photo_element().assure(exist).get_attribute("src")

    def get_photo_element(self):
        return self.search(self.structure.PHOTO)

    def get_available_thumbnails(self):
        """

        :rtype: WaitingElementCollection[ProductThumbnailsWE]
        """
        return self.search_all(self.structure.THUMBNAILS, elements_type=ProductThumbnailsWE)

    def get_social_network_share_elements(self):
        """

        :rtype: WaitingElementCollection[ProductSocialNetworkShareWE]
        """
        return self.search_all(self.structure.SOCIAL_NETWORK_SHARE, elements_type=ProductSocialNetworkShareWE)

    def set_size_by_index(self, index):
        self.get_available_sizes_elements()[index].click()