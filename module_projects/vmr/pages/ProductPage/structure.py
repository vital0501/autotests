from module_projects.base.structure import BasePageStructure


class VMRProductPageStructure(BasePageStructure):
    CONFIRM = '//*[@class="product__info-name"]'
    NAME = CONFIRM

    COLORS = "//li[ancestor::div[@id='product_color']]"
    SIZES = "//li[ancestor::div[@id='product_size']]"
    ACTIVE_SIZE = SIZES + "[contains(@class, 'active')]"
    PRODUCT_TYPES = '//*[@itemprop="isSimilarTo"]'
    D3_COLOR = "//*[@id='product_color']//i"

    PHOTO = "//*[@itemprop='image']"

    THUMBNAILS = '//*[@id="swiper-thumbnails"]//a'
    SOCIAL_NETWORK_SHARE = '//*[contains(@class, "product__socials")]//li'

    DESIGNER_HREF = '//*[@class="product-designer"]/a'

    ACTIVE_COLOR = COLORS + "[contains(@class, 'active')]"

    ADD_TO_CART_BUTTON = "//*[@id='add_to_button'][@value='Добавить в корзину']"
    GO_TO_CART_BUTTON = "//*[@id='add_to_button'][@value='Перейти в корзину']"

    TYPE_NAME = "//*[@class='product__info-model']"
    PRODUCT_ID = '//*[@class="product"]//*[@itemprop="productID"]'

    PRICE = '//*[@data-autotest-id="actual_price"]'
    OLD_PRICE = '//*[@data-autotest-id="old_price"]'

    PRODUCT_FULLPRINT = '//*[@class="fullprint"]'
    PRODUCT_COLOR = "//li[ancestor::div[@id='product_color']][@class='active']"
    PRODUCT_AVAILABLE = "//*[contains(@class, 'not-found')]"

    ABOUTPODUCT_LINK = "//*[@id='color_link']"
    ABOUTPRODUCT_TABLE = "//*[@id='colors_tableLabel']"
    CLOSE_POPUP_ABOUTPRODUCT = ".//*[@id='colors_table']//button"

    IMAGE_HREF = "//*[@id='product_images']//a[@class='zoom']/img"
    IMAGE_PRODUCT_MORE = "//*[@class='fancybox-skin']"
    CLOSE_IMAGE_MORE = "//*[@class='fancybox-item fancybox-close']"

    PRODUCT_DESIGNER = "//span[@class='product-designer']/a"

    SIZE_TABLE_LINK = "//*[@id='sizes_table_link']"
    SIZE_TABLE = "//*[@id='sizes_tableLabel']"
    SIZE_RUSSIAN = "//*[@id='sizes_table']//tr[1]/td[1]"
    CLOSE_SIZE_TABLE = "//*[@id='sizes_table']//button"

    TYPES = "//*[@class='owl-item'][ancestor::div[@id='owl-product-types']]/div/a"

    FAVORITES_DEL = '//*[@class="to-favorites hidden-xs hidden-sm"]//*[@class="fa fa-heart"]'
    FAVORITES_ADD = '//*[@class="to-favorites hidden-xs hidden-sm"]//*[@class="fa fa-heart-o"]'