from autotests_base.conditions.element_conditions import exist
from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseProductWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class SizeWaitingElement(BaseProductWE):
    def get_name(self) -> str:
        return SubWaitingElement(self, "/span").text

    def get_name_russian(self) -> str:
        return SubWaitingElement(self, "/i").text

    def get_key(self) -> str:
        return self._finder().get_attribute('data-tkey')


class ColorWaitingElement(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_name(self) -> str:
        return self.get_attribute('title')

    def get_key(self) -> str:
        return self.assure(exist).get_attribute('data-tkey')

    def is_active(self):
        return bool(self.get_attribute('class') == 'active')


class ProductProductTypeWE(BaseProductWE):
    def is_active(self):
        return True if 'active' in self.get_attribute('class') else False

    def get_name(self):
        return SubWaitingElement(self, '/a').get_attribute('title')


class ProductThumbnailsWE(BaseProductWE):
    def get_href(self):
        return SubWaitingElement(self, '/img').get_attribute("src")


class ProductSocialNetworkShareWE(BaseProductWE):
    def get_name(self):
        return SubWaitingElement(self, '/a').get_attribute('title')


class SizeTableWaitingElement(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class TypeWaitingElement(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_name(self):
        return SubWaitingElement(self, "//a").get_attribute('title')

    def is_active(self):
        if SubWaitingElement(self, "//div[@class='active']").exist():
            return True
        else:
            return False

    def is_visible(self) -> bool:
        return self.is_displayed()