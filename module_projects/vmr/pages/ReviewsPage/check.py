from module_projects.base.check import BaseCheck


class ReviewsCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.ReviewsPage.page.VMRReviewsPage
        """
        super().__init__(page)
        self.page = page
