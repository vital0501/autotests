from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class ReviewsException(VMRBasePageException):
    pass


class ReviewsPagePaginationException(ReviewsException):
    pass


class ReviewsPageNotOpenSocialUrl(ReviewsException):
    pass


class ReviewsPageNotOpenServicesUrl(ReviewsException):
    pass