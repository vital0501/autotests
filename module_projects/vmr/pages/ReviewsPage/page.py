from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_collection_conditions import len_more
from autotests_base.conditions.element_conditions import exist
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.ReviewsPage.check import ReviewsCheck
from module_projects.vmr.pages.ReviewsPage.structure import VMRReviewsPageStructure
from module_projects.vmr.pages.ReviewsPage.waiting_elements import ReviewsElementWE, SocialNetworkWE, ReviewServiceWE


class VMRReviewsPage(VMRBasePage):
    page_name = 'VMRReviews'

    @staticmethod
    def get_structure():
        return VMRReviewsPageStructure()

    def _get_check(self):
        """

        :rtype: ReviewsCheck
        """
        return ReviewsCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_all_reviews_element(self):
        return self.search_all(self.structure.REVIEWS_ELEMENT, elements_type=ReviewsElementWE).assure(len_more, 0)

    def go_next_pagination(self):
        """

        :rtype: module_projects.vmr.pages.ReviewsPage.page.ReviewsPage
        """
        self.search(self.structure.NEXT_PAGINATION).assure(exist).click()
        return self.create_page_object(VMRReviewsPage)

    def go_previsions_pagination(self):
        """

        :rtype: module_projects.vmr.pages.ReviewsPage.page.ReviewsPage
        """
        self.search(self.structure.PREVISIONS_PAGINATION).assure(exist).click()
        return self.create_page_object(VMRReviewsPage)

    def get_vk_element(self):
        return self.get_social_network_element('vkontakte').assure(exist)

    def get_facebook_element(self):
        return self.get_social_network_element('facebook').assure(exist)

    def get_moymir_element(self):
        return self.get_social_network_element('moimir').assure(exist)

    def get_ok_element(self):
        return self.get_social_network_element('odnoklassniki').assure(exist)

    def get_twitter_element(self):
        return self.get_social_network_element('twitter').assure(exist)

    def get_google_plus_element(self):
        return self.get_social_network_element('gplus').assure(exist)

    def get_social_network_element(self, data_service):
        for item in self.get_social_networks_elements():
            if item.get_type() == data_service:
                return item
        else:
            print(data_service)

    def get_social_networks_elements(self):
        """

        :rtype: WaitingElementCollection[SocialNetworkWE]
        """
        return self.search_all(self.structure.SOCIAL_NETWORK, elements_type=SocialNetworkWE)

    def get_review_services_elements(self):
        """

        :rtype: WaitingElementCollection[ReviewServiceWE]
        """
        return self.search_all(self.structure.REVIEW_SERVICE, elements_type=ReviewServiceWE)

    def get_ya_element(self):
        return self.get_review_service('ya').assure(exist)

    def get_ir_element(self):
        return self.get_review_service('ir').assure(exist)

    def get_or_element(self):
        return self.get_review_service('ot').assure(exist)

    def get_vr_element(self):
        return self.get_review_service('vs').assure(exist)

    def get_review_service(self, data_service):
        for item in self.get_review_services_elements():
            if item.get_type() == data_service:
                return item

