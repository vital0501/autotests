from module_projects.base.structure import BasePageStructure


class VMRReviewsPageStructure(BasePageStructure):
    CONFIRM = "//h3[text()='Отзывы наших покупателей']"
    REVIEWS_ELEMENT = "//article"
    NEXT_PAGINATION = "//a[@title='Следующая страница']"
    PREVISIONS_PAGINATION = "//a[@title='Предыдущая страница']"
    SOCIAL_NETWORK = "//*[@class='likes']//span[@class='b-share-btn__wrap']"
    REVIEW_SERVICE = "//*[contains(@class, 'networks')]//ul/li"
