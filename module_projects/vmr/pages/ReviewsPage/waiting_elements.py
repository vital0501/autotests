from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class ReviewsElementWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_text(self):
        return SubWaitingElement(self, "//*[@class='review-text']").text

    def get_user(self):
        return SubWaitingElement(self, "//h5").text


class SocialNetworkWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_type(self):
        sn_id = SubWaitingElement(self, '/a').get_attribute("data-service")

        for item in SocialNetwork.all_sn:
            if item.data_service == sn_id:
                return item.data_service

    def get_expect_url(self):
        sn_id = SubWaitingElement(self, '/a').get_attribute("data-service")

        for item in SocialNetwork.all_sn:
            if item.data_service == sn_id:
                return item.url

class SocialNetwork(object):
    class VK(object):
        data_service = "vkontakte"
        url = 'https://oauth.vk.com'

    class Facebook(object):
        data_service = "facebook"
        url = 'https://www.facebook.com'

    class MoiMir(object):
        data_service = "moimir"
        url = 'http://connect.mail.ru'

    class Twitter(object):
        data_service = "twitter"
        url = 'https://twitter.com'

    class Ok(object):
        data_service = "odnoklassniki"
        url = 'https://connect.ok.ru/'

    class GooglePlus(object):
        data_service = "gplus"
        url = 'https://accounts.google.com'

    all_sn = [VK, Facebook, MoiMir, Twitter, Ok, GooglePlus]

    def get_sn(self, data_service):
        for item in self.all_sn:
            if item.data_service == data_service:
                return item


class ReviewServiceWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_type(self):
        rs_id = self.get_attribute('class')

        for item in ReviewService.all_rs:
            if item.data_service == rs_id:
                return item.data_service

    def is_external(self):
        rs_id = self.get_attribute('class')
        for item in ReviewService.all_rs:
            if item.data_service == rs_id:
                return item.external

    def get_expect_url(self):
        return SubWaitingElement(self, '/a').get_attribute("href")

class ReviewService(object):
    class VM(object):
        data_service = "vm"
        external = False

    class YA(object):
        data_service = "ya"
        external = True

    class IR(object):
        data_service = "ir"
        external = True

    class OT(object):
        data_service = "ot"
        external = True

    class VS(object):
        data_service = "vs"
        external = True

    all_rs = [VM, YA, IR, OT, VS]