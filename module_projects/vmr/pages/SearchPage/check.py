from module_projects.vmr.pages.CatalogPage.check import CatalogCheck


class SearchCheck(CatalogCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.SearchPage.page.VMRSearchPage
        """
        super().__init__(page)
        self.page = page
