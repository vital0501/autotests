from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class SearchException(VMRBasePageException):
    pass


class SearchPageNotProductMsgException(SearchException):
    pass


class SearchNotRelevantException(SearchException):
    pass
