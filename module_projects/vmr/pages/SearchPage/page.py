from typing import Union

from selenium import webdriver

from module_projects.vmr.pages.CatalogPage.page import VMRCatalogPage
from module_projects.vmr.pages.SearchPage.check import SearchCheck
from module_projects.vmr.pages.SearchPage.structure import VMRSearchPageStructure
from module_projects.vmr.pages.SearchPage.waiting_elements import SearchProductWaitingElement


class VMRSearchPage(VMRCatalogPage):
    page_name = 'VMRSearchPage'

    @staticmethod
    def get_structure():
        return VMRSearchPageStructure()

    def _get_check(self):
        """

        :rtype: SearchCheck
        """
        return SearchCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_products_elements(self):
        """

        :rtype: WaitingElementCollection[SearchProductWaitingElement]
        """
        return self.search_all(self.structure.PRODUCTS, elements_type=SearchProductWaitingElement)

    def is_empty_result(self):
        return self.search(self.structure.EMPTY).exist()