from module_projects.vmr.pages.CatalogPage.structure import VMRCatalogPageStructure


class VMRSearchPageStructure(VMRCatalogPageStructure):

    EMPTY = "//h1[text()='Не найдено']"
    CONFIRM = "//*[@id='view_products']//h1[contains(text(), 'Поиск')]" + '|' + EMPTY
    PRODUCTS = "//*[@data-autotest-id='product']"
