from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseSearchWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class SearchProductWaitingElement(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_product_type_name(self):
        return SubWaitingElement(self, '//span[@itemprop="model"]').text