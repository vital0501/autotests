from module_projects.base.check import BaseCheck


class SellCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.SellPage.page.VMRSellPage
        """
        super().__init__(page)
        self.page = page
