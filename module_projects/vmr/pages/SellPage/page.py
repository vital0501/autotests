from typing import Union

from selenium import webdriver

from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.SellPage.check import SellCheck
from module_projects.vmr.pages.SellPage.structure import VMRSellPageStructure


class VMRSellPage(VMRBasePage):
    page_name = 'VMRSell'

    @staticmethod
    def get_structure():
        return VMRSellPageStructure()

    def _get_check(self):
        """

        :rtype: SellCheck
        """
        return SellCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()
