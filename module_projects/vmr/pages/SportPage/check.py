from module_projects.base.check import BaseCheck


class SportPageCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.SportpagePage.page.VMRSportPagePage
        """
        super().__init__(page)
        self.page = page

    def check_price_not_eq(self, **kwargs):
        return True