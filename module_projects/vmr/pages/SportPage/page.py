import time
from typing import Union

from selenium import webdriver

from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.SportPage.check import SportPageCheck
from module_projects.vmr.pages.SportPage.structure import VMRSportPageStructure

from autotests_base.conditions.element_conditions import exist, is_displayed


class VMRSportPage(VMRBasePage):
    page_name = 'VMRSportPage'

    @staticmethod
    def get_structure():
        return VMRSportPageStructure()

    def _get_check(self):
        """

        :rtype: SportPageCheck
        """
        return SportPageCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def click_sport_form(self, form):
        time.sleep(1)
        if form == 'football':
            self.get_button_football_element().click()
        if form == 'basketball':
            self.get_button_basketball_element().click()
        else:
            self.get_button_volleyball_element().click()

    def get_button_football_element(self):
        return self.search(self.structure.CREATE_FOOTBALL_BUTTON).assure(exist).assure(is_displayed)

    def get_button_basketball_element(self):
        return self.search(self.structure.CREATE_BASKETBALL_BUTTON).assure(exist).assure(is_displayed)

    def get_button_volleyball_element(self):
        return self.search(self.structure.CREATE_VOLLEYBALL_BUTTON).assure(exist).assure(is_displayed)