from module_projects.base.structure import BasePageStructure


class VMRSportPageStructure(BasePageStructure):
    CONFIRM = "//*[@id='sport_designer']"
    SPORT_LOADING = '//*[@class="newloader text-center"]'
    CREATE_FOOTBALL_BUTTON = '//*[@id="sport_designer"]/div/div/div[1]//button'
    CREATE_BASKETBALL_BUTTON = '//*[@id="sport_designer"]//div[2]//button'
    CREATE_VOLLEYBALL_BUTTON = '//*[@id="sport_designer"]//div[3]//button'
    FORM_NAME = '//*[@class="rgKmICzbNHHjjaSALPAR0"]'
    FORM_PRICE = '//*[@class="_1H-ECiZmTYFha2Mg60yLHu"]'