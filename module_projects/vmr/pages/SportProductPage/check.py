from autotests_base.conditions.element_conditions import exist, is_displayed
from module_projects.base.check import BaseCheck
from module_projects.vmr.pages.SportProductPage.exception import SportProductPriceException, \
    SportProductException
from module_projects.vmr.pages.SportProductPage.exception import SportProductPriceException, \
    SportProductNullPriceException, SportProductLoaderPriceException


class SportProductCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.SportProductPage.page.VMRSportProductPage
        """
        super().__init__(page)
        self.page = page

    def check_price_eq(self, value, raise_error=False):
        try:
            result = self.page.get_price() == value
            _error_text = "Цена продукта не равна {0}" .format(value)
        except (SportProductNullPriceException, SportProductLoaderPriceException) as e:
            result = False
            _error_text = str(e)

        if not result and raise_error:
            raise SportProductPriceException(_error_text)
        return result

    def check_price_not_eq(self, value, raise_error=False):
        try:
            result = self.page.get_price() != value
            _error_text = "Цена продукта равна {0}" .format(value)
        except (SportProductNullPriceException, SportProductLoaderPriceException) as e:
            result = False
            _error_text = str(e)
        if not result and raise_error:
            raise SportProductPriceException(_error_text)
        return result

    def is_exist(self, _table_element, raise_error=False):
        _error_text = ''
        try:
            _table_element.assure(exist).assure(is_displayed)
            result = True
        except SportProductException as e:
            _error_text = str(e)
            result = False

        if not result and raise_error:
            raise SportProductException(_error_text)
        return result
