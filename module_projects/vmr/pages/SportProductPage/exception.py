from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class SportProductException(VMRBasePageException):
    pass

class SportProductPriceException(SportProductException):
    pass

class SportProductLoaderPriceException(SportProductException):
    pass


class SportProductNullPriceException(SportProductException):
    pass

class SportProductAboutTshirtException(SportProductException):
    pass

class SportProductAboutShortsException(SportProductException):
    pass

class SportProductTableSizeTshirtException(SportProductException):
    pass

class SportProductTableSizeShortsException(SportProductException):
    pass

class SportProductPopupException(SportProductException):
    pass