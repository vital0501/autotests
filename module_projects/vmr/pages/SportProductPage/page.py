import re
from typing import Union

from selenium import webdriver
from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_conditions import is_displayed, is_not_displayed, exist
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.CartPage.exception import CartException
from module_projects.vmr.pages.CartPage.page import VMRCartPage
from module_projects.vmr.pages.SportProductPage.check import SportProductCheck
from module_projects.vmr.pages.SportProductPage.exception import SportProductLoaderPriceException, \
    SportProductNullPriceException
from module_projects.vmr.pages.SportProductPage.structure import VMRSportProductPageStructure

from module_projects.vmr.pages.SportProductPage.waiting_elements import AboutProductTableWaitingElement,\
    SportFormPatternWaitingElement, SizeTableWaitingElement, CreatedPopUpWA
from module_projects.vmr.pages.SportProductPage.exception import SportProductException


class VMRSportProductPage(VMRBasePage):
    page_name = 'VMRSportProduct'

    @staticmethod
    def get_structure():
        return VMRSportProductPageStructure()

    def _get_check(self):
        """

        :rtype: SportProductCheck
        """
        return SportProductCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_price(self):
        try:
            self.get_price_loader_element().assure(is_displayed, timeout=2)
        except TimeoutException:
            pass

        try:
            self.get_price_loader_element().assure(is_not_displayed)
        except TimeoutException:
            raise SportProductLoaderPriceException("Долгая загрузка цены")

        try:
            price = str(self.get_price_element().assure(exist).assure(is_displayed).text)
            return int(''.join(re.findall('(\d+)', price)))
        except TimeoutException:
            raise SportProductNullPriceException("Показывается некорректная цена")

    def get_price_loader_element(self):
        return self.search(self.structure.SPORT_FORM_LOADING).assure(exist)

    def get_price_element(self):
        return self.search(self.structure.PRICE).assure(exist).assure(is_displayed)

    def open_about_tshirt(self):
        self.search(self.structure.ABOUT_TSHIRT_LINK).assure(exist).assure(is_displayed).click()

    def get_about_product_element(self):
        """

        :rtype: WaitingElementCollection[AboutProductTableWaitingElement]
        """
        return self.search(self.structure.ABOUT_PRODUCT_TABLE, element_type=AboutProductTableWaitingElement)

    def open_about_shorts(self):
        self.search(self.structure.ABOUT_SHORTS_LINK).assure(exist).assure(is_displayed).click()

    def get_pattern_element(self, index: int) -> SportFormPatternWaitingElement:
        return self.get_patterns_elements()[index]

    def get_patterns_elements(self):
        """

        :rtype: WaitingElementCollection[SportFormPatternWaitingElement]
        """
        return self.search_all(self.structure.PATTERNS_FORM, elements_type=SportFormPatternWaitingElement)

    def open_table_sizes_tshirt(self):
        self.search(self.structure.TABLE_SIZE_TSHIRT_LINK).assure(exist).assure(is_displayed).click()

    def get_size_table_element(self):
        """

        :rtype: WaitingElementCollection[SizeTableWaitingElement]
        """
        return self.search(self.structure.SIZE_TABLE, element_type=SizeTableWaitingElement)

    def open_table_sizes_shorts(self):
        self.search(self.structure.TABLE_SIZE_SHORTS_LINK).assure(exist).assure(is_displayed).click()


    def set_surname(self, surname, player_index):
        self.get_player_form_surname_element(player_index=player_index).clear()
        self.get_player_form_surname_element(player_index=player_index).send_keys(surname)

    def get_player_form_surname_element(self, player_index):
        return self.search("({0})[2]/input" .format(self.structure.PLAYER_FORM.format(player_index))).assure(exist)

    def get_player_form_number_element(self, number_index):
        return self.search("({0})[3]/input" .format(self.structure.PLAYER_FORM.format(number_index))).assure(exist)

    def set_number(self, num, number_index):
        self.get_player_form_number_element(number_index=number_index).clear()
        self.get_player_form_number_element(number_index=number_index).send_keys(num)

    def add_player(self):
        self.search(self.structure.ADD_PLAYER).assure(exist).assure(is_displayed).click()

    def add_cart(self):
        self.search(self.structure.ADD_CART_BUTTON).assure(exist).assure(is_displayed).click()

    def is_created_pop_up(self):
        try:
            self.get_created_pop_up_element().assure(exist).assure(is_displayed, timeout=30)
            return True
        except SportProductException:
            return False

    def get_created_pop_up_element(self):
        return self.search(self.structure.CREATED_POP_UP, element_type=CreatedPopUpWA).assure(exist, timeout=20)

    def go_to_cart(self):
        return self.search(self.structure.POPUP_ADD_CART_GO).assure(exist).assure(is_displayed).click()