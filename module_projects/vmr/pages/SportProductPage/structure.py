from module_projects.base.structure import BasePageStructure


class VMRSportProductPageStructure(BasePageStructure):
    CONFIRM = "//*[@id='sport_designer']//h1"
    SPORT_FORM_LOADING = '//*[@class="newloader text-center"]'
    PRICE = "//*[@class='SZbdu7rN_PGguMxO1Z5M3 rHMEZVDNxDTeW-tZ_cESN']"

    ABOUT_TSHIRT_LINK = '(//*[@data-reactroot]//div[@class="_2dbkf57PqI5K0PeYmOXS0c"]//a)[1]'
    ABOUT_SHORTS_LINK = '(//*[@data-reactroot]//div[@class="_2dbkf57PqI5K0PeYmOXS0c"]//a)[2]'
    TABLE_SIZE_TSHIRT_LINK = '(//*[@class="DcJy68sJsAaWlhDZ67DI1"]//th[@class="_2eL-vtAc3ZG2ZwYjOoA4LB"]//a)[1]'
    TABLE_SIZE_SHORTS_LINK = '(//*[@class="DcJy68sJsAaWlhDZ67DI1"]//th[@class="_2eL-vtAc3ZG2ZwYjOoA4LB"]//a)[2]'

    ABOUT_PRODUCT_TABLE = '//*[@class="_3ncqODYiqb8sji7z6qbDq_ _26igY9X2oPJX6MmqBJVYc"]'
    PATTERNS_FORM = '//*[@class="_14q2rOfZAvy13xbn4ScFOb"]//button'
    LOADER = '//*[@id="product-creation-loader"]/div/span'
    SIZE_TABLE = '//*[@class="_3ncqODYiqb8sji7z6qbDq_ _26igY9X2oPJX6MmqBJVYc"]'
    ADD_PLAYER = '//*[@class="fa fa-plus"]'
    ADD_CART_BUTTON = './/*[@class="brand _1oDhfOugr8WOEpS0BISmCs _91EzMEoBqkYNbgtqeWxAm _2vMIF1Gmc5yB1PmV91tlQw"]'
    CREATED_POP_UP = '//*[@id="success_popupLabel"]'
    POPUP_ADD_CART_GO = '//*[@class="btn btn-vm mrg-link"]'
    PLAYER_FORM = "((//*[@class='DcJy68sJsAaWlhDZ67DI1']/tbody/tr)[{0}]/td)"