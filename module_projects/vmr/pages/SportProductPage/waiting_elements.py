from autotests_base.conditions.element_conditions import exist, is_displayed
from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseSportProductWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

class AboutProductTableWaitingElement(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def close(self):
        SubWaitingElement(self, "/button").assure(exist).assure(is_displayed).click()

class SportFormPatternWaitingElement(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def set_active(self):
        self.assure(exist).click()

class SizeTableWaitingElement(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def close(self):
        SubWaitingElement(self, "/button").assure(exist).assure(is_displayed).click()

class CreatedPopUpWA(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def close(self):
        SubWaitingElement(self, "/button").assure(exist).assure(is_displayed).click()