from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_conditions import exist
from module_projects.base.check import BaseCheck
from module_projects.vmr.pages.StatusPage.exception import StatusPageEmptyInputException, \
    StatusPageColorEmptyInputException, StatusPageOrderNumberException


class StatusCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.StatusPage.page.VMRStatusPage
        """
        super().__init__(page)
        self.page = page

    def check_error_input_txt_eq(self, value, raise_error=False):
        try:
            result = value == self.page.get_input_status_form_element().get_text_error()
        except TimeoutException:
            result = False
        if not result and raise_error:
            raise StatusPageEmptyInputException("Некорректное сообщение о неверном вводе данных. Ожидаем: {0}. Видим: {1}"
                                                .format(value, self.page.get_input_status_form_element().get_text_error()))
        return result

    def check_error_input_color(self, error_element, raise_error=False):
        result = error_element
        try:
            if not result:
                result = False
        except TimeoutException:
            result = False
        if not result and raise_error:
            raise StatusPageColorEmptyInputException("Пустое поле не подсвечивается красным")
        return result

    def check_order_num_eq(self, order_num, raise_error=False):
        try:
            result = order_num == self.page.get_order_status_form_element().assure(exist).get_number()
        except TimeoutException:
            result = False
        if not result and raise_error:
            raise StatusPageOrderNumberException("Не соответствует номер заказа. Ожидаем {0}. Видим: {1}"
                                                .format(order_num, self.page.get_order_status_form_element().assure(exist).get_number()))
        return result
