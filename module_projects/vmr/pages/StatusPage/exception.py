from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class StatusException(VMRBasePageException):
    pass


class StatusPageEmptyInputException(VMRBasePageException):
    pass


class StatusPageColorEmptyInputException(VMRBasePageException):
    pass


class StatusPageOrderNumberException(VMRBasePageException):
    pass
