from typing import Union

from selenium import webdriver

from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.StatusPage.check import StatusCheck
from module_projects.vmr.pages.StatusPage.structure import VMRStatusPageStructure
from module_projects.vmr.pages.StatusPage.waiting_elements import StatusInputFormWA, StatusOrderFormWA


class VMRStatusPage(VMRBasePage):
    page_name = 'VMRStatus'

    @staticmethod
    def get_structure():
        return VMRStatusPageStructure()

    def _get_check(self):
        """

        :rtype: StatusCheck
        """
        return StatusCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_input_status_form_element(self):
        return self.search(self.structure.STATUS_FORM, element_type=StatusInputFormWA)

    def get_order_status_form_element(self):
        return self.search(self.structure.ORDER_FORM, element_type=StatusOrderFormWA)