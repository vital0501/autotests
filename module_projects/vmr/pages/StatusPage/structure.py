from module_projects.base.structure import BasePageStructure


class VMRStatusPageStructure(BasePageStructure):
    CONFIRM = "//h4[text()='Статус вашего заказа']"
    STATUS_FORM = "//*[@id='status_form']"
    ORDER_FORM = "//*[@class='well']"
