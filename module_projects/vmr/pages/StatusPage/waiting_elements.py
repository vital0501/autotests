import time

from autotests_base.conditions.element_conditions import exist, text_not_none
from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseStatusWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class StatusInputWA(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def has_error(self):
        if 'error' in SubWaitingElement(self, "/parent::*/parent::*").get_attribute('class'):
            return True
        return False


class StatusEmailWA(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def has_error(self):
        for i in range(0, self._timeout):
            if 'error' in SubWaitingElement(self, "/parent::*/parent::*").get_attribute('class'):
                return True
            time.sleep(1)
        return False


class StatusInputFormWA(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def click_get_status_button(self):
        SubWaitingElement(self, "//input[@type='submit']").assure(exist).click()

    def get_text_error(self):
        return SubWaitingElement(self, '//p').assure(text_not_none).text

    def get_number_input_element(self) -> StatusInputWA:
        return StatusInputWA(self._driver, "//*[@id='id']", self._selector_type, self._timeout, self._is_collection_item)

    def set_number(self, number):
        self.get_number_input_element().send_keys(number)

    def get_email_input_element(self) -> StatusEmailWA:
        return StatusEmailWA(self._driver, "//*[@id='email']", self._selector_type, self._timeout, self._is_collection_item)

    def set_email(self, email):
        self.get_email_input_element().send_keys(email)


class StatusOrderFormWA(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_number(self):
        return int(SubWaitingElement(self, '/h4').assure(exist).assure(text_not_none).text.replace("Заказ №", ""))