from module_projects.vmr.pages.BasePage.check import DataLayerCheck, VMRBaseCheck
from module_projects.vmr.pages.SuccessfulOrderPage.exception import PrivateOfficeOrderStatusBadException


class SuccessfulOrderCheck(VMRBaseCheck, DataLayerCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.pages.SuccessfulOrderPage.page.VMRSuccessfulOrderPage
        """
        super().__init__(page)
        self.page = page

    def check_order_status_eq(self, status, raise_error=False):
        _error_text = ''
        try:
            result = status == self.page.get_status()
            _error_text = "Статус заказа {0}" .format(status)
        except (PrivateOfficeOrderStatusBadException) as e:
            result = False
            _error_text = str(e)

        if not result and raise_error:
            raise PrivateOfficeOrderStatusBadException("Не совпадает статус заказа. Ожидаем: {0}, Видим: {1}"
                                                       .format(status, self.page.get_status()))
        return result