from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class SuccessfulOrderPageException(VMRBasePageException):
    pass


class SuccessfulPageDataLayerSectionException(SuccessfulOrderPageException):
    pass


class SuccessfulPageDataLayerSectionExistException(SuccessfulOrderPageException):
    pass


class SuccessfulDataLayerException(SuccessfulOrderPageException):
    pass

class PrivateOfficeOrderStatusBadException(SuccessfulOrderPageException):
    pass