from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import exist, text_not_none, not_exist, is_displayed
from module_projects.vmr.pages.BasePage.page import VMRBasePage
from module_projects.vmr.pages.SuccessfulOrderPage.check import SuccessfulOrderCheck
from module_projects.vmr.pages.SuccessfulOrderPage.structure import VMRSuccessfulOrderPageStructure
from module_projects.vmr.pages.SuccessfulOrderPage.waiting_elements import SuccessPopUpOrderWA


class VMRSuccessfulOrderPage(VMRBasePage):
    page_name = 'VMRSuccessfulorder'

    @staticmethod
    def get_structure():
        return VMRSuccessfulOrderPageStructure()

    def _get_check(self):
        """

        :rtype: SuccessfulOrderCheck
        """
        return SuccessfulOrderCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], url, project_url, timeout=10, confirm=True, init=False):
        super().__init__(driver, url, project_url, timeout, confirm=confirm, init=init)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_order_num(self) -> int:
        return int(self.search(self.structure.ORDER_NUM).assure(exist, timeout=20).assure(text_not_none).text)

    def close_pop_up(self):
        self.search(self.structure.CLOSE_BUTTON).assure(exist).click()
        self.search(self.structure.POP_UP, element_type=SuccessPopUpOrderWA).assure(not_exist)

    def get_status(self):
        return str(self.search(self.structure.ORDER_STATUS).assure(exist).assure(is_displayed).assure(text_not_none).text).replace("Статус: ", "")

    def click_cancel_order_button(self):
        self.search(self.structure.CANCEL_ORDER_BUTTON).assure(exist).assure(is_displayed).click()