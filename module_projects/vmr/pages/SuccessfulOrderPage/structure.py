from module_projects.base.structure import BasePageStructure


class VMRSuccessfulOrderPageStructure(BasePageStructure):
    CONFIRM = "//h4[contains(text(),'Заказ')]"

    POP_UP = "//*[@class='fancybox-skin']"
    ORDER_NUM = "//div[@class='order-id hidden-xs']/p/span"
    CLOSE_BUTTON = "//a[@title='Закрыть']"
    ORDER_STATUS = "//h4[contains(text(), 'Заказ')]/small"
    CANCEL_ORDER_BUTTON = "//a[contains(text(), 'Отменить')]"