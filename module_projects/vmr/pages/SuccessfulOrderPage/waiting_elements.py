from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BaseSuccessfulorderWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class SuccessPopUpOrderWA(BaseSuccessfulorderWE):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False, main_page=None):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)
        self._main_page = main_page

    def get_order_num(self):
        return SubWaitingElement(self, "//div[@class='order-id hidden-xs']/p/span").text