from typing import Union

from selenium import webdriver

from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRBreadCrumbs.structure import VMRBreadCrumbsStructure
from module_projects.vmr.parts.VMRBreadCrumbs.waiting_elements import BreadCrumbsElement


class VMRBreadCrumbs(BasePage):
    page_name = 'VMRBreadCrumbs'

    @staticmethod
    def get_structure():
        return VMRBreadCrumbsStructure

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)

    def get_breadcrumbs(self):
        """

        :rtype: WaitingElementCollection[BreadCrumbsElement]
        """
        return self.search_all(self.structure.ELEMENTS, elements_type=BreadCrumbsElement)