from module_projects.base.structure import BasePageStructure


class VMRBreadCrumbsStructure(BasePageStructure):
    CONFIRM = '//*[@class="breadcrumb"]'
    ELEMENTS = '//*[@class="breadcrumb"]/a|//*[@class="breadcrumb"]/li'
