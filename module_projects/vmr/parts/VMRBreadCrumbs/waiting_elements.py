from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class BreadCrumbsElement(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_name(self):
        return SubWaitingElement(self, '').text if SubWaitingElement(self, '').text else SubWaitingElement(self, '/a').text

    def get_tkey(self):
        href = self.get_attribute("href") if self.get_attribute("href") else SubWaitingElement(self, '/a').get_attribute("href")
        return None if href is None else href.split("/")[-1]