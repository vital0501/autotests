from module_projects.base.check import BaseCheck
from module_projects.vmr.pages.CatalogPage.exception import CatalogPageCatalogMenuException


class CatalogMenuCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.parts.VMRCatalogMenu.page.VMRCatalogMenu
        """
        super().__init__(page)
        self.page = page

    def check_min_catalogs(self, raise_error=False):
        result = len(self.page.get_catalogs_elements()) > 5
        if not result and raise_error:
            raise CatalogPageCatalogMenuException(
                "Некорретное колчество каталогов в боковом меню. Ожидаем \" >5 \"."
                " Видим: {0}".format(len(self.page.get_catalogs_elements())))
        return result