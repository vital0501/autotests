class VMRCatalogMenuException(Exception):
    pass


class CatalogMenuNotFoundException(VMRCatalogMenuException):
    pass