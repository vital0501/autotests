from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import exist, is_displayed
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection

from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRCatalogMenu.check import CatalogMenuCheck

from module_projects.vmr.parts.VMRCatalogMenu.exception import CatalogMenuNotFoundException
from module_projects.vmr.parts.VMRCatalogMenu.structure import VMRCatalogMenuStructure
from module_projects.vmr.parts.VMRCatalogMenu.waiting_elements import CatalogMenuItemWE


class VMRCatalogMenu(BasePage):
    page_name = 'VMRCatalogMenu'

    @staticmethod
    def get_structure():
        return VMRCatalogMenuStructure()

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)
        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def _get_check(self):
        """

        :rtype: CatalogMenuCheck
        """
        return CatalogMenuCheck(self)

    def go_to_catalog_by_index(self, index):
        """

        :rtype: module_projects.vmr.pages.CatalogPage.page.VMRCatalogPage
        """
        self.get_catalogs_elements()[index].click()
        from module_projects.vmr.pages.CatalogPage.page import VMRCatalogPage
        return self.create_page_object(VMRCatalogPage)

    def go_to_catalog_by_name(self, name):
        """

        :rtype: module_projects.vmr.pages.CatalogPage.page.VMRCatalogPage
        """
        for item in self.get_catalogs_elements():
            if item.get_name() == name:
                item.click()
                from module_projects.vmr.pages.CatalogPage.page import VMRCatalogPage
                return self.create_page_object(VMRCatalogPage)
        else:
            raise CatalogMenuNotFoundException("Не найден каталог с именем \"{0}\"" .format(name))

    def get_catalogs_elements(self):
        """

        :rtype: WaitingElementCollection[CatalogMenuItemWE]
        """
        return self.search_all(self.structure.CATALOGS, elements_type=CatalogMenuItemWE)

    def get_catalog_element(self, index: int) -> CatalogMenuItemWE:
        return self.get_catalogs_elements()[index]

    def go_to_constructor(self):
        """

        :rtype: module_projects.vmr.pages.ConstructorPage.page.VMRConstructorPage
        """
        self.get_constructor_element().click()
        from module_projects.vmr.pages.ConstructorPage.page import VMRConstructorPage
        return self.create_page_object(VMRConstructorPage)

    def get_constructor_element(self):
        return self.search(self.structure.CONSTRUCTOR).assure(exist).assure(is_displayed)