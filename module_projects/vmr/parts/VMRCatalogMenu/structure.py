from module_projects.base.structure import BasePageStructure


class VMRCatalogMenuStructure(BasePageStructure):
    CONFIRM = '//*[@class="left-menu"]'

    CATALOGS = "//nav[@class='left-menu']//li[not(contains(@class, 'constructor'))]/a"

    CONSTRUCTOR = "//nav[@class='left-menu']//li[(contains(@class, 'constructor'))]/a"

