from module_projects.base.check import BaseCheck
from module_projects.vmr.parts.VMRDropDownMenu.exception import DropDownMenuNoneException


class DropDownMenuCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.parts.VMRDropDownMenu.page.VMRDropDownMenu
        """
        super().__init__(page)
        self.page = page

    def check_menu_by_name(self, retail, name, raise_error=False):

        result = self.page.menu_element.find_in_menu_structure(name=name) is not None

        if not result and raise_error:
            raise DropDownMenuNoneException('В "{0}" отсутствуют "Услуги"!'.format(retail))
        return result
