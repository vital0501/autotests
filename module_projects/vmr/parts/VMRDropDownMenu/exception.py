from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class DropDownMenuException(VMRBasePageException):
    pass


class DropDownMenuNoneException(VMRBasePageException):
    pass
