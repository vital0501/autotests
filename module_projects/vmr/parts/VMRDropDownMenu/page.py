from typing import Union

import time
from selenium import webdriver

from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRDropDownMenu.structure import VMRDropDownMenuStructure
from module_projects.vmr.parts.VMRDropDownMenu.check import DropDownMenuCheck
from module_projects.vmr.parts.VMRDropDownMenu.waiting_elements import MenuWE


class VMRDropDownMenu(BasePage):
    page_name = 'VMRDropDownMenu'

    @staticmethod
    def get_structure():
        return VMRDropDownMenuStructure

    def _get_check(self):
        """

        :rtype: DropDownMenuCheck
        """
        return DropDownMenuCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

        self.menu_element = self.get_menu_element()

    def go_to_menu_item_by_name(self, name, main_name=None, sub_menu_name=None):
        element = self.menu_element.find_in_menu_structure(name=name, main_name=main_name, sub_menu_name=sub_menu_name)
        print('Find:', element)
        self.menu_element.click_element_by_element(element=element)

        if element.element_type == 'catalog':
            from module_projects.vmr.pages.CatalogPage.page import VMRCatalogPage
            return self.create_page_object(VMRCatalogPage)
        elif element.element_type == 'constructor' and element.name not in ['Волейбольная форма', 'Баскетбольная форма', 'Футбольная форма', 'Спортивная форма']:
            from module_projects.vmr.pages.ConstructorPage.page import VMRConstructorPage
            return self.create_page_object(VMRConstructorPage)
        elif element.element_type == 'constructor' and element.name in ['Волейбольная форма', 'Баскетбольная форма', 'Футбольная форма']:
            from module_projects.vmr.pages.SportProductPage.page import VMRSportProductPage
            return self.create_page_object(VMRSportProductPage)
        elif element.element_type == 'constructor' and element.name == 'Спортивная форма':
            from module_projects.vmr.pages.SportPage.page import VMRSportPage
            return self.create_page_object(VMRSportPage)
        elif element.element_type == 'pixelmart':
            for item in range(0, 10):
                if len(self.driver.window_handles) == 2:
                    break
                else:
                    time.sleep(1)
            else:
                from module_projects.base.exception import BaseModuleException
                raise BaseModuleException('Не смогли открыть внешнюю ссылку в отдельной вкладке')

            self.driver.switch_to.window(self.driver.window_handles[-1])
            from project.pixelmart.pages.Catalog.page import CatalogPage as PixelmartPage
            return PixelmartPage(driver=self.driver, main_href='https://pixelmart.ru', timeout=10)

    def get_menu_structure(self, type_filter=None):
        return self.menu_element.get_menu_structure(type_filter=type_filter)

    def get_menu_element(self):
        """

        :rtype: MenuWE
        """
        return self.search(self.structure.MENU, element_type=MenuWE)




