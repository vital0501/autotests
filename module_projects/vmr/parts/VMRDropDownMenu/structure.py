from module_projects.base.structure import BasePageStructure


class VMRDropDownMenuStructure(BasePageStructure):
    CONFIRM = '//*[@class="row main-menu"]/ul'
    MENU = "//section[@class='row main-menu']/ul"
