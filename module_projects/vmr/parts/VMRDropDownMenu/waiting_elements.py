from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_conditions import is_displayed
from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement
import lxml.html

from module_projects.vmr.parts.VMRDropDownMenu.exception import DropDownMenuException


class StructureElement(object):
    def __init__(self, name, href, element_type, main_name, sub_menu_name, sub_menu, main_menu, sub_menu_index=None):
        self.name = name
        self.href = href
        self.element_type = element_type
        self.main_name = main_name
        self.sub_menu_name = sub_menu_name
        self.sub_menu = sub_menu
        self.main_menu = main_menu
        self.sub_menu_index = sub_menu_index

    def to_json(self):
        return {
            'name': self.name,
            'href': self.href,
            'element_type': self.element_type,
            'main_name': self.main_name,
            'sub_menu_name': self.sub_menu_name,
            'sub_menu': self.sub_menu,
            'main_menu': self.main_menu,
            'sub_menu_index': self.sub_menu_index
        }

    def __str__(self):
        return str(self.to_json())


class MenuWE(WaitingElement):
    MAIN_XPATH = '//li/a[contains(text(),"{0}")]'
    MAIN_SUB_XPATH = "//li[child::a[contains(text(),'{0}')]]"
    SUB_PATH = MAIN_SUB_XPATH + "//a[contains(text(), '{1}')]"
    CATALOG_SUB_PATH = MAIN_SUB_XPATH + "//div[@data-child-index='{1}']//a[contains(text(), '{2}')]"
    CATALOG_PATH = SUB_PATH

    def _init_menu_structure(self):
        result = []

        lxml_text = self.get_attribute('innerHTML').replace("\n", '')
        main_lxml_element = lxml.html.fromstring(lxml_text)

        _main_menu_elements = main_lxml_element.findall('./li')

        for lxml_main_element in _main_menu_elements:
            name = lxml_main_element.find('./a').text.strip()
            try:
                href = lxml_main_element.find('./a').attrib['href']
            except KeyError:
                href = None

            main_menu_element = StructureElement(name=name,
                                                 href=href,
                                                 element_type='constructor' if name == 'Свой дизайн' else 'catalog',
                                                 main_name=None,
                                                 sub_menu_name=None,
                                                 sub_menu=False,
                                                 main_menu=True)

            result.append(main_menu_element)

            _tmp_elements = lxml_main_element.findall('./div/div')

            if len(_tmp_elements) > 1:
                lxml_catalog_elements_collection = _tmp_elements[-1].findall('./div')

                for lxml_collection in lxml_catalog_elements_collection:
                    collection_index = lxml_collection.attrib['data-child-index']
                    lxml_sub_element = _tmp_elements[0].find('.//li[@data-parent-index="{0}"]'.format(collection_index))

                    try:
                        sub_href = lxml_sub_element.find('./a').attrib['href']
                    except KeyError:
                        sub_href = None

                    sub_element = StructureElement(name=lxml_sub_element.find('./a').text.strip(),
                                                   href=sub_href,
                                                   element_type=main_menu_element.element_type,
                                                   main_name=main_menu_element.name,
                                                   sub_menu_name=None,
                                                   sub_menu=True,
                                                   main_menu=False)

                    result.append(sub_element)

                    lxml_catalog_elements = lxml_collection.findall('.//a[@href]')

                    for lxml_catalog_element in lxml_catalog_elements:
                        catalog_element = StructureElement(name=lxml_catalog_element.text.strip(),
                                                           href=lxml_catalog_element.attrib['href'],
                                                           element_type=main_menu_element.element_type,
                                                           main_name=main_menu_element.name,
                                                           sub_menu_name=sub_element.name,
                                                           sub_menu=False,
                                                           main_menu=False,
                                                           sub_menu_index=collection_index)

                        result.append(catalog_element)
            else:
                lxml_catalog_elements = _tmp_elements[-1].findall('.//a[@href]')

                for lxml_catalog_element in lxml_catalog_elements:
                    catalog_element = StructureElement(name=lxml_catalog_element.text.strip(),
                                                       href=lxml_catalog_element.attrib['href'],
                                                       element_type=main_menu_element.element_type if 'pixelmart' not in lxml_catalog_element.attrib['href'] else 'pixelmart',
                                                       main_name=main_menu_element.name,
                                                       sub_menu_name=None,
                                                       sub_menu=False,
                                                       main_menu=False)

                    result.append(catalog_element)

        return result

    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        """

        :type menu_structure: list[StructureElement]
        """
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)
        self._menu_structure = self._init_menu_structure()

    def get_menu_structure(self, type_filter=None):
        structure = self._menu_structure
        if type_filter is None:
            return structure
        else:
            if not isinstance(type_filter, list) and not isinstance(type_filter, dict):
                type_filter = [type_filter, ]

            return [item for item in structure if (item.element_type in type_filter)]

    def get_main_menu_element(self, name):
        return SubWaitingElement(self, self.MAIN_XPATH.format(name))

    def get_sub_menu_element(self, main_name, name):
        return SubWaitingElement(self, self.SUB_PATH.format(main_name, name))

    def get_catalog_menu_element(self, main_name, sub_menu_name, name):
        if sub_menu_name:
            element = self.find_in_menu_structure(main_name=main_name, sub_menu_name=sub_menu_name, name=name)
            return SubWaitingElement(self, self.CATALOG_SUB_PATH.format(main_name, element.sub_menu_index, name))
        else:
            return SubWaitingElement(self, self.CATALOG_PATH.format(main_name, name))

    def click_element_by_element(self, element: StructureElement):
        main_element = self.get_main_menu_element(name=element.main_name if element.main_name else element.name)
        sub_element = self.get_sub_menu_element(main_name=element.main_name,
                                                name=element.sub_menu_name if element.sub_menu_name else element.name) if element.sub_menu_name or element.sub_menu else None

        target_element = self.get_catalog_menu_element(main_name=element.main_name,
                                                sub_menu_name=element.sub_menu_name,
                                                name=element.name)

        for item in range(0, 10):
            stage = "главного меню"
            if element.main_menu:
                main_element.click()
                return
            elif element.main_name:
                main_element.mouse_over_element()

                try:
                    if element.sub_menu_name:
                        stage = "сабменю"
                        sub_element.assure(is_displayed)
                        sub_element.mouse_over_element()
                    elif element.sub_menu:
                        sub_element.click()
                        return
                    target_element.assure(is_displayed).click()
                    break
                except TimeoutException:
                    continue
        else:
            raise DropDownMenuException('Не показывается пункт меню на этапе {0}'.format(stage))

    def find_in_menu_structure(self, **kwargs) -> StructureElement:
        for item in self._menu_structure:
            for key in kwargs:
                if not hasattr(item, key):
                    break
                elif getattr(item, key) != kwargs[key]:
                    break
            else:
                return item
        return None



