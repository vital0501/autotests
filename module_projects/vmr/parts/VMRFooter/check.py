from module_projects.base.check import BaseCheck


class FooterCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.parts.VMRFooter.page.VMRFooter
        """
        super().__init__(page)
        self.page = page