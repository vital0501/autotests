from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class FooterException(VMRBasePageException):
    pass


class FooterFormMsgNotFoundException(FooterException):
    pass


class FooterPhoneIncorrectedException(FooterException):
    pass


class FooterIosAppException(FooterException):
    pass


class FooterAndroidAppException(FooterException):
    pass


class FooterSubscribeNotFoundException(FooterException):
    pass


class FooterSubscribeErrorMsgException(FooterException):
    pass