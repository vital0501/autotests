from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import exist, is_displayed, is_enabled
from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRFooter.check import FooterCheck
from module_projects.vmr.parts.VMRFooter.structure import VMRFooterStructure
from module_projects.vmr.parts.VMRFooter.waiting_elements import TroubleWaitingElement
from module_projects.vmr.parts.VMRFooterApp.page import VMRFooterApp
from module_projects.vmr.parts.VMRLowerMenu.page import VMRLowerMenu
from module_projects.vmr.parts.VMRSocialNetwork.page import VMRSocialNetwork


class VMRFooter(BasePage):
    page_name = 'VMRFooter'

    @staticmethod
    def get_structure():
        return VMRFooterStructure()

    def _get_check(self):
        """

        :rtype: FooterCheck
        """
        return FooterCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome],
                 project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)
        self.LOWER_MENU = VMRLowerMenu(driver, project_url, timeout)
        self.APP = VMRFooterApp(driver, project_url, timeout)
        self.SOCIAL_NETWORK = VMRSocialNetwork(driver, project_url, timeout)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_trouble_element(self):
        return self.search(self.structure.TROUBLE, element_type=TroubleWaitingElement)

    def click_subscribe_button(self):
        self.search(self.structure.SUBSCRIBE_BUTTON).assure(exist)

    def close_email_error_message(self):
        self.search(self.structure.CLOSE_EMAIL_FORMAT_ERROR_MESSAGE).click()

    def enter_subscribe_email(self, text):
        self.search(self.structure.SUBSCRIBE_EMAIL).assure(exist).assure(is_enabled).clear()
        self.search(self.structure.SUBSCRIBE_EMAIL).assure(exist).assure(is_enabled).send_keys(text)

    def get_email_confirm_message_element(self):
        return self.search(self.structure.CLOSE_EMAIL_SUBSCRIBE_CONFIRM_MESSAGE).assure(exist).assure(is_displayed)

    def send_trouble_msg(self, text, email):
        self.get_trouble_element().text_input().assure(exist).assure(is_displayed).clear()
        self.get_trouble_element().text_input().assure(exist).assure(is_displayed).send_keys(text)
        self.get_trouble_element().email_input().assure(exist).assure(is_displayed).clear()
        self.get_trouble_element().email_input().assure(exist).assure(is_displayed).send_keys(email)
        self.get_trouble_element().button().click()
