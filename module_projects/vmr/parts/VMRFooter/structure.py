from module_projects.base.structure import BasePageStructure


class VMRFooterStructure(BasePageStructure):
    CONFIRM = "//*[@class='footer-menu']"

    TROUBLE = "//*[@id='trouble']"
    SUBSCRIBE_BUTTON = "//*[@id='client_subscribe']/button"
    CLOSE_EMAIL_FORMAT_ERROR_MESSAGE = "//*[@class='modal fade vm-modal in']//button"
    SUBSCRIBE_EMAIL = "//*[@id='client_subscribe']//input[2]"
    CLOSE_EMAIL_SUBSCRIBE_CONFIRM_MESSAGE = "//*[@id='empty']//span"