from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class TroubleWaitingElement(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def text_input(self):
        return SubWaitingElement(self, '//*[@id="text1"]')

    def email_input(self):
        return SubWaitingElement(self, '//*[@id="email5"]')

    def button(self):
        return SubWaitingElement(self, '//*[@type="submit"]')

    def close(self):
        return SubWaitingElement(self, '//button')