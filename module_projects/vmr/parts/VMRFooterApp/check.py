from module_projects.base.check import BaseCheck


class FooterAppCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.parts.VMRFooterApp.page.VMRFooterApp
        """
        super().__init__(page)
        self.page = page