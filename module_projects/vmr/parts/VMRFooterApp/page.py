from autotests_base.conditions.element_conditions import exist
from module_projects.vmr.parts.VMRFooterApp.structure import VMRFooterAppStructure
import settings
from autotests_base.pages.base_page.page import BasePageObject


class VMRFooterApp(BasePageObject):
    def _error_page_name(self) -> str:
        return 'VMRFooterAppStructure error'

    def _get_page_load_timeout(self) -> int:
        return settings.TIMEOUT

    def _load_structure(self):
        return VMRFooterAppStructure

    def __init__(self, driver, main_href, timeout):
        super().__init__(driver, main_href, timeout, confirm=False)

    def click_ios_app(self):
        self.search(self.structure.IOS_APP).assure(exist).click()

    def click_android_app(self):
        self.search(self.structure.ANDROID_APP).assure(exist).click()