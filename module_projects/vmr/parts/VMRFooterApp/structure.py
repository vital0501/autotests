from autotests_base.pages.base_page.structure import BaseStructure


class VMRFooterAppStructure(BaseStructure):
    IOS_APP = "//a[ancestor::div[@class='ios-button hidden-xs']]"
    ANDROID_APP = "//a[ancestor::div[@class='android-button hidden-xs']]"