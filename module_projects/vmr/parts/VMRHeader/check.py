from selenium.common.exceptions import TimeoutException

from module_projects.base.check import BaseCheck
from module_projects.vmr.parts.VMRHeader.exception import HeaderCityException, HeaderFavoritesCountException


class HeaderCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.parts.VMRHeader.page.VMRHeader
        """
        super().__init__(page)
        self.page = page

    def check_city_eq(self, city, raise_error=False):
        page_city = self.page.get_city_name()
        result = page_city == city
        if not result and raise_error:
            raise HeaderCityException("Город не совпадает. Ожидаем: {0}, Видим: {1}" .format(city, page_city))
        return result

    def check_value_favorites_eq(self, value, raise_error=False):
        try:
            result = value == self.page.get_favorites_count()
        except TimeoutException:
            result = False
        if not result and raise_error:
            raise HeaderFavoritesCountException("Некорректное значение избранного. Ожидаем: {0}. Видим: {1}"
                                                .format(value, self.page.get_favorites_count()))
        return result
