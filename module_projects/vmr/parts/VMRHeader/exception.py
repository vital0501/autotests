from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class HeaderException(VMRBasePageException):
    pass


class HeaderCountValueException(HeaderException):
    pass


class HeaderCityException(HeaderException):
    pass


class HeaderFavoritesCountException(HeaderException):
    pass


class HeaderOpenHelpPageException(HeaderException):
    pass


class HeaderOpenStatusPageException(HeaderException):
    pass


class HeaderOpenDeliveryPageException(HeaderException):
    pass


class HeaderCityNotFoundExceptions(HeaderException):
    pass


class HeaderPhoneIncorrectedException(HeaderException):
    pass
