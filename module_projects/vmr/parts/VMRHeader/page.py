from typing import Union

import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.keys import Keys

from autotests_base.conditions.element_collection_conditions import len_more
from autotests_base.conditions.element_conditions import exist, is_displayed
from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRDropDownMenu.page import VMRDropDownMenu
from module_projects.vmr.parts.VMRHeader.check import HeaderCheck
from module_projects.vmr.parts.VMRHeader.exception import HeaderCountValueException, HeaderCityNotFoundExceptions
from module_projects.vmr.parts.VMRHeader.structure import VMRHeaderStructure
from module_projects.vmr.parts.VMRLogin.page import VMRLogin
from module_projects.vmr.parts.VMRRegistration.page import VMRRegistration
from module_projects.vmr.parts.VMRSearch.page import VMRSearch
from module_projects.vmr.parts.VMRSocialNetwork.page import VMRSocialNetwork
from module_projects.vmr.parts.VMRUpperMenu.page import VMRUpperMenu
from module_projects.vmr.parts.VMRHeader.waiting_elements import LinkWaitingElement, SubLinkWaitingElement, DropDownCityWE

class VMRHeader(BasePage):
    page_name = 'VMRHeader'

    @staticmethod
    def get_structure():
        return VMRHeaderStructure()

    def _get_check(self):
        """

        :rtype: HeaderCheck
        """
        return HeaderCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome],
                 project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)
        self.SEARCH = VMRSearch(driver, project_url, timeout)
        self.DROP_DOWN_MENU = VMRDropDownMenu(driver, project_url, timeout)
        self.UPPER_MENU = VMRUpperMenu(driver, project_url, timeout)
        self.LOGIN = VMRLogin(driver, project_url, timeout)
        self.REGISTRATION = VMRRegistration(driver, project_url, timeout)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_cart_count(self) -> int:
        try:
            return int(self.get_cart_count_element().assure(exist).text)
        except (ValueError, NoSuchElementException, TimeoutException):
            return 0

    def get_cart_count_element(self):
        return self.search(self.structure.CART_COUNT)

    def get_go_to_cart_element(self):
        return self.search(self.structure.CART_BUTTON)

    def go_to_cart(self):
        """

        :rtype: module_projects.vmr.pages.CartPage.page.VMRCartPage
        """
        self.get_go_to_cart_element().click()
        from module_projects.vmr.pages.CartPage.page import VMRCartPage
        return self.create_page_object(VMRCartPage)

    def wait_count_eq(self, value, timeout=4):
        for item in range(0, timeout * 2):
            if self.get_cart_count() == value:
                break
            else:
                time.sleep(0.5)
        else:
            raise HeaderCountValueException("Значение счетчика корзины не равно {0}" .format(value))

    def get_city_name(self):
        return self.get_city_string_element().text.strip()

    def get_city_string_element(self):
        return self.search(self.structure.CITY_STRING)

    def get_city_phone_element(self):
        return self.search(self.structure.CITY_PHONE).assure(exist).assure(is_displayed)

    def set_city_by_input(self, city: str) -> None:
        self.input_city(city, key=Keys.ENTER)

    def search_product(self, text):
        """

        :rtype: module_projects.vmr.pages.SearchPage.page.VMRSearchPage
        """
        self.get_search_input_element().clear()
        self.get_search_input_element().send_keys(text, Keys.ENTER)
        from module_projects.vmr.pages.SearchPage.page import VMRSearchPage
        return self.create_page_object(VMRSearchPage)

    def get_search_input_element(self):
        return self.search(self.structure.SEARCH_INPUT).assure(exist).assure(is_displayed)

    def get_favorites_count(self) -> int:
        count_element = self.get_count_label_element()
        if count_element.text == '':
            return 0
        else:
            return int(count_element.text)

    def get_count_label_element(self):
        return self.search(self.structure.FAVORITES_COUNT)

    def go_to_favorite_page(self):
        """

        :rtype: module_projects.vmr.pages.FavoritesPage.page.VMRFavoritesPage
        """
        self.get_favorite_button_element().click()
        from module_projects.vmr.pages.FavoritesPage.page import VMRFavoritesPage
        return self.create_page_object(VMRFavoritesPage)

    def get_favorite_button_element(self):
        return self.search(self.structure.FAVORITES_BUTTON).assure(exist).assure(is_displayed)

    def get_submenu_holder_element(self):
        """

        :rtype: WaitingElementCollection[LinkWaitingElement]
        """
        return self.search(self.structure.SUBMENU_HOLDER).assure(exist).assure(is_displayed)

    def go_to_link(self, link, get_name):
        """

        :rtype: module_projects.vmr.pages.HelpPage.page.VMRHelpPage
        """
        page = None

        if get_name() == 'Помощь':
            from module_projects.vmr.pages.HelpPage.page import VMRHelpPage
            page = VMRHelpPage
        elif get_name() == 'Доставка':
            from module_projects.vmr.pages.DeliveryPage.page import VMRDeliveryPage
            page = VMRDeliveryPage
        elif get_name() == 'Где мой заказ?':
            from module_projects.vmr.pages.StatusPage.page import VMRStatusPage
            page = VMRStatusPage

        if page is not None:
            link.click()
        else:
            return None
        return self.create_page_object(page)

    def get_links_elements(self):
        """

        :rtype: WaitingElementCollection[LinkWaitingElement]
        """
        return self.search_all(self.structure.LINKS, elements_type=LinkWaitingElement)

    def get_sub_links_elements(self):
        """

        :rtype: WaitingElementCollection[SubLinkWaitingElement]
        """
        return self.search_all(self.structure.SUBMENU_LINKS, elements_type=SubLinkWaitingElement)

    def get_pop_up(self):
        self.search(self.structure.CITY_STRING).assure(exist).assure(is_displayed).click()
        self.search(self.structure.LOCATION_COUNTRY_SELECT).assure(exist).assure(is_displayed)

    def set_city_drop_down_list(self, search_world, city=None):
        self.input_city(search_world)
        if city:
            for item in self.get_get_available_city_elements().assure(len_more, 0):
                if item.get_name() == city:
                    item.click()
                    break
            else:
                raise HeaderCityNotFoundExceptions("Не найден город '{0}'" .format(city if city else search_world))
        else:
            self.get_get_available_city_elements().assure(len_more, 0)[0].assure(is_displayed).click()

    def get_get_available_city_elements(self):
        """

        :rtype: WaitingElementCollection[DropDownCityWE]
        """
        return self.search_all(self.structure.LOCATION_CITY_PAC, elements_type=DropDownCityWE).assure(len_more, 0)

    def input_city(self, city: str, key: object = None) -> None:
        self.search(self.structure.LOCATION_ADDRESS_CITY).assure(exist).assure(is_displayed).click()
        self.search(self.structure.LOCATION_ADDRESS_CITY).assure(exist).assure(is_displayed).clear()

        element = self.search(self.structure.LOCATION_ADDRESS_CITY).assure(is_displayed)

        for letter in city:
            element.send_keys(letter)

        if key:
            self.get_get_available_city_elements().assure(len_more,0)[0].assure(exist).assure(is_displayed)
            element.send_keys(key)

        if city.lower() not in self.search(self.structure.LOCATION_ADDRESS_CITY).get_attribute('value').lower():
            self.input_city(city)

    def set_city_by_list(self, _city: str) -> None:
        for city in self.search_all(self.structure.LOCATION_CITY_LIST):
            if city.text == _city:
                city.click()
