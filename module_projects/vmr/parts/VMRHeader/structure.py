from module_projects.base.structure import BasePageStructure


class VMRHeaderStructure(BasePageStructure):
    CONFIRM = '//*[@class="header"]'

    # XPATH относящиеся к функциональности корзины
    CART_COUNT = "//*[@class='cart-block__counter']"
    CART_BUTTON = '//*[@class="cart-block"]//*[@href="/cart"]'

    CITY = "//*[@class='top-header__link location__city']"
    CITY_STRING = '//*[@data-autotest-id="header_city_text"]'
    CITY_PHONE = '//*[@class="top-header__link location__phone"]'
    LOCATION_COUNTRY_SELECT = "//*[@id='country_select']"
    LOCATION_ADDRESS_CITY = "//input[@id='address_city_fias']"
    LOCATION_CITY_PAC = '//*[contains(@class, "ui-autocomplete")]/li'
    LOCATION_CITY_LIST = "//div[contains(@class, 'city-list')]//li/a"

    FAVORITES_BUTTON = "//a[@href='/favorites']/span"
    FAVORITES_ICON = "//a[@href='/favorites']/i"
    FAVORITES_COUNT = "//*[contains(@class,'favorites-block__counter')]"

    SEARCH_INPUT = '//*[@data-autotest-id="search_input"][not(ancestor::div[@class="left-mobile-menu"])]'

    SUBMENU_HOLDER = "//*[@class='top-header__link top-header-submenu__title-link']"
    LINKS = "//div[@class='info-menu']//*[contains(@class, 'top-header__link')]"
    SUBMENU_LINKS = "//*[@class='top-header-submenu-holder']//a"