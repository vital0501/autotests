from autotests_base.conditions.element_conditions import exist
from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement


class LinkWaitingElement(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)
        self._name = None

    def get_name(self) -> str:
        return SubWaitingElement(self, "").assure(exist).text


class SubLinkWaitingElement(LinkWaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)
        self._name = None


class DropDownCityWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

    def get_name(self):
        return SubWaitingElement(self, '/b').text