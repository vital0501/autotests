from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import exist, is_displayed
from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRLeftMenuInfo.structure import VMRLeftMenuInfoStructure


class VMRLeftMenuInfo(BasePage):
    page_name = 'VMRLeftMenuInfo'

    @staticmethod
    def get_structure():
        return VMRLeftMenuInfoStructure

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)

    def go_to_about_company(self):
        """

        :rtype: module_projects.vmr.pages.AboutPage.page.VMRAboutPage
        """
        self.search(self.structure.ABOUT_COMPANY).assure(exist).assure(is_displayed).click()
        from module_projects.vmr.pages.AboutPage.page import VMRAboutPage
        return self.create_page_object(VMRAboutPage)

    def go_to_delivery_page(self):
        """

        :rtype: module_projects.vmr.pages.DeliveryPage.page.VMRDeliveryPage
        """
        self.search(self.structure.DELIVERY_PAGE).assure(exist).assure(is_displayed).click()
        from module_projects.vmr.pages.DeliveryPage.page import VMRDeliveryPage
        return self.create_page_object(VMRDeliveryPage)

    def go_to_reviews_page(self):
        """

        :rtype: module_projects.vmr.pages.ReviewsPage.page.ReviewsPage
        """
        self.search(self.structure.REVIEWS).assure(exist).assure(is_displayed).click()
        from module_projects.vmr.pages.ReviewsPage.page import VMRReviewsPage
        return self.create_page_object(VMRReviewsPage)

