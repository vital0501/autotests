from autotests_base.pages.base_page.structure import BaseStructure


class VMRLeftMenuInfoStructure(BaseStructure):
    CONFIRM = "//h4[text()='Информация']"
    ABOUT_COMPANY = "//a[contains(text(), 'О компании')][ancestor::nav]"
    DELIVERY_PAGE = "//a[contains(text(), 'Доставка')][ancestor::nav]"
    REVIEWS = "//a[contains(text(), 'Отзывы')]"