from selenium.common.exceptions import TimeoutException

from module_projects.base.check import BaseCheck
from module_projects.vmr.parts.VMRLogin.exception import VMRLoginFakeLoginException


class VMRLoginCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.parts.VMRLogin.page.VMRLogin
        """
        super().__init__(page)
        self.page = page

    def check_error_login_txt_eq(self, value, raise_error=False):
        try:
            result = value == self.page.get_error_login_msg_element().text
        except TimeoutException:
            result = False
        if not result and raise_error:
            raise VMRLoginFakeLoginException("Некорректное сообщение об ошибочной авторизации. Ожидаем: {0}. Видим: {1}"
                                                .format(value, self.page.get_error_login_msg_element().text))
        return result