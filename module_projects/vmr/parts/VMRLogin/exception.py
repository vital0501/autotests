from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class VMRLoginFakeLoginException(VMRBasePageException):
    pass