from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import is_displayed, exist
from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRLogin.check import VMRLoginCheck
from module_projects.vmr.parts.VMRLogin.structure import VMRLoginStructure


class VMRLogin(BasePage):
    page_name = 'VMRLogin'

    @staticmethod
    def get_structure():
        return VMRLoginStructure

    def _get_check(self):
        """

        :rtype: LoginCheck
        """
        return VMRLoginCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def set_login(self, value):
        self.get_login_element().assure(is_displayed).clear()
        self.get_login_element().send_keys(value)

    def set_password(self, value):
        self.get_password_element().assure(is_displayed).clear()
        self.get_password_element().send_keys(value)

    def get_login_button_element(self):
        return self.search(self.structure.LOGIN_BUTTON)

    def get_login_element(self):
        return self.search(self.structure.LOGIN)

    def get_password_element(self):
        return self.search(self.structure.PASSWORD)

    def login(self, login, password):
        """

        :rtype: module_projects.vmr.pages.PrivateOfficePage.page.VMRPrivateOfficePage
        """
        self.set_login(login)
        self.set_password(password)
        self.get_login_button_element().assure(is_displayed).click()

        from module_projects.vmr.pages.PrivateOfficePage.page import VMRPrivateOfficePage
        return self.create_page_object(VMRPrivateOfficePage)

    def fake_login(self, login, password):
        self.set_login(login)
        self.set_password(password)
        self.get_login_button_element().assure(is_displayed).click()

    def get_error_login_msg_element(self):
        return self.search(self.structure.ERROR_LOGIN_MSG).assure(exist).assure(is_displayed)