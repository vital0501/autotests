from module_projects.base.structure import BasePageStructure


class VMRLoginStructure(BasePageStructure):
    CONFIRM = '//*[@id="login_popupLabel"]'

    LOGIN = '//*[@id="login1"]'
    PASSWORD = '//*[@id="pass1"]'
    LOGIN_BUTTON = "//*[@id='login_form']//button"
    ERROR_LOGIN_MSG = "//*[@id='login_form']//fieldset[1]//p"