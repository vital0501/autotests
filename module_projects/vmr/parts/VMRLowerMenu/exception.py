from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class LowerMenuException(VMRBasePageException):
    pass


class LowerMenuItemNotFound(LowerMenuException):
    pass