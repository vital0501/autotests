from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import exist, is_displayed
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRLowerMenu.exception import LowerMenuItemNotFound
from module_projects.vmr.parts.VMRLowerMenu.structure import VMRLowerMenuStructure
from module_projects.vmr.parts.VMRLowerMenu.waiting_elements import LowerMenuItemWe


class VMRLowerMenu(BasePage):
    page_name = 'VMRLowerMenu'

    @staticmethod
    def get_structure():
        return VMRLowerMenuStructure

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)
        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_lower_menu_elements(self):
        """

        :rtype: WaitingElementCollection[LowerMenuItemWe]
        """
        return self.search_all(self.structure.LOWER_MENU, elements_type=LowerMenuItemWe)

    def find_menu_item_by_name(self, name):
        for item in self.get_lower_menu_elements():
            if item.text == name:
                item.scroll_to_element()
                return item
        raise LowerMenuItemNotFound("Не найден пункт нижнего меню \"{0}\"" .format(name))

    def go_to_menu_item_by_name(self, name):
        from module_projects.vmr.pages.HowCreateOrderPage.page import VMRHowCreateOrderPage
        from module_projects.vmr.pages.DeliveryPage.page import VMRDeliveryPage
        from module_projects.vmr.pages.BrakPage.page import VMRBrakPage
        from module_projects.vmr.pages.StatusPage.page import VMRStatusPage
        from module_projects.vmr.pages.PaymentInfoPage.page import VMRPaymentinfoPage
        from module_projects.vmr.pages.AgreementPage.page import VMRAgreementPage
        from module_projects.vmr.pages.AboutPage.page import VMRAboutPage
        from module_projects.vmr.pages.ContactsPage.page import VMRContactsPage
        from module_projects.vmr.pages.ReviewsPage.page import VMRReviewsPage
        from module_projects.vmr.pages.ManufacturePage.page import VMRManufacturePage
        from module_projects.vmr.pages.OptPage.page import VMROptPage
        from module_projects.vmr.pages.SellPage.page import VMRSellPage
        from module_projects.vmr.pages.CouriersPage.page import VMRCouriersPage
        menu_page_structure = {
            "Как сделать заказ?": VMRHowCreateOrderPage,
            "Доставка": VMRDeliveryPage,
            "Обмен и возврат": VMRBrakPage,
            "Где мой заказ?": VMRStatusPage,
            "Способы оплаты": VMRPaymentinfoPage,
            "Пользовательское соглашение": VMRAgreementPage,
            "О нас": VMRAboutPage,
            "Контакты": VMRContactsPage,
            "Отзывы": VMRReviewsPage,
            "Наше производство": VMRManufacturePage,
            "Оптовым клиентам": VMROptPage,
            "Дизайнерам": VMRSellPage,
            "Партнерская программа": VMRSellPage,
            "Курьерским службам": VMRCouriersPage
        }

        if name not in menu_page_structure.keys():
            raise LowerMenuItemNotFound("Неопределена страница для открытия пункта \"{0}\"" .format(name))

        self.find_menu_item_by_name(name).assure(exist).assure(is_displayed).click()
        return self.create_page_object(menu_page_structure[name])

