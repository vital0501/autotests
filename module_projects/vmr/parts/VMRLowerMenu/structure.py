from module_projects.base.structure import BasePageStructure


class VMRLowerMenuStructure(BasePageStructure):
    CONFIRM = "//*[@class='footer-menu']"
    LOWER_MENU = "//*[@class='footer-menu']//a"
