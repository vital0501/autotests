from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import exist, is_displayed
from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRPRLeftMenu.structure import VMRPRLeftMenuStructure


class VMRPRLeftMenu(BasePage):
    page_name = 'VMRPRLeftMenu'

    @staticmethod
    def get_structure():
        return VMRPRLeftMenuStructure

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)

    def go_to_order_list(self):
        """

        :rtype: module_projects.vmr.pages.PrivateOfficeOrdersListPage.page.VMRPrivateOfficeOrdersListPage
        """
        self.get_orders_list_menu_element().click()
        from module_projects.vmr.pages.PrivateOfficeOrdersListPage.page import VMRPrivateOfficeOrdersListPage
        return self.create_page_object(VMRPrivateOfficeOrdersListPage)

    def get_orders_list_menu_element(self):
        return self.search(self.structure.ORDERS_LIST_ELEMENT)

    def go_to_questions(self):
        """

        :rtype: module_projects.vmr.pages.PrivateOfficeQuestionsPage.page.VMRPrivateOfficeQuestionsPage
        """
        self.get_questions_menu_element().click()
        from module_projects.vmr.pages.PrivateOfficeQuestionsPage.page import VMRPrivateOfficeQuestionsPage
        return self.create_page_object(VMRPrivateOfficeQuestionsPage)

    def get_questions_menu_element(self):
        return self.search(self.structure.QUESTIONS).assure(exist).assure(is_displayed)