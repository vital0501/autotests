from module_projects.base.structure import BasePageStructure


class VMRPRLeftMenuStructure(BasePageStructure):
    CONFIRM = "//h4[text()='Информация покупателя']"
    ORDERS_LIST_ELEMENT = '//*[@href="/privateoffice/myOrders"]'
    QUESTIONS = '//*[@href="/lk-help/faq"]'
    INSTRUCTIONS = '//*[@href="/lk-help/instruction"]'
    RECOMMENDATION = '//*[@href="/lk-help/recommendation"]'
    SUPPORT = '//*[@href="/support"]'
    MY_SHOP = '//*[@href="/privateoffice/shop/index"]'
    SALES_TOOLS = '//*[@href="/privateoffice/sale_tools"]'
    ADRESS_DELIVERY = '//*[@href="/privateoffice/myAddresses"]'