from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_collection_conditions import len_more
from autotests_base.conditions.element_conditions import is_displayed
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRRecommended.structure import VMRRecommendedProductsStructure
from module_projects.vmr.parts.VMRRecommended.waiting_elements import ProductRecommendedWE


class VMRRecommendedProducts(BasePage):
    page_name = 'VMRRecommendedProducts'

    @staticmethod
    def get_structure():
        return VMRRecommendedProductsStructure

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)
        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def click_next_recommended_product_button(self):
        self.get_next_recommended_button_element().assure(is_displayed).click()

    def get_next_recommended_button_element(self):
        return self.search(self.structure.RECOMMENDED_PRODUCT_NEXT)

    def get_products_elements(self):
        """

        :rtype: WaitingElementCollection[ProductRecommendedWE]
        """
        return self.search_all(self.structure.RECOMMENDED_PRODUCT, elements_type=ProductRecommendedWE)