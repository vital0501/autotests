from module_projects.base.structure import BasePageStructure


class VMRRecommendedProductsStructure(BasePageStructure):
    RECOMMENDED_PRODUCT = '//*[@class="product__recommended-item"][parent::div[not(contains(@class, "swiper-slide-duplicate"))]]'
    RECOMMENDED_PRODUCT_NEXT = '//*[@class="swiper-recommended-next"]/i'
