from selenium.common.exceptions import TimeoutException

from module_projects.base.check import BaseCheck
from module_projects.vmr.parts.VMRRegistration.exception import VMRRegistrationErrorException


class VMRRegistrationCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.parts.VMRRegistration.page.VMRRegistration
        """
        super().__init__(page)
        self.page = page

    def check_error_registration_txt_eq(self, value, raise_error=False):
        try:
            result = value == self.page.get_error_registration_msg_element().text
            text = self.page.get_error_registration_msg_element().text
        except TimeoutException:
            text = 'Не появилось сообщение об ошибке'
            result = False
        if not result and raise_error:
            raise VMRRegistrationErrorException("Некорректное сообщение об ошибочной регистрации. Ожидаем: {0}. Видим: {1}"
                                                .format(value, text))
        return result