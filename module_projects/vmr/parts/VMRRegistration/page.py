from typing import Union

from selenium import webdriver

from autotests_base.conditions.element_conditions import is_displayed, exist
from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRRegistration.check import VMRRegistrationCheck
from module_projects.vmr.parts.VMRRegistration.structure import VMRRegistrationStructure


class VMRRegistration(BasePage):
    page_name = 'VMRRegistration'

    @staticmethod
    def get_structure():
        return VMRRegistrationStructure

    def _get_check(self):
        """

        :rtype: LoginCheck
        """
        return VMRRegistrationCheck(self)

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)

        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def set_registration_login(self, value):
        self.get_login_registration_element().assure(is_displayed).clear()
        self.get_login_registration_element().send_keys(value)

    def set_registration_password(self, value):
        self.get_password_registration_element().assure(is_displayed).clear()
        self.get_password_registration_element().send_keys(value)

    def get_registration_button_element(self):
        return self.search(self.structure.REGISTR_BUTTON)

    def get_first_registration_button_element(self):
        return self.search(self.structure.FIRST_REGISTR_BUTTON)

    def get_login_registration_element(self):
        return self.search(self.structure.REGISTR_LOGIN)

    def get_password_registration_element(self):
        return self.search(self.structure.REGISTR_PASSWORD)

    def registration(self, login, password):
        """

        :rtype: module_projects.vmr.pages.PrivateOfficePage.page.VMRPrivateOfficePage
        """
        self.set_registration_login(login)
        self.set_registration_password(password)
        self.get_password_registration_element().assure(is_displayed).click()

        from module_projects.vmr.pages.PrivateOfficePage.page import VMRPrivateOfficePage
        return self.create_page_object(VMRPrivateOfficePage)

    def get_error_registration_msg_element(self):
        return self.search(self.structure.ERROR_MSG_REGISTR).assure(exist).assure(is_displayed)

    def new_login_registration(self, login, password):
        self.get_first_registration_button_element().assure(is_displayed).click()
        self.set_registration_login(login)
        self.set_registration_password(password)
        self.get_registration_button_element().assure(is_displayed).click()

    def get_loader_registration_element(self):
        return self.search(self.structure.LOADER_REGISTRATION)