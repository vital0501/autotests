from module_projects.base.structure import BasePageStructure


class VMRRegistrationStructure(BasePageStructure):
    CONFIRM = "//*[@id='register_popupLabel']"

    ERROR_MSG_REGISTR = "//*[@id='register_form']//fieldset[1]/p"
    REGISTR_BUTTON = "//*[@id='register_form']//button"
    FIRST_REGISTR_BUTTON = "//*[@id='login_form']//fieldset/span/a"
    REGISTR_LOGIN = '//*[@id="login2"]'
    REGISTR_PASSWORD = '//*[@id="pass2"]'
    LOADER_REGISTRATION = '//*[@id="register_form"]//*[@class="vm-spinner vm-modal__spinner"]'