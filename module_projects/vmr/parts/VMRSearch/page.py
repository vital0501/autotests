from typing import Union

from selenium import webdriver

from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRSearch.structure import VMRSearchStructure


class VMRSearch(BasePage):
    page_name = 'VMRSearch'

    @staticmethod
    def get_structure():
        return VMRSearchStructure

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)

    def search_product(self, search_text):
        """

        :rtype: module_projects.vmr.pages.SearchPage.page.VMRSearchPage
        """
        self.get_input_search_element().clear()
        self.get_input_search_element().send_keys(search_text)
        self.get_search_button_element().click()

        from module_projects.vmr.pages.SearchPage.page import VMRSearchPage
        return self.create_page_object(VMRSearchPage)

    def get_input_search_element(self):
        return self.search(self.structure.INPUT_ELEMENT)

    def get_search_button_element(self):
        return self.search(self.structure.SEARCH_BUTTON)
