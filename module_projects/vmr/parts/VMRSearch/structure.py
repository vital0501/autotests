from module_projects.base.structure import BasePageStructure


class VMRSearchStructure(BasePageStructure):
    CONFIRM = '//*[@id="smart_search"]'

    INPUT_ELEMENT = CONFIRM + '//input'
    SEARCH_BUTTON = CONFIRM + '//button[@title="Найти"]'
