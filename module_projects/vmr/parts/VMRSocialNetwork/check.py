from module_projects.base.check import BaseCheck
from module_projects.vmr.parts.VMRSocialNetwork.exception import SocialNetworkNotFoundException


class SocialNetworkCheck(BaseCheck):
    def __init__(self, page):
        """

        :type page: module_projects.vmr.parts.VMRSocialNetwork.page.VMRSocialNetwork
        """
        super().__init__(page)
        self.page = page

    def check_all_social_network_exist(self, raise_error=False):
        result = True
        for sc in self.page.ALL_SC.get_all_sc():
            for page_sc in self.page.get_all_social_network_elements():
                if sc.get_id() == page_sc.network_id:
                    break
            else:
                result = False
                error_id = sc.get_id()
                break

            if not result:
                break

        if not result and raise_error:
            raise SocialNetworkNotFoundException("Не известная соцсеть с ID = '{0}'".format(error_id))




