from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class SocialNetworkException(VMRBasePageException):
    pass


class SocialNetworkNotFoundException(SocialNetworkException):
    pass
