from typing import Union


from selenium import webdriver

from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRSocialNetwork.check import SocialNetworkCheck
from module_projects.vmr.parts.VMRSocialNetwork.structure import VMRSocialNetworkStructure
from module_projects.vmr.parts.VMRSocialNetwork.waiting_elements import SocialNetwork, SocialNetworkWE


class VMRSocialNetwork(BasePage):
    page_name = 'VMRSocialNetwork'

    @staticmethod
    def get_structure() -> VMRSocialNetworkStructure:
        return VMRSocialNetworkStructure()

    def _get_check(self):
        """

        :rtype: SocialNetworkCheck
        """
        return SocialNetworkCheck(self)

    ALL_SC = SocialNetwork()

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome],
                 project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)
        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def get_all_social_network_elements(self):
        """

        :rtype: WaitingElementCollection[SocialNetworkWE]
        """
        return self.search_all(self.structure.SK, elements_type=SocialNetworkWE)
