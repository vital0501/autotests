from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement
from module_projects.vmr.parts.VMRSocialNetwork.exception import SocialNetworkNotFoundException


class _SK(object):
    url = None

    def get_id(self):
        return self.__class__.__name__.split("_")[0].lower()


class SocialNetwork(object):

    class VK_SN(_SK):
        url = 'https://vk.com/club_vsemayki'

    class FB_SN(_SK):
        url = 'https://www.facebook.com/vsemayki'

    class IG_SN(_SK):
        url = 'https://www.instagram.com/vsemayki/'

    class TW_SN(_SK):
        url = 'https://twitter.com/vsemayki'

    class OK_SN(_SK):
        url = 'https://ok.ru/group/50868321255604'

    def get_all_sc(self):
        return [self.get_sc_by_id(item.split('_')[0]) for item in dir(self) if item.endswith('_SN')]

    def get_sc_by_id(self, sk_id: str):
        """

        :rtype: _SK
        """
        sk_name = sk_id.upper() + "_SN"
        if hasattr(self, sk_name):
            return getattr(self, sk_name)()
        else:
            raise SocialNetworkNotFoundException("Не найдена соц сеть с именем '{0}'" .format(sk_id))


class SocialNetworkWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)

        self.network_id = self.get_attribute('class').split('--')[-1].lower()
        self.url = SocialNetwork().get_sc_by_id(self.network_id).url

