from module_projects.vmr.pages.BasePage.exception import VMRBasePageException


class UpperMenuException(VMRBasePageException):
    pass


class UpperMenuItemNotFound(UpperMenuException):
    pass