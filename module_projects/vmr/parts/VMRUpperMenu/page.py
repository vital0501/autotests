from typing import Union

from selenium import webdriver

from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from module_projects.base.page import BasePage
from module_projects.vmr.parts.VMRLogin.page import VMRLogin
from module_projects.vmr.parts.VMRRegistration.page import VMRRegistration
from module_projects.vmr.parts.VMRUpperMenu.exception import UpperMenuItemNotFound
from module_projects.vmr.parts.VMRUpperMenu.structure import VMRUpperMenuStructure
from module_projects.vmr.parts.VMRUpperMenu.waiting_elements import UpperMenuItemWe


class VMRUpperMenu(BasePage):
    page_name = 'VMRUpperMenu'

    @staticmethod
    def get_structure():
        return VMRUpperMenuStructure

    def __init__(self, driver: Union[webdriver.Firefox, webdriver.Chrome], project_url, timeout):
        super().__init__(driver, None, project_url, timeout, init=False, confirm=False)
        self.LOGIN = VMRLogin(driver, project_url, timeout)
        self.REGISTRATION = VMRRegistration(driver, project_url, timeout)
        self.structure = self.get_structure()
        self.CHECK = self._get_check()

    def open_login_pop_up(self):
        if not self.is_login():
            self.get_upper_menu_element_by_name(name='Личный кабинет').click()
        else:
            self.get_upper_menu_elements()[-1].click()

    def get_upper_menu_element_by_name(self, name):
        for item in self.get_upper_menu_elements():
            if item.text == name:
                return item

    def get_upper_menu_elements(self):
        """

        :rtype: WaitingElementCollection[UpperMenuItemWe]
        """
        return self.search_all(self.structure.UPPER_MENU, elements_type=UpperMenuItemWe)

    def find_menu_item_by_name(self, name):
        for item in self.get_upper_menu_elements():
            if item.text == name:
                return item
        else:
            for item in self.get_upper_menu_elements():
                item.mouse_over_element()

                for sub_item in item.get_sub_menu_items_elements():
                    if sub_item.text == name:
                        return sub_item
        raise UpperMenuItemNotFound("Не найден пункт верхнего меню \"{0}\"" .format(name))

    def go_to_menu_item_by_name(self, name):
        from module_projects.vmr.pages.DeliveryPage.page import VMRDeliveryPage
        menu_page_structure = {
            "Доставка": VMRDeliveryPage
        }

        if name not in menu_page_structure.keys():
            raise UpperMenuItemNotFound("Неопределена страница для открытия пункта \"{0}\"" .format(name))

        self.find_menu_item_by_name(name).click()
        return self.create_page_object(menu_page_structure[name])

    def login(self, login, password):
        """

        :rtype: module_projects.vmr.pages.PrivateOfficePage.page.VMRPrivateOfficePage
        """
        self.open_login_pop_up()

        if not self.is_login():
            return self.LOGIN.login(login, password)
        else:
            from module_projects.vmr.pages.PrivateOfficePage.page import VMRPrivateOfficePage
            return self.create_page_object(VMRPrivateOfficePage)

    def is_login(self):
        return self.search(self.structure.LOGIN_DROPDOWN_ELEMENTS).exist()

    def fake_login(self, login, password):
        self.open_login_pop_up()

        if not self.is_login():
            return self.LOGIN.fake_login(login, password)

    def new_login_registration(self, login, password):
        self.open_login_pop_up()

        if not self.is_login():
            return self.REGISTRATION.new_login_registration(login, password)