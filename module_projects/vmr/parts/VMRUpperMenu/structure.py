from module_projects.base.structure import BasePageStructure


class VMRUpperMenuStructure(BasePageStructure):
    CONFIRM = '//*[@class="info-menu"]'
    UPPER_MENU = CONFIRM + "//a[not(ancestor::ul)]"
    LOGIN_DROPDOWN_ELEMENTS = '//*[@class="list-unstyled top-header-privateoffice__dropdown"]'