from autotests_base.elements.waiting_elements import WaitingElement, SubWaitingElement, SubWaitingElementCollection


class BaseVMRUpperMenuWE(WaitingElement):
    def __init__(self, driver, selector, selector_type, timeout, is_collection_item=False):
        super().__init__(driver, selector, selector_type, timeout, is_collection_item=is_collection_item)


class UpperMenuItemWe(BaseVMRUpperMenuWE):
    def get_sub_menu_items_elements(self):
        return SubWaitingElementCollection(self, "/../ul/li")