import pytest

import settings
from core.baseTest import BaseTest
from module_projects.vmr_android.pages.MainPage.page import MainPage


class AndroidProjectBaseTest(BaseTest):
    def get_main_page(self) -> MainPage:
        with pytest.allure.step('Открытие главной страницы'):
            return MainPage(driver=self.driver, timeout=settings.TIMEOUT)
