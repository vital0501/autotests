from core.exception import AutotestCoreException


class AndroidException(AutotestCoreException):
    pass


class AndroidPhoneInputError(AndroidException):
    pass


class AndroidProgressBarException(AndroidException):
    pass


class AndroidNewVersionException(AndroidException):
    pass
