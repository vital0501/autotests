from autotests_base.elements.waiting_elements import AndroidWaitingElement, SubWaitingElement


class AndroidAboutPageWE(AndroidWaitingElement):
    def get_text(self):
        return SubWaitingElement(self, '/android.widget.LinearLayout/android.widget.TextView').text
