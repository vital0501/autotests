from selenium.webdriver.common.by import By
import settings
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from autotests_base.elements.waiting_elements import SubWaitingElement
from autotests_base.pages.base_page.page import BaseAndroidPageObject
from module_projects.vmr_android.pages.CartPage.structure import CartPageStructure
from module_projects.vmr_android.pages.CartPage.waiting_elements import AndroidCartPageWE


class CartPage(BaseAndroidPageObject):
    def _get_page_load_timeout(self) -> int:
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _load_structure(self) -> CartPageStructure:
        return CartPageStructure()

    def _error_page_name(self) -> str:
        return "AndroidCartPage"

    def __init__(self, driver, timeout):
        super().__init__(driver, timeout)

    def create_order(self):
        """

        :rtype: module_projects.vmr_android.pages.OrderPage.page.OrderPage
        """
        self.search(self.structure.CREATE_ORDER).click()
        from module_projects.vmr_android.pages.OrderPage.page import OrderPage
        return self._get_page_object(OrderPage)

    def go_to_back(self):
        """

        :rtype: module_projects.vmr_android.pages.MyOrdersPage.page.MyOrdersPage
        """
        self.search(self.structure.BACK_BUTTON).click()
        from module_projects.vmr_android.pages.MyOrdersPage.page import MyOrdersPage
        return self._get_page_object(MyOrdersPage)

    def go_to_back_button(self):
        """

        :rtype: module_projects.vmr_android.pages.CartPage.page.CartPage
        """
        return self.search(self.structure.BACK_BUTTON).click()

    def get_products(self):
        """

        :rtype: WaitingElementCollection[AndroidCartPageWE]
        """
        return self.search_all(self.structure.PRODUCTS, selector_type=By.XPATH, elements_type=AndroidCartPageWE)

    def get_price(self):
        return SubWaitingElement(self, "com.vsemayki.app:id/product_total")

    def get_name(self):
        return SubWaitingElement(self, "com.vsemayki.app:id/product_name")


