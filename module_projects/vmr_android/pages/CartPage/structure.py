from autotests_base.pages.base_page.structure import BaseStructure


class CartPageStructure(BaseStructure):
    CONFIRM = '//android.widget.TextView[@text = "Моя корзина"]'
    CREATE_ORDER = 'com.vsemayki.app:id/checkout_order'
    BACK_BUTTON = '//android.widget.ImageButton[@index="0" and @content-desc="Перейти вверх"]'
    PRODUCTS = '//android.support.v7.widget.RecyclerView/android.widget.LinearLayout'
