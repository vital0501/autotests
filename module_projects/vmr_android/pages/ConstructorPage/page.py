import settings
from autotests_base.pages.base_page.page import BaseAndroidPageObject
from module_projects.vmr_android.pages.ConstructorPage.structure import ConstructorPageStructure


class ConstructorPage(BaseAndroidPageObject):
    def _get_page_load_timeout(self) -> int:
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _load_structure(self) -> ConstructorPageStructure:
        return ConstructorPageStructure()

    def _error_page_name(self) -> str:
        return "AndroidConstructorPage"

    def __init__(self, driver, timeout):
        super().__init__(driver, timeout)

    def add_to_cart(self):
        self.search(self.structure.ADD_TO_CART).click()

    def go_to_cart(self):
        """

        :rtype: module_projects.vmr_android.pages.CartPage.page.CartPage
        """
        self.search(self.structure.CART_BUTTON).click()
        from module_projects.vmr_android.pages.CartPage.page import CartPage
        return self._get_page_object(CartPage)

    def go_to_text(self):
        self.search(self.structure.GO_TO_TEXT).click()

    def add_text(self):
        self.search(self.structure.ADD_TEXT).click()

    def enter_text(self):
        return self.search(self.structure.ENTER_TEXT).send_keys('Test')

    def ok_button_text(self):
        self.search(self.structure.OK_BUTTON).click()

    def buy_button(self):
        """

        :rtype: module_projects.vmr_android.pages.ProductPage.page.ProductPage
        """

        self.search(self.structure.BUY_BUTTON).click()
        from module_projects.vmr_android.pages.ProductPage.page import ProductPage
        return self._get_page_object(ProductPage)

    def get_progress_bar(self):
        return self.search(self.structure.PROGRESS_BAR)
