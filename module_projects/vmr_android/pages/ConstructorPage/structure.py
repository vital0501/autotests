from autotests_base.pages.base_page.structure import BaseStructure


class ConstructorPageStructure(BaseStructure):
    CONFIRM = '//android.widget.Button[@text = "Купить"]'
    GO_TO_TEXT = '//android.widget.TextView[@text = "Текст"]'
    ADD_TEXT = 'com.vsemayki.app:id/add_text'
    ENTER_TEXT = 'com.vsemayki.app:id/enter_text'
    OK_BUTTON = 'com.vsemayki.app:id/enter'
    BUY_BUTTON = 'com.vsemayki.app:id/buy'
    PROGRESS_BAR = 'android:id/progress'
