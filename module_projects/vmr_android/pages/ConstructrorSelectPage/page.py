from selenium.webdriver.common.by import By

import settings
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from autotests_base.pages.base_page.page import BaseAndroidPageObject
from module_projects.vmr_android.pages.ConstructrorSelectPage.structure import ConstructorSelectPageStructure
from module_projects.vmr_android.pages.ConstructrorSelectPage.waiting_elements import AndroidConstructorSelectPageWE


class ConstructorSelectPage(BaseAndroidPageObject):
    def _get_page_load_timeout(self) -> int:
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _load_structure(self) -> ConstructorSelectPageStructure:
        return ConstructorSelectPageStructure()

    def _error_page_name(self) -> str:
        return "AndroidConstructorSelectPage"

    def __init__(self, driver, timeout):
        super().__init__(driver, timeout)

    def go_to_menu(self):
        """

        :rtype: module_projects.vmr_android.pages.MainPage.page.MainPage
        """
        return self.search(self.structure.SANDWICH_MENU_BUTTON).click()

    def get_progress_bar(self):
        return self.search(self.structure.PROGRESS_BAR)

    def go_to_shop_menu(self):
        """

        :rtype: module_projects.vmr_android.pages.ShopPage.page.ShopPage
        """
        self.search(self.structure.SHOP_MENU_BUTTON).click()
        from module_projects.vmr_android.pages.ShopPage.page import ShopPage
        return self._get_page_object(ShopPage)

    def get_select(self):
        """

        :rtype: WaitingElementCollection[AndroidConstructorSelectPageWE]
        """
        return self.search_all(self.structure.CONSTRUCTOR_SELECT,
                               selector_type=By.XPATH,
                               elements_type=AndroidConstructorSelectPageWE)

    def go_to_product(self, product):
        """
        :type product: AndroidShopProductWE
        :rtype: module_projects.vmr_android.pages.ProductPage.page.ProductPage
        """
        product.click()
        from module_projects.vmr_android.pages.ConstructorPage.page import ConstructorPage
        return self._get_page_object(ConstructorPage)

    def get_products(self):
        """

        :rtype: WaitingElementCollection[AndroidConstructorSelectPageWE]
        """
        return self.search_all(self.structure.PRODUCTS,
                               selector_type=By.XPATH,
                               elements_type=AndroidConstructorSelectPageWE)
