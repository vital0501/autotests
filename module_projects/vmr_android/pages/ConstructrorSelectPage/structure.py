from autotests_base.pages.base_page.structure import BaseStructure


class ConstructorSelectPageStructure(BaseStructure):
    CONFIRM = '//android.widget.TextView[@text = "Выберите товар"]'
    SANDWICH_MENU_BUTTON = '//android.widget.ImageButton[@index="0" and @content-desc="Открыть"]'
    MAN_SELECT = '//android.widget.TextView[@text="Мужчинам"]'
    PROGRESS_BAR = 'com.vsemayki.app:id/progress_bar'
    SHOP_MENU_BUTTON = '//android.widget.TextView[@text="Магазин"]'
    CONSTRUCTOR_SELECT = '//android.widget.TextView[@text!="Выберите товар"]'
    PRODUCTS = '//android.widget.TextView[@text!="Выберите товар"]'






