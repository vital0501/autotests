import settings
from autotests_base.exception import ConfirmPageException
from autotests_base.pages.base_page.page import BaseAndroidPageObject
from module_projects.vmr_android.exception import AndroidNewVersionException
from module_projects.vmr_android.pages.MainPage.structure import MainPageStructure


class MainPage(BaseAndroidPageObject):
    def _get_page_load_timeout(self) -> int:
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _load_structure(self) -> MainPageStructure:
        return MainPageStructure()

    def _error_page_name(self) -> str:
        return "AndroidMainPage"

    def __init__(self, driver, timeout):
        super().__init__(driver, timeout)

    def go_to_shop(self):
        """

        :rtype: module_projects.vmr_android.pages.ShopPage.page.ShopPage
        """
        self.search(self.structure.SHOP_BUTTON).click()

        try:
            from module_projects.vmr_android.pages.ShopPage.page import ShopPage
            return self._get_page_object(ShopPage)
        except ConfirmPageException:
            raise AndroidNewVersionException("Открывается новая версия")

    def go_to_constructor_select(self):
        """

        :rtype: module_projects.vmr_android.pages.ConstructrorSelectPage.page.ConstructorSelectPage
        """
        self.search(self.structure.CONSTRUCTOR_BUTTON).click()
        from module_projects.vmr_android.pages.ConstructrorSelectPage.page import ConstructorSelectPage
        return self._get_page_object(ConstructorSelectPage)

    def go_to_menu(self):
        """

        :rtype: module_projects.vmr_android.pages.MainPage.page.MainPage
        """
        self.search(self.structure.SANDWICH_MENU_BUTTON).click()
        return self._get_page_object(MainPage)

    def go_to_private_office(self):
        """

        :rtype: module_projects.vmr_android.pages.PrivateOfficePage.page.PrivateOfficePage
        """
        self.search(self.structure.PRIVATE_OFFICE_BUTTON).click()
        from module_projects.vmr_android.pages.PrivateOfficePage.page import PrivateOfficePage
        return self._get_page_object(PrivateOfficePage)

    def go_to_main(self):
        """

        :rtype: module_projects.vmr_android.pages.MainPage.page.MainPage
        """
        self.search(self.structure.MAIN_BUTTON).click()
        return self._get_page_object(MainPage)

    def go_to_constructor(self):
        """

        :rtype: module_projects.vmr_android.pages.ConstructrorSelectPage.page.ConstructorSelectPage
        """
        self.search(self.structure.CONSTRUCTOR_MENU_BUTTON).click()
        from module_projects.vmr_android.pages.ConstructrorSelectPage.page import ConstructorSelectPage
        return self._get_page_object(ConstructorSelectPage)
