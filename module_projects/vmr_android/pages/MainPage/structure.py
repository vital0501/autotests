from autotests_base.pages.base_page.structure import BaseStructure


class MainPageStructure(BaseStructure):
    CONFIRM = 'com.vsemayki.app:id/store_btn'
    SHOP_BUTTON = 'com.vsemayki.app:id/store_btn'
    SANDWICH_MENU_BUTTON = '//android.widget.ImageButton[@index="0" and @content-desc="Открыть"]'
    PRIVATE_OFFICE_BUTTON = 'com.vsemayki.app:id/username_lbl'
    MAIN_BUTTON = '//android.widget.TextView[@text="Главная"]'
    CONSTRUCTOR_MENU_BUTTON = '//android.widget.TextView[@text="Создать товар"]'
    CONSTRUCTOR_BUTTON = '//android.widget.Button[@text="Создать свой товар"]'

