import settings
from autotests_base.pages.base_page.page import BaseAndroidPageObject
from module_projects.vmr_android.pages.MyOrdersPage.structure import MyOrdersPageStructure


class MyOrdersPage(BaseAndroidPageObject):
    def _get_page_load_timeout(self) -> int:
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _load_structure(self) -> MyOrdersPageStructure:
        return MyOrdersPageStructure()

    def _error_page_name(self) -> str:
        return "AndroidMyOrdersPage"

    def __init__(self, driver, timeout):
        super().__init__(driver, timeout)

    def go_to_menu(self):
        """

        :rtype: module_projects.vmr_android.pages.MyOrdersPage.page.MyOrdersPage
        """
        return self.search(self.structure.SANDWICH_MENU_BUTTON).click()

    def get_progress_bar(self):
        return self.search(self.structure.PROGRESS_BAR)

    def go_to_my_cart(self):
        """

        :rtype: module_projects.vmr_android.pages.CartPage.page.CartPage
        """
        self.search(self.structure.MY_CART_BUTTON).click()
        from module_projects.vmr_android.pages.CartPage.page import CartPage
        return self._get_page_object(CartPage)

    def go_to_our_clients(self):
        """

        :rtype: module_projects.vmr_android.pages.OurClientsPage.page.OurClientsPage
        """
        self.search(self.structure.OUR_CLIENTS_BUTTON).click()
        from module_projects.vmr_android.pages.OurClientsPage.page import OurClientsPage
        return self._get_page_object(OurClientsPage)

    def go_to_feedback(self):
        """

        :rtype: module_projects.vmr_android.pages.FeedbackPage.page.FeedbackPage
        """
        self.search(self.structure.FEEDBACK_BUTTON).click()
        from module_projects.vmr_android.pages.FeedbackPage.page import FeedbackPage
        return self._get_page_object(FeedbackPage)

    def go_to_about(self):
        """

        :rtype: module_projects.vmr_android.pages.AboutPage.page.AboutPage
        """
        self.search(self.structure.ABOUT_BUTTON).click()
        from module_projects.vmr_android.pages.AboutPage.page import AboutPage
        return self._get_page_object(AboutPage)
