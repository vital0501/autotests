from autotests_base.pages.base_page.structure import BaseStructure


class MyOrdersPageStructure(BaseStructure):
    CONFIRM = '//android.widget.TextView[@text = "Мои заказы"]'
    SANDWICH_MENU_BUTTON = '//android.widget.ImageButton[@index="0" and @content-desc="Открыть"]'
    PROGRESS_BAR = 'com.vsemayki.app:id/progress_bar'
    SHOP_MENU_BUTTON = '//android.widget.TextView[@text="Магазин"]'
    MY_ORDERS_BUTTON = '//android.widget.TextView[@text="Мои заказы"]'
    MY_CART_BUTTON = '//android.widget.TextView[@text="Моя корзина"]'
    OUR_CLIENTS_BUTTON = '//android.widget.TextView[@text="Наши клиенты"]'
    FEEDBACK_BUTTON = '//android.widget.TextView[@text="Обратная связь"]'
    ABOUT_BUTTON = '//android.widget.TextView[@text="О программе"]'






