from autotests_base.pages.base_page.structure import BaseStructure


class MyProductPageStructure(BaseStructure):
    CONFIRM = '//android.widget.TextView[@text = "Мои товары"]'
    SANDWICH_MENU_BUTTON = '//android.widget.ImageButton[@index="0" and @content-desc="Открыть"]'
    PROGRESS_BAR = 'com.vsemayki.app:id/progress_bar'
    SHOP_MENU_BUTTON = '//android.widget.TextView[@text="Магазин"]'
    MY_ORDERS_BUTTON = '//android.widget.TextView[@text="Мои заказы"]'






