import time
from selenium.common.exceptions import TimeoutException
import settings
from autotests_base.conditions.element_conditions import exist, not_exist
from autotests_base.pages.base_page.page import BaseAndroidPageObject
from module_projects.vmr_android.exception import AndroidProgressBarException
from module_projects.vmr_android.pages.OrderPage.structure import OrderPageStructure


class OrderPage(BaseAndroidPageObject):
    def _get_page_load_timeout(self) -> int:
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _load_structure(self) -> OrderPageStructure:
        return OrderPageStructure()

    def _error_page_name(self) -> str:
        return "AndroidOrderPage"

    def __init__(self, driver, timeout):
        super().__init__(driver, timeout)

    def input_order_data(self, phone, email, name, city, address, region):
        elements = {'name': [self.get_name_element(), name],
                    'city': [self.get_city_element(), city],
                    'address': [self.get_address_element(), address]}

        self.driver.set_value(self.get_email_element(), email)
        self.driver.hide_keyboard()

        for number in [phone[0: 1], phone[1: 3], phone[3:6], phone[6:7], phone[7:8], phone[8:9], phone[9:10]]:
            self.driver.set_value(self.get_phone_element(), number)
        self.driver.hide_keyboard()

        for elements_key in elements.keys():
            if elements[elements_key][0].exist():
                if elements_key != 'city':
                    self.driver.set_value(elements[elements_key][0], elements[elements_key][1])
                    self.driver.hide_keyboard()
                else:
                    self.set_city(city=elements[elements_key][1], region=region)
            else:
                self.scroll_screen()
                elements[elements_key][0].assure(exist).send_keys(elements[elements_key][1])

    def input_order_data_promo_code(self, promo_code):
        self.driver.set_value(self.get_promo_code_element(), promo_code)
        self.driver.hide_keyboard()

    def get_phone_element(self):
        return self.search(self.structure.PHONE)

    def get_address_element(self):
        return self.search(self.structure.ADDRESS)

    def get_email_element(self):
        return self.search(self.structure.EMAIL)

    def get_city_element(self):
        return self.search(self.structure.CITY)

    def get_name_element(self):
        return self.search(self.structure.NAME)

    def get_city_input_element(self):
        return self.search(self.structure.CITY_INPUT)

    def set_city(self, city, region):
        self.get_city_element().click()
        self.driver.hide_keyboard()

        if self.get_city_region_element(city, region).exist():
            self.get_city_region_element(city, region).click()
        else:
            self.get_city_input_element().send_keys(city)
            self.driver.hide_keyboard()
            self.get_city_region_element(city, region).assure(exist).click()

        self.get_progress_bar().assure(exist)
        try:
            self.get_progress_bar().assure(not_exist, timeout=10)
        except TimeoutException:
            raise AndroidProgressBarException

    def get_city_region_element(self, city, region):
        return self.search(self.structure.CITY_REGION.format(city, region))

    def get_progress_bar(self):
        return self.search(self.structure.PROGRESS_BAR)

    def create_order(self):
        """

        :rtype: module_projects.vmr_android.pages.OrderPage.page.OrderPage
        """
        button_element = self.get_create_order_button_element()
        if not button_element.exist():
            for i in range(0, 10):
                self.scroll_screen()
                if button_element.exist():
                    break
        button_element.click()
        return self._get_page_object(OrderPage)

    def get_create_order_button_element(self):
        return self.search(self.structure.CREATE_ORDER_BUTTON)

    def promo_code__entry(self):
        """

        :rtype: module_projects.vmr_android.pages.OrderPage.page.OrderPage
        """
        button_element = self.get_promo_code_element()
        if not button_element.exist():
            for i in range(0, 10):
                self.scroll_screen()
                if button_element.exist():
                    break
        button_element.click()
        return self._get_page_object(OrderPage)

    def get_promo_code_element(self):
        return self.search(self.structure.PROMO_CODE)

    def delivery_select(self):
        delivery_element = self.get_delivery_cdek_element()
        if not delivery_element.exist():
            for i in range(0, 5):
                self._mini_scroll()
                if delivery_element.exist():
                    break
        delivery_element.assure(exist).click()
        delivery_element.assure(exist).click()

    def get_delivery_dpd_element(self):
        return self.search(self.structure.DELIVERY_DPD)

    def get_delivery_cdek_element(self):
        return self.search(self.structure.DELIVERY_CDEK)

    def get_delivery_pvz_element(self):
        return self.search(self.structure.DELIVERY_PVZ)

    def get_order_num(self):
        return self.get_order_num_element().assure(exist).text

    def get_order_num_element(self):
        return self.search(self.structure.ORDER_NUM)

    def error_delivery_msg(self):
        delivery_error = self.error_delivery_msg_element()
        if not delivery_error.exist():
            for i in range(0, 30):
                time.sleep(0.1)
                i += 1
                if delivery_error.exist():
                    return False
            return True

    def error_delivery_msg_element(self):
        return self.search(self.structure.ERROR_MESSAGE_DELIVERY)
