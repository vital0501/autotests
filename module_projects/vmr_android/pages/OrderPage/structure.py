from autotests_base.pages.base_page.structure import BaseStructure


class OrderPageStructure(BaseStructure):
    CONFIRM = '//android.widget.TextView[@text="Оформление заказа"]'
    PHONE = 'com.vsemayki.app:id/checkout_order_phone_edit'
    ADDRESS = 'com.vsemayki.app:id/checkout_order_address_edit'
    EMAIL = 'com.vsemayki.app:id/checkout_order_email_edit'
    CITY = 'com.vsemayki.app:id/city_field'
    NAME = 'com.vsemayki.app:id/checkout_order_full_name_edit'
    CITY_INPUT = 'com.vsemayki.app:id/search_view'
    CITY_REGION = '//android.widget.FrameLayout/android.widget.TextView[@text="{0} ({1})"]'
    PROGRESS_BAR = 'android:id/progress'
    CREATE_ORDER_BUTTON = 'com.vsemayki.app:id/checkout_order'
    PROMO_CODE = 'com.vsemayki.app:id/checkout_order_promo_code_edit'
    DELIVERY_CDEK = '//android.widget.LinearLayout/android.widget.TextView[@text="Доставка курьером СДЭК"]'
    DELIVERY_DPD = '//android.widget.LinearLayout/android.widget.TextView[@text="Доставка курьером DPD"]'
    DELIVERY_PVZ = '//android.widget.LinearLayout/android.widget.TextView[@text="Самовывоз из пункта выдачи"]'
    ORDER_NUM = 'com.vsemayki.app:id/order_number_text'
    ERROR_MESSAGE_DELIVERY = '//android.widget.LinearLayout/android.widget.TextView[@text="Ошибка при обработке данных"]'