import settings
from autotests_base.pages.base_page.page import BaseAndroidPageObject
from module_projects.vmr_android.pages.OurClientsPage.structure import OurClientsPageStructure


class OurClientsPage(BaseAndroidPageObject):
    def _get_page_load_timeout(self) -> int:
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _load_structure(self) -> OurClientsPageStructure:
        return OurClientsPageStructure()

    def _error_page_name(self) -> str:
        return "AndroidOurClientsPage"

    def __init__(self, driver, timeout):
        super().__init__(driver, timeout)

    def go_to_menu(self):
        """

        :rtype: module_projects.vmr_android.pages.OurClientsPage.page.OurClientsPage
        """
        return self.search(self.structure.SANDWICH_MENU_BUTTON).click()

    def get_progress_bar(self):
        return self.search(self.structure.PROGRESS_BAR)

    def go_to_back(self):
        """

        :rtype: module_projects.vmr_android.pages.MyOrdersPage.page.MyOrdersPage
        """
        self.search(self.structure.BACK_BUTTON).click()
        from module_projects.vmr_android.pages.MyOrdersPage.page import MyOrdersPage
        return self._get_page_object(MyOrdersPage)
