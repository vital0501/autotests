import settings
from autotests_base.pages.base_page.page import BaseAndroidPageObject
from module_projects.vmr_android.pages.PrivateOfficePage.structure import PrivateOfficePageStructure


class PrivateOfficePage(BaseAndroidPageObject):
    def _get_page_load_timeout(self) -> int:
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _load_structure(self) -> PrivateOfficePageStructure:
        return PrivateOfficePageStructure()

    def _error_page_name(self) -> str:
        return "AndroidPrivateOfficePage"

    def __init__(self, driver, timeout):
        super().__init__(driver, timeout)

    def go_to_back(self):
        """

        :rtype: module_projects.vmr_android.pages.MainPage.page.MainPage
        """
        self.search(self.structure.BACK_BUTTON).click()
        from module_projects.vmr_android.pages.MainPage.page import MainPage
        return self._get_page_object(MainPage)
