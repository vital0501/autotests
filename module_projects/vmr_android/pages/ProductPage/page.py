import settings
from selenium.webdriver.common.by import By
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from autotests_base.pages.base_page.page import BaseAndroidPageObject
from module_projects.vmr_android.pages.ProductPage.structure import ProductPageStructure
from module_projects.vmr_android.pages.ProductPage.waiting_elements import AndroidProductSizeWE
from autotests_base.conditions.element_conditions import exist, is_displayed


class ProductPage(BaseAndroidPageObject):
    def _get_page_load_timeout(self) -> int:
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _load_structure(self) -> ProductPageStructure:
        return ProductPageStructure()

    def _error_page_name(self) -> str:
        return "AndroidProductPage"

    def __init__(self, driver, timeout):
        super().__init__(driver, timeout)

    def get_available_sizes_elements(self):
        """

        :rtype: WaitingElementCollection[AndroidProductSizeWE]
        """
        return self.search_all(self.structure.SIZES, elements_type=AndroidProductSizeWE, selector_type=By.XPATH)

    def add_to_cart(self):

        self.search(self.structure.ADD_TO_CART).assure(exist).assure(is_displayed).click()

    def go_to_cart(self):
        """

        :rtype: module_projects.vmr_android.pages.CartPage.page.CartPage
        """
        self.search(self.structure.CART_BUTTON).assure(exist).assure(is_displayed).click()
        from module_projects.vmr_android.pages.CartPage.page import CartPage
        return self._get_page_object(CartPage)

    def get_progress_bar_element(self):
        return self.search(self.structure.PROGRESS_BAR)
