from autotests_base.pages.base_page.structure import BaseStructure


class ProductPageStructure(BaseStructure):
    CONFIRM = 'com.vsemayki.app:id/add_in_card'
    ADD_TO_CART = '//android.widget.Button[@text="ДОБАВИТЬ В КОРЗИНУ"]'
    CART_BUTTON = '//android.widget.Button[@text="Перейти в корзину"]'
    SIZES = '//android.support.v7.widget.RecyclerView/android.widget.LinearLayout'
    BACK_BUTTON = '//android.widget.ImageButton[@index="0" and @content-desc="Перейти вверх"]'
    PROGRESS_BAR = '//android.widget.ProgressBar'
