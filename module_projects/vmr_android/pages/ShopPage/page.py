from selenium.webdriver.common.by import By
import settings
from autotests_base.conditions.element_conditions import exist
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from autotests_base.pages.base_page.page import BaseAndroidPageObject

from module_projects.vmr_android.pages.ShopPage.structure import ShopPageStructure
from module_projects.vmr_android.pages.ShopPage.waiting_elements import AndroidShopProductWE


class ShopPage(BaseAndroidPageObject):
    def _get_page_load_timeout(self) -> int:
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _load_structure(self) -> ShopPageStructure:
        return ShopPageStructure()

    def _error_page_name(self) -> str:
        return "AndroidShopPage"

    def __init__(self, driver, timeout):
        super().__init__(driver, timeout)

    def get_products(self):
        """

        :rtype: WaitingElementCollection[AndroidShopProductWE]
        """
        return self.search_all(self.structure.PRODUCTS, selector_type=By.XPATH, elements_type=AndroidShopProductWE)

    def go_to_product(self, product):
        """
        :type product: AndroidShopProductWE
        :rtype: module_projects.vmr_android.pages.ProductPage.page.ProductPage
        """
        product.click()
        from module_projects.vmr_android.pages.ProductPage.page import ProductPage
        return self._get_page_object(ProductPage)

    def go_to_topics(self):
        """

        :rtype: module_projects.vmr_android.pages.TopicsPage.page.TopicsPage
        """
        self.search(self.structure.TOPICS_BUTTON).click()
        from module_projects.vmr_android.pages.TopicsPage.page import TopicsPage
        return self._get_page_object(TopicsPage)

    def go_to_product_types(self):
        """

        :rtype: module_projects.vmr_android.pages.TopicsPage.page.TopicsPage
        """
        self.search(self.structure.PRODUCT_TYPE_BUTTON).click()
        from module_projects.vmr_android.pages.TopicsPage.page import TopicsPage
        return self._get_page_object(TopicsPage)

    def get_progress_bar_element(self):
        return self.search(self.structure.PROGRESS_BAR)

    def search_product(self, text):
        self.get_search_button_element().assure(exist).click()
        self.input_search_text(text=text)

    def get_search_button_element(self):
        return self.search(self.structure.SEARCH_BUTTON)

    def input_search_text(self, text):
        for item in text:
            self.driver.set_value(self.get_search_element().assure(exist), item)

    def get_search_element(self):
        return self.search(self.structure.SEARCH_ELEMENT)

    def go_to_menu(self):
        """

        :rtype: module_projects.vmr_android.pages.ShopPage.page.ShopPage
        """
        return self.search(self.structure.SANDWICH_MENU_BUTTON).click()

    def go_to_my_product(self):
        """

        :rtype: module_projects.vmr_android.pages.MyProductPage.page.MyProduct
        """
        self.search(self.structure.MY_PRODUCT_BUTTON).click()
        from module_projects.vmr_android.pages.MyProductPage.page import MyProductPage
        return self._get_page_object(MyProductPage)
