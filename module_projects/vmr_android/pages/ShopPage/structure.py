from autotests_base.pages.base_page.structure import BaseStructure


class ShopPageStructure(BaseStructure):
    CONFIRM = 'com.vsemayki.app:id/subjects_text'
    PRODUCTS = '//android.support.v7.widget.RecyclerView/android.widget.FrameLayout'
    PROGRESS_BAR = 'com.vsemayki.app:id/progressBar'
    SEARCH_BUTTON = 'com.vsemayki.app:id/action_search'
    SEARCH_ELEMENT = 'com.vsemayki.app:id/search_src_text'
    TOPICS_BUTTON = 'com.vsemayki.app:id/subjects_text'
    SANDWICH_MENU_BUTTON = '//android.widget.ImageButton[@index="0" and @content-desc="Открыть"]'
    MY_PRODUCT_BUTTON = '//android.widget.TextView[@text="Мои товары"]'


