from autotests_base.conditions.element_conditions import exist
from autotests_base.elements.waiting_elements import AndroidWaitingElement, SubWaitingElement


class AndroidShopProductWE(AndroidWaitingElement):
    def get_text(self):
        return SubWaitingElement(self, '/android.widget.LinearLayout/android.widget.TextView').assure(exist).text
