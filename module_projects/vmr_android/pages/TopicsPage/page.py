from selenium.webdriver.common.by import By

import settings
from autotests_base.elements.waiting_elements_collection import WaitingElementCollection
from autotests_base.pages.base_page.page import BaseAndroidPageObject

from module_projects.vmr_android.pages.TopicsPage.structure import TopicsPageStructure
from module_projects.vmr_android.pages.TopicsPage.waiting_elements import AndroidTopicsPageWE


class TopicsPage(BaseAndroidPageObject):
    def _get_page_load_timeout(self) -> int:
        return settings.STANDARD_PAGE_LOAD_TIMEOUT

    def _load_structure(self) -> TopicsPageStructure:
        return TopicsPageStructure()

    def _error_page_name(self) -> str:
        return "AndroidTopicsPage"

    def __init__(self, driver, timeout):
        super().__init__(driver, timeout)

    def get_topics(self):
        """

        :rtype: WaitingElementCollection[AndroidTopicsPageWE]
        """
        return self.search_all(self.structure.TOPICS, selector_type=By.XPATH, elements_type=AndroidTopicsPageWE)

    def go_to_popular(self):
        """

        :rtype: module_projects.vmr_android.pages.TopicsPage.page.TopicsPage
        """
        return self.search(self.structure.POPULAR_RADIOBUTTON).click()

    def topics_del(self):
        """

        :rtype: module_projects.vmr_android.pages.TopicsPage.page.TopicsPage
        """
        return self.search(self.structure.TOPICS_DEL).click()

    def go_to_new(self):
        """

        :rtype: module_projects.vmr_android.pages.TopicsPage.page.TopicsPage
        """
        return self.search(self.structure.NEW_RADIOBUTTON).click()

    def product_type_del(self):
        """

        :rtype: module_projects.vmr_android.pages.TopicsPage.page.TopicsPage
        """
        return self.search(self.structure.PRODUCTS_TYPE_DEL).click()

    def go_to_product_types(self):
        """

        :rtype: module_projects.vmr_android.pages.TopicsPage.page.TopicsPage
        """
        self.search(self.structure.PRODUCT_TYPE_BUTTON).click()
        return self._get_page_object(TopicsPage)

    def go_to_topics(self):
        """

        :rtype: module_projects.vmr_android.pages.TopicsPage.page.TopicsPage
        """
        self.search(self.structure.TOPICS_BUTTON).click()
        return self._get_page_object(TopicsPage)

    def get_progress_bar(self):
        return self.search(self.structure.PROGRESS_BAR)
