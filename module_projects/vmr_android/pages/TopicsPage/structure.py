from autotests_base.pages.base_page.structure import BaseStructure


class TopicsPageStructure(BaseStructure):
    CONFIRM = 'com.vsemayki.app:id/toolbar'
    TOPICS = '//android.widget.TextView[@index="0" and @text!="Все темы"]'
    PRODUCT_TYPE_BUTTON = 'com.vsemayki.app:id/kind_of_goods_text'
    PRODUCTS_TYPE = '//android.widget.TextView[@index="0"]'
    POPULAR_RADIOBUTTON = '//android.widget.RadioButton[@text!="ПОПУЛЯРНОЕ"]'
    TOPICS_DEL = 'com.vsemayki.app:id/subjects_icon'
    NEW_RADIOBUTTON = '//android.widget.RadioButton[@text!="НОВИНКИ"]'
    PRODUCTS_TYPE_DEL = 'com.vsemayki.app:id/kind_of_goods_icon'
    TOPICS_BUTTON = 'com.vsemayki.app:id/subjects_text'
    PROGRESS_BAR = 'com.vsemayki.app:id/progressBar'



