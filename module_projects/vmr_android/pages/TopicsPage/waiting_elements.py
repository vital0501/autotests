from autotests_base.elements.waiting_elements import AndroidWaitingElement, SubWaitingElement


class AndroidTopicsPageWE(AndroidWaitingElement):
    def get_text(self):
        return SubWaitingElement(self, '/android.widget.LinearLayout/android.widget.TextView').text
