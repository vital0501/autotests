import getpass
import json
import os
from optparse import OptionParser

import shutil

import settings
from config_server_worker.config_server_api_worker import TestConfigApiWorker
from config_server_worker.utils import get_response_data, get_response_message
from config_server_worker.exceptions import ConfigServerException
from core.runner.runner import Runner3

BASE_DIR = str(os.path.normpath(os.path.dirname(os.path.realpath(__file__))))
testConfigApiWorker = TestConfigApiWorker(server=settings.CONFIG_SERVER_URL, api_version=settings.API_VER)
server_testrun_id = None
_task_name = None

exit_code = -1
__version__ = '0.6'
task_name, build_number = None, None


def main():
    parser = OptionParser(version=__version__, description='Run autotest')

    # Обязательные параметры

    parser.add_option('--suite',
                      action='append',
                      dest='suites',
                      help='Suite name')

    parser.add_option('--mobile',
                      action='store_true',
                      dest='mobile',
                      help='Is mobile tests')

    parser.add_option('--apk_id',
                      action='store',
                      dest='apk_id',
                      default=None,
                      help='apk_id')

    parser.add_option('--android_unicode_keyboard',
                      action='store',
                      dest='android_unicode_keyboard',
                      default=True,
                      help='android_unicode_keyboard')

    parser.add_option('--device_name',
                      action='store',
                      dest='device_name',
                      default=None,
                      help='device_name')

    parser.add_option('--browser',
                      action='store',
                      dest='browser',
                      help='Set browser',
                      default='chrome')

    parser.add_option('--headless',
                      action='store',
                      dest='headless',
                      help='chrome headless',
                      default=True)

    parser.add_option('--local',
                      action='store_true',
                      dest='local_run',
                      help='is local_run',
                      default=False)

    # Интеграция

    parser.add_option('--testrail_notification',
                      action='store_true',
                      dest='testrail',
                      help='Sync testrail',
                      default=False)

    parser.add_option('--email_notification',
                      action='store_true',
                      dest='email_notification',
                      help='Set email notification',
                      default=False)

    parser.add_option('--zabbix_notification',
                      action='store_true',
                      dest='zabbix',
                      help='Set zabbix integration',
                      default=False)

    parser.add_option('--telegram',
                      action='store_true',
                      dest='telegram',
                      help='Set telegram integration',
                      default=False)

    # Дополнительные параметры синхронизации

    parser.add_option('--default_email',
                      action='store',
                      dest='default_email',
                      help='Set default_email',
                      default=None)

    parser.add_option('--append_emails',
                      action='store',
                      dest='append_emails',
                      help='Set append_emails',
                      default=None)

    parser.add_option('--testrail_run_id',
                      action='store',
                      dest='testrail_run_id',
                      help='Set testrail_run_id',
                      default=None)

    # Дополнительные параметры

    parser.add_option('--callback_url',
                      action='store',
                      dest='callback_url',
                      help='Set callback_url',
                      default=None)

    parser.add_option('--server_test_run_id',
                      action='store',
                      dest='server_test_run_id',
                      help='Set server_test_run_id',
                      default=None)

    parser.add_option('--task_id',
                      action='store',
                      dest='task_id',
                      help='Set task_id',
                      default=None)

    parser.add_option('--site_version',
                      action='store',
                      dest='site_version',
                      help='Set site version',
                      default=None)

    parser.add_option('--flow',
                      action='store',
                      dest='flow',
                      help='Set flow',
                      default=1)

    (options, args) = parser.parse_args()

    # Проверка на сьюты
    if not options.suites:
        print('Not found suites')
        exit(exit_code)

    if options.site_version:
        print('Deprecated. Use "--suite suite_name|url=version_site_page"')
        exit(exit_code)

    # Определяем параметры запуска (локальный или на машине тестов)
    report_dir = settings.REPORT_DIR

    global build_number
    build_number = None

    global task_name
    task_name = None

    if options.task_id:
        task_name, build_number = options.task_id.split('-')
        local_run = False
    else:
        task_name = 'Local_run'
        build_number = None
        local_run = True

    global server_testrun_id
    server_testrun_id = None if options.server_test_run_id is None else int(options.server_test_run_id)
    print('Server TestRun ID = ', server_testrun_id)
    init_data = {'status': 5}

    # Создаем или логируем ServerTestRun
    param = {"jenkins_task_name": task_name, "jenkins_build_number": build_number}

    if server_testrun_id:
        if task_name and build_number:
            init_data.update(param)
        testConfigApiWorker.server_testrun_log(server_testrun_id, **init_data)
    else:
        num_find_elements = 0
        search_result = None

        if task_name and build_number:
            search_result = testConfigApiWorker.search_testrun(**param)
            num_find_elements = len(search_result)

        if num_find_elements == 0:
            init_data.update({'is_email_integrations': options.email_notification,
                              'is_zabbix_integrations': options.zabbix,
                              'is_testrail_integrations': options.testrail,
                              'is_telegram_integration': options.telegram,
                              'autotest_callback_url': options.callback_url,
                              'test_suites': options.suites
                              })
            init_data.update(param)

            if options.default_email:
                init_data.update({'report_email': options.default_email})

            if options.append_emails:
                init_data.update({'append_emails': options.append_emails})

            if options.default_email:
                init_data.update({'report_email': options.default_email})

            request = testConfigApiWorker.new_server_testrun(incoming_point='jenkins', server_test_run_data=init_data)
            request_data = get_response_data(request)
            print('Request data = ', request_data)
            if not request or not request_data:
                print('Server TestRun creation or updating exception')
                msg = get_response_message(request)
                if msg:
                    print(msg)
                exit(exit_code)
            else:
                server_testrun_id = request_data['server_test_run_id']

        elif num_find_elements == 1:
            server_testrun_id = search_result[0]['id']
        else:
            print('Multiply search result for ServerTestRun')
            exit(exit_code)

    # Запускаем тесты
    try:
        testrun_data = testConfigApiWorker.get_server_testrun_data(server_testrun_id=server_testrun_id)
        print('TestRun data = ', testrun_data)

        suites = testrun_data['server_suites'].keys()

        for suite in suites:
            testcases = testrun_data['server_suites'][suite]['include_testcases']
            exclude_testcases = testrun_data['server_suites'][suite]['exclude_testcases']

            Runner3(base_dir=BASE_DIR,
                    suite_id=suite,
                    browser=options.browser,
                    url=testrun_data['url'],
                    report_dir=report_dir,
                    testcase="|".join(testcases) if testcases else None,
                    exclude_testcase="|".join(exclude_testcases) if exclude_testcases else None,
                    server_test_run_id=server_testrun_id,
                    mobile=options.mobile,
                    apk_id=options.apk_id,
                    device_name=options.device_name,
                    android_unicode_keyboard=options.android_unicode_keyboard,
                    headless=options.headless,
                    task_name=task_name,
                    build_number=build_number,
                    flow=options.flow,
                    local_run=options.local_run).run()

            testConfigApiWorker.server_testrun_log(server_testrun_id, status=7)

        global _task_name
        _task_name = task_name

    except ConfigServerException as e:
        print('ConfigServerException', e)
        exit(exit_code)


if __name__ == '__main__':
    main()
    print(server_testrun_id)
    print(_task_name)

    if server_testrun_id:
        testConfigApiWorker.server_testrun_log(server_testrun_id, status=8).content.decode('utf-8')

        from subprocess import call

        allure_path = os.path.join(BASE_DIR, "drivers", "allure", "bin", "allure")

        main_path = os.path.join(settings.REPORT_DIR, str(server_testrun_id))
        dest_path = os.path.join(settings.REPORT_HTML_DIR, str(server_testrun_id))

        if _task_name != "Local_run":
            try:
                last_testcase = testConfigApiWorker.find_testcases(task_name=_task_name)[1]

                history_path = os.path.join(settings.REPORT_HTML_DIR, str(last_testcase['id']), 'history')
                run_report_path = os.path.join(settings.REPORT_DIR, str(server_testrun_id))

                if os.path.exists(history_path):
                    shutil.copytree(history_path, os.path.join(run_report_path, 'history'))
            except Exception as e:
                pass

            structure = {
                  "name": "Jenkins",
                  "type": "jenkins",
                  "url": "http://jenkins.vsemayki.pro/",
                  "buildOrder": build_number,
                  "buildName": "{0}#{1}".format(task_name, build_number),
                  "buildUrl": "http://jenkins.vsemayki.pro/job/{0}/{1}/".format(task_name, build_number),
                  "reportUrl": "http://testserver1.vsemayki.pro/allure-html/{0}".format(server_testrun_id),
                  "reportName": "AllureReport"
                }

            with open(os.path.join(main_path, 'executor.json'), 'w', encoding='UTF-8') as fp:
                fp.write(json.dumps(structure))

            call([allure_path, "generate", main_path, "-o", dest_path])
            print('Report href: http://testserver1.vsemayki.pro/allure-html/{0}'.format(server_testrun_id))

        if testConfigApiWorker.get_test_run(testrun_id=server_testrun_id)['errors']:
            print('TESTS FAILED!!!')
            exit(-1)