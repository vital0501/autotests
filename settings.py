import getpass
import platform

DEBUG = True
TESTRAIL_HREF = 'http://31.131.248.251:85/'
TESTRAIL_USER = 'autotest@ksprojetcs.ru'
TESTRAIL_API_KEY = 'MzHIIwYZ7IZaH.mjmxrr-rMt3w6p.hMi0/fvdtCW4'

CONFIG_SERVER_URL = 'http://testserver1.vsemayki.pro/'
APPIUM_SERVER = 'http://nsk-office.vsemayki.ru:4723/wd/hub'
FILE_REPORT_DIR = '/var/log/autotests/'
DEFAULT_EMAIL = "development@vsemayki.ru"

if getpass.getuser() == 'nsk-wks185':
    # CONFIG_SERVER_URL = 'http://10.21.2.101:8000/'
    APPIUM_SERVER = 'http://10.21.2.33:4723/wd/hub'

REPORT_DIR = "/home/vsemayki/projects/allure-report"
REPORT_HTML_DIR = "/home/vsemayki/projects/allure-html"

if getpass.getuser() == 'nsk-wks185':
    REPORT_DIR = '/home/nsk-wks185/PycharmProjects/allure-reports/'
    REPORT_HTML_DIR = "/home/nsk-wks185/PycharmProjects/allure-html"

if platform.system() == 'Windows':
    chrome_driver_name = 'chromedriver.exe'
    gecko_driver_name = 'geckodriver.exe'
    REPORT_DIR = 'D:\AllureReport'
    REPORT_HTML_DIR = 'D:\AllureReport'

else:
    chrome_driver_name = 'chromedriver64'
    gecko_driver_name = 'geckodriver64'

API_VER = 7
TIMEOUT = 4

EMAIL_USERNAME = 'vmr.autotest@gmail.com'
EMAIL_PASSWORD = 'FGDlldv75GHGH'
EMAIL_SERVER = 'smtp.gmail.com'
EMAIL_PORT = 587
IMAP_SERER_ADDRESS = 'imap.gmail.com'

STANDARD_PAGE_LOAD_TIMEOUT = 10

LoadMainPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadCatalogPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadProductPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadCartPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadContactsPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadDeliveryPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadHelpPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadOptPagePageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadPaymentsInfoOperationPagePageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadRetailPagePageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
ReviewsPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadFavoritesPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadNewsPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadNewsItemPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadStatusPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadHowCreateOrderPageStructurePageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadDesignerPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadOrderInfoPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadSportPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadSportFormPageTimeout = STANDARD_PAGE_LOAD_TIMEOUT
LoadConstructorPageTimeout = 20


# str(os.path.normpath(os.path.join(os.path.join(*os.path.split(BASE_DIR)[0:-1]), 'allure-reports')))


