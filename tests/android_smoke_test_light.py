import random

import time

import pytest
from selenium.common.exceptions import TimeoutException

from autotests_base.conditions.element_collection_conditions import len_more
from autotests_base.conditions.element_conditions import attribute_equal
from autotests_base.exception import ConfirmPageException
from module_projects.vmr_android.base_test import AndroidProjectBaseTest
from module_projects.vmr_android.exception import AndroidNewVersionException, AndroidPhoneInputError, \
    AndroidProgressBarException


class TestAutotest(AndroidProjectBaseTest):
    def test_android_smoke_test(self):
        self.subtest_start(1156)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.get_main_page()
        with pytest.allure.step('Переходим в каталог'):
            try:
                page = page.go_to_shop()
            except AndroidNewVersionException:
                page.search_product(text='1305241')

        with pytest.allure.step('Загружаем список продуктов продуктов'):
            i = 0
            while page.get_progress_bar_element().exist():
                time.sleep(0.1)
                i += 0.1
                if i > 10 * 20:
                    self.subtest_fail(1156, 'Долгая загрузка продуктов')
                    pytest.fail('Долгая загрузка продуктов')

        with pytest.allure.step('Ищем продукты по артикулу 1305241'):
            page.search_product(text='1305241')

        with pytest.allure.step('Переходим в карточку товара'):
            page = page.go_to_product(
                page.get_products().assure(len_more, 0)[0].assure(attribute_equal, 'get_text', 'Тестовый яркий'))
        with pytest.allure.step('Нажимаем кнопку добавить в корзину'):
            i = 0
            while page.get_progress_bar_element().exist():
                time.sleep(0.1)
                i += 0.1
                if i > 10 * 20:
                    self.subtest_fail(1156, 'Не появилась кнопка добавить в корзину')
                    pytest.fail('Не появилась кнопка добавить в корзину')
            page.add_to_cart()

        with pytest.allure.step('Выбираем размер'):
            if len(page.get_available_sizes_elements()) > 0:
                random.choice(page.get_available_sizes_elements().assure(len_more, 0)).click()

        with pytest.allure.step('Нажимаем кнопку перейти в корзину'):
            i = 0
            while page.get_progress_bar_element().exist():
                time.sleep(0.1)
                i += 0.1
                if i > 10 * 20:
                    self.subtest_fail(1156, 'Не появилась кнопка перейти в корзину')
                    pytest.fail('Не появилась кнопка перейти в корзину')
            page = page.go_to_cart()

        with pytest.allure.step('Переходим к оформлению заказа'):
            page = page.create_order()

        try:
            page.input_order_data(phone=self.active_test.data.phone,
                                  email=self.active_test.data.email,
                                  name=self.active_test.data.name,
                                  city=self.active_test.data.city,
                                  address=self.active_test.data.address,
                                  region=self.active_test.data.region)
        except AndroidPhoneInputError:
            pytest.fail('Не получилось корректно ввести номер телефона')
        except AndroidProgressBarException:
            pytest.fail('Долгая закгрузка доставок')

        with pytest.allure.step('Проверяем на наличие сообщения об ошибке'):
            if not page.error_delivery_msg():
                self.subtest_fail(1156, 'Ошибка данных при получении доставок')
                pytest.fail('Ошибка данных при получении доставок')
            else:
                pass

        with pytest.allure.step('Выбираем курьерскую службу'):
            try:
                page.delivery_select()
            except TimeoutException:
                self.subtest_fail(1156, message='Не удалось выбрать доставку')
                pytest.fail('Не удалось выбрать доставку')

        with pytest.allure.step('Создаем заказ'):
            try:
                page = page.create_order()
            except ConfirmPageException:
                self.subtest_fail(1156, message='Не удалось создать заказ')
                pytest.fail('Не удалось создать заказ')

        with pytest.allure.step('Проверяем наличие попапа с номером заказа'):
            try:
                self.log_text("Номер заказа: {0}" .format(page.get_order_num()))
            except TimeoutException:
                self.subtest_fail(1156, message='Не появился попоп с номером заказа')
                pytest.fail('Не появился попоп с номером заказа')

        self.subtest_pass(1156)
