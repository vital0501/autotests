import random

import time

import pytest

from autotests_base.conditions.element_collection_conditions import len_more
from autotests_base.exception import ConfirmPageException
from autotests_base.conditions.element_conditions import attribute_equal
from module_projects.vmr_android.base_test import AndroidProjectBaseTest
from module_projects.vmr_android.exception import AndroidPhoneInputError, AndroidProgressBarException


class TestAutotest(AndroidProjectBaseTest):

    def test_android_menu_smoke_test(self):
        self.subtest_start(1159)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.get_main_page()
        with pytest.allure.step('Открываем сэндвич меню'):
            try:
                page = page.go_to_menu()
            except ConfirmPageException:
                pytest.fail('Не открылось меню')
                self.subtest_fail(1159)

        with pytest.allure.step('Открываем страницу ЛК'):
            try:
                page = page.go_to_private_office()
            except ConfirmPageException:
                pytest.fail('Не открылась страница ЛК')
                self.subtest_fail(1159)

        with pytest.allure.step('Переходим назад в меню'):
            try:
                page = page.go_to_back()
            except ConfirmPageException:
                pytest.fail('Не открылась страница меню')
                self.subtest_fail(1159)

        with pytest.allure.step('Открываем главную страницу'):
            try:
                page.go_to_menu()
                page = page.go_to_main()
            except ConfirmPageException:
                pytest.fail('Не открылась главная страница')
                self.subtest_fail(1159)

        with pytest.allure.step('Открываем конструктор'):
             page.go_to_menu()
             page = page.go_to_constructor()
             i = 0
             while page.get_progress_bar().exist():
                time.sleep(0.1)
                i += 0.1
                if i > 10 * 20:
                    self.subtest_fail(1159, 'Долгая загрузка конструктора')
                    pytest.fail('Долгая загрузка конструктора')

        with pytest.allure.step('Открываем главную страницу'):
            page.go_to_menu()
            page = page.go_to_shop_menu()
            with pytest.allure.step('Загружаем список продуктов продуктов'):
                i = 0
                while page.get_progress_bar_element().exist():
                    time.sleep(0.1)
                    i += 0.1
                    if i > 10 * 20:
                        self.subtest_fail(1159, 'Долгая загрузка продуктов')
                        pytest.fail('Долгая загрузка продуктов')

        with pytest.allure.step('Открываем "Мои товары"'):
            try:
                page.go_to_menu()
                page = page.go_to_my_product()
            except ConfirmPageException:
                pytest.fail('Не открылась страница "Мои товары"')
                self.subtest_fail(1159)

        with pytest.allure.step('Открываем "Мои заказы"'):
            try:
                page.go_to_menu()
                page = page.go_to_my_orders()
            except ConfirmPageException:
                pytest.fail('Не открылась страница "Мои заказы"')
                self.subtest_fail(1159)

        with pytest.allure.step('Открываем стрниицу "Моя корзина"'):
            try:
                page.go_to_menu()
                page = page.go_to_my_cart()
                page = page.go_to_back()
            except ConfirmPageException:
                pytest.fail('Не открылась страница "Моя корзина"')
                self.subtest_fail(1159)

        with pytest.allure.step('Открываем стрниицу "Наши клиенты"'):
            try:
                page.go_to_menu()
                page = page.go_to_our_clients()
                page = page.go_to_back()
            except ConfirmPageException:
                pytest.fail('Не открылась страница "Наши клиенты"')
                self.subtest_fail(1159)

        with pytest.allure.step('Открываем стрниицу "Обратная связь"'):
            try:
                page.go_to_menu()
                page = page.go_to_feedback()
                page = page.go_to_back()
            except ConfirmPageException:
                pytest.fail('Не открылась страница "Обратная связь"')
                self.subtest_fail(1159)

        with pytest.allure.step('Открываем стрниицу "О программе"'):
            try:
                page.go_to_menu()
                page = page.go_to_about()
                page = page.go_to_back()
                page.go_to_menu()
            except ConfirmPageException:
                pytest.fail('Не открылась страница "О программе"')
                self.subtest_fail(1159)

        self.subtest_pass(1159)

    def test_android_catalog_smoke_test(self):
        self.subtest_start(1167)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.get_main_page()
        with pytest.allure.step('Переходим в каталог'):
            page = page.go_to_shop()

        with pytest.allure.step('Загружаем список продуктов продуктов'):
            i = 0
            while page.get_progress_bar_element().exist():
                time.sleep(0.1)
                i += 0.1
                if i > 10 * 20:
                    self.subtest_fail(1167, 'Долгая загрузка продуктов')
                    pytest.fail('Долгая загрузка продуктов')

        for i1 in range(0, 2):
            with pytest.allure.step('Переходим к выбору темы'):
                try:
                    page = page.go_to_topics()
                except ConfirmPageException:
                    pytest.fail('Не загрузились подкатегории')
                    self.subtest_fail(1167)

                for i2 in range(0, 2):
                    random.choice(page.get_topics().assure(len_more, 0)).click()
                    time.sleep(4)

            with pytest.allure.step('Сортируем по новинкам'):
                i = 0
                while page.get_progress_bar().exist():
                    time.sleep(0.1)
                    i += 0.1
                    if i > 10 * 20:
                        self.subtest_fail(1167, 'Долгая загрузка')
                        pytest.fail('Долгая загрузка')
                try:
                    page.go_to_new()
                except ConfirmPageException:
                    pytest.fail('Не сортирует по новинкам')
                    self.subtest_fail(1167)

            with pytest.allure.step('Сортируем по популярности'):
                i = 0
                while page.get_progress_bar().exist():
                    time.sleep(0.1)
                    i += 0.1
                    if i > 10 * 20:
                        self.subtest_fail(1167, 'Долгая загрузка')
                        pytest.fail('Долгая загрузка')
                try:
                    page.go_to_popular()
                except ConfirmPageException:
                    pytest.fail('Не сортирует по популярности')
                    self.subtest_fail(1167)

            with pytest.allure.step('Открываем продуктипы'):
                i = 0
                while page.get_progress_bar().exist():
                    time.sleep(0.1)
                    i += 0.1
                    if i > 10 * 20:
                        self.subtest_fail(1167, 'Долгая загрузка')
                        pytest.fail('Долгая загрузка')
                try:
                    page = page.go_to_product_types()
                    time.sleep(4)
                except ConfirmPageException:
                    pytest.fail('Не загрузились продуктипы')
                    self.subtest_fail(1167)

            with pytest.allure.step('Выбираем продуктип'):
                for i3 in range(0, 3):
                    random.choice(page.get_topics().assure(len_more, 0)).click()
                    time.sleep(4)

                with pytest.allure.step('Сбрасываем фильтр по продуктипам '):
                    i = 0
                    while page.get_progress_bar().exist():
                        time.sleep(0.1)
                        i += 0.1
                        if i > 10 * 20:
                            self.subtest_fail(1167, 'Долгий сброс фильтра')
                            pytest.fail('Долгий сброс фильтра')
                    try:
                        page.product_type_del()
                    except ConfirmPageException:
                        pytest.fail('Не сбросился фильтр продуктипов')
                        self.subtest_fail(1167)

                with pytest.allure.step('Сбрасываем фильтр по категории'):
                    i = 0
                    while page.get_progress_bar().exist():
                        time.sleep(0.1)
                        i += 0.1
                        if i > 10 * 20:
                            self.subtest_fail(1167, 'Долгий сброс фильтра')
                            pytest.fail('Долгий сброс фильтра')
                    try:
                        page.topics_del()
                    except ConfirmPageException:
                        pytest.fail('Не сбросился фильтр категорий')
                        self.subtest_fail(1167)

        self.subtest_pass(1167)


    def test_android_constructor_smoke_test(self):
        self.subtest_start(1169)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.get_main_page()
        with pytest.allure.step('Переходим к выбору в конструкторе'):
            page = page.go_to_constructor_select()
            i = 0
            while page.get_progress_bar().exist():
                time.sleep(0.1)
                i += 0.1
                if i > 10 * 20:
                    self.subtest_fail(1169, 'Долгая загрузка конструктора')
                    pytest.fail('Долгая загрузка конструктора')

        with pytest.allure.step('Выбираем случайную категорию в конструкторе'):
            try:
                random.choice(page.get_select().assure(len_more, 0)).click()
            except ConfirmPageException:
                pytest.fail('Не выбралась случайная категория в конструкторе')
                self.subtest_fail(1169)

        with pytest.allure.step('Переходим в конструктор товара'):
            i = 0
            while page.get_progress_bar().exist():
                time.sleep(0.1)
                i += 0.1
                if i > 10 * 20:
                    self.subtest_fail(1169, 'Долгая загрузка товара в конструктор')
                    pytest.fail('Долгая загрузка товара в конструктор')
            try:
                page = page.go_to_product(random.choice(page.get_products()))
            except ConfirmPageException:
                pytest.fail('Не открылся товар в конструкторе')
                self.subtest_fail(1169)

        with pytest.allure.step('Вводим тест на товар'):
            try:
                page.go_to_text()
                page.add_text()
                page.enter_text()
                page.ok_button_text()
            except ConfirmPageException:
                pytest.fail('Ошибка при вводе текста')
                self.subtest_fail(1169)

        with pytest.allure.step('Нажимаем "Купить"'):
            try:
                page = page.buy_button()
            except ConfirmPageException:
                pytest.fail('Не открылась карточка товара')
                self.subtest_fail(1169)

        with pytest.allure.step('Добавляем товар в корзину'):
            if len(page.get_available_sizes()) > 0:
                random.choice(page.get_available_sizes().assure(len_more, 0)).click()
                page.add_to_cart()
                time.sleep(10)
                page = page.go_to_cart()
                time.sleep(8)
            else:
                page.add_to_cart()
                time.sleep(10)
                page = page.go_to_cart()
                time.sleep(8)

        with pytest.allure.step('Переходим к оформлению заказа'):
            page = page.create_order()
        try:
            page.input_order_data(phone=self.active_test.data.phone,
                                  email=self.active_test.data.email,
                                  name=self.active_test.data.name,
                                  city=self.active_test.data.city,
                                  address=self.active_test.data.address,
                                  region=self.active_test.data.region)

        except AndroidPhoneInputError:
            pytest.fail('Не получилось корректно ввести номер телефона')
        except AndroidProgressBarException:
            pytest.fail('Долгая закгрузка доставок')

        with pytest.allure.step('Создаем заказ'):
            try:
                page = page.create_order()
                print(page.get_order_num())
            except ConfirmPageException:
                self.subtest_fail(1169)

        self.subtest_pass(1169)

    def test_android_order_smoke_test(self):
        self.subtest_start(1170)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.get_main_page()

        with pytest.allure.step('Переходим в каталог'):
            page = page.go_to_shop()

        with pytest.allure.step('Загружаем список продуктов продуктов'):
            i = 0
            while page.get_progress_bar_element().exist():
                time.sleep(0.1)
                i += 0.1
                if i > 10 * 20:
                    self.subtest_fail(1170, 'Долгая загрузка продуктов')
                    pytest.fail('Долгая загрузка продуктов')

        with pytest.allure.step('Ищем продукты по артикулу 643006'):
            page.search_product(text='643006')

        with pytest.allure.step('Оформляем 2 товара'):
            i = 0
            for i in range(0, 2):
                with pytest.allure.step('Переходим в карточку товара'):
                    page = page.go_to_product(
                        page.get_products().assure(len_more, 0)[random.randint(0, 3)].assure(attribute_equal, 'get_text', 'Pink Floyd'))
                with pytest.allure.step('Добавляем товар в корзину'):
                    time.sleep(2)
                    page.add_to_cart()
                with pytest.allure.step('Выбираем размер'):
                    random.choice(page.get_available_sizes().assure(len_more, 0)).click()
                with pytest.allure.step('Возвращаемся в каталог'):
                    page = page.go_to_back_button()

        with pytest.allure.step('Оформляем третий товар и переходим в корзину'):
            page = page.go_to_product(
                page.get_products().assure(len_more, 0)[random.randint(0, 3)].assure(attribute_equal, 'get_text', 'Pink Floyd'))
        with pytest.allure.step('Добавляем товар в корзину'):
            time.sleep(2)
            page.add_to_cart()
        with pytest.allure.step('Выбираем размер'):
            random.choice(page.get_available_sizes().assure(len_more, 0)).click()

        with pytest.allure.step('Переходим в корзину'):
            page = page.go_to_cart()

        with pytest.allure.step('Переходим к оформлению заказа'):
            page = page.create_order()

            try:
                page.input_order_data(phone=self.active_test.data.phone,
                                      email=self.active_test.data.email,
                                      name=self.active_test.data.name,
                                      city=self.active_test.data.city,
                                      address=self.active_test.data.address,
                                      region=self.active_test.data.region)

            except AndroidPhoneInputError:
                pytest.fail('Не получилось корректно ввести номер телефона')
            except AndroidProgressBarException:
                pytest.fail('Долгая закгрузка доставок')

        with pytest.allure.step('Вводим промокод'):
            try:
                page = page.promo_code__entry()
                page.input_order_data_promo_code(promo_code=self.active_test.data.promo_code)
            except ConfirmPageException:
                self.subtest_fail(1170)

        with pytest.allure.step('Создаем заказ'):
            try:
                page = page.create_order()
            except ConfirmPageException:
                self.subtest_fail(1170)

        self.subtest_pass(1170)

