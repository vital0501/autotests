import pytest
import requests

import settings
from autotests_base.conditions.element_conditions import exist
from core.baseTest import BaseTest
from project.exceptions import RobotsResponseException

from project.external.pages.google.page import GoogleRobotsPage
from project.external.pages.yandex.page import YandexRobotsPage


class TestAutotest(BaseTest):
    GOOGLE_ROBOT_TESTING_URL = "https://www.google.com/webmasters/tools/"
    YANDEX_ROBOT_TESTING_URL = "https://webmaster.yandex.ru/tools/server-response/"

    logpass = [{'login': 'pixelmartru@gmail.com', 'password': 'Zi3xH7db', 'url': 'https://www.vsemayki.ru/'},
               {'login': 'pixelmartmonitoring@gmail.com', 'password': 'GKi8Pmz5', 'url': 'https://pixelmart.ru/'},
               {'login': 'emaykamonitoring@gmail.com', 'password': 'c2aqKT8Q', 'url': 'http://emayka.ru/'}]

    def test_external_google_robot_vse_mayki(self):
        self.log_text('Проверяем сайт "{0}"'.format(self.logpass[0]['url']))
        page = GoogleRobotsPage(driver=self.driver, main_href=self.GOOGLE_ROBOT_TESTING_URL, timeout=settings.TIMEOUT)
        page.login(self.logpass[0]['login'], self.logpass[0]['password'])
        page.set_server(self.logpass[0]['url'])
        page.go_to_scan()
        if page.has_capcha():
            pytest.fail('Обнаружена капча')
        page.get_scan_button_element().assure(exist, timeout=20).click()
        page.go_to_last_scan()
        robot_text = page.get_robot_text(crop=750, optimization=True)
        cookie = {'Test-Version': '-A' .format(self.active_test.data.server_version)}
        server_text_full = requests.get(self.logpass[0]['url'], cookies=cookie).text
        server_text = server_text_full.replace("\r\n", '').replace(" ", '').replace('\n', '')[0:750]
        self.log_txt_data(log_message='Ответ от робота (crop=750, optimization=True)', log_data=robot_text)
        self.log_txt_data(log_message='Ответ от сервера', log_data=server_text_full)
        self.log_txt_data(log_message='Ответ от сервера (crop=750, optimization=True)', log_data=server_text)
        with pytest.allure.step('Скрин ответа робота "ПК"'):
            self.log_screenshot()

        if robot_text != server_text:
            pytest.fail('Ответы сервера и робота не совпадают')

    def test_external_google_robot_pixelmart(self):
        self.log_text('Проверяем сайт "{0}"'.format(self.logpass[1]['url']))
        page = GoogleRobotsPage(driver=self.driver, main_href=self.GOOGLE_ROBOT_TESTING_URL, timeout=settings.TIMEOUT)
        page.login(self.logpass[1]['login'], self.logpass[1]['password'])
        page.set_server(self.logpass[1]['url'])
        page.go_to_scan()
        if page.has_capcha():
            pytest.fail('Обнаружена капча')
        page.get_scan_button_element().assure(exist, timeout=20).click()
        page.go_to_last_scan()
        robot_text = page.get_robot_text(crop=600, optimization=True)
        server_text_full = requests.get(self.logpass[1]['url']).text
        server_text = server_text_full.replace("\r\n", '').replace(" ", '').replace('\n', '')[0:600]
        self.log_txt_data(log_message='Ответ от робота (crop=600, optimization=True)', log_data=robot_text)
        self.log_txt_data(log_message='Ответ от сервера', log_data=server_text_full)
        self.log_txt_data(log_message='Ответ от сервера (crop=600, optimization=True)', log_data=server_text)
        with pytest.allure.step('Скрин ответа робота "ПК"'):
            self.log_screenshot()

        if robot_text != server_text:
            pytest.fail('Ответы сервера и робота не совпадают')

    def test_external_google_robot_emayka(self):
        self.log_text('Проверяем сайт "{0}"'.format(self.logpass[2]['url']))
        page = GoogleRobotsPage(driver=self.driver, main_href=self.GOOGLE_ROBOT_TESTING_URL, timeout=settings.TIMEOUT)
        page.login(self.logpass[2]['login'], self.logpass[2]['password'])
        page.set_server(self.logpass[2]['url'])
        page.go_to_scan()
        if page.has_capcha():
            pytest.fail('Обнаружена капча')
        page.get_scan_button_element().assure(exist, timeout=20).click()
        page.go_to_last_scan()
        robot_text = page.get_robot_text(crop=750, optimization=True)
        server_text_full = requests.get(self.logpass[2]['url']).text
        server_text = server_text_full.replace("\r\n", '').replace(" ", '').replace('\n', '')[0:750]
        self.log_txt_data(log_message='Ответ от робота (crop=750, optimization=True)', log_data=robot_text)
        self.log_txt_data(log_message='Ответ от сервера', log_data=server_text_full)
        self.log_txt_data(log_message='Ответ от сервера (crop=750, optimization=True)', log_data=server_text)
        with pytest.allure.step('Скрин ответа робота "ПК"'):
            self.log_screenshot()

        if robot_text != server_text:
            pytest.fail('Ответы сервера и робота не совпадают')

    def test_external_yandex_robot(self):
        page = YandexRobotsPage(self.driver, main_href=self.YANDEX_ROBOT_TESTING_URL, timeout=settings.TIMEOUT)
        robots_names = page.get_robots_names()

        for server in 'https://www.vsemayki.ru/|https://pixelmart.ru/|http://emayka.ru/'.split('|'):
            with pytest.allure.step('Проверяем сайт: "{0}"'.format(server)):
                for robot_index in range(0, len(robots_names)):
                    robot_name = robots_names[robot_index]
                    with pytest.allure.step('Проверяем робота "{0}"'.format(robot_name)):
                        server_text_full = requests.get(server).text
                        server_text = server_text_full.replace("\r\n", '').replace(" ", '').replace('\n', '')[0:600]
                        self.log_txt_data(log_message='Ответ от сервера', log_data=server_text_full)
                        self.log_txt_data(log_message='Ответ от сервера (crop=600, optimization=True)', log_data=server_text)

                        try:
                            robot_text = page.get_response_text(robot_index, server, crop=600, optimization=True, timeout=20)
                            self.log_txt_data(log_message='Ответ от робота (crop=600, optimization=True)', log_data=robot_text)
                        except RobotsResponseException:
                            pytest.fail('Робот {0} не вернул текст ответа для {0}'.format(robot_name, server))

                        with pytest.allure.step('Скрин ответа робота "{0}"'.format(robot_name)):
                            self.log_screenshot()

                        if robot_text != server_text:
                            pytest.fail('Ответы сервера и робота не совпадают')
