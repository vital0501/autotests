import random
import time

import pytest

from module_projects.smart_mobile.base_test import ProjectBaseTest
from module_projects.smart_mobile.pages.CartPage.page import SMARTCartPage
from module_projects.smart_mobile.pages.OrderPage.page import SMARTOrderPage
from module_projects.smart_mobile.pages.ProductPage.page import SMARTProductPage


class TestAutotest(ProjectBaseTest):
    def test_smart_android_light_test(self):

        def clean_wait(**kwargs):
            time.sleep(5)

        def clean_refresh(_test: TestAutotest, _page: SMARTOrderPage, **kwargs):
            _page.scroll_up()

        def step_open_product_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page.CHECK.check_length_collection_gt(_page.get_products_elements(), 0,
                                                   collection_name='Продукты на странице', raise_error=True)
            product = random.choice(_page.get_products_elements())
            return _page.go_to_product_by_element(product.scroll_to_element())

        def step_add_product_to_cart(_test: TestAutotest, _page: SMARTProductPage, **kwargs):
            _page.set_random_size()
            _page.set_random_color()
            _page.add_product_to_cart()
            _page.HEADER.wait_cart_count_change(number=1)
            _test.log_text(_page.get_name())
            _test.log_screenshot()

        def step_go_to_cart(_test: TestAutotest, _page: SMARTProductPage = None, **kwargs):
            if _page is None:
                return _test.open_main_page().HEADER.go_to_cart()
            else:
                return _page.HEADER.go_to_cart()

        def clean_open_product_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()

            if _page.HEADER.get_cart_count != 0:
                _page = _page.HEADER.go_to_cart()
                _page.clean_cart()

            _page = _test.open_main_page()

            return _page.go_to_product_by_element(random.choice(_page.get_products_elements()))

        def step_open_order_page(_test: TestAutotest, _page: SMARTCartPage):
            _page.CHECK.check_length_collection_eq(_page.get_products_elements(), 1,
                                                   collection_name='Продукты в корзине', raise_error=True)
            return _page.go_to_order_page()

        def step_create_order(_test: TestAutotest, _page: SMARTOrderPage):
            with pytest.allure.step('Вводим данные'):
                _page.fill_data(name=_test.active_test.data.name,
                                phone=_test.active_test.data.phone_number,
                                email=_test.active_test.data.login,
                                city=_test.active_test.data.result_city,
                                address=_test.active_test.data.address,
                                comment=_test.active_test.data.comment)

            with pytest.allure.step('Ждем появления доставок'):
                _page.CHECK.check_length_collection_gt(_page.get_delivery_elements(), 0, "Доставки", True)

            with pytest.allure.step('Выбираем случайную доставку'):
                random.choice(_page.get_courier_delivery_elements()).set_active()

            with pytest.allure.step('Проверяем что выбрана оплата "При получении"'):
                _page.CHECK.check_payments_postal_active(raise_error=True)

            with pytest.allure.step('Оформляем заказ'):
                return _page.create_order()

        with pytest.allure.step('Открываем случайный продукт'):
            page = self.step(step_open_product_page, clean_wait, self, None)

        with pytest.allure.step('Устанавливаем цвет и размер. Добавляем продукт в корзину'):
            self.step(step_add_product_to_cart, clean_open_product_page, self, page)

        with pytest.allure.step('Переходим в корзину'):
            page = self.step(step_go_to_cart, clean_wait, self, page)

        with pytest.allure.step('Переходим к оформлению заказа'):
            page = self.step(step_open_order_page, step_go_to_cart, self, page)

        with pytest.allure.step('Оформляем заказ'):
            page = self.step(step_create_order, clean_refresh, self, page)

        self.log_text('Номер заказа: {0}'.format(page.get_order_num()))