import datetime
import json
import tempfile
from json import JSONDecodeError

import os

import shutil
import zeep
from requests import Session
from requests.auth import HTTPBasicAuth
import lxml.html
import pytest
import requests
import time

from selenium.common.exceptions import TimeoutException
from zeep import Transport

from autotests_base.conditions.element_collection_conditions import len_change
from core.exception import AutotestCoreException
from module_projects.vmr.base_test import ProjectBaseTest
from module_projects.vmr.pages.ProductPage.page import VMRProductPage


class TestAutotest(ProjectBaseTest):
    """Class"""
    @pytest.yield_fixture(scope='class', autouse=True)
    def send_1c_request(self, request, flow, server_test_run_id):

        search_html = requests.get('http://www.vsemayki.ru/search/{0}'.format(1305241)).text
        products_elements_link = lxml.html.fromstring(search_html).xpath('//*[@data-autotest-id="product"]/div/a')
        res = [item.attrib['href'] for item in products_elements_link]
        request.config.cache.set("pages", res)
        tmp_dir = os.path.join(tempfile.gettempdir(), 'a_test_{0}'.format(server_test_run_id))
        session_dir = os.path.join(tmp_dir, 'session')

        if not os.path.exists(tmp_dir):
            os.mkdir(tmp_dir)

        if not os.path.exists(session_dir):
            os.mkdir(session_dir)

        request.config.cache.set("tmp_dir", tmp_dir)

        yield

        with open(os.path.join(session_dir, '{0}'.format(datetime.datetime.now().timestamp())), 'w') as fp:
            pass

        if len(os.listdir(session_dir)) == int(flow):

            def get_reserved_response_text(data):
                return requests.post('http://v4.api.vsemayki.ru/product/default/reserved',
                                     headers={'ApiAccessToken': '764232f3d837bc0608d203e13d8ff77c'},
                                     json=data).text

            result = {}
            for item in os.listdir(tmp_dir):
                if os.path.isfile(os.path.join(tmp_dir, item)):
                    with open(os.path.join(tmp_dir, item), 'r', encoding='UTF8') as file:
                        result.update({item: json.loads(file.read())})
            print(result)

            print('Общее количество продуктов = {0}'.format(len(result.keys())))
            print('Генерируем структуру для запросов резервов')
            reserved_structure = []
            for product in result.keys():
                for color in result[product]['colors'].keys():
                    for size in result[product]['colors'][color].keys():
                        reserved_structure.append({'product': product, 'color': color, 'size': size})
            print("Сгенерированная структура\n", reserved_structure)

            print('Общее количество элементов = {0}'.format(len(reserved_structure)))

            print('Запрашиваем резервы')
            response_text = None
            for item in range(0, 3):
                try:
                    response_text = get_reserved_response_text(reserved_structure)
                    reserved_data = json.loads(response_text)
                    print("Ответ от сервера: {0}".format(reserved_data))
                    break
                except JSONDecodeError:
                    time.sleep(2)
                    self.log_txt_data(log_data=response_text)
                    continue
            else:
                raise AutotestCoreException("Ошибка обработки запроса от v4.")

            print('Отправляем результаты в 1С')
            print('Инициируем сессию 1С')
            session = Session()
            session.auth = HTTPBasicAuth('Service', 'fg89h4h9DK0')
            transport_with_basic_auth = Transport(session=session)

            client = zeep.Client(wsdl='http://10.0.0.92/UPP/ws/vm1c?wsdl', transport=transport_with_basic_auth)

            print('Инициируем ProdAvailabilityList')
            try:
                prod_list = client.get_type('ns0:ProdAvailabilityList')
            except Exception as e:
                print(e)
                pytest.fail('Ошибка инициализации "ElementProdAvailability"')

            print('Инициируем ProdAvailabilityList')
            try:
                prod_element = client.get_type('ns0:ElementProdAvailability')
            except Exception as e:
                print(e)
                pytest.fail('Ошибка инициализации "ElementProdAvailability"')

            print('Подготавливаем данные')
            product_list = []

            for item in reserved_data:
                data_line = {'ProdAlias': item['product'],
                             'ColorAlias': item['color'],
                             'SizeAlias': item['size'],
                             'VerificationDate': datetime.datetime.now().utcnow(),
                             'Qty': 0,
                             'Reserve': item['reserved'],
                             'minQty': 0,
                             'Availability': result[item['product']]['available']}

                element = prod_element(**data_line)
                product_list.append(element)

            print('Преобразуем данные для 1С')
            sending_data = prod_list(product_list)

            print(sending_data)

            print('Отправляем данные')
            data = client.service.SendProductAvailability(Shop=31913, ProdAvailabilityList=sending_data)
            print('Ответ сервера: {0}'.format(data))

            print('Удаляем временные файлы')
            shutil.rmtree(tmp_dir)

    @pytest.mark.parametrize("num", range(0, 150))
    def test_1c_parametrize(self, request, num):
        pages = request.config.cache.get("pages", None)
        if len(pages) <= num:
            pytest.skip()

        _tmp_dir = request.config.cache.get("tmp_dir", None)

        def step_open_page(_test: TestAutotest, _page=None, page_url=None, **kwargs):
            return _test.open_page_by_url(page_url, VMRProductPage)

        def clean_wait(**kwargs):
            time.sleep(5)

        def step_get_page_data(_test: TestAutotest, _page: VMRProductPage, page_url):

            def get_product_type(href):
                return href.split('/')[-2]

            product_type = get_product_type(page_url)
            _test.log_text('Продукттип = {0}'.format(product_type))

            with pytest.allure.step('Проверяем доступность продукта'):
                product_available = _page.is_product_available()
                _test.log_text('Продукт доступен' if product_available else 'Продукт недоступен')

                _product_info = {'colors': {}, 'available': product_available}

                if not product_available:
                    _test.log_screenshot()

                sizes_num = 0
                with pytest.allure.step('Собираем информацию о цветах продукта'):
                    for color_element in _page.get_available_color_elements():

                        color_element.click()
                        color_key = color_element.get_key() if color_element.get_key() is not None else 'white'

                        with pytest.allure.step('Цвет: {0}'.format(color_key)):
                            with pytest.allure.step('Активируем цвет'):
                                try:
                                    sizes_elements = _page.get_available_sizes_elements().assure(len_change,
                                                                                                 sizes_num,
                                                                                                 timeout=1)
                                except TimeoutException:
                                    sizes_elements = _page.get_available_sizes_elements()

                            with pytest.allure.step('Записываем доступные цвета и размеры:'):

                                sizes = dict([(item.get_key(), None) for item in sizes_elements])

                                _product_info['colors'][color_key] = sizes
                                _test.log_text('Обнаруженные цвета: {0}'.format(sizes))

                                sizes_num = len(sizes)
                print(_product_info)
                tmp = os.path.join(_tmp_dir, product_type)
                print(tmp)
                if not os.path.exists(tmp):
                    with open(tmp, "w", encoding='UTF8') as file:
                        file.write(json.dumps(_product_info))

                if not _product_info['available']:
                    pytest.fail('Не доступен продукт')

        self.log_text(pages[num])
        page = self.step(step_open_page, clean_wait, self, None, page_url=pages[num])
        self.step(step_get_page_data, step_open_page, self, page, page_url=pages[num])


