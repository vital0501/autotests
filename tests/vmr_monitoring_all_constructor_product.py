import time

import pytest

from module_projects.vmr.base_test import ProjectBaseTest
from module_projects.vmr.pages.ConstructorPage.page import VMRConstructorPage


test_elements_number = 170


class TestAutotest(ProjectBaseTest):
    @pytest.yield_fixture(autouse=True, scope='class')
    def clean_cache(self, request, server_test_run_id):
        yield
        request.config.cache.set("elements_{0}".format(server_test_run_id), None)

    @pytest.mark.parametrize("element_number", range(0, test_elements_number))
    def test_vmr_monitoring_constructor_prices(self, element_number, request, server_test_run_id):

        def step_get_elements(_test: TestAutotest, _server_test_run_id, _element_number, **kwargs):
            _elements = request.config.cache.get("elements_{0}".format(_server_test_run_id), None)

            if _elements is None:
                print('INIT MENU CACHE')
                _page = self.open_main_page().go_to_constructor()
                _elements = _page.PRODUCT_TYPES_WORKER.get_menu_structure()

                _elements = [item.to_json() for item in _elements]
                request.config.cache.set("elements_{0}".format(_server_test_run_id), _elements)

            if _element_number > len(_elements) - 1:
                pytest.skip()

            if test_elements_number < len(_elements):
                pytest.fail('Требуется увеличить количество тестов в соответствии с количеством продуктов.')

            return _elements

        def clean_wait(**kwargs):
            time.sleep(5)

        def step_open_product_constructor(_test: TestAutotest, _page: None, _element):
            _page = _test.open_main_page().go_to_constructor()
            _page.set_product_type_by_name(name=_element['name'], group_name=_element['group_name'])
            return _page

        def step_check_product(_test: TestAutotest, _page: VMRConstructorPage, **kwargs):
            a_colors_elements = _page.get_available_color_elements()
            for color_index, color_element in enumerate(a_colors_elements):
                _page.set_color_by_index(index=color_index)
                with pytest.allure.step("Проверяем цвет '{0}'".format(color_element.get_name())):
                    a_size_elements = _page.get_available_sizes_elements()
                    for size_index, size_element in enumerate(a_size_elements):
                        with pytest.allure.step("Проверяем размер '{0}'".format(size_element.get_name())):
                            _page.set_size_by_index(size_index)
                            with pytest.allure.step("Проверяем что цена больше 0"):
                                _page.CHECK.check_price_not_eq(0, raise_error=True)

        elements = self.step(step_get_elements, clean_wait, self, None, _server_test_run_id=server_test_run_id, _element_number=element_number)
        element = elements[element_number]
        self.log_json_data(log_data=element, log_message="Продукт")

        with pytest.allure.step("Проверяем продукт '{0}'".format(element['name'])):
            with pytest.allure.step("Открываем страницу"):
                page = self.step(step_open_product_constructor, clean_wait, self, None, _element=element)
            with pytest.allure.step("Проверяем продукт"):
                self.step(step_check_product, step_open_product_constructor, self, page, _element=element)
