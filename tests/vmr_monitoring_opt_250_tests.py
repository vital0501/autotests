import pytest
import time

from autotests_base.conditions.element_collection_conditions import len_equal, len_more
from module_projects.vmr.base_test import ProjectBaseTest
from module_projects.vmr.pages.CartPage.page import VMRCartPage
from module_projects.vmr.pages.DesignerPage.page import VMRDesignerPage
from module_projects.vmr.pages.OrderPage.page import VMROrderPage
from module_projects.vmr.pages.ProductPage.page import VMRProductPage


class TestAutotest(ProjectBaseTest):
    def test_vmr_regression_opt_250(self):

        def step_open_designer_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.open_by_url('designer/VQ', VMRDesignerPage)
            return _page

        def clean_wait(_test: TestAutotest, **kwargs):
            time.sleep(5)

        def clean_page(_test: TestAutotest, **kwargs):
            _test.driver.refresh()

        def clean_cart_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()

            if _page.HEADER.get_cart_count() != 0:
                _page = _page.HEADER.go_to_cart()
                _page.clean_cart()

            _page.open_by_url('designer/VQ', VMRDesignerPage)

        def step_get_all_products(_test: TestAutotest, _page: VMRDesignerPage, **kwargs):
            _page_products = page.get_products_elements().assure(len_more, 0)
            return _page_products

        def step_open_product_page(_test: TestAutotest, _page: VMRDesignerPage, **kwargs):
            return _page.go_to_product_by_element(product)

        def step_add_product(_test: TestAutotest, _page: VMRProductPage, pagination, **kwargs):
            for color in _page.get_available_color_elements():
                color.click()

                for size in _page.get_available_sizes_elements():
                    size.click()
                    _page.add_product_to_cart()
                    products["{0} {1} {2}".format(_page.get_name(), size.get_key(),
                                                  color.get_key())] = _page.get_price()

            with pytest.allure.step("Возвращаемся на страницу дизайнера"):
                _page = _page.open_by_url('designer/VQ?sort=sell&page={0}'.format(pagination), VMRDesignerPage)
            return _page

        def step_open_cart_page(_test: TestAutotest, _page, **kwargs):
            _page = _test.open_main_page()
            return _page.HEADER.go_to_cart()

        def step_check_cart_page(_test: TestAutotest, _page: VMRCartPage, _products: dict):
            with pytest.allure.step('Проверяем что в корзине не меньше 250 товаров'):
                _page.HEADER.wait_count_eq(len(_products))

            with pytest.allure.step('Проверяем что стоимость заказа считается корректно'):
                _page.CHECK.check_order_price_eq(sum(_products.values()), raise_error=True)
                _test.log_text(log_message='Сумма заказа = "{0}"'.format(_page.get_order_price()))

        def step_check_order(_test: TestAutotest, _page: VMROrderPage, _total_price):
            with pytest.allure.step('Вводим данные'):
                _page.set_data(email=self.active_test.data.login,
                               phone=self.active_test.data.phone_number,
                               name=self.active_test.data.name,
                               comment=self.active_test.data.comment,
                               address=self.active_test.data.address,
                               city=self.active_test.data.result_city,
                               region=self.active_test.data.result_region
                               )

            with pytest.allure.step("Ждем появления способов доставки"):
                _page.get_all_delivery_elements(num_gte=1, timeout=20)

            with pytest.allure.step('Проверяем что доступна только 1 доставка ""'):
                page.get_all_delivery_elements().assure(len_equal, 1)
                page.get_all_delivery_elements()[0].set_active()

            with pytest.allure.step('Проверяем что доступен только 1 способ оплаты ""'):
                page.get_all_payment_methods_elements().assure(len_equal, 1)
                page.get_all_payment_methods_elements()[0].set_active()

            with pytest.allure.step("Создаем заказ"):
                return _page.go_to_create_order()

        def step_open_order_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.HEADER.go_to_cart()
            return _page.go_to_order_page()

        self.subtest_start(1089)
        max_count = 250
        products = {}
        added_products = []
        pagination = 1
        with pytest.allure.step('Открываем страницу дизайнера VQ'):
            page = self.step(step_open_designer_page, clean_wait, self, None)

        with pytest.allure.step("Добавляем 250 товаров в корзину"):
            while page.HEADER.get_cart_count() < max_count:

                with pytest.allure.step("Получаем список всех продуктов на странице"):
                    page_products = self.step(step_get_all_products, clean_wait, self, page)

                    for product in page_products:
                        if product.get_name() not in added_products:
                            with pytest.allure.step("Добавляем все размеры и цвета товара '{0}'".format(product.get_name())):
                                page = self.step(step_open_product_page, clean_page, self, page)
                                added_products.append(page.get_name())
                                page = self.step(step_add_product, clean_wait, self, page, pagination=pagination)
                                if page.HEADER.get_cart_count() >= max_count:
                                    break
                    pagination += 1

        with pytest.allure.step('Переходим в корзину'):
            page = self.step(step_open_cart_page, clean_wait, self, page)

        with pytest.allure.step('Проверяем страницу корзины'):
            self.step(step_check_cart_page, step_open_cart_page, self, page, _products=products)

        with pytest.allure.step('Открываем страницу заказа'):
            page = self.step(step_open_order_page, step_open_cart_page, self, None)

        with pytest.allure.step('Создаем заказ'):
            page = self.step(step_check_order, step_open_order_page, self, page, _total_price=sum(products.values()))

        self.log_text(log_message='Номер заказа: {0}'.format(page.get_order_num()))
        self.subtest_pass(1089)