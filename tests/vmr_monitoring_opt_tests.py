import time

import pytest

from autotests_base.conditions.element_collection_conditions import len_more
from autotests_base.conditions.element_conditions import is_displayed, is_not_displayed
from module_projects.vmr.base_test import ProjectBaseTest
from module_projects.vmr.pages.CartPage.exception import CartEndedProductException
from module_projects.vmr.pages.ConstructorPage.page import VMRConstructorPage
from module_projects.vmr.pages.DesignerPage.page import VMRDesignerPage
from module_projects.vmr.pages.MainPage.page import VMRMainPage
from module_projects.vmr.pages.OrderPage.page import VMROrderPage
from module_projects.vmr.pages.ProductPage.page import VMRProductPage


class TestAutotest(ProjectBaseTest):
    def test_opt_order_50_product(self):
        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_wait(**kwargs):
            time.sleep(5)

        def step_open_constructor_page(_test: TestAutotest, _page: VMRMainPage):
            return _page.HEADER.DROP_DOWN_MENU.go_to_menu_item_by_name(name='Свой дизайн')

        def clean_open_constructor_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            return _page.HEADER.DROP_DOWN_MENU.go_to_menu_item_by_name(name='Свой дизайн')

        def step_add_constructor_product(_test: TestAutotest, _page: VMRConstructorPage, _product_type_index, _size_index, _color_index):
            with pytest.allure.step("Задаем продукт-тип"):
                if _page.get_active_product_type().get_name() != _page.get_active_product_types_elements()[_product_type_index].get_name():
                    _page.set_product_type_by_index(index=_product_type_index)
                    time.sleep(5)

            with pytest.allure.step("Задаем цвет"):
                _page.set_color_by_index(index=_color_index)
                time.sleep(1)

            with pytest.allure.step("Задаем размер"):
                _page.set_size_by_index(index=_size_index)
                time.sleep(1)

            with pytest.allure.step("Добавляем в корзину"):
                _page.add_product_to_cart()

            with pytest.allure.step("Ждем подтверждающий поп-ап"):
                _pop_up = _page.get_add_product_pop_up().assure(is_displayed, timeout=20)

            with pytest.allure.step("Закрываем поп-ап"):
                _pop_up.close()
                page.get_add_product_pop_up().assure(is_not_displayed)

        def step_open_designer_page(_test, _page: VMRMainPage, **kwargs):
            return _page.open_by_url("designer/VQ", VMRDesignerPage)

        def step_open_product_page(_test: TestAutotest, _page: VMRDesignerPage, product_index: int, **kwargs):
            return _page.go_to_product_by_index(index=product_index)

        def clean_open_product_page(_test: TestAutotest, product_index, **kwargs):
            return _test.open_main_page().open_by_url("designer/VQ", VMRDesignerPage).go_to_product_by_index(index=product_index)

        def step_add_product_page(_test, _page: VMRProductPage, _color_index: int, _size_index: int, **kwargs):
            _prev_count = _page.HEADER.get_cart_count()
            _page.set_color_by_index(index=_color_index)
            time.sleep(0.5)
            _page.set_size_by_index(index=_size_index)
            time.sleep(0.5)
            _page.add_product_to_cart()
            _page.HEADER.wait_count_eq(_prev_count + 1)

        def step_open_order_order(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.HEADER.go_to_cart()
            try:
                return _page.go_to_order_page()
            except CartEndedProductException:
                _test.log_screenshot()
                for item in _page.get_product_elements():
                    if item.is_ended():
                        item.scroll_to_element()
                        _test.log_screenshot()
                pytest.fail("Заказ содержит закончившиеся продукты")

        def step_create_order(_test: TestAutotest, _page: VMROrderPage, **kwargs):
            with pytest.allure.step('Вводим данные'):
                _page.set_data(email=self.active_test.data.login,
                               phone=self.active_test.data.phone_number,
                               name=self.active_test.data.name,
                               comment=self.active_test.data.comment,
                               address=self.active_test.data.address,
                               city=self.active_test.data.result_city,
                               region=self.active_test.data.result_region
                               )

            with pytest.allure.step("Ждем появления способов доставки"):
                _page.get_all_delivery_elements(num_gte=1, timeout=20)

            with pytest.allure.step("Создаем заказ"):
                return _page.go_to_create_order()

        page = self.step(step_open_main_page, clean_wait, self, None)
        page = self.step(step_open_constructor_page, step_open_main_page, self, page)

        added_products = []

        with pytest.allure.step('Добавление конструкторских продуктов'):
            exit_conditions = False
            for product_type_index, product_type_element in enumerate(page.get_active_product_types_elements().assure(len_more, 0)):
                for color_index, color_element in enumerate(page.get_colors_elements().assure(len_more, 0)):
                    for size_index, size_element in enumerate(page.get_sizes_elements().assure(len_more, 0)):
                        with pytest.allure.step('Получение данных'):
                            pr_type_name = product_type_element.get_name()
                            pr_color_name = color_element.get_name()
                            pr_size_name = size_element.get_name()

                        with pytest.allure.step('Добавление "{0}", цвет: {1}, размер: {2}'.format(pr_type_name,
                                                                                                  pr_color_name,
                                                                                                  pr_size_name)):
                            if (pr_type_name, pr_size_name, pr_color_name) not in added_products:
                                self.step(step_add_constructor_product, clean_open_constructor_page, self, page,
                                          _product_type_index=product_type_index, _size_index=size_index, _color_index=color_index)
                                added_products.append((product_type_element.get_name(), size_element.get_name(), color_element.get_name()))
                            else:
                                continue
                            if len(added_products) > 25:
                                exit_conditions = True
                                break
                    if exit_conditions:
                        break
                if exit_conditions:
                    break

        page = self.step(step_open_designer_page, clean_wait, self, page)

        with pytest.allure.step('Добавление продуктов из каталога'):
            # page = VMRDesignerPage()
            exit_conditions = False

            products = page.get_products_elements()
            for index, product in enumerate(products):
                page = self.step(step_open_product_page, step_open_designer_page, self, page, product_index=index)
                # page = VMRProductPage()
                for color_index, color_element in enumerate(page.get_available_color_elements()):
                    for size_index, size_element in enumerate(page.get_available_sizes_elements()):
                        with pytest.allure.step('Получение данных'):
                            pr_name = page.get_name()
                            pr_color_name = color_element.get_name()
                            pr_size_name = size_element.get_name()

                        with pytest.allure.step('Добавление "{0}", цвет: {1}, размер: {2}'.format(pr_name,
                                                                                                  pr_color_name,
                                                                                                  pr_size_name)):
                            if (pr_name, pr_size_name, pr_color_name) not in added_products:
                                self.step(step_add_product_page, clean_open_product_page,
                                          self,
                                          page,
                                          product_index=index,
                                          _color_index=color_index,
                                          _size_index=size_index)
                                added_products.append((pr_name, pr_size_name, pr_color_name))
                            else:
                                continue

                            if len(added_products) > 50:
                                exit_conditions = True
                                break

                    if exit_conditions:
                        break
                if exit_conditions:
                    break

                # page = VMRDesignerPage()
                page = page.back(VMRDesignerPage)

        page = self.step(step_open_order_order, clean_wait, self, None)
        self.step(step_create_order, step_open_order_order, self, page)