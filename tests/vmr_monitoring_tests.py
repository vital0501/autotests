import pytest

from module_projects.vmr.base_test import ProjectBaseTest
from module_projects.vmr.pages.CatalogPage.exception import EmptyCatalogPageException
from module_projects.vmr.pages.CatalogPage.page import VMRCatalogPage
from module_projects.vmr.pages.MainPage.page import VMRMainPage


class TestAutotest(ProjectBaseTest):

    def test_vmr_monitoring_empty_catalog(self):

        def step_open_main_page(_test, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test, _page):
            _test.driver.refresh()

        def step_go_to_catalog(_test, _page: VMRMainPage, catalog_index):
            _page.CATALOG_MENU.go_to_catalog_by_index(catalog_index)
            return _page

        def step_get_name_catalog(_test, _page: VMRCatalogPage, catalog_index):
            catalog = page.CATALOG_MENU.get_catalog_element(catalog_index)
            return catalog.text

        def clean_catalog(_test, _page, catalog_index):
            _test.driver.refresh()

        def step_check_catalog(_test, _page: VMRCatalogPage, catalog_name):
            try:
                page.CHECK.check_not_empty_catalog(catalog_name, raise_error=True)
            except EmptyCatalogPageException as e:
               return str(e)

        def clean_check_catalog(_test, _page, catalog_name):
            _test.driver.refresh()

        self.subtest_start(984)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
            errors = []

        with pytest.allure.step('Проверяем, что каталогов > 5'):
            page.CATALOG_MENU.CHECK.check_min_catalogs( raise_error=True)

            with pytest.allure.step('Открываем по порядку все категории'):
                for catalog_index in range(0, len(page.CATALOG_MENU.get_catalogs_elements())):
                    page = self.step(step_go_to_catalog, clean_catalog, self, page, catalog_index=catalog_index)

                    with pytest.allure.step('Получаем наименование категории'):
                        catalog_name = self.step(step_get_name_catalog, clean_catalog, self, page, catalog_index=catalog_index)

                        with pytest.allure.step('Проверяем, что каталог "{0}" не пустой'.format(catalog_name)):
                            _catalog_result = self.step(step_check_catalog, clean_check_catalog, self, page, catalog_name=catalog_name)
                            if _catalog_result is not None:
                                errors.append(_catalog_result)

        if errors:
            pytest.fail("\nОбнаружены ошибки:\n\t" + "\n\t".join(errors))
        self.subtest_pass(984)
