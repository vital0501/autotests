import pytest
import time

from module_projects.vmr.base_test import ProjectBaseTest
from module_projects.vmr.pages.BasePage.exception import ValueException
from module_projects.vmr.pages.CatalogPage.exception import CatalogPageProductException
from module_projects.vmr.pages.CatalogPage.page import VMRCatalogPage


class TestAutotest(ProjectBaseTest):

    @pytest.mark.parametrize("page_num", range(0, 50))
    def test_vmr_null_price(self, page_num):
        def step_open_page(_test: TestAutotest, _page, page_num, **kwargs):
            return _test.open_page_by_url('/catalog/?sort=new&page={0}' .format(page_num + 1), VMRCatalogPage)

        def clean_wait(**kwargs):
            time.sleep(5)

        def step_check_catalog_page(_test: TestAutotest, _page: VMRCatalogPage, **kwargs):
            _error = []

            for item in _page.get_products_elements():
                _product_error = False
                try:
                    _product_price = item.get_price()
                    if not _page.CHECK.check_product_null_price(_product_price):
                        _product_error = True, "Нулевая цена"
                except CatalogPageProductException:
                    _product_error = True, "Не найдена цена"

                if _product_error:
                    _name = item.get_name()
                    _msg = 'Продукт {0}. Ошибка: {1}'.format(_name, _product_error[1])
                    with pytest.allure.step(_msg):
                        _test.log_screenshot()
                        _error.append(_msg)

            return _error

        with pytest.allure.step('Открываем страницу'):
            page = self.step(step_open_page, clean_wait, self, None, page_num=page_num)
        with pytest.allure.step('Проверяем нулевые цены'):
            errors = self.step(step_check_catalog_page, step_open_page, self, page)

        if errors:
            msg = "\nОбнаружены ошибки:" + "\n\t".join([item for item in errors])
            raise ValueException(msg)