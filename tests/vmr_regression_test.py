import random

import math
import re

import pytest
import time
from selenium.common.exceptions import TimeoutException

import settings
from autotests_base.conditions.element_collection_conditions import len_equal, len_more
from autotests_base.conditions.element_conditions import is_displayed, exist, name_not_none, text, is_not_displayed
from autotests_base.exception import RegistrationEmptyEmailException, RegistrationIncorrectPasswordException, \
    RegistrationEmailAlreadyExistException, ExternalProjectPageException
from module_projects.vmr.base_test import ProjectBaseTest
from module_projects.vmr.pages.AboutPage.page import VMRAboutPage
from module_projects.vmr.pages.CatalogPage.page import VMRCatalogPage
from module_projects.vmr.pages.ConstructorPage.page import VMRConstructorPage
from module_projects.vmr.pages.ContactsPage.page import VMRContactsPage
from module_projects.vmr.pages.DeliveryPage.page import VMRDeliveryPage
from module_projects.vmr.pages.HelpPage.exception import HelpNotInfoException
from module_projects.vmr.pages.HelpPage.page import VMRHelpPage
from module_projects.vmr.pages.MainPage.exception import VMRMainPageRedirectException
from module_projects.vmr.pages.ProductPage.exception import ProductException
from module_projects.vmr.pages.ProductPage.page import VMRProductPage
from module_projects.vmr.pages.DesignerPage.page import VMRDesignerPage
from module_projects.vmr.pages.OrderPage.page import VMROrderPage
from module_projects.vmr.pages.CartPage.exception import CartException, CartNotEmptyException, \
    CartNotFoundRecommendProductException, CartNotRefreshRecommendProductException, CartValueException, \
    CartProductNotFoundException, CartBigImageException, CartProductDeleteException
from module_projects.vmr.pages.PrivateOfficeAdressPage.page import VMRPrivateOfficeAdressPage
from module_projects.vmr.pages.PrivateOfficePage.page import VMRPrivateOfficePage
from module_projects.vmr.pages.PrivateOfficeQuestionsPage.page import VMRPrivateOfficeQuestionsPage
from module_projects.vmr.pages.ReviewsPage.exception import ReviewsPagePaginationException, ReviewsPageNotOpenSocialUrl, \
    ReviewsPageNotOpenServicesUrl
from module_projects.vmr.pages.ReviewsPage.page import VMRReviewsPage
from module_projects.vmr.pages.SearchPage.exception import SearchPageNotProductMsgException, SearchNotRelevantException
from module_projects.vmr.pages.SearchPage.page import VMRSearchPage
from module_projects.vmr.parts.VMRFooter.exception import FooterFormMsgNotFoundException, \
    FooterPhoneIncorrectedException, FooterIosAppException, FooterAndroidAppException, FooterSubscribeNotFoundException, \
    FooterSubscribeErrorMsgException
from module_projects.vmr.parts.VMRHeader.exception import HeaderOpenHelpPageException, HeaderOpenDeliveryPageException
from module_projects.vmr.pages.StatusPage.page import VMRStatusPage
from module_projects.vmr.parts.VMRHeader.exception import HeaderPhoneIncorrectedException
from module_projects.vmr.parts.VMRSocialNetwork.exception import SocialNetworkException
from module_projects.vmr.pages.CartPage.page import VMRCartPage

from module_projects.vmr.pages.MainPage.page import VMRMainPage
from module_projects.vmr.pages.SportPage.exception import SportPageException
from module_projects.vmr.pages.SportPage.page import VMRSportPage
from module_projects.vmr.pages.SportProductPage.exception import SportProductPopupException
from module_projects.vmr.pages.SportProductPage.page import VMRSportProductPage
from module_projects.vmr.pages.SportProductPage.exception import SportProductException


class TestAutotest(ProjectBaseTest):

    def test_vmr_favorites(self):

        def step_open_main_page(_test: TestAutotest, **kwargs):
            return _test.open_main_page()

        def clean_page(_test: TestAutotest, **kwargs):
            _test.driver.refresh()

        def check_favorites_count(_test: TestAutotest, _page: VMRMainPage, value, **kwargs):
            _page.HEADER.CHECK.check_value_favorites_eq(value=value, raise_error=True)

        def check_empty_favorites_page(_test: TestAutotest, _page: VMRMainPage, value, **kwargs):
            _page = _page.HEADER.go_to_favorite_page()
            _page.CHECK.check_is_empty_text_favorites(value, raise_error=True)
            _page = _test.open_main_page()
            return _page

        def step_add_product_to_favorites(_test: TestAutotest, _page: VMRMainPage, product, index, _product_add, **kwargs):
            product.mouse_over_element().add_favorites()
            _page.HEADER.CHECK.check_value_favorites_eq(value=index, raise_error=True)
            _product_add.append(product.get_name())
            return _product_add

        def check_products_favorites_page(_test: TestAutotest, _page, product_add, **kwargs):
            _page = page.HEADER.go_to_favorite_page()
            time.sleep(2)
            products_favorits = _page.get_products_elements()
            with pytest.allure.step('Проверяем количество в избранном'):
                _page.CHECK.check_is_product_favorites_count(
                    product_add=product_add, products_favorits=products_favorits, raise_error=True)
            with pytest.allure.step('Проверяем соответствие наименовния в избранном'):
                _page.CHECK.check_is_product_favorites_name(
                    product_add=product_add, products_favorits=products_favorits, raise_error=True)
            return _page

        def step_delete_product_from_favorites(_test: TestAutotest, _page, index, **kwargs):
            _page.delete_product_from_favorites(index)

        def step_open_product_page(_test: TestAutotest, _page: VMRCatalogPage, **kwargs):
            return _page.go_to_product_by_product(random.choice(_page.get_products_elements()))

        def step_add_favorites_product_page(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            _page.add_favorites()

        def step_del_favorites_product_page(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            _page.del_favorites()

        def step_add_product_to_cart(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            _page.set_random_size()
            _page.add_product_to_cart()
            return _page.go_to_cart()

        def step_add_favorites_cart_page(_test: TestAutotest, _page: VMRCartPage, **kwargs):
            _page.add_favorites()
            return _page.get_product_element(0).get_name()

        def check_cart_page_favorites(_test: TestAutotest, _page, **kwargs):
            _page = page.HEADER.go_to_cart()
            with pytest.allure.step('Проверяем, что "сердечко" избранного не активно'):
                _page.CHECK.check_favorites_active(raise_error=True)
            return _page

        self.subtest_start(302)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверяем отображение пустого "Избранного"'):
            self.step(check_favorites_count, clean_page, self, page, value=0)
        self.subtest_pass(302)

        self.subtest_start(295)
        with pytest.allure.step('Проверяем, что страница избранного пустая'):
            page = self.step(check_empty_favorites_page, clean_page, self, page, value='Вы еще ничего не добавили в избранное. Вернуться в каталог')
        self.subtest_pass(295)

        self.subtest_start(301)
        product_add = []
        i = 0
        with pytest.allure.step('Добавляем 3-и товара в избранное'):
            for product in page.get_products_elements():
                i+= 1
                with pytest.allure.step('Добавляем товар в избранное и проверяем счетчик'):
                    product_add = self.step(step_add_product_to_favorites, clean_page, self, page,
                                            product=product, index=i, _product_add=product_add)
                if i == 3:
                    break
        with pytest.allure.step('Проверяем соответствие товара в избранном'):
            page = self.step(check_products_favorites_page, clean_page, self, page, product_add=product_add)
        self.subtest_pass(301)

        self.subtest_start(300)
        with pytest.allure.step('Удаляем первый товар из избранного'):
            self.step(step_delete_product_from_favorites, clean_page, self, page, index=0)
        with pytest.allure.step('Проверяем соответствие товара в избранном. И очищаем "избранное"'):
            page = self.step(check_products_favorites_page, clean_page, self, page, product_add=product_add[1:])
            self.step(step_delete_product_from_favorites, clean_page, self, page, index=0)
            self.step(step_delete_product_from_favorites, clean_page, self, page, index=0)
        self.subtest_pass(300)

        self.subtest_start(296)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открываем карточку рандомного товара'):
            page = self.step(step_open_product_page, clean_page, self, page)
        with pytest.allure.step('Проверяем отображение пустого "Избранного"'):
            self.step(check_favorites_count, clean_page, self, page, value=0)
        with pytest.allure.step('Добавляем товар в избранное'):
            self.step(step_add_favorites_product_page, clean_page, self, page)
        with pytest.allure.step('Проверяем, что в "Избранном" 1 товар'):
            self.step(check_favorites_count, clean_page, self, page, value=1)
        with pytest.allure.step('Удаляем товар из "избранного" в карточке продукта'):
            self.step(step_del_favorites_product_page, clean_page, self, page)
        with pytest.allure.step('Проверяем, что в "Избранном" нет товаров'):
            self.step(check_favorites_count, clean_page, self, page, value=0)
        with pytest.allure.step('Добавляем товар в избранное'):
            self.step(step_add_favorites_product_page, clean_page, self, page)
        with pytest.allure.step('Проверяем, что в "избранное" добавился товар из карточки'):
            page = self.step(check_products_favorites_page, clean_page, self, page, product_add=[page.get_name()])
        with pytest.allure.step('Удаляем товар из "избранного"'):
            self.step(step_delete_product_from_favorites, clean_page, self, page, index=0)
        self.subtest_pass(296)

        self.subtest_start(300)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открываем карточку рандомного товара'):
            page = self.step(step_open_product_page, clean_page, self, page)
        with pytest.allure.step('Добавляем товар в корзину'):
            page = self.step(step_add_product_to_cart, clean_page, self, page)
        with pytest.allure.step('Добавляем товар в избранное'):
            product_cart = self.step(step_add_favorites_cart_page, clean_page, self, page)
        with pytest.allure.step('Проверяем, что в "избранное" добавился товар из корзины'):
            page = self.step(check_products_favorites_page, clean_page, self, page, product_add=[product_cart])
        with pytest.allure.step('Удаляем товар из "избранного"'):
            self.step(step_delete_product_from_favorites, clean_page, self, page, index=0)
        with pytest.allure.step('Проверяем, что в корзине товар удален из избранного'):
            self.step(check_cart_page_favorites, clean_page, self, page)
        self.subtest_pass(300)

    def test_vmr_login(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_private_office(_test: TestAutotest, _page, **kwargs):
            with pytest.allure.step('Вводим логин с некорректными данными'):
                _page.HEADER.UPPER_MENU.fake_login(login=_test.active_test.data.fake_login,
                                                     password=_test.active_test.data.password)
            with pytest.allure.step('Проверяем наличие сообщения об ошибочном вводе'):
                _page.HEADER.UPPER_MENU.LOGIN.CHECK.check_error_login_txt_eq(
                    value='Пользователь с такой парой логин-пароль не найден', raise_error=True)

        self.subtest_start(12)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверяем наличие сообщения при ошибочной авторизации'):
            self.step(step_open_private_office, step_open_main_page, self, page)
        self.subtest_pass(12)

    def test_vmr_registration(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_registration(_test: TestAutotest, _page, **kwargs):
            with pytest.allure.step('Вводим существующий логин'):
                _page.HEADER.UPPER_MENU.new_login_registration(login='autotest@vsemayki.ru', password='qazwsxedc')
            with pytest.allure.step('Проверяем наличие сообщения об ошибочной регистрации'):
                _page.HEADER.UPPER_MENU.REGISTRATION.get_loader_registration_element().assure(is_not_displayed, timeout=100)
                _page.HEADER.UPPER_MENU.REGISTRATION.CHECK.check_error_registration_txt_eq(
                    value='Ошибка регистрации', raise_error=True)

        self.subtest_start(18)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверяем наличие сообщения при ошибочной авторизации'):
            self.step(step_open_registration, step_open_main_page, self, page)
        self.subtest_pass(18)

    def test_vmr_location(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_city_popup(_test: TestAutotest, _page: VMRMainPage):
            with pytest.allure.step('Установка локации из выпадающего списка городов'):
                _page.HEADER.get_pop_up()
                _city = 'Новосибирск' if page.HEADER.get_city_name() != 'Новосибирск' else 'Москва'
                _search_word = 'новоси' if _city != 'Москва' else 'москва'
                _page.HEADER.set_city_drop_down_list(_search_word, _city)
            return _city

        def step_open_city_top_popup(_test: TestAutotest, _page: VMRMainPage):
            _city =_test.active_test.data.city
            _page.HEADER.get_pop_up()
            _page.HEADER.set_city_by_list(_city)
            return _city

        def step_city_input(_test: TestAutotest, _page: VMRMainPage):
            with pytest.allure.step('Установка локации из выпадающего списка городов'):
                _city = 'Ижевск'
                _page.HEADER.get_pop_up()
                _page.HEADER.set_city_by_input(_city)
            return _city

        def check_set_city(_test: TestAutotest, _page: VMRMainPage, city, **kwargs):
            with pytest.allure.step("Проверяем совпадение города."):
                _page.HEADER.CHECK.check_city_eq(city, raise_error=True)

        def check_set_city_phone(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            if _page.HEADER.get_city_phone_element().text != _test.active_test.data.phone_city:
                raise HeaderPhoneIncorrectedException('Телефон не изменился или не совпадает с "{0}"'
                                                      .format(self.active_test.data.phone_city))

        self.subtest_start(132)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Установка локации из выпадающего списка городов'):
            city = self.step(step_open_city_popup, step_open_main_page, self, page)
        with pytest.allure.step('Проверка установленного города'):
            self.step(check_set_city, clean_page, self, page, city=city)
        self.subtest_start(132)

        self.subtest_start(307)
        with pytest.allure.step('Проверка изменения телефона'):
            self.step(check_set_city_phone, clean_page, self, page)
        self.subtest_pass(307)

        self.subtest_start(305)
        with pytest.allure.step('Установка локации путем прямого ввода с клавиатуры'):
            city = self.step(step_city_input, step_open_main_page, self, page)
        with pytest.allure.step('Проверка установленного города'):
            self.step(check_set_city, clean_page, self, page, city=city)
        self.subtest_pass(305)

        self.subtest_start(306)
        with pytest.allure.step('Установка локации из топ списка городов'):
            city = self.step(step_open_city_top_popup, step_open_main_page, self, page)
        with pytest.allure.step('Проверка установленного города'):
            self.step(check_set_city, clean_page, self, page, city=city)
        self.subtest_pass(306)

    def test_vmr_cart(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_cart_page(_test: TestAutotest, _page: VMRMainPage):
            _page = _page.HEADER.go_to_cart()
            with pytest.allure.step('Проверяем что корзина пустая'):
                if not _page.is_empty():
                    raise CartNotEmptyException('Корзина не пустая')
            return _page

        def step_open_main_page_from_cart(_test: TestAutotest, _page: VMRCartPage):
            with pytest.allure.step('Ждем загрузки главной страницы'):
                _page = _page.set_choice_in_shop()
            return _page

        def step_open_constructor_page_from_cart(_test: TestAutotest, _page: VMRCartPage):
            with pytest.allure.step('Открываем конструктор через "Создать товар"'):
                _page = _page.create_product()
            return _page

        def step_open_product_page(_test: TestAutotest, _page: VMRCatalogPage, **kwargs):
            return _page.go_to_product_by_product(random.choice(_page.get_products_elements()))

        def step_add_product_to_cart(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            _page.set_random_size()
            _page.add_product_to_cart()
            return _page.go_to_cart()

        def step_interesting_products(_test: TestAutotest, _page: VMRCartPage, **kwargs):
            names = []
            names2 = []
            with pytest.allure.step('Получаем список рекомендованных продуктов'):
                try:
                    for item in page.get_interesting_products().assure(len_more, 1):
                        if item.assure(exist).assure(is_displayed):
                            names.append(item.assure(name_not_none).get_name())
                except TimeoutException:
                    raise CartNotFoundRecommendProductException("Не показываются продукты в секции \"Вас может заинтересовать\"")

            with pytest.allure.step('Обновляем список рекомендованных продуктов'):
                page.refresh_interesting_products()

            with pytest.allure.step('Сравниваем списки рекомендованных продуктов'):
                for i in range(0, 3):
                    names2.clear()
                    for item in page.get_interesting_products().assure(len_more, 1):
                        if item.assure(exist).assure(is_displayed):
                            names2.append(item.assure(name_not_none).get_name())

                if names == names2:
                    raise CartNotRefreshRecommendProductException("Значения не обновились. Было: {0}. Стало: {1}"
                                                                  .format(names, names2))

        def step_add_from_interesting_products(_test: TestAutotest, _page: VMRCartPage, count_product, **kwargs):
            for item in _page.get_interesting_products().assure(len_more, 1):
                if item.assure(exist).assure(is_displayed):
                    product_name = item.assure(name_not_none).get_name()
                    item.add_to_cart()
                    break
                time.sleep(1)
            with pytest.allure.step('Ждем появляения товара в корзине'):
                _products = _page.get_products_elements().assure(len_equal, count_product)
                with pytest.allure.step('Проверяем что у корзины изенился счетчик'):
                    if page.HEADER.get_cart_count() != count_product:
                        raise CartValueException("Некорректное значение счетчика. Ожидаем: {0}. Видим: {1}"
                                                 .format(count_product, page.HEADER.get_cart_count()))

                for product in _products:
                    if product.get_name() == product_name:
                        break
                else:
                    raise CartProductNotFoundException("Продукт не найден в корзине")
            return _products

        def clean_cart_page(_test: TestAutotest, **kwargs):
            with pytest.allure.step('Очищаем корзину'):
                _page = _test.open_main_page()
            if _page.HEADER.get_cart_count() != 0:
                _page = _page.HEADER.go_to_cart()
                _page.clean_cart()

        def step_more_img(_test: TestAutotest, _page: VMRCartPage, products, **kwargs):
            with pytest.allure.step('Проверяем что появляется поп-ап увеличенного просмотра'):
                full_href = products[1].get_full_img_href()
                products[1].open_img_pop_up()

                with pytest.allure.step('Проверяем что ссылка на картинку у товара совпадает с ссылкой в поп-апе'):
                    if page.get_pop_up_img_href() != full_href:
                        raise CartBigImageException("Ссылки на картинку и картинку в поп-апе не совпадают")

            with pytest.allure.step('Проверяем листание картинок'):
                page.next_pop_up_image()
                time.sleep(1)
                with pytest.allure.step('Проверяем что ссылка изменилась'):
                    if page.get_pop_up_img_href() == full_href:
                        raise CartBigImageException("Ссылка на картинку не изменилась")

            with pytest.allure.step('Закрываем поп-ап увеличенного просмотра'):
                page.close_pop_up()
                time.sleep(1)
                if page.is_pop_up():
                    raise CartBigImageException("Увеличенная картинка не закрылась")

        def step_delete_product_from_cart(_test: TestAutotest, _page: VMRCartPage, products, **kwargs):
            product_name = products[2].get_name()
            products[2].del_from_cart()
            time.sleep(1)

            for product in page.get_products_elements().assure(len_equal, 2):
                if product_name == product.assure(exist).get_name():
                    raise CartProductDeleteException("Продукт не удален из корзины")
                try:
                    page.HEADER.get_cart_count_element().assure(text, '2')
                except TimeoutException:
                    raise CartProductDeleteException("Счетчик корзины не изменился")

        def step_return_main_page(_test: TestAutotest, _page: VMRCartPage, **kwargs):
            _page = _page.return_main_page()
            return _page

        def step_go_to_product_page(_test: TestAutotest, _page: VMRCartPage, **kwargs):
            with pytest.allure.step('Переходим в корзину'):
                _page = _page.HEADER.go_to_cart()
            with pytest.allure.step('Открываем карточку товара'):
                _page = _page.go_to_product_page(1)
            return _page

        self.subtest_start(134)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Переходим в корзину'):
            page = self.step(step_open_cart_page, clean_page, self, page)
        self.subtest_pass(134)

        # self.subtest_start(266)
        # with pytest.allure.step('Переходим в корзину'):
        #     page = self.step(step_open_cart_page, clean_page, self, page)
        # with pytest.allure.step('Кликаем на "Создать товар"'):
        #     page = self.step(step_open_constructor_page_from_cart, step_open_cart_page, self, page)
        # self.subtest_pass(266)

        self.subtest_start(267)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Переходим в корзину'):
            page = self.step(step_open_cart_page, clean_page, self, page)
        with pytest.allure.step('Кликаем на "Выбрать в магазине"'):
            page = self.step(step_open_main_page_from_cart, step_open_cart_page, self, page)
        with pytest.allure.step('Открываем карточку рандомного товара'):
            page = self.step(step_open_product_page, clean_page, self, page)
        with pytest.allure.step('Добавляем товар в корзину'):
            page = self.step(step_add_product_to_cart, clean_page, self, page)
        self.subtest_pass(267)

        self.subtest_start(280)
        with pytest.allure.step('Проверяем обновление секции "Вас может заинтересовать"'):
            self.step(step_interesting_products, clean_page, self, page)
        self.subtest_pass(280)

        self.subtest_start(268)
        with pytest.allure.step('Добавляем товар из секции "Вас может заинтересовать"'):
            products = self.step(step_add_from_interesting_products, clean_cart_page, self, page, count_product=2)
        self.subtest_pass(268)

        self.subtest_start(279)
        with pytest.allure.step('Проверяем поп-ап увеличенного просмотра'):
            self.step(step_more_img, clean_page, self, page, products=products)
        self.subtest_pass(279)

        self.subtest_start(269)
        with pytest.allure.step('Добавляем еще 1 товар из секции "Вас может заинтересовать"'):
            products = self.step(step_add_from_interesting_products, clean_cart_page, self, page, count_product=3)
        self.subtest_pass(269)

        self.subtest_start(270)
        with pytest.allure.step('Проверяем удаление товара из корзины'):
            self.step(step_delete_product_from_cart, step_add_from_interesting_products, self, page, products=products)
        self.subtest_pass(270)

        self.subtest_start(271)
        with pytest.allure.step('Проверяем работу кнопки "Вернуться к выбору"'):
            page = self.step(step_return_main_page, clean_page, self, page, products=products)
        self.subtest_pass(271)

        self.subtest_start(272)
        with pytest.allure.step('Проверяем переход к карточке товара"'):
            page = self.step(step_go_to_product_page, clean_page, self, page)
        self.subtest_pass(272)

        self.subtest_start(273)
        with pytest.allure.step('Проверяем добавление в избранное из корзины"'):
            page = page.HEADER.go_to_cart_page()
            page.add_to_favorites(1)

            for i in range(0, 5):
                if page.is_product_in_favorites(1):
                    break
                    time.sleep(1)
                else:
                    pytest.fail("У продукта не изменилась иконка избранного")
                if page.HEADER.get_favorites_count() != 1:
                    time.sleep(1)
                    pytest.fail("Счетчик избранного не изменился")
        self.subtest_pass(273)

    def test_vmr_reviews(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_contacts_page(_test: TestAutotest, _page: VMRMainPage, _name):
            with pytest.allure.step('Открываем страницу "Контакты"'):
                _page = page.FOOTER.LOWER_MENU.go_to_menu_item_by_name(_name)
            with pytest.allure.step('Открываем страницу отзывов'):
                _page = _page.LEFT_MENU.go_to_reviews_page()
            return _page

        def step_open_reviews_page(_test: TestAutotest, _page: VMRMainPage):
            _page = _page.go_to_reviews_page()
            return _page

        def step_reviews_page_pagination(_test: TestAutotest, _page: VMRReviewsPage):
            reviews = [[item.get_user(), item.get_text()] for item in _page.get_all_reviews_element()]
            _page = _page.go_next_pagination()
            reviews2 = [[item.get_user(), item.get_text()] for item in page.get_all_reviews_element()]
            _page.go_previsions_pagination()
            if reviews == reviews2:
                raise ReviewsPagePaginationException('При пагинации отзывы не меняются')

        def step_reviews_page_go_social_net(_test: TestAutotest, _page: VMRReviewsPage):

            def get_test_number_in_dict(sn_array, sn_item):
                for item in sn_array.keys():
                    if sn_array[item].get_type() == sn_item.get_type():
                        return item

            with pytest.allure.step('Проверка что присутствуют все ссылки соц. сетей'):
                sn = {}
                sn.update({176: _page.get_vk_element()})
                sn.update({177: _page.get_facebook_element()})
                sn.update({180: _page.get_moymir_element()})
                sn.update({179: _page.get_ok_element()})
                sn.update({178: _page.get_twitter_element()})
                sn.update({181: _page.get_google_plus_element()})

            with pytest.allure.step('Проверка переходов на социальные сети'):
                result = []
                items = _page.get_social_networks_elements()
                for item in items:
                    self.subtest_start(get_test_number_in_dict(sn, item))
                    item.click()
                    with pytest.allure.step('Проверка {0}'.format(item.get_type().__class__.__name__)):
                        i = 0
                        while len(_test.driver.window_handles) != 2:
                            time.sleep(0.2)
                            i += 1
                            if i == 50:
                                raise ReviewsPageNotOpenSocialUrl('Не открывается ссылка в отдельном окне')

                        expect_url = item.get_expect_url()

                        _test.driver.switch_to_window(self.driver.window_handles[1])

                        if expect_url not in _test.driver.current_url:
                            result.append('Ссылка открытая по клику на {0} не совпадает с ожидаемой "{1}"'
                                          .format(_test.driver.current_url, expect_url))
                            _test.subtest_fail(get_test_number_in_dict(sn, item))
                        else:
                            _test.driver.close()
                            _test.driver.switch_to_window(self.driver.window_handles[0])
                            _test.subtest_pass(get_test_number_in_dict(sn, item))

                if result:
                    raise ReviewsPageNotOpenSocialUrl ("Обнаружены ошибки: {0}".format(result))
            return _page

        def step_go_servises_rewiews(_test: TestAutotest, _page: VMRReviewsPage):

            def get_test_number_in_dict(sn_array, sn_item):
                for item in sn_array.keys():
                    if sn_array[item].get_type() == sn_item.get_type():
                        return item

            with pytest.allure.step('Проверка что присутствуют все ссылки сервисов отзывов'):
                rs = {}
                rs.update({183: _page.get_ya_element()})
                rs.update({184: _page.get_ir_element()})
                rs.update({185: _page.get_or_element()})
                rs.update({186: _page.get_vr_element()})

            result = []
            items = _page.get_review_services_elements()
            for item in items:
                test_num = get_test_number_in_dict(rs, item)
                if item.is_external():
                    _test.subtest_start(test_num)
                    item.click()
                    with pytest.allure.step('Проверка {0}'.format(item.get_type().__class__.__name__)):
                        i = 0
                        while len(_test.driver.window_handles) != 2:
                            time.sleep(0.2)
                            i += 1
                            if i == 50:
                                raise ReviewsPageNotOpenServicesUrl('Не открывается ссылка в отдельном окне')

                        expect_url = item.get_expect_url()

                        _test.driver.switch_to_window(self.driver.window_handles[1])

                        if expect_url not in _test.driver.current_url:
                            if _test.driver.current_url != expect_url.replace("http", "https"):
                                result.append('Ссылка открытая по клику на {0} не совпадает с ожидаемой "{1}"'
                                              .format(_test.driver.current_url, expect_url))
                                _test.subtest_fail(test_num)
                            else:
                                _test.subtest_pass(test_num)
                        else:
                            _test.subtest_pass(test_num)
                        self.driver.close()
                        self.driver.switch_to_window(self.driver.window_handles[0])

            if result:
                raise ReviewsPageNotOpenServicesUrl("Обнаружены ошибки: {0}".format(result))

        self.subtest_start(172)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открываем страницу "Контакты"'):
            page = self.step(step_open_contacts_page, step_open_main_page, self, page, _name='Контакты')
        self.subtest_pass(172)

        self.subtest_start(172)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открываем страницу отзывов'):
            page = self.step(step_open_reviews_page, step_open_main_page, self, page)
        self.subtest_pass(172)

        self.subtest_pass(173)
        with pytest.allure.step('Проверяем пагтнацию на странице отзывов'):
            self.step(step_reviews_page_pagination, clean_page, self, page)
        self.subtest_start(173)

        self.subtest_start(877)
        with pytest.allure.step('Проверяем соц-сети'):
            page = self.step(step_reviews_page_go_social_net, step_open_reviews_page, self, page)
        self.subtest_pass(877)

        self.subtest_start(878)
        with pytest.allure.step('Проверяем сервисы отзывов'):
            self.step(step_go_servises_rewiews, step_open_reviews_page, self, page)
        self.subtest_pass(878)

    def test_vmr_social_network(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def check_all_social_network_exist(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            _page.FOOTER.SOCIAL_NETWORK.CHECK.check_all_social_network_exist(raise_error=True)

        def step_open_all_social_network(_test: TestAutotest, _page: VMRMainPage, **kwargs):

            # Так как идет работа с несколькими страницами на низком уровне бизнес логика реализована
            # именно в тесте, хоть это и не правильно.
            # Можно, конечно для каждой соц сети создать стандартизированную страницу, но это дольше и труднее поддерживать.

            errors = []
            for item in page.FOOTER.SOCIAL_NETWORK.get_all_social_network_elements():

                item.click()

                i = 0
                while len(self.driver.window_handles) != 2:
                    time.sleep(0.2)
                    i += 1
                    if i == 50:
                        raise SocialNetworkException('Не открывается ссылка в отдельном окне')

                expect_url = item.url

                self.driver.switch_to.window(self.driver.window_handles[1])

                if self.driver.current_url != expect_url:
                    errors.append([item.network_id, self.driver.current_url, expect_url])

                self.driver.close()
                self.driver.switch_to.window(self.driver.window_handles[0])

            if errors:
                err_text = ["{0}, {1}, {2}".format(item[0], item[1], item[2]) for item in errors]
                raise SocialNetworkException("Не удалось открыть: \n" + '\n'.join(err_text))

        self.subtest_start(902)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверяем наличие ссылок социальных сетей'):
            self.step(check_all_social_network_exist, step_open_main_page, self, page)
        with pytest.allure.step('Открываем все ссылки социальных сетей'):
            self.step(step_open_all_social_network, step_open_main_page, self, page)
        self.subtest_pass(902)

    def test_vmr_footer(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_item_lower_menu_page(_test: TestAutotest, _page: VMRMainPage, _name):
            _page = page.FOOTER.LOWER_MENU.go_to_menu_item_by_name(_name)
            return _page

        def step_send_trouble_lower_menu_item(_test: TestAutotest, _page: VMRMainPage, _name):
            _page.FOOTER.LOWER_MENU.find_menu_item_by_name(_name).click()
            if _page.CHECK.check_element_exist(xpath="//*[@id='trouble']"):
                _page.FOOTER.get_trouble_element().close()
            else:
                raise FooterFormMsgNotFoundException('Не появилась форма для отправки сообщения')

        def step_send_trouble_msg(_test: TestAutotest, _page: VMRMainPage, _name):
            _page.FOOTER.LOWER_MENU.find_menu_item_by_name(_name).click()
            _page.FOOTER.send_trouble_msg(text='test', email='{0}@test.test'.format(time.time()))
            if _page.CHECK.check_element_exist(xpath="//*[@id='empty']//p"):
                _page.FOOTER.get_email_confirm_message_element().click()
            else:
                raise FooterSubscribeErrorMsgException('Не показалось сообщение об успешной отправке сообщения')

        def step_click_footer_phone_email(_test: TestAutotest, _page: VMRMainPage):
            if _page.FOOTER.LOWER_MENU.find_menu_item_by_name('info@vsemayki.ru').text != _test.active_test.data.email:
                raise FooterPhoneIncorrectedException('Некорректный Email в подвале сайте. Должен быть: info@vsemayki.ru.')
            _page.FOOTER.LOWER_MENU.find_menu_item_by_name((_page.HEADER.get_city_phone_element().text)).click()

        def step_open_footer_app_ios(_test: TestAutotest, _page: VMRMainPage):
            _page.FOOTER.APP.click_ios_app()
            while len(self.driver.window_handles) != 2:
                time.sleep(0.2)
            _test.driver.switch_to_window(self.driver.window_handles[1])
            if 'https://itunes.apple.com/ru/app/vsemayki/id1031850362' not in _test.driver.current_url:
                raise FooterIosAppException("Некорректный url приложения при переходе на ITunes")
            _test.driver.close()
            _test.driver.switch_to_window(self.driver.window_handles[0])

        def step_open_footer_app_android(_test: TestAutotest, _page: VMRMainPage):
            _page.FOOTER.APP.click_android_app()
            while len(self.driver.window_handles) != 2:
                time.sleep(0.2)
            _test.driver.switch_to_window(self.driver.window_handles[1])
            if 'https://play.google.com/store/apps/details?id=com.vsemayki.app' not in _test.driver.current_url:
                raise FooterAndroidAppException("Некорректный url приложения при переходе на Google Store")
            _test.driver.close()
            _test.driver.switch_to_window(self.driver.window_handles[0])

        def step_subscribe_exist(_test: TestAutotest, _page: VMRMainPage):
            if _page.CHECK.check_element_exist(xpath="//*[@id='client_subscribe']"):
                pass
            else:
                raise FooterSubscribeNotFoundException('Отсутствует блок подписки')

        def step_subscribe_error_msg_exist(_test: TestAutotest, _page: VMRMainPage):
            _page.FOOTER.click_subscribe_button()
            if _page.CHECK.check_element_exist(xpath="//p[text()='Неверный формат email']"):
                _page.FOOTER.close_email_error_message()
            else:
                raise FooterSubscribeErrorMsgException('Не показалось сообщение об ошибке')

        def step_subscribe_error_input_msg_exist(_test: TestAutotest, _page: VMRMainPage):
            _page.FOOTER.enter_subscribe_email('12356789')
            _page.FOOTER.click_subscribe_button()
            if _page.CHECK.check_element_exist(xpath="//p[text()='Неверный формат email']"):
                _page.FOOTER.close_email_error_message()
            else:
                raise FooterSubscribeErrorMsgException('Не показалось сообщение об ошибке')

        def step_subscribe_successful_msg_exist(_test: TestAutotest, _page: VMRMainPage):
            _page.FOOTER.enter_subscribe_email('{0}@test.test'.format(time.time()))
            _page.FOOTER.click_subscribe_button()
            if _page.CHECK.check_element_exist(xpath="//*[@id='empty']//p", timeout=5):
                _page.FOOTER.get_email_confirm_message_element().click()
            else:
                raise FooterSubscribeErrorMsgException('Не показалось сообщение об успешной подписке')

        self.subtest_start(235)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Как сделать заказ?"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Как сделать заказ?")
        self.subtest_pass(235)

        self.subtest_start(368)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Доставка"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Доставка")
        self.subtest_pass(368)

        self.subtest_start(362)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Обмен и возврат"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Обмен и возврат")
        self.subtest_pass(362)

        self.subtest_start(363)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Где мой заказ?"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Где мой заказ?")
        self.subtest_pass(363)

        self.subtest_start(364)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Способы оплаты"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Способы оплаты")
        self.subtest_pass(364)

        self.subtest_start(365)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие формы "Сообщить о проблеме "'):
            self.step(step_send_trouble_lower_menu_item, step_open_main_page, self, page, _name="Сообщить о проблеме")
        self.subtest_pass(365)

        self.subtest_start(367)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверка наличия сообщения об успешной отправке формы "Сообщить о проблеме "'):
            self.step(step_send_trouble_msg, step_open_main_page, self, page, _name="Сообщить о проблеме")
        self.subtest_pass(367)

        self.subtest_start(1008)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Пользовательское соглашение"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Пользовательское соглашение")
        self.subtest_pass(1008)

        self.subtest_start(1009)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "О нас"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="О нас")
        self.subtest_pass(1009)

        self.subtest_start(369)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Контакты"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Контакты")
        self.subtest_pass(369)

        self.subtest_start(370)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Отзывы"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Отзывы")
        self.subtest_pass(370)

        self.subtest_start(1239)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Наше производство"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Наше производство")
        self.subtest_pass(1239)

        self.subtest_start(372)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Оптовым клиентам"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Оптовым клиентам")
        self.subtest_pass(372)

        self.subtest_start(373)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Дизайнерам"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Дизайнерам")
        self.subtest_pass(373)

        self.subtest_start(374)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Партнерская программа"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Партнерская программа")
        self.subtest_pass(374)

        self.subtest_start(375)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Курьерским службам"'):
            page = self.step(step_open_item_lower_menu_page, step_open_main_page, self, page, _name="Курьерским службам")
        self.subtest_pass(375)

        self.subtest_start(233)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверка перехода по ссылке "Загрузите в App Store"'):
            self.step(step_open_footer_app_ios, step_open_main_page, self, page)
        self.subtest_pass(233)

        self.subtest_start(234)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверка перехода по ссылке "Загрузите на Google play"'):
            self.step(step_open_footer_app_android, step_open_main_page, self, page)
        self.subtest_pass(234)

        self.subtest_start(217)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверка присутствия блока подписки'):
            self.step(step_subscribe_exist, clean_page, self, page)
        self.subtest_pass(217)

        self.subtest_start(218)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверка подписки без ввода email'):
            self.step(step_subscribe_error_msg_exist, clean_page, self, page)
        self.subtest_pass(218)

        self.subtest_start(219)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверка подписки c некорректным email'):
            self.step(step_subscribe_error_input_msg_exist, clean_page, self, page)
        self.subtest_pass(219)

        self.subtest_start(220)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверка подписки c корректным email'):
            self.step(step_subscribe_successful_msg_exist, clean_page, self, page)
        self.subtest_pass(220)

        self.subtest_start(376)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Сравниваем телефоны и email'):
            self.step(step_click_footer_phone_email, clean_page, self, page)
        self.subtest_pass(376)

    def test_vmr_product_cart(self):

        def step_open_designer_page(_test: TestAutotest, _page=None, **kwargs):
            return _test.open_main_page().open_by_url("designer/VQ", VMRDesignerPage)

        def step_find_product_with_colors(_test: TestAutotest, _page: VMRDesignerPage, **kwargs):
            for item in _page.get_products_elements():
                _page = _page.go_to_product_by_element(item)
                if len(_page.get_available_color_elements()) > 1:
                    return _page, '/'.join(_test.driver.current_url.split('?')[0].split("/")[-3:])
                _page = _page.open_by_url("designer/VQ", VMRDesignerPage)
            else:
                raise ProductException("Не найдены продукты с несколькими цветами")

        def clean_open_product_page_by_href(_test: TestAutotest, href=None, **kwargs):
            return _test.open_main_page().open_by_url(href, VMRProductPage)

        def clean_wait(**kwargs):
            time.sleep(5)

        def step_check_color_change(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            _color_name = _page.get_active_color_element().get_name()
            _page.set_color_by_index(0 if _page.get_available_color_elements()[0].get_name() != _color_name else 1)
            _page.CHECK.check_color_change(old_name=_color_name, raise_error=True)

        def check_change_product_type(_test: TestAutotest, _page: VMRProductPage, **kwargs):

            _old_product_type_name = _page.get_product_type_name()

            for index, item in enumerate(_page.get_available_product_types_elements()):
                if item.is_active():
                    _page.set_product_type_by_index(index=index + 1)
                    break

            for item in range(0, 10):
                _result = _page.CHECK.change_product_type_name(old_name=_old_product_type_name)
                if _result:
                    break
                time.sleep(0.5)
            else:
                raise ProductException("При изменении продукттипа не менятся продукттип в имени продукта.")

        def step_click_thumbnails(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            for image in _page.get_available_thumbnails():
                image.click()

                for item in range(0, 40):
                    if image.get_href() == _page.get_photo_href():
                        break
                else:
                    raise ProductException("При тапе на превью не меняется картинка на странице")

        def check_designer_href(_test: TestAutotest, _page: VMRDesignerPage):
            for index in range(0, 10):
                item = _page.get_products_elements()[index]
                _name = item.get_name()
                with pytest.allure.step("Проверяем продукт \"{0}\"" .format(_name)):
                    with pytest.allure.step("Открываем страницу продукта"):
                        _page = _page.go_to_product_by_element(item)
                    with pytest.allure.step("Открываем наличие ссылки"):
                        _page.CHECK.check_element_exist(
                            xpath=_page.structure.DESIGNER_HREF, raise_error=True,
                            error_text="У продукта \"{0}\" отсутствует ссылка на дизайнера" .format(_name),
                            error_class=ProductException)
                    with pytest.allure.step("Возвращаемся на страницу дизайнера"):
                        _page = _page.back(VMRDesignerPage, timeout=10)

        def step_check_social_network(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            supported_share = {}
            supported_share.update({"ВКонтакте": ["https://oauth.vk.com", 109]})
            supported_share.update({"Facebook": ["https://www.facebook.com", 110]})
            supported_share.update({"Twitter": ["https://twitter.com", 112]})
            supported_share.update({"Одноклассники": ["https://connect.ok.ru", 113]})
            supported_share.update({"Google+": ["https://accounts.google.com", 111]})

            _errors = []
            for social_network in _page.get_social_network_share_elements():
                social_network_name = social_network.get_name()
                with pytest.allure.step("Проверяем '{0}'".format(social_network_name)):
                    if social_network_name not in supported_share.keys():
                        raise ProductException("Неизвестная социальная сеть \"{0}\"".format(social_network_name))

                    test_network = supported_share[social_network_name]
                    _test.subtest_start(test_network[1])

                    social_network.click()

                    _test.wait_tab(tab_num=2, change_focus=True, timeout=5)

                    if test_network[0] not in self.driver.current_url:
                        _errors.append("Открылась некорректная ссылка. Ожидаем: Ссылка содержит \"{0}\". Видим: \"{1}\""
                                       .format(test_network[0], self.driver.current_url))

                    _test.driver.close()
                    _test.wait_tab(tab_num=1, change_focus=True, timeout=5)

                    _test.subtest_pass(test_network[1])

            if _errors:
                raise ProductException("\n".join(_errors))

        with pytest.allure.step('Открываем страницу дизайнера'):
            page = self.step(step_open_designer_page, clean_wait, self, None)

        with pytest.allure.step('Открываем страницу продукта с несколькими цветами'):
            page, product_href = self.step(step_find_product_with_colors, step_open_designer_page, self, page)

        self.subtest_start(97)
        with pytest.allure.step("Проверяем изменение цвета"):
            self.step(step_check_color_change, clean_open_product_page_by_href, self, page, href=product_href)
        self.subtest_pass(97)

        self.subtest_start(103)
        with pytest.allure.step("Проверяем измененения продукттипов"):
            self.step(check_change_product_type, clean_open_product_page_by_href, self, page, href=product_href)
        self.subtest_pass(103)

        self.subtest_start(104)
        with pytest.allure.step("Проверяем клик на уменьшенное изображение продукта"):
            self.step(step_click_thumbnails, clean_open_product_page_by_href, self, page, href=product_href)
        self.subtest_pass(104)

        self.subtest_start(105)
        with pytest.allure.step("Проверяем ссылки на дизайнера"):
            page = self.step(step_open_designer_page, clean_wait, self, None)
            self.step(check_designer_href, step_open_designer_page, self, page)
        self.subtest_pass(105)

        page = self.step(clean_open_product_page_by_href, clean_wait, self, page, href=product_href)

        with pytest.allure.step("Проверяем переход на соцсети"):
            self.step(step_check_social_network, clean_open_product_page_by_href, self, page, href=product_href)

    def test_vmr_help(self):

        def step_open_main_page(_test: TestAutotest, **kwargs):
            return _test.open_main_page()

        def clean_page(_test: TestAutotest, **kwargs):
            _test.driver.refresh()

        def step_open_help_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            link = _page.HEADER.get_submenu_holder_element()
            if link.text == "Помощь":
                _page = _page.HEADER.go_to_link(link, lambda: link.text)
            else:
                raise HeaderOpenHelpPageException("Не удалось найти пункт меню \"Помощь\"")
            return _page

        def check_help_how_bay(_test: TestAutotest, _page: VMRHelpPage, **kwargs):
            with pytest.allure.step("Проверка что дополнительная информация по пункту \"Как купить?\" скрыта"):
                if page.is_help_link_open("Как купить?"):
                    raise HelpNotInfoException("Показывается информация для пункта \"Как купить?\"")

            with pytest.allure.step("Кликаем на пункт \"Как купить?\""):
                page.open_help_link_by_name("Как купить?")

            with pytest.allure.step("Проверка что показывается дополнительная информация по пункту \"Как купить?\""):
                if not page.is_help_link_open("Как купить?"):
                    raise HelpNotInfoException("Не показывается информация для пункта \"Как купить?\"")

            with pytest.allure.step("Кликаем на пункт \"Как купить?\""):
                page.open_help_link_by_name("Как купить?")

            with pytest.allure.step("Проверка что дополнительная информация по пункту \"Как купить?\" скрыта"):
                if page.is_help_link_open("Как купить?"):
                    raise HelpNotInfoException("Показывается информация для пункта \"Как купить?\"")

        def check_help_exchange_return(_test: TestAutotest, _page: VMRHelpPage, **kwargs):

            with pytest.allure.step("Проверка что дополнительная информация по пункту \"Обмен и возврат\" скрыта"):
                if page.is_help_link_open("Обмен и возврат"):
                    raise HelpNotInfoException("Показывается информация для пункта \"Обмен и возврат\"")

            with pytest.allure.step("Кликаем на пункт \"Обмен и возврат\""):
                page.open_help_link_by_name("Обмен и возврат")

            with pytest.allure.step("Проверка что показывается дополнительная информация по пункту \"Обмен и возврат\""):
                if not page.is_help_link_open("Обмен и возврат"):
                    raise HelpNotInfoException("Не показывается информация для пункта \"Обмен и возврат\"")

            with pytest.allure.step("Кликаем на пункт \"Обмен и возврат\""):
                page.open_help_link_by_name("Обмен и возврат")

            with pytest.allure.step("Проверка что дополнительная информация по пункту \"Обмен и возврат\" скрыта"):
                if page.is_help_link_open("Обмен и возврат"):
                    raise HelpNotInfoException("Показывается информация для пункта \"Обмен и возврат\"")

        def check_help_self_design(_test: TestAutotest, _page: VMRHelpPage, **kwargs):
            with pytest.allure.step("Проверка пункта \"Как создать товар с индивидуальным дизайном?\", доп.инф. скрыта"):
                if page.is_help_link_open("Как создать товар с индивидуальным дизайном?"):
                    raise HelpNotInfoException("Показывается информация для пункта \"Как создать товар с индивидуальным дизайном?\"")

            with pytest.allure.step("Кликаем на пункт \"Как создать товар с индивидуальным дизайном?\""):
                page.open_help_link_by_name("Как создать товар с индивидуальным дизайном?")

            with pytest.allure.step("Проверка пункта \"Как создать товар с индивидуальным дизайном?\""):
                if not page.is_help_link_open("Как создать товар с индивидуальным дизайном?"):
                    raise HelpNotInfoException("Не показывается информация для пункта \"Как создать товар с индивидуальным дизайном?\"")

            with pytest.allure.step("Кликаем на пункт \"Как создать товар с индивидуальным дизайном?\""):
                page.open_help_link_by_name("Как создать товар с индивидуальным дизайном?")

            with pytest.allure.step("Проверка пункта \"Как создать товар с индивидуальным дизайном?\", доп.инф. скрыта"):
                if page.is_help_link_open("Как создать товар с индивидуальным дизайном?"):
                    raise HelpNotInfoException("Показывается информация для пункта \"Как создать товар с индивидуальным дизайном?\"")

        def check_help_technology_print(_test: TestAutotest, _page: VMRHelpPage, **kwargs):
            with pytest.allure.step("Проверка что дополнительная информация по пункту \"Технологии печати\" скрыта"):
                if page.is_help_link_open("Технологии печати"):
                    raise HelpNotInfoException("Показывается информация для пункта \"Технологии печати\"")

            with pytest.allure.step("Кликаем на пункт \"Технологии печати\""):
                page.open_help_link_by_name("Технологии печати")

            with pytest.allure.step(
                    "Проверка что показывается дополнительная информация по пункту \"Технологии печати\""):
                if not page.is_help_link_open("Технологии печати"):
                    raise HelpNotInfoException("Не показывается информация для пункта \"Технологии печати\"")

            with pytest.allure.step("Кликаем на пункт \"Технологии печати\""):
                page.open_help_link_by_name("Технологии печати")

            with pytest.allure.step("Проверка что дополнительная информация по пункту \"Технологии печати\" скрыта"):
                if page.is_help_link_open("Технологии печати"):
                    raise HelpNotInfoException("Показывается информация для пункта \"Технологии печати\"")

        def check_help_questions(_test: TestAutotest, _page: VMRHelpPage, **kwargs):
            with pytest.allure.step("Проверка пункта \"Часто задаваемые вопросы\", доп.инф. скрыта"):
                if page.is_help_link_open("Часто задаваемые вопросы"):
                    raise HelpNotInfoException("Показывается информация для пункта \"Часто задаваемые вопросы\"")

            with pytest.allure.step("Кликаем на пункт \"Часто задаваемые вопросы\""):
                page.open_help_link_by_name("Часто задаваемые вопросы")

            with pytest.allure.step("Проверка пункта \"Часто задаваемые вопросы\""):
                if not page.is_help_link_open("Часто задаваемые вопросы"):
                    raise HelpNotInfoException("Не показывается информация для пункта \"Часто задаваемые вопросы\"")

            with pytest.allure.step("Кликаем на пункт \"Часто задаваемые вопросы\""):
                page.open_help_link_by_name("Часто задаваемые вопросы")

            with pytest.allure.step("Проверка пункта \"Часто задаваемые вопросы\" скрыта"):
                if page.is_help_link_open("Часто задаваемые вопросы"):
                    raise HelpNotInfoException("Показывается информация для пункта \"Часто задаваемые вопросы\"")

        self.subtest_start(164)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step("Переходим на страницу \"Помощь\""):
            page = self.step(step_open_help_page, clean_page, self, page)
        self.subtest_pass(164)

        self.subtest_start(165)
        with pytest.allure.step("Проверка раздела \"Как купить\" страницы \"Помощь\""):
            self.step(check_help_how_bay, step_open_help_page, self, page)
        self.subtest_pass(165)

        self.subtest_start(166)
        with pytest.allure.step("Просмотр раздела \"Обмен и возврат\" страницы \"Помощь\""):
            self.step(check_help_exchange_return, step_open_help_page, self, page)
        self.subtest_pass(166)

        self.subtest_start(167)
        with pytest.allure.step("Просмотр раздела \"Как создать товар с индивидуальным дизайном?\" страницы \"Помощь\""):
            self.step(check_help_self_design, step_open_help_page, self, page)
        self.subtest_pass(167)

        self.subtest_start(169)
        with pytest.allure.step("Просмотр раздела \"Технологии печати\" страницы \"Помощь\""):
            self.step(check_help_technology_print, step_open_help_page, self, page)
        self.subtest_pass(169)

        self.subtest_start(168)
        with pytest.allure.step("Просмотр раздела \"Часто задаваемые вопросы\" страницы \"Помощь\""):
            self.step(check_help_questions, step_open_help_page, self, page)
        self.subtest_pass(168)

    def test_vmr_delivery_info(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_delivery_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            for link in _page.HEADER.get_links_elements():
                if link.get_name() == "Помощь":
                    link.assure(exist).mouse_over_element()

                    for sub_link in _page.HEADER.get_sub_links_elements():
                        if sub_link.get_name() == "Доставка":
                            _page = _page.HEADER.go_to_link(sub_link, lambda: sub_link.text)
                            break
                    else:
                        raise HeaderOpenDeliveryPageException("Не удалось найти пункт меню \"Доставка\"")
            return _page

        def step_open_contacts_page(_test: TestAutotest, _page: VMRMainPage):
            _page = _page.FOOTER.LOWER_MENU.go_to_menu_item_by_name('Контакты')
            return _page

        def step_open_delivery_page_left_menu(_test: TestAutotest, _page: VMRContactsPage):
            _page = _page.LEFT_MENU.go_to_delivery_page()
            return _page

        def step_set_city_top_popup(_test: TestAutotest, _page: VMRMainPage):
            _city =_test.active_test.data.city
            _page.HEADER.get_pop_up()
            _page.HEADER.set_city_by_list(_city)
            return _city

        def check_set_city(_test: TestAutotest, _page: VMRDeliveryPage, city, **kwargs):
            with pytest.allure.step("Проверяем совпадение города."):
                _page.CHECK.check_city_eq(city, raise_error=True)

        def step_open_designer_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            return _page.open_by_url('designer/VQ', VMRDesignerPage)

        def clean_page(_test: TestAutotest, **kwargs):
            _test.driver.refresh()

        def clean_cart_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()

            if _page.HEADER.get_cart_count() != 0:
                _page = _page.HEADER.go_to_cart()
                _page.clean_cart()

        def step_add_product(_test: TestAutotest, _page: VMRDesignerPage, **kwargs):
            _added_product = {}

            for i in range(0, len(_page.get_products_elements())):
                _product_element = _page.get_random_product_element()
                _page = _page.go_to_product_by_element(_product_element)

                _page.set_random_color()
                _page.set_random_size()

                _p_prise = _page.get_price()
                _p_name = _page.get_name().replace('«', '').replace('»', '')

                _page.add_product_to_cart()
                _added_product[_p_name] = _p_prise

                _page.HEADER.wait_count_eq(len(_added_product))

                _test.log_text('Добавляем товар "{0}" со стомостью "{1}"'.format(_p_name, _p_prise))
                _test.log_text("Количество товаров в корзине: {0}" .format(_page.HEADER.get_cart_count()))
                _test.log_txt_data(log_message="Данные шага", log_data=_added_product)

                if _page.HEADER.get_cart_count() == 1:
                    return _added_product
                else:
                    _page = _page.back(VMRDesignerPage, timeout=10)

        def step_open_cart_page(_test: TestAutotest, _page, **kwargs):
            _page = _test.open_main_page()
            return _page.HEADER.go_to_cart()

        def step_open_order_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.HEADER.go_to_cart()
            return _page.go_to_order_page()

        def step_input_data(_test: TestAutotest, _page: VMROrderPage, **kwargs):
            with pytest.allure.step("Заполняем страну и город СНГ"):
                _page.set_country(self.active_test.data.sng_country)
                _page.set_sng_city(self.active_test.data.sng_city)
                _page.HEADER.get_search_input_element().click()

        def check_delivery_sng(_test: TestAutotest, _page: VMROrderPage, **kwargs):
            with pytest.allure.step("Ждем появления способов доставки"):
                _page.get_all_delivery_elements().assure(len_equal, 1)

            with pytest.allure.step("Проверяем доставки в блоке \"Почта России\""):
                _page.CHECK.check_len_delivery_element_eg(len_size=1, raise_error=True)
                _page.CHECK.check_name_delivery_block_eg(name='Почта России', raise_error=True)
                _page.CHECK.check_name_delivery_element_eg(name='Почта России 1 класса', raise_error=True)
                _page.CHECK.check_delivery_price(value=0, raise_error=True)

            with pytest.allure.step("Проверяем оплату для стран СНГ"):
                _page.get_all_payment_methods_elements().assure(len_equal, 1)
                _page.CHECK.check_name_payment_element_eg(name='Оплата онлайн:', raise_error=True)

        self.subtest_start(187)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Доставка"'):
            self.step(step_open_delivery_page, step_open_main_page, self, page)
        self.subtest_pass(187)

        self.subtest_start(198)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Контакты"'):
            page = self.step(step_open_contacts_page, step_open_main_page, self, page)
        with pytest.allure.step('Открытие страницы "Контакты"'):
            self.step(step_open_delivery_page_left_menu, step_open_contacts_page, self, page)
        self.subtest_pass(198)

        self.subtest_start(189)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Доставка"'):
            page = self.step(step_open_delivery_page, step_open_main_page, self, page)
        with pytest.allure.step('Установка локации из топ списка городов'):
            city = self.step(step_set_city_top_popup, step_open_main_page, self, page)
        with pytest.allure.step('Проверка установленного города'):
            self.step(check_set_city, clean_page, self, page, city=city)
        self.subtest_pass(189)

        self.subtest_start(434)
        with pytest.allure.step('Открываем страницу дизайнера VQ'):
            page = self.step(step_open_designer_page, clean_page, self, None)
        with pytest.allure.step('Добавляем случайный продукт в корзину'):
            self.step(step_add_product, clean_cart_page, self, page)
        with pytest.allure.step('Открываем страницу корзины'):
            page = self.step(step_open_cart_page, clean_page, self, None)
        with pytest.allure.step('Открываем страницу заказа'):
            page = self.step(step_open_order_page, step_open_cart_page, self, page)
        with pytest.allure.step('Вводим данный для заказа в СНГ'):
            self.step(step_input_data, step_open_order_page, self, page)
        with pytest.allure.step('Проверяем доставки и оплату для заказа в СНГ'):
            self.step(check_delivery_sng, step_input_data, self, page)
        self.subtest_pass(434)

    def test_vmr_status_page(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_status_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            for link in _page.HEADER.get_links_elements():
                if link.get_name() == "Помощь":
                    link.assure(exist).mouse_over_element()

                    for sub_link in _page.HEADER.get_sub_links_elements():
                        if sub_link.get_name() == "Где мой заказ?":
                            _page = _page.HEADER.go_to_link(sub_link, lambda: sub_link.text)
                            break
                    else:
                        raise HeaderOpenDeliveryPageException("Не удалось найти пункт меню \"Где мой заказ?\"")
            return _page

        def check_empty_input_status_page(_test: TestAutotest, _page: VMRStatusPage, **kwargs):
            with pytest.allure.step('Кликаем на кнопку "Узнать статус"'):
                _page.get_input_status_form_element().click_get_status_button()
            with pytest.allure.step('Проверяем что появился текст ошибки'):
                _page.CHECK.check_error_input_txt_eq(value='Поле должно быть заполнено', raise_error=True)
            with pytest.allure.step('Проверяем что поле для ввода номера подсвечивается красным'):
                _page.CHECK.check_error_input_color(
                    error_element=_page.get_input_status_form_element().get_number_input_element().has_error(), raise_error=True)

        def check_empty_email_status_page(_test: TestAutotest, _page: VMRStatusPage, **kwargs):
            with pytest.allure.step('Обновляем страницу'):
                _test.driver.refresh()
            with pytest.allure.step('Вводим данные'):
                _page.get_input_status_form_element().set_number(self.active_test.data.order_number)
            with pytest.allure.step('Кликаем на кнопку "Узнать статус"'):
                _page.get_input_status_form_element().click_get_status_button()
            with pytest.allure.step('Проверяем что появился текст ошибки'):
                _page.CHECK.check_error_input_txt_eq(value='Поле должно быть заполнено', raise_error=True)
            with pytest.allure.step('Проверяем что поле для ввода E-mail подсвечивается красным'):
                _page.CHECK.check_error_input_color(
                    error_element=_page.get_input_status_form_element().get_email_input_element().has_error(), raise_error=True)

        def check_incorrected_email_status_page(_test: TestAutotest, _page: VMRStatusPage, **kwargs):
            with pytest.allure.step('Обновляем страницу'):
                _test.driver.refresh()
            with pytest.allure.step('Вводим данные'):
                _page.get_input_status_form_element().set_number(self.active_test.data.order_number)
                _page.get_input_status_form_element().set_email('1234')
            with pytest.allure.step('Кликаем на кнопку "Узнать статус"'):
                _page.get_input_status_form_element().click_get_status_button()
            with pytest.allure.step('Проверяем что появился текст ошибки'):
                _page.CHECK.check_error_input_txt_eq(value='Не правильный формат email', raise_error=True)
            with pytest.allure.step('Проверяем что поле для ввода E-mail подсвечивается красным'):
                _page.CHECK.check_error_input_color(
                    error_element=_page.get_input_status_form_element().get_email_input_element().has_error(), raise_error=True)

        def check_incorrected_order_status_page(_test: TestAutotest, _page: VMRStatusPage, **kwargs):
            with pytest.allure.step('Обновляем страницу'):
                _test.driver.refresh()
            with pytest.allure.step('Вводим данные'):
                _page.get_input_status_form_element().set_number('1234')
                _page.get_input_status_form_element().set_email(self.active_test.data.login)
            with pytest.allure.step('Кликаем на кнопку "Узнать статус"'):
                _page.get_input_status_form_element().click_get_status_button()
            with pytest.allure.step('Проверяем что появился текст ошибки'):
                _page.CHECK.check_error_input_txt_eq(value='По вашему запросу ничего не найдено', raise_error=True)
            with pytest.allure.step('Проверяем что поле для ввода номера подсвечивается красным'):
                _page.CHECK.check_error_input_color(
                    error_element=_page.get_input_status_form_element().get_number_input_element().has_error(), raise_error=True)

        def check_corrected_order_status_page(_test: TestAutotest, _page: VMRStatusPage, **kwargs):
            with pytest.allure.step('Обновляем страницу'):
                _test.driver.refresh()
            with pytest.allure.step('Вводим данные'):
                _page.get_input_status_form_element().set_number(self.active_test.data.order_number)
                _page.get_input_status_form_element().set_email(self.active_test.data.login)
            with pytest.allure.step('Кликаем на кнопку "Узнать статус"'):
                _page.get_input_status_form_element().click_get_status_button()
            with pytest.allure.step('Проверяем что открывшийся заказ имеет тот же номер что и при вводе'):
                _page.CHECK.check_order_num_eq(order_num=int(self.active_test.data.order_number), raise_error=True)

        self.subtest_start(153)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Где мой заказ?"'):
            page = self.step(step_open_status_page, step_open_main_page, self, page)
        self.subtest_pass(153)

        self.subtest_start(154)
        with pytest.allure.step('Информация о заказе без ввода данных'):
            self.step(check_empty_input_status_page, step_open_status_page, self, page)
        self.subtest_pass(154)

        self.subtest_start(155)
        with pytest.allure.step('Информация о заказе без ввода e-mail'):
            self.step(check_empty_email_status_page, step_open_status_page, self, page)
        self.subtest_pass(155)

        self.subtest_start(157)
        with pytest.allure.step('Информация о заказе с не корректным вводом e-mail'):
            self.step(check_incorrected_email_status_page, step_open_status_page, self, page)
        self.subtest_pass(157)

        self.subtest_start(159)
        with pytest.allure.step('Информация о заказе с не корректным вводом номера заказа'):
            self.step(check_incorrected_order_status_page, step_open_status_page, self, page)
        self.subtest_pass(159)

        self.subtest_start(156)
        with pytest.allure.step('Информация о заказе с корректным вводом номера заказа'):
            self.step(check_corrected_order_status_page, step_open_status_page, self, page)
        self.subtest_pass(156)

    def test_vmr_contacts(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_contacts_page(_test: TestAutotest, _page: VMRMainPage):
            _page = page.FOOTER.LOWER_MENU.go_to_menu_item_by_name('Контакты')
            return _page

        self.subtest_start(135)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Контакты"'):
            page = self.step(step_open_contacts_page, step_open_main_page, self, page)
        self.subtest_pass(135)

    def test_vmr_promo_code(self):

        def step_open_designer_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            return _page.open_by_url('designer/VQ', VMRDesignerPage)

        def clean_page(_test: TestAutotest, **kwargs):
            _test.driver.refresh()

        def clean_cart_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()

            if _page.HEADER.get_cart_count() != 0:
                _page = _page.HEADER.go_to_cart()
                _page.clean_cart()

            _page.open_by_url('designer/VQ', VMRDesignerPage)

        def step_add_3_product(_test: TestAutotest, _page: VMRDesignerPage, **kwargs):
            _added_product = {}

            for i in range(0, len(_page.get_products_elements())):
                _product_element = _page.get_random_product_element()

                if _product_element.get_name() in _added_product:
                    continue

                _page = _page.go_to_product_by_element(_product_element)

                _page.set_random_color()
                _page.set_random_size()

                _p_prise = _page.get_price()
                _p_name = _page.get_name().replace('«', '').replace('»', '')

                _page.add_product_to_cart()
                _added_product[_p_name] = _p_prise

                _page.HEADER.wait_count_eq(len(_added_product))

                _test.log_text('Добавляем товар "{0}" со стомостью "{1}"'.format(_p_name, _p_prise))
                _test.log_text("Количество товаров в корзине: {0}" .format(_page.HEADER.get_cart_count()))
                _test.log_json_data(log_message="Данные шага", log_data=_added_product)

                if _page.HEADER.get_cart_count() == 3:
                    return _added_product
                else:
                    _page = _page.back(VMRDesignerPage, timeout=10)

        def step_open_cart_page(_test: TestAutotest, _page, **kwargs):
            _page = _test.open_main_page()
            return _page.HEADER.go_to_cart()

        def step_check_cart_page(_test: TestAutotest, _page: VMRCartPage, _products: dict):
            with pytest.allure.step('Проверяем что в корзине 3 продукта'):
                _page.HEADER.wait_count_eq(len(_products))
            with pytest.allure.step('Получаем данные с корзины'):
                _cart_product = [(item.get_name(), item.get_price()) for item in _page.get_product_elements()]
            with pytest.allure.step('Проверяем что продуты в корзине и их цены совпадают с добавленными'):
                _page.CHECK.check_dict_eq(_products, dict(_cart_product))
            with pytest.allure.step('Проверяем что стоимость заказа подсчитывается корректно'):
                _page.CHECK.check_order_price_eq(sum(_products.values()))

        def step_input_promocode(_test: TestAutotest, _page: VMRCartPage, **kwargs):
            _page.set_promo_code(self.active_test.data.promo_code)

        def check_cart_after_promocode(_test: TestAutotest, _page: VMRCartPage, _products, **kwargs):
            with pytest.allure.step('Проверяем, что появилось корректное сообщение о скидке '):
                _page.CHECK.check_promocode_text_eg(value="Скидка 10% на заказ", raise_error=True)
            with pytest.allure.step('Проверяем, что цена каждого товара уменьшилась на 10%'):
                i = 0
                for item in _products.values():
                    _page.CHECK.check_order_price_element_eq(value=math.ceil(0.9*item), index=i, raise_error=True)
                    i += 1
            with pytest.allure.step('Проверяем, что общая стоимость заказа после промокода подсчитывается корректно'):
                _page.CHECK.check_order_price_eq(value=math.ceil(0.9*(sum(_products.values()))), raise_error=True)

        def step_open_order_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.HEADER.go_to_cart()
            return _page.go_to_order_page()

        def check_total_price_order_page(_test: TestAutotest, _page: VMROrderPage, _products, **kwargs):
            _page.CHECK.check_order_price_eq(value=math.ceil(0.9*(sum(_products.values()))), raise_error=True)

        self.subtest_start(277)
        with pytest.allure.step('Открываем страницу дизайнера VQ'):
            page = self.step(step_open_designer_page, clean_page, self, None)
        with pytest.allure.step('Добавляем 3 продукта в корзину'):
            products = self.step(step_add_3_product, clean_cart_page, self, page)
        with pytest.allure.step('Открываем страницу корзины'):
            page = self.step(step_open_cart_page, clean_page, self, None)
        with pytest.allure.step('Проверяем страницу корзины на правильность добавления товаров'):
            self.step(step_check_cart_page, step_open_cart_page, self, page, _products=products)
        with pytest.allure.step('Вводим промокод'):
            self.step(step_input_promocode, step_open_cart_page, self, page)
        with pytest.allure.step('Проверяем цены и текст в корзине, после ввода промокода'):
            self.step(check_cart_after_promocode, step_input_promocode, self, page, _products=products)
        with pytest.allure.step('Открываем страницу заказа'):
            page = self.step(step_open_order_page, step_open_cart_page, self, page)
        with pytest.allure.step('Проверяем общую цену заказа со скидкой'):
            self.step(check_total_price_order_page, step_open_order_page, self, page, _products=products)
        self.subtest_pass(277)

    def test_vmr_about_company(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_contacts_page(_test: TestAutotest, _page: VMRMainPage):
            _page = page.FOOTER.LOWER_MENU.go_to_menu_item_by_name('Контакты')
            return _page

        def step_open_about_company_page(_test: TestAutotest, _page: VMRContactsPage):
            _page = _page.LEFT_MENU.go_to_about_company()
            return _page

        def check_photos(_test: TestAutotest, _page: VMRAboutPage):
            with pytest.allure.step('Получаем все фотографии и открываем первую'):
                photos = _page.get_photos_elements()
                _page.open_photo(photos[0])

            with pytest.allure.step('Проверяем, что href 1-ой фотографии совпадает с ожидаемым'):
                _page.CHECK.check_href_photos(value=photos[0].get_href(), photo=_page.get_photo_element().get_href(), raise_error=True)

            with pytest.allure.step('Листаем на 2-ое фото и проверяем, что href 2-ой фотографии совпадает с ожидаемым'):
                _page.get_photo_element().next_photo()
                _page.CHECK.check_href_photos(value=photos[1].get_href(), photo=_page.get_photo_element().get_href(), raise_error=True)

            with pytest.allure.step('Листаем на 1-ое фото и проверяем, что href 1-ой фотографии совпадает с ожидаемым'):
                _page.get_photo_element().prevision_photo()
                _page.CHECK.check_href_photos(value=photos[0].get_href(), photo=_page.get_photo_element().get_href(), raise_error=True)
            _page.get_photo_element().close()

        self.subtest_start(150)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открытие страницы "Контакты"'):
            page = self.step(step_open_contacts_page, step_open_main_page, self, page)
        with pytest.allure.step('Открытие страницы "О компании"'):
            page = self.step(step_open_about_company_page, step_open_contacts_page, self, page)
        self.subtest_pass(150)

        self.subtest_start(151)
        with pytest.allure.step('Проверяем работу с фотографиями "О компании"'):
            self.step(check_photos, clean_page, self, page)
        self.subtest_pass(151)

    def test_vmr_search(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_search_page(_test: TestAutotest, _page: VMRMainPage):
            _page = _page.HEADER.search_product('asdfghj')
            if not _page.is_empty_result():
                raise SearchPageNotProductMsgException("Отсутствует надпись \"Не найдено\"")
            return _page

        def step_open_product_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            _page = _page.go_to_product_by_index(index=random.randint(0, len(page.get_products_elements()) - 1))
            _vendor_code = _page.get_product_int_id()
            _test.log_screenshot()
            return _vendor_code

        def step_search_product(_test: TestAutotest, _page: VMRProductPage, _vendor_code):
            _page = _page.HEADER.search_product(_vendor_code)
            _page = _page.go_to_product_by_index(index=random.randint(0, len(page.get_products_elements()) - 1))

            if vendor_code != _page.get_product_int_id():
                raise SearchNotRelevantException('Некорреткный артикул товара при поиске. Ожидаем: {0}. Видим: {1}'.
                                                 format(vendor_code, _page.get_product_int_id()))

        self.subtest_start(428)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('В строке поиска ищем "asdfghj"'):
            page = self.step(step_open_search_page, clean_page, self, page)
        self.subtest_pass(428)

        self.subtest_start(265)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открываем карточку товара и запоминаем артикул'):
            vendor_code = self.step(step_open_product_page, step_open_main_page, self, page)
        with pytest.allure.step('Ищем продукты по запомненному артикулу и верифицируем поиск'):
            self.step(step_search_product, step_open_main_page, self, page, _vendor_code=vendor_code)
        self.subtest_pass(265)

    def test_vmr_regression_constructor(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def step_open_constructor_page_from_catalog(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            return _page.CATALOG_MENU.go_to_constructor()

        def step_open_constructor_from_main_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            return _page.go_to_constructor()

        def step_open_constructor_from_top_menu(_test: TestAutotest, _page: VMRMainPage, _element_data):
            return _page.HEADER.DROP_DOWN_MENU.go_to_menu_item_by_name(name=_element_data)

        def step_sizes_table(_test: TestAutotest, _page: VMRConstructorPage):
            _page.open_sizes_table()
            _table_element = _page.get_sizes_table_element()

            if not _page.CHECK.is_exist(_table_element):
                _test.log_text(log_message="Таблица не отображается")
                _test.log_screenshot()

            with pytest.allure.step('Закрываем таблицу'):
                _page.close_sizes_table()

        def step_about_product(_test: TestAutotest, _page: VMRConstructorPage):
            _page.open_about_tshirt()
            _table_element = _page.get_about_product_element()

            if not _page.CHECK.is_exist(_table_element):
                _test.log_text(log_message="Таблица не отображается")
                _test.log_screenshot()

            with pytest.allure.step('Закрываем таблицу'):
                _page.close_about_product()

        self.subtest_start(200)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step("Открыть страницу \"Конструктор\" через каталог \"Создай свой дизайн\""):
            page = self.step(step_open_constructor_page_from_catalog, step_open_main_page, self, page)
        self.subtest_pass(200)

        self.subtest_start(231)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step("Открыть страницу \"Конструктор\" через товар \"ваше фото\""):
            page = self.step(step_open_constructor_from_main_page, step_open_main_page, self, page)
        self.subtest_pass(231)

        self.subtest_start(201)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step("Открыть страницу \"Конструктор\" через верхнее меню \"Свой дизайн\""):
            page = self.step(step_open_constructor_from_top_menu, step_open_main_page, self, page, _element_data='Свой дизайн')
        self.subtest_pass(201)

        self.subtest_start(203)
        with pytest.allure.step("Открываем ссылку \"Таблица размеров\""):
            self.step(step_sizes_table, clean_page, step_test=self, step_page=page)
        with pytest.allure.step("Открываем ссылку \"Подробнее о товаре\""):
            self.step(step_about_product, clean_page, step_test=self, step_page=page)
        self.subtest_pass(203)

    def test_vmr_3d_color(self):

        def step_open_search_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page().HEADER.search_product('3d')
            return _page

        def clean_page(_test: TestAutotest, **kwargs):
            _test.driver.refresh()
            time.sleep(1)

        def step_get_product_list(_test: TestAutotest, _page: VMRSearchPage, _products_list, **kwargs):
            for product in _page.get_products_elements():
                if product.get_product_type_name() not in self.active_test.data.products:
                    _products_list.append(product.get_index())
            return _products_list

        def clean_step_get_product_list(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page().HEADER.search_product('3d')
            _page.get_products_elements().assure(len_more, 0)
            return _page

        def step_open_product_page(_test: TestAutotest, _page: VMRSearchPage, products_list, **kwargs):
            _page = _page.go_to_product_by_index(random.choice(products_list))
            return _page

        def check_3D_color_available(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            _page.CHECK.check_3D_color_available(raise_error=True)

        self.subtest_start(435)
        products_list = []
        with pytest.allure.step('Открываем главную страницу и ищем товары "3D"'):
            page = self.step(step_open_search_page, clean_page, self, None)

        with pytest.allure.step('Получаем список продуктов. Не включаем: {}'.format(self.active_test.data.products)):
            products_list = self.step(step_get_product_list, clean_step_get_product_list, self, page, _products_list=products_list)

        with pytest.allure.step('Открываем рандомную страницу товара'):
            page = self.step(step_open_product_page, step_open_search_page, self, page, products_list=products_list)

        with pytest.allure.step('Проверяем наименование цвета "3D" в {}'.format(page.get_product_type_name())):
            self.step(check_3D_color_available, clean_page, self, page)

        self.subtest_pass(435)

    def test_vmr_redirect(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test: TestAutotest, _page, **kwargs):
            _test.driver.refresh()

        def check_redirect_main_page(_test: TestAutotest, _page: VMRMainPage, url, raise_error=False,  **kwargs):
            try:
                result = _test.driver.current_url != url
            except TimeoutException:
                result = False
            if not result and raise_error:
                raise VMRMainPageRedirectException('Происходит редирект с https на {0}'.format(url))
            return result

        self.subtest_start(859)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Проверяем отсутствие редиректа на "http://"'):
            self.step(check_redirect_main_page, clean_page, self, page, url='http://www.vsemayki.ru/', raise_error=True)
        self.subtest_pass(859)

    def test_vmr_card_goods_info(self):

        def step_open_designer_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.open_by_url('designer/VQ', VMRDesignerPage)
            return _page

        def clean_wait(_test: TestAutotest, **kwargs):
            _test.driver.refresh()

        def open_product_page(_test: TestAutotest, _page: VMRDesignerPage, **kwargs):
            _page = _page.go_to_random_product()
            return _page

        def check_card_goods_info(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            _page.open_about_product_table()
            _page.CHECK.check_about_product_visible(msg ='Информация о товаре', raise_error=True)
            _page.close_about_product_table()

        def check_image_more(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            _page.open_product_img_href_element()
            _page.CHECK.check_image_more_visible(raise_error=True)
            _page.close_image_more()

        self.subtest_start(114)
        with pytest.allure.step('Открываем страницу дизайнера VQ'):
            page = self.step(step_open_designer_page, clean_wait, self, None)
        with pytest.allure.step("Переходим в случайный продукт"):
            page = self.step(open_product_page, clean_wait, self, page)
        with pytest.allure.step("Проверяем наличие таблицы \"Подробнее о товаре\""):
            self.step(check_card_goods_info, clean_wait, self, page)
        self.subtest_pass(114)

        self.subtest_start(120)
        with pytest.allure.step('Проверяем отображение увеличенного изображения товара'):
            self.step(check_image_more, clean_wait, self, page)
        self.subtest_pass(120)

    def test_vmr_size_rusland(self):

        def step_open_designer_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.open_by_url('designer/VQ', VMRDesignerPage)
            return _page

        def clean_page(_test: TestAutotest, **kwargs):
            _test.driver.refresh()

        def clean_step_add_product_to_cart(_test: TestAutotest, size_item, added_size, **kwargs):
            _test.driver.refresh()
            _test.step(check_russian_size, clean_page, self, page, _size_item=size_item, added_size=added_size)

        def open_product_page(_test: TestAutotest, _page: VMRDesignerPage, **kwargs):
            for item in _page.get_products_elements():
                if item.get_product_type() == 'manshortfull':
                    return _page.go_to_product_by_element(item)

        def check_size_table(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            _page.open_size_table()
            _page.CHECK.check_size_table_visible(value='Таблица размеров', raise_error=True)
            _page.CHECK.check_russian_size_visible(value='Российский', raise_error=True)
            _page.close_size_table()

        def check_russian_size(_test: TestAutotest, _page: VMRProductPage, _size_item, added_size, **kwargs):
            _size_item.click()
            _page.CHECK.check_size_len(size_item=size_item, raise_error=True)
            added_size.append(page.get_name())
            return added_size

        def step_add_product_to_cart(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            _page.set_type(page.get_product_available_types()[0])
            _page.add_product_to_cart()
            _page = page.go_to_cart()
            return _page

        def check_russian_size_cart(_test: TestAutotest, _page: VMRCartPage, **kwargs):
            _page.CHECK.check_len_russian_size_gte(len_size=2, raise_error=True)
            _page = _page.go_to_order_page()
            return _page

        def check_russian_size_order(_test: TestAutotest, _page: VMROrderPage, **kwargs):
            _page.CHECK.check_len_russian_size_gte(len_size=2, raise_error=True)

        self.subtest_start(1145)
        added_size = []
        with pytest.allure.step('Открываем страницу дизайнера "VQ"'):
            page = self.step(step_open_designer_page, clean_page, self, None)

        with pytest.allure.step("Открываем продукт 'Мужская футболка 3D'"):
            page = self.step(open_product_page, clean_page, self, page)

        with pytest.allure.step('Проверка таблицы размеров'):
            self.step(check_size_table, clean_page, self, page)

        with pytest.allure.step("Переходим по всем размерам"):
            for size_item in page.get_available_sizes_elements():
                with pytest.allure.step("Проверяем отображение российского размера"):
                    added_size = self.step(check_russian_size, clean_page, self, page, _size_item=size_item, added_size=added_size)

        with pytest.allure.step("Добавляем товар и переходим корзину"):
            page = self.step(step_add_product_to_cart, clean_step_add_product_to_cart, self, page)

        with pytest.allure.step("Проверяем корзину и переходим к странице оформления заказа"):
            page = self.step(check_russian_size_cart, clean_page, self, page)

        with pytest.allure.step("Проверяем отображение русского размера в заказе"):
            self.step(check_russian_size_order, clean_page, self, page)
        self.subtest_pass(1145)

    def test_vmr_cancel_order(self):

        def step_open_designer_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.open_by_url('designer/VQ', VMRDesignerPage)
            return _page

        def clean_page(_test: TestAutotest, **kwargs):
            _test.driver.refresh()

        def clean_cart_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()

            if _page.HEADER.get_cart_count() != 0:
                _page = _page.HEADER.go_to_cart()
                _page.clean_cart()

            _page.open_by_url('designer/VQ', VMRDesignerPage)

        def step_add_product(_test: TestAutotest, _page: VMRDesignerPage, **kwargs):
            _added_product = {}

            for i in range(0, len(_page.get_products_elements())):
                _product_element = _page.get_random_product_element()

                if _product_element.get_name() in _added_product:
                    continue

                _page = _page.go_to_product_by_element(_product_element)

                _page.set_random_color()
                _page.set_random_size()

                _p_prise = _page.get_price()
                _p_name = _page.get_name().replace('«', '').replace('»', '')

                _page.add_product_to_cart()
                _added_product[_p_name] = _p_prise

                _page.HEADER.wait_count_eq(len(_added_product))

                _test.log_text(log_message='Добавляем товар "{0}" со стомостью "{1}"'.format(_p_name, _p_prise))
                _test.log_text(log_message="Количество товаров в корзине: {0}".format(_page.HEADER.get_cart_count()))
                _test.log_json_data(log_message="Данные шага", log_data=_added_product)

                if _page.HEADER.get_cart_count() == 1:
                    return _added_product
                else:
                    _page = _page.back(VMRDesignerPage, timeout=10)

        def step_open_cart_page(_test: TestAutotest, _page, **kwargs):
            _page = _test.open_main_page()
            return _page.HEADER.go_to_cart()

        def step_check_cart_page(_test: TestAutotest, _page: VMRCartPage, _products: dict):
            with pytest.allure.step('Проверяем что в корзине 3 продукта'):
                _page.HEADER.wait_count_eq(len(_products))
            with pytest.allure.step('Получаем данные с корзины'):
                _cart_product = [(item.get_name(), item.get_price()) for item in _page.get_product_elements()]
            with pytest.allure.step('Проверяем что продуты в корзине и их цены совпадают с добавленными'):
                _page.CHECK.check_dict_eq(_products, dict(_cart_product))
            with pytest.allure.step('Проверяем что стоимость заказа подсчитывается корректно'):
                _page.CHECK.check_order_price_eq(sum(_products.values()))

        def step_check_order(_test: TestAutotest, _page: VMROrderPage, _total_price):
            with pytest.allure.step('Вводим данные'):
                _page.set_data(email=self.active_test.data.login,
                               phone=self.active_test.data.phone_number,
                               name=self.active_test.data.name,
                               address=self.active_test.data.address,
                               city=self.active_test.data.result_city,
                               region=self.active_test.data.result_region
                               )

            with pytest.allure.step("Ждем появления способов доставки"):
                _page.get_all_delivery_elements(num_gte=1, timeout=20)

            with pytest.allure.step("Проверяем стоимость курьерских доставок"):
                for _courier_delivery_element in _page.get_all_courier_delivery_elements():
                    with pytest.allure.step(
                            "Проверяем доставку \"{0}\"".format(_courier_delivery_element.get_name())):
                        with pytest.allure.step("Устанавливаем доставку"):
                            _courier_delivery_element.set_active()
                            time.sleep(1)
                            _delivery_price = _courier_delivery_element.get_price()

                        with pytest.allure.step("Проверяем соответствие стоимости доставки в информации о заказе"):
                            _test.log_text(log_message="Стоимость доставки = {0}".format(_delivery_price))
                            _page.CHECK.check_order_delivery_price_eq(_delivery_price, raise_error=False)

                        with pytest.allure.step("Проверяем общую стоимость заказа"):
                            _page.CHECK.check_order_price_eq(_total_price + _delivery_price, raise_error=False)

            with pytest.allure.step("Создаем заказ"):
                return _page.go_to_create_order()

        def step_open_order_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.HEADER.go_to_cart()
            return _page.go_to_order_page()

        def check_private_office(_test: TestAutotest, _order_num, **kwargs):
            _page = _test.open_main_page()
            _page = _page.HEADER.UPPER_MENU.login(login=_test.active_test.data.login,
                                                  password=_test.active_test.data.password)

            _page = _page.PR_LEFT_MENU.go_to_order_list()
            _page.CHECK.check_order_exist(_order_num, raise_error=True)
            return _page

        def step_open_privat_office_order_page(_test: TestAutotest, _order_num, **kwargs):
            _page = page.go_to_order_by_num(order_num)
            return _page

        def step_privat_office_order_page_cancel(_test: TestAutotest, _page, **kwargs):
            with pytest.allure.step("Отменяем заказ"):
                _test.driver.execute_script("window.confirm = function(){return true;}")
                _page.click_cancel_order_button()
                time.sleep(1)

        def check_privat_office_order_page_status(_test: TestAutotest, _page, status, **kwargs):
            _page.CHECK.check_order_status_eq(status, raise_error=True)

        self.subtest_start(1220)

        with pytest.allure.step('Открываем страницу дизайнера VQ'):
            page = self.step(step_open_designer_page, clean_page, self, None)
        with pytest.allure.step('Добавляем 3 продукта в корзину'):
            products = self.step(step_add_product, clean_cart_page, self, page)
        with pytest.allure.step('Открываем страницу корзины'):
            page = self.step(step_open_cart_page, clean_page, self, None)
        with pytest.allure.step('Проверяем страницу корзины'):
            self.step(step_check_cart_page, step_open_cart_page, self, page, _products=products)
        with pytest.allure.step('Открываем страницу заказа'):
            page = self.step(step_open_order_page, step_open_cart_page, self, None)
        with pytest.allure.step('Создаем заказ'):
            page = self.step(step_check_order, step_open_order_page, self, page, _total_price=sum(products.values()))
        order_num = page.get_order_num()
        self.log_text(log_message='Номер заказа: {0}'.format(page.get_order_num()))
        with pytest.allure.step('Проверяем заказ в личном кабинете'):
            page = self.step(check_private_office, clean_page, self, None, _order_num=order_num)
        with pytest.allure.step("Переходим на страницу заказа"):
            page = self.step(step_open_privat_office_order_page, clean_page, self, None, _order_num=order_num)
        with pytest.allure.step("Проверяем статусы заказа и отменяем заказ"):
            self.step(check_privat_office_order_page_status, clean_page, self, page, status='Принят')
            self.step(step_privat_office_order_page_cancel, clean_page, self, page)
            self.step(check_privat_office_order_page_status, clean_page, self, page, status='Отказ')

        self.subtest_pass(1220)

    def test_vmr_privat_office(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.driver.refresh()

        def clean_phone_input(_test: TestAutotest, _page: VMRPrivateOfficePage, **kwargs):
            _page.get_phone_element().clear()
            _test.driver.refresh()

        def step_open_private_office(_test: TestAutotest, _page, **kwargs):
            _page = page.HEADER.UPPER_MENU.login(login=_test.active_test.data.login,
                                                  password=_test.active_test.data.password)
            return _page

        def check_phone_input(_test: TestAutotest, _page: VMRPrivateOfficePage, **kwargs):
            _page.set_phone(self.active_test.data.phone)
            _page.CHECK.check_phone_error_eq(
                msg_error='Данный номер телефона уже используется в другом личном кабинете', raise_error=True)
            _page.close_phone_form()

        def step_save_data(_test: TestAutotest, _page: VMRPrivateOfficePage, **kwargs):
            _page.set_name(self.active_test.data.name)
            _page.check_news()
            _page.save_data()

        def step_open_questoins_page(_test: TestAutotest, _page: VMRPrivateOfficePage, **kwargs):
            _page = _page.PR_LEFT_MENU.go_to_questions()
            return _page

        def check_questoins_text(_test: TestAutotest, _page: VMRPrivateOfficeQuestionsPage, **kwargs):
            element = random.choice(page.get_all_question_elements())
            element.click()
            _page.CHECK.check_is_question_text_visible(element.get_index(), raise_error=True)

        def step_open_adress_delivery_page(_test: TestAutotest, _page: VMRPrivateOfficeQuestionsPage, **kwargs):
            _page = _page.go_to_adress_delivery_page()
            _page.add_adress()
            return _page

        def step_new_adress_delivery_add(_test: TestAutotest, _page: VMRPrivateOfficeAdressPage, **kwargs):
            _page.set_country(self.active_test.data.country)
            _page.set_region(self.active_test.data.region)
            _page.set_city(self.active_test.data.city)
            _page.set_adress(self.active_test.data.address)
            _page.set_post_index(self.active_test.data.post_index)
            _page.save_form()
            time.sleep(1)
            _test.driver.refresh()

        def step_adress_delivery_modify(_test: TestAutotest, _page: VMRPrivateOfficeAdressPage, **kwargs):
            _page.modify_adress()
            _page.set_country(self.active_test.data.country_modify)
            _page.set_region(self.active_test.data.region_modify)
            _page.set_city(self.active_test.data.city_modify)
            _page.set_adress(self.active_test.data.address_modify)
            _page.set_post_index(self.active_test.data.post_index_modify)
            _page.save_form()
            time.sleep(1)
            _test.driver.refresh()

        def step_adress_delivery_del(_test: TestAutotest, _page: VMRPrivateOfficeAdressPage, **kwargs):
            _page.del_adress()
            _test.driver.refresh()

        def step_send_msg_support_page(_test: TestAutotest, _page: VMRPrivateOfficeAdressPage, **kwargs):
            _page = _page.go_to_support()
            _page.set_random_category()
            _page.set_email(self.active_test.data.login)
            _page.set_subject(self.active_test.data.subject)
            _page.set_message(self.active_test.data.message)
            _page.send_message()
            _page.CHECK.check_successful_message_eq(
                msg_1='Ваш вопрос отправлен в службу поддержки. Вы получите ответ в личном кабинете',
                msg_2='Ваш вопрос отправлен в службу поддержки. Вы получите ответ на указанный email', raise_error=True)
            _page.close_pop_up()
            return _page

        self.subtest_start(13)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)
        with pytest.allure.step('Открываем личный кабинет'):
            page = self.step(step_open_private_office, clean_page, self, page)
        self.subtest_pass(13)

        self.subtest_start(1213)
        with pytest.allure.step('Проверяем ввод телефона'):
            self.step(check_phone_input, clean_phone_input, self, page)
        with pytest.allure.step('Вводим имя, подписываемся на новости, сохраняем информацию'):
            self.step(step_save_data, clean_page, self, page)
        self.subtest_pass(1213)

        self.subtest_start(1215)
        with pytest.allure.step("Открываем страницу вопросов"):
            page = self.step(step_open_questoins_page, clean_page, self, page)
        with pytest.allure.step("Проверяем наличие ответа в вопросе"):
            self.step(check_questoins_text, clean_page, self, page)
        self.subtest_pass(1215)

        self.subtest_start(1216)
        with pytest.allure.step("Открываем форму для добавления адреса доставки"):
            page = self.step(step_open_adress_delivery_page, clean_page, self, page)
        with pytest.allure.step("Заполняем адресную форму и сохраняем"):
            self.step(step_new_adress_delivery_add, clean_page, self, page)
        self.subtest_pass(1216)

        self.subtest_start(1217)
        with pytest.allure.step("Изменяем данные адреса'"):
            self.step(step_adress_delivery_modify, clean_page, self, page)
        self.subtest_pass(1217)

        self.subtest_start(1218)
        with pytest.allure.step("Удаляем адрес доставки"):
            self.step(step_adress_delivery_del, clean_page, self, page)
        self.subtest_pass(1218)

        self.subtest_start(1221)
        with pytest.allure.step("Проверяем отправку сообщения в службу поддержки"):
            page =self.step(step_send_msg_support_page, clean_page, self, page)

        self.subtest_pass(1221)

    def test_vmr_regression_sport_page(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test: TestAutotest, _page, **kwargs):
            _test.driver.refresh()

        def step_go_sport_page(_test: TestAutotest, _page: VMRMainPage):
            try:
                _page = _test.init_page_by_url(VMRSportPage, None, 'https://www.vsemayki.ru/sport-designer', 10, True)
            except SportPageException:
                _test.log_text('Ошибка при открытии страницы')
                _test.log_screenshot()
            return _page

        def clean_page(_test, _page, **kwargs):
            _test.driver.refresh()

        def step_go_to_sport_form(_test: TestAutotest, _page: VMRSportPage):
            _page.click_sport_form(form=(random.choice(['football', 'basketball', 'volleyball'])))
            try:
                _page = VMRSportProductPage(driver=_test.driver, url="",
                                            project_url=_test.main_page, timeout=10, confirm=True, init=False)
            except SportProductException:
                _test.log_text('Ошибка при открытии страницы')
                _test.log_screenshot()
            return _page

        def step_about_tshirt(_test: TestAutotest, _page: VMRSportProductPage):
            _page.open_about_tshirt()
            _table_element = _page.get_about_product_element()

            if not _page.CHECK.is_exist(_table_element):
                _test.log_text(log_message="Информация о футболке не отображается")
                _test.log_screenshot()

            with pytest.allure.step('Закрываем информацию о товаре'):
                _table_element.assure(exist).assure(is_displayed).close()

        def step_about_shorts(_test: TestAutotest, _page: VMRSportProductPage):
            _page.open_about_shorts()
            _table_element = _page.get_about_product_element()

            if not _page.CHECK.is_exist(_table_element):
                _test.log_text(log_message="Информация о шортах не отображается")
                _test.log_screenshot()

            with pytest.allure.step('Закрываем информацию о товаре'):
                _table_element.assure(exist).assure(is_displayed).close()

        def step_get_pattern(_test, _page: VMRSportProductPage):
            _page.get_pattern_element(random.randint(1, len(_page.get_patterns_elements().assure(len_more, 0))) - 1).set_active()

        def step_table_sizes_tshirt(_test: TestAutotest, _page: VMRSportProductPage):
            _page.open_table_sizes_tshirt()
            _table_element = _page.get_size_table_element()

            if not _page.CHECK.is_exist(_table_element):
                _test.log_text(log_message="Таблица размеров для футболки не отображается")
                _test.log_screenshot()

            with pytest.allure.step('Закрываем информацию о товаре'):
                _table_element.assure(exist).assure(is_displayed).close()

        def step_table_sizes_shorts(_test: TestAutotest, _page: VMRSportProductPage):
            _page.open_table_sizes_shorts()
            _table_element = _page.get_size_table_element()

            if not _page.CHECK.is_exist(_table_element):
                _test.log_text(log_message="Таблица размеров для шорт не отображается")
                _test.log_screenshot()

            with pytest.allure.step('Закрываем информацию о товаре'):
                _table_element.assure(exist).assure(is_displayed).close()

        def step_get_price(_test, _page: VMRSportProductPage):
            _price = _page.get_price()
            return _price

        def step_data_input(_test, _page: VMRSportProductPage, price):
            for i in range(1, 6):
                with pytest.allure.step("Вводим фамилию игрока"):
                    _page.set_surname("{0}-ая Фамилия" .format(i), player_index=i)

                    with pytest.allure.step("Вводим номер игрока"):
                        _page.set_number("{0}" .format(i), number_index=i)

                        with pytest.allure.step("Добавляем нового игрока"):
                            if i < 5:
                                _page.add_player()

            with pytest.allure.step("Проверяем общую цену"):
                _page.CHECK.check_price_eq(value=price*5, raise_error=True)
                _total_price = _page.get_price()

            with pytest.allure.step('Нажимаем "добавить в корзину" и ждем появления попапа'):
                _page.add_cart()
                i = 0
                while _page.is_created_pop_up() is False:
                    time.sleep(0.1)
                    i += 0.1
                    if i > 10 * 20:
                        raise SportProductPopupException('Долгая закрузка попапа')
                    pass
            return _total_price

        def step_go_to_cart(_test: TestAutotest, _page: VMRSportProductPage):
            _page.go_to_cart()
            try:
                _page = VMRCartPage(driver=self.driver, url="", project_url=self.main_page, timeout=20, confirm=True,
                                   init=False)
            except CartException:
                _test.log_text("Не открылась страница корзины")
                _test.log_screenshot()
            return _page

        def step_price_cart_check(_test, _page: VMRCartPage, total_price):
            _page.CHECK.check_order_price_eq(value=total_price, raise_error=True)

        self.subtest_start(1228)

        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)

        with pytest.allure.step("Открываем страницу спортивной формы"):
            page = self.step(step_go_sport_page, clean_page, step_test=self, step_page=page)

        with pytest.allure.step("Переходим на случайную страницу создания формы"):
            page = self.step(step_go_to_sport_form, clean_page, step_test=self, step_page=page)

        with pytest.allure.step("Открываем ссылку \"Подробнее о товаре\" для футболки"):
            self.step(step_about_tshirt, clean_page, step_test=self, step_page=page)

        with pytest.allure.step("Открываем ссылку \"Подробнее о товаре\" для шорт"):
            self.step(step_about_shorts, clean_page, step_test=self, step_page=page)

        with pytest.allure.step("Выбираем случайный узор формы"):
            self.step(step_get_pattern, clean_page, step_test=self, step_page=page)

        with pytest.allure.step("Открываем ссылку \"Таблица размеров\" для футболки"):
            self.step(step_table_sizes_tshirt, clean_page, step_test=self, step_page=page)

        with pytest.allure.step("Открываем ссылку \"Таблица размеров\" для шорт"):
            self.step(step_table_sizes_shorts, clean_page, step_test=self, step_page=page)

        with pytest.allure.step("Запоминаем цену"):
            price = self.step(step_get_price, clean_page, step_test=self, step_page=page)

        with pytest.allure.step("Заполняем данные для 5 игроков, проверяем цену, добавляем в корзину"):
            total_price = self.step(step_data_input, clean_page, step_test=self, step_page=page, price=price)

        with pytest.allure.step('Переходим в корзину'):
            page = self.step(step_go_to_cart, clean_page, step_test=self, step_page=page)

        with pytest.allure.step('Сравниваем цены'):
            self.step(step_price_cart_check, clean_page, step_test=self, step_page=page, total_price=total_price)

        self.subtest_pass(1228)