import pytest
import settings
from config_server_worker.config_server_api_worker import TestConfigApiWorker
from module_projects.vmr.base_test import ProjectBaseTest
from module_projects.vmr.pages.CartPage.page import VMRCartPage
from module_projects.vmr.pages.DesignerPage.page import VMRDesignerPage
from module_projects.vmr.pages.MainPage.page import VMRMainPage
from module_projects.vmr.pages.OrderPage.exception import OrderEmptyCartException
from module_projects.vmr.pages.OrderPage.page import VMROrderPage
from module_projects.vmr.pages.ProductPage.page import VMRProductPage

testConfigApiWorker = TestConfigApiWorker(server=settings.CONFIG_SERVER_URL, api_version=settings.API_VER)


class TestAutotest(ProjectBaseTest):
    def test_vmr_open(self):

        self.subtest_start(441)

        def clean_open_main_page(_test, _page=None):
            VMRMainPage(driver=_test.driver, project_url=_test.main_page, timeout=10, init=True, confirm=True)

        def clean_open_designer_page(_test, _page=None):
            _page = VMRMainPage(driver=_test.driver, project_url=_test.main_page, timeout=10, init=True, confirm=True)
            return _page.open_by_url("designer/VQ", VMRDesignerPage)

        def clean_open_product_page(_test, _page=None):
            _page = VMRMainPage(driver=_test.driver, project_url=_test.main_page, timeout=10, init=True, confirm=True)

            need_add = False

            if _page.HEADER.get_cart_count() != 0:
                need_add = True
                _page = _page.HEADER.go_to_cart()
                _page.clean_cart()

            _page = _page.open_by_url("designer/VQ", VMRDesignerPage)
            _page = _page.go_to_random_product()

            if need_add:
                _page.set_random_size()
                _page.set_random_color()
                _page.add_product_to_cart()

            return _page

        def clean_open_cart_page(_test, _page=None):
            _page = VMRMainPage(driver=_test.driver, project_url=_test.main_page, timeout=10, init=True, confirm=True)
            return _page.HEADER.go_to_cart()

        def clean_open_order_page(_test, _page=None):
            _page = VMRMainPage(driver=_test.driver, project_url=_test.main_page, timeout=10, init=True, confirm=True)
            if _page.HEADER.get_cart_count() == 0:
                raise OrderEmptyCartException("Заказ не был создан, но корзина очистилась")
            _page = _page.HEADER.go_to_cart()
            return _page.go_to_order_page()

        def step_open_main_page(_test, _page):
            return _test.open_main_page()

        def step_open_designer_page(_test, _page: VMRMainPage):
            return _page.open_by_url("designer/VQ", VMRDesignerPage)

        def step_check_designer_page(_test: TestAutotest, _page: VMRDesignerPage):
            with pytest.allure.step("Проверяем количество продуктов на странице."):
                _page.CHECK.check_products_length_gt(0, raise_error=True)
                _test.log_text("Количество продуктов на странице: {0}" .format(len(_page.get_products_elements())))

            with pytest.allure.step("Проверяем что в адрессной строке отображается артикул продукта"):
                _page.CHECK.check_vendor_code_is_int(raise_error=True)

        def step_open_product_page(_test, _page: VMRDesignerPage):
            return _page.go_to_random_product()

        def step_add_product_page(_test:TestAutotest, _page: VMRProductPage):
            _page.set_random_size()
            _page.set_random_color()
            _page.add_product_to_cart()
            _page.HEADER.wait_count_eq(1)
            _test.log_screenshot()
            _test.log_text(_page.get_name())

        def step_open_cart_page(_test, _page: VMRProductPage):
            return _page.go_to_cart()

        def step_check_cart_page(_test, _page: VMRCartPage):
            with pytest.allure.step("Проверяем что добавленный продукт в единственном экземпляре"):
                _page.CHECK.check_products_length_qe(1, raise_error=True)

        def step_open_order_page(_test, _page: VMRCartPage):
            return _page.go_to_order_page()

        def step_create_order_page(_test: TestAutotest, _page: VMROrderPage):
            with pytest.allure.step('Вводим данные'):
                _page.set_data(email=self.active_test.data.login,
                               phone=self.active_test.data.phone_number,
                               name=self.active_test.data.name,
                               comment=self.active_test.data.comment,
                               address=self.active_test.data.address,
                               city=self.active_test.data.result_city,
                               region=self.active_test.data.result_region
                               )

            with pytest.allure.step("Ждем появления способов доставки"):

                _page.get_all_delivery_elements(num_gte=1, timeout=20)

                _page.set_courier_delivery_data()

            with pytest.allure.step("Создаем заказ и получаем его номер"):
                _page = _page.go_to_create_order()
                order_num = _page.get_order_num()
                _test.log_text('Номер заказа "{0}"'.format(order_num))
                _page.close_pop_up()

        page = self.step(step_open_main_page, clean_open_main_page, self, None)

        with pytest.allure.step('Открываем страницу дизайнера VQ.'):
            page = self.step(step_open_designer_page, clean_open_main_page, self, page)

        with pytest.allure.step('Проверки страницы дизайнера.'):
            self.step(step_check_designer_page, clean_open_designer_page, self, page)

        with pytest.allure.step('Открываем страницу продукта.'):
            page = self.step(step_open_product_page, clean_open_designer_page, self, page)

        with pytest.allure.step('Добаление продукта в корзину.'):
            self.step(step_add_product_page, clean_open_product_page, self, page)

        with pytest.allure.step('Открываем страницу корзины.'):
            page = self.step(step_open_cart_page, clean_open_product_page, self, page)

        with pytest.allure.step('Проверки страницы корзины.'):
            self.step(step_check_cart_page, clean_open_cart_page, self, page)

        with pytest.allure.step('Открываем страницу заказа.'):
            page = self.step(step_open_order_page, clean_open_cart_page, self, page)

        with pytest.allure.step('Создание заказа'):
            self.step(step_create_order_page, clean_open_order_page, self, page)

        self.subtest_pass(441)
