import json
import random

import pytest
import time
import ast

import requests

from autotests_base.conditions.element_collection_conditions import len_more, len_equal
from module_projects.base.page import BasePage
from module_projects.vmr.base_test import ProjectBaseTest
from module_projects.vmr.pages.CartPage.exception import CartValueException
from module_projects.vmr.pages.CartPage.page import VMRCartPage
from module_projects.vmr.pages.CatalogPage.page import VMRCatalogPage
from module_projects.vmr.pages.ConstructorPage.exception import ConstructorException
from module_projects.vmr.pages.ConstructorPage.page import VMRConstructorPage
from module_projects.vmr.pages.DesignerPage.page import VMRDesignerPage
from module_projects.vmr.pages.MainPage.page import VMRMainPage
from module_projects.vmr.pages.OrderPage.exception import OrderEmptyCartException
from module_projects.vmr.pages.OrderPage.page import VMROrderPage
from module_projects.vmr.pages.ProductPage.page import VMRProductPage
from module_projects.vmr.pages.SearchPage.page import VMRSearchPage
from module_projects.vmr.pages.SuccessfulOrderPage.page import VMRSuccessfulOrderPage
from module_projects.vmr.pages.OptPage.page import VMROptPage


class TestAutotest(ProjectBaseTest):
    def test_vmr_smoke_sql_injection(self):

        def step_open_main_page(_test, _page: VMRMainPage):
            return _test.open_main_page()

        def step_open_page_sql(_test, _page: VMRMainPage, _sql=None):
            _test.driver.get(_test.main_page + _sql)
            _page.CHECK.check_page_with_sql_inject(_page, raise_error=True)

        def clean_page(_test, _page: VMRMainPage, _sql=None):
            time.sleep(1)

        self.subtest_start(943)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)

        with pytest.allure.step('Проверяем sql-инъекции с %'):
            self.step(step_open_page_sql, clean_page, self, page,
                      _sql='/catalog/group/man_hoodies%27-alert(document.domain),%27')

        with pytest.allure.step("Проверяем sql-инъекции с '"):
            self.step(step_open_page_sql, clean_page, self, page,
                      _sql="/catalog/group/man_hoodies'-alert(document.domain),'")
        self.subtest_pass(943)

    def test_vmr_smoke_retails_pages_open(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def step_go_retail(_test: TestAutotest, _page: VMRMainPage, retails_keys_data):
            _errors = []
            for retail in retails_keys_data.keys():
                with pytest.allure.step('Открытие страницы "{0}"'.format(retails_keys_data[retail])):
                    _page = _page.open_by_url('?access_key={0}'.format(retail), VMRMainPage)

                with pytest.allure.step('Проверка наличия "Услуг" в принт-центре'):
                    if not _page.HEADER.DROP_DOWN_MENU.CHECK.check_menu_by_name(retail=retails_keys_data[retail],
                                                                                name='Услуги'):
                        _errors.append(retails_keys_data[retail])

            return _errors

        def clean_page(_test, _page: VMRMainPage, **kwargs):
            return _test.open_main_page()

        self.subtest_start(869)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)

        with pytest.allure.step('Проверяем отображение пункта "Услуги" в Принт-центре'):
            retails_keys = {'msk1': 'ПЦ Афимолл Сити',
                            'ekb1': 'ПЦ Екатеринбург',
                            'tAu7CmTn8PsASFHOxbamBgcjcmwdZZfGbYnqQfQIQzw=': 'ПЦ Сибирский молл',
                            'krr1': 'ПЦ Краснодар',
                            'nsk4': 'ПЦ Сибирский молл 2',
                            'nsk_versal': 'ПЦ Версаль'}

            _errors = self.step(step_go_retail, clean_page, step_test=self,
                                step_page=page, retails_keys_data=retails_keys)

        if _errors:
            pytest.fail("\nОбнаружены ошибки:\n\t" + "\n\t".join(_errors))
        self.subtest_pass(869)

    def test_vmr_smoke_order_cost_check(self):
        self.subtest_start(1086)

        def step_open_designer_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.open_by_url('designer/VQ', VMRDesignerPage)
            return _page

        def clean_wait(**kwargs):
            time.sleep(5)

        def clean_cart_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()

            if _page.HEADER.get_cart_count() != 0:
                _page = _page.HEADER.go_to_cart()
                _page.clean_cart()

            _page.open_by_url('designer/VQ', VMRDesignerPage)

        def step_add_3_product(_test: TestAutotest, _page: VMRDesignerPage, **kwargs):
            _added_product = {}

            for i in range(0, len(_page.get_products_elements())):
                _product_element = _page.get_random_product_element()

                if _product_element.get_name() in _added_product:
                    continue

                _page = _page.go_to_product_by_element(_product_element)

                _page.set_random_color()
                _page.set_random_size()

                _p_prise = _page.get_price()
                _p_name = _page.get_name().replace('«', '').replace('»', '')

                _page.add_product_to_cart()
                _added_product[_p_name] = _p_prise

                _page.HEADER.wait_count_eq(len(_added_product))

                _test.log_text('Добавляем товар "{0}" со стомостью "{1}"'.format(_p_name, _p_prise))
                _test.log_text("Количество товаров в корзине: {0}" .format(_page.HEADER.get_cart_count()))
                _test.log_json_data(log_message="Данные шага", log_data=_added_product)

                if _page.HEADER.get_cart_count() == 3:
                    return _added_product
                else:
                    _page = _page.back(VMRDesignerPage, timeout=10)

        def step_open_cart_page(_test: TestAutotest, _page, **kwargs):
            _page = _test.open_main_page()
            return _page.HEADER.go_to_cart()

        def step_check_cart_page(_test: TestAutotest, _page: VMRCartPage, _products: dict):
            with pytest.allure.step('Проверяем что в корзине 3 продукта'):
                _page.HEADER.wait_count_eq(len(_products))
            with pytest.allure.step('Получаем данные с корзины'):
                _cart_product = [(item.get_name(), item.get_price()) for item in _page.get_product_elements()]
            with pytest.allure.step('Проверяем что продуты в корзине и их цены совпадают с добавленными'):
                _page.CHECK.check_dict_eq(_products, dict(_cart_product))
            with pytest.allure.step('Проверяем что стоимость заказа подсчитывается корректно'):
                _page.CHECK.check_order_price_eq(sum(_products.values()))

        def step_check_order(_test: TestAutotest, _page: VMROrderPage, _total_price):
            with pytest.allure.step('Вводим данные'):
                _page.set_data(email=self.active_test.data.login,
                               phone=self.active_test.data.phone_number,
                               name=self.active_test.data.name,
                               comment=self.active_test.data.comment,
                               address=self.active_test.data.address,
                               city=self.active_test.data.result_city,
                               region=self.active_test.data.result_region
                               )

            with pytest.allure.step("Ждем появления способов доставки"):
                _page.get_all_delivery_elements(num_gte=1, timeout=20)

            with pytest.allure.step("Проверяем стоимость курьерских доставок"):
                for _courier_delivery_element in _page.get_all_courier_delivery_elements():
                    with pytest.allure.step("Проверяем доставку \"{0}\"" .format(_courier_delivery_element.get_name())):
                        with pytest.allure.step("Устанавливаем доставку"):
                            _courier_delivery_element.set_active()
                            time.sleep(1)
                            _delivery_price = _courier_delivery_element.get_price()

                        with pytest.allure.step("Проверяем соответствие стоимости доставки в информации о заказе"):
                            _test.log_text("Стоимость доставки = {0}" .format(_delivery_price))
                            _page.CHECK.check_order_delivery_price_eq(_delivery_price, raise_error=True)

                        with pytest.allure.step("Проверяем общую стоимость заказа"):
                            _page.CHECK.check_order_price_eq(_total_price + _delivery_price, raise_error=True)

            with pytest.allure.step("Создаем заказ"):
                return _page.go_to_create_order()

        def step_open_order_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.HEADER.go_to_cart()
            return _page.go_to_order_page()

        def check_private_office(_test: TestAutotest, _order_num, **kwargs):
            _page = _test.open_main_page()
            _page = _page.HEADER.UPPER_MENU.login(login=_test.active_test.data.login,
                                                  password=_test.active_test.data.password)

            _page = _page.PR_LEFT_MENU.go_to_order_list()
            _page.CHECK.check_order_exist(_order_num, raise_error=True)

        with pytest.allure.step('Открываем страницу дизайнера VQ'):
            page = self.step(step_open_designer_page, clean_wait, self, None)
        with pytest.allure.step('Добавляем 3 продукта в корзину'):
            products = self.step(step_add_3_product, clean_cart_page, self, page)
        with pytest.allure.step('Открываем страницу корзины'):
            page = self.step(step_open_cart_page, clean_wait, self, None)
        with pytest.allure.step('Проверяем страницу корзины'):
            self.step(step_check_cart_page, step_open_cart_page, self, page, _products=products)
        with pytest.allure.step('Открываем страницу заказа'):
            page = self.step(step_open_order_page, step_open_cart_page, self, None)
        with pytest.allure.step('Создаем заказ'):
            page = self.step(step_check_order, step_open_order_page, self, page, _total_price=sum(products.values()))
        order_num = page.get_order_num()
        self.log_text('Номер заказа: {0}'.format(page.get_order_num()))
        with pytest.allure.step('Проверяем заказ в личном кабинете                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         '):
            self.step(check_private_office, clean_wait, self, None, _order_num=order_num)

        self.subtest_pass(1086)

    def test_vmr_smoke_constructor(self):
        self.subtest_start(495)

        def step_open_main_page(_test: TestAutotest, _page=None, **kwargs):
            return _test.open_main_page()

        def step_go_to_constructor(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            return _page.go_to_constructor()

        def clean_wait(**kwargs):
            time.sleep(5)

        def clean_open_constructor_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            if _page.HEADER.get_cart_count() != 0:
                _page = _page.HEADER.go_to_cart()
                _page.clean_cart()
            _page = _test.open_main_page()
            return _page.go_to_constructor()

        def step_check_price_change_product_type(_test: TestAutotest, _page: VMRConstructorPage):
            _old_price = -1
            _price_change = False

            for index in range(0, 3):
                _page.set_product_type_by_index(index)

                if _old_price != -1:
                    for num_try in range(5):
                        if not _page.CHECK.check_price_eq(_old_price):
                            _price_change = True
                            break
                        time.sleep(1)
                else:
                    _old_price = _page.get_price()

                if _price_change:
                    break

            else:
                raise ConstructorException("Одинаковая цена у трех продукттипов подряд.")

        def step_add_product_to_cart(_test: TestAutotest, _page: VMRConstructorPage):
            with pytest.allure.step('Устанавливаем случайный цвет и размер'):
                _page.set_random_color()
                _page.set_random_size()

            with pytest.allure.step('Получаем конечные данные продкта'):

                _active_size_element = _page.get_active_size_element()

                _active_size_element.scroll_to_element()
                _size_id = _active_size_element.get_id()
                _size_name = _active_size_element.get_name()

                _product_type_element = _page.get_active_product_type()
                _product_type_name = _product_type_element.get_name()

                _price = _page.get_price()

                _test.log_text("Код размера - {0}".format(_size_id))
                _test.log_text("Имя размера - {0}".format(_size_name))
                _test.log_text("Продуктип {0}".format(_product_type_name))
                _test.log_text("Цена - {0}".format(_price))

                _result_data = {"size_id": _size_id,
                                "price": _price,
                                "size_name": _size_name,
                                "product_type_name": _product_type_name
                                }

            with pytest.allure.step('Создаем продукт'):
                _page.create_product()

            with pytest.allure.step('Ждем поп-ап с сообщением что продукт добавлен в корзину'):
                _pop_up = _page.get_created_pop_up(timeout=30)
                _pop_up.close()

            with pytest.allure.step('Ждем изменения счетчика корзины'):
                _page.HEADER.wait_count_eq(1, timeout=5)

            return _result_data

        def step_open_cart(_test: TestAutotest, _page: BasePage):
            _page = _test.open_main_page()
            return _page.HEADER.go_to_cart()

        def clean_open_cart_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.HEADER.go_to_cart()
            _test.log_screenshot()
            return _page

        def step_check_cart_page(_test: TestAutotest, _page: VMRCartPage,  _product_data):
            with pytest.allure.step('Проверяем количество продуктов в корзине'):
                _page.CHECK.check_len_products_eq(1)

            with pytest.allure.step('Ждем генерации картинки'):
                _cart_product = _page.get_product_element(0)
                _cart_product.wait_to_generate_img()

            with pytest.allure.step('Получаем данные продута в корзине'):
                _cart_price = _cart_product.get_price()
                _cart_type = _cart_product.get_type_name()
                _cart_size_name = _cart_product.get_size_str()
                _cart_size_id = _cart_product.get_size_key()

                _cart_data = {"size_id": _cart_size_id,
                              "price": _cart_price,
                              "size_name": _cart_size_name,
                              "product_type_name": _cart_type
                              }
                _test.log_json_data(log_message="Данные корзины", log_data=_cart_data)

            with pytest.allure.step('Сверяем данные.'):
                _page.CHECK.check_dict_eq(dict_1=_cart_data, dict_2=_product_data,
                                          error_class=CartValueException,
                                          dict_name="Данные продукта",
                                          raise_error=True)

        def step_open_order_page(_test: TestAutotest, _page: VMRCartPage, **kwargs):
            _page = _page.go_to_order_page()
            _test.log_screenshot()
            return _page

        def clean_open_order_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            _page = _page.HEADER.go_to_cart()
            return _page.go_to_order_page()

        def step_check_order_page(_test: TestAutotest, _page: VMROrderPage, _product_data):
            _total_price = _product_data['price']
            with pytest.allure.step('Вводим данные'):
                _page.set_data(email=self.active_test.data.login,
                               phone=self.active_test.data.phone_number,
                               name=self.active_test.data.name,
                               comment=self.active_test.data.comment,
                               address=self.active_test.data.address,
                               city=self.active_test.data.result_city,
                               region=self.active_test.data.result_region
                               )

                with pytest.allure.step("Ждем появления способов доставки"):
                    _page.get_all_delivery_elements(num_gte=1, timeout=20)

                with pytest.allure.step("Проверяем стоимость курьерских доставок"):
                    for _courier_delivery_element in _page.get_all_courier_delivery_elements():
                        with pytest.allure.step(
                                "Проверяем доставку \"{0}\"".format(_courier_delivery_element.get_name())):
                            with pytest.allure.step("Устанавливаем доставку"):
                                _courier_delivery_element.set_active()
                                time.sleep(1)
                                _delivery_price = _courier_delivery_element.get_price()

                            with pytest.allure.step("Проверяем соответствие стоимости доставки в информации о заказе"):
                                _test.log_text("Стоимость доставки = {0}".format(_delivery_price))
                                _page.CHECK.check_order_delivery_price_eq(_delivery_price, raise_error=True)

                            with pytest.allure.step("Проверяем общую стоимость заказа"):
                                _page.CHECK.check_order_price_eq(_total_price + _delivery_price, raise_error=True)

                with pytest.allure.step("Создаем заказ"):
                    return _page.go_to_create_order()

        with pytest.allure.step("Открываем главную страницу"):
            page = self.step(step_open_main_page, clean_wait, self, None)
        with pytest.allure.step("Открываем страницу конструктора"):
            page = self.step(step_go_to_constructor, step_open_main_page, self, page)
        with pytest.allure.step("Проверяем страницу конструктора"):
            self.step(step_check_price_change_product_type, clean_open_constructor_page, self, page)
        with pytest.allure.step("Добавляем продукт в корзину"):
            product_data = self.step(step_add_product_to_cart, clean_open_constructor_page, self, page)
        with pytest.allure.step("Открываем страницу корзины"):
            page = self.step(step_open_cart, clean_wait, self, page)
        with pytest.allure.step("Проверяем страницу корзины"):
            self.step(step_check_cart_page, clean_open_cart_page, self, page, _product_data=product_data)
        with pytest.allure.step("Переходим на страницу заказа"):
            page = self.step(step_open_order_page, clean_open_cart_page, self, page)
        with pytest.allure.step("Проверяем страницу заказа"):
            page = self.step(step_check_order_page, clean_open_order_page, self, page, _product_data=product_data)
        self.log_text('Номер заказа: {0}' .format(page.get_order_num()))

        self.subtest_pass(495)

    def test_vmr_smoke_city_detect(self):
        self.subtest_start(1210)

        def step_check_city(_test: TestAutotest, _page: None):
            _page = _test.open_main_page()

            ip_request_string = 'https://api.ipify.org?format=json'
            city_request_string = 'http://api.sypexgeo.net/json/{0}'

            with pytest.allure.step("Определяем ip и город через сервис"):
                browser_data = json.loads(requests.get(ip_request_string).content.decode('utf-8'))
                browser_ip = browser_data['ip']

                city_data = json.loads(requests.get(city_request_string.format(browser_ip)).content.decode('utf-8'))
                city = city_data['city']['name_ru']

                _test.log_text('IP: {0}'.format(browser_ip))
                _test.log_text('Город: {0}'.format(city))

            with pytest.allure.step("Проверяем совпадение города."):
                _page.HEADER.CHECK.check_city_eq(city, raise_error=True)

        def clean(_test, _page, **kwargs):
            time.sleep(5)

        self.step(step_check_city, clean, step_test=self, step_page=None)
        self.subtest_pass(1210)

    def test_vmr_smoke_data_layer(self):

        def step_open_main_page(_test: TestAutotest, _page=None, **kwargs):
            _page = _test.open_main_page()
            return _page

        def step_go_to_catalog(_test: TestAutotest, _page: VMRMainPage, _catalog_index, **kwargs):
            return _page.CATALOG_MENU.go_to_catalog_by_index(index=_catalog_index)

        def clean_catalog_page(_test: TestAutotest, _page: VMRMainPage, _catalog_index, **kwargs):
            _page = _test.open_main_page()
            _page.CATALOG_MENU.go_to_catalog_by_index(_catalog_index)

        def step_check_catalog_page(_test: TestAutotest, _page: VMRCatalogPage, **kwargs):
            with pytest.allure.step('Получаем данные DataLayer'):
                _page.init_data_layer()
                _test.log_json_data(log_message="DataLayer", log_data=_page.data_layer.data)

            with pytest.allure.step('Получаем данные со страницы'):

                # Требуется приведение цены и id к str, так как в dataLayer получаются именно str.
                impressions = [{"list": item.get_init_product_list(),
                                "name": "{0} {1}".format(item.get_init_category(), item.get_name()),
                                "category": item.get_init_category(),
                                "price": str(item.get_price()),
                                "variant": item.get_product_type(),
                                "id": str(item.get_init_product_id())} for item in _page.get_products_elements()
                               ]

                _test.log_json_data(log_message="Page data impressions", log_data=impressions)

            with pytest.allure.step('Сверяем собранные данные.'):
                with pytest.allure.step('Сравниваем параметр "page"'):
                    _page.CHECK.check_data_layer_section_exist('page', raise_error=True)
                    _page.CHECK.check_data_layer_value('page', 'ListingPage', raise_error=True)

                with pytest.allure.step('Сравниваем параметр "currencyCode"'):
                    _page.CHECK.check_data_layer_section_exist('ecommerce', raise_error=True)
                    _page.CHECK.check_data_layer_value('ecommerce.currencyCode', 'RUB', raise_error=True)

                    with pytest.allure.step('Сравниваем параметр "EntryCategory"'):
                        _page.CHECK.check_data_layer_value('EntryCategory', _page.get_str_id(), raise_error=True)

                    with pytest.allure.step('Сравниваем параметр "impressions"'):
                        with pytest.allure.step('Сравнение общего количества.'):
                            _page.CHECK.check_data_layer_length_impressions_eq(value=len(impressions), raise_error=True)

                        with pytest.allure.step('Сравнение продуктов на странице с элементами dataLayer'):
                            _page.CHECK.check_page_data_impressions_with_data_layer_eq(value=impressions, raise_error=True)

                        with pytest.allure.step('Сравнение элементов dataLayer с продуктами на странице'):
                            _page.CHECK.check_data_layer_impressions_data_with_page_eq(value=impressions, raise_error=True)

        def step_check_main_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            with pytest.allure.step('Получаем данные DataLayer'):
                _page.init_data_layer()
                _test.log_json_data(log_message="DataLayer", log_data=_page.data_layer.data)

            with pytest.allure.step('Получаем данные со страницы'):

                # Требуется приведение цены и id к str, так как в dataLayer получаются именно str.
                impressions = [{"list": item.get_init_product_list(),
                                "name": "{0} {1}".format(item.get_init_category(), item.get_name()),
                                "category": item.get_init_category(),
                                "price": str(item.get_price()),
                                "variant": item.get_product_type(),
                                "id": str(item.get_init_product_id())} for item in _page.get_products_elements()
                               ]

                _test.log_json_data(log_message="Page data impressions", log_data=impressions)

            with pytest.allure.step('Сверяем собранные данные.'):
                with pytest.allure.step('Сравниваем параметр "page"'):
                    _page.CHECK.check_data_layer_section_exist('page', raise_error=True)
                    _page.CHECK.check_data_layer_value('page', 'HomePage', raise_error=True)

                with pytest.allure.step('Сравниваем параметр "currencyCode"'):
                    _page.CHECK.check_data_layer_section_exist('ecommerce', raise_error=True)
                    _page.CHECK.check_data_layer_value('ecommerce.currencyCode', 'RUB', raise_error=True)

                with pytest.allure.step('Сравниваем параметр "impressions"'):
                    with pytest.allure.step('Сравнение общего количества.'):
                        _page.CHECK.check_data_layer_length_impressions_eq(value=len(impressions), raise_error=True)

                    with pytest.allure.step('Сравнение продуктов на странице с элементами dataLayer'):
                        _page.CHECK.check_page_data_impressions_with_data_layer_eq(value=impressions, raise_error=True)

                    with pytest.allure.step('Сравнение элементов dataLayer с продуктами на странице'):
                        _page.CHECK.check_data_layer_impressions_data_with_page_eq(value=impressions, raise_error=True)

        def step_go_to_search_page(_test: TestAutotest, _page: VMRMainPage, **kwargs):
            _page.HEADER.SEARCH.search_product(search_text='test')
            return _page

        def step_check_search_page(_test: TestAutotest, _page: VMRSearchPage):
            with pytest.allure.step('Получаем данные DataLayer'):
                _page.init_data_layer()
                _test.log_json_data(log_message="DataLayer", log_data=_page.data_layer.data)

            with pytest.allure.step('Получаем данные со страницы'):

                # Требуется приведение цены и id к str, так как в dataLayer получаются именно str.
                impressions = [{"list": item.get_init_product_list(),
                                "name": "{0} {1}".format(item.get_init_category(), item.get_name()),
                                "category": item.get_init_category(),
                                "price": str(item.get_price()),
                                "variant": item.get_product_type(),
                                "id": str(item.get_init_product_id())} for item in _page.get_products_elements()
                               ]

                _test.log_json_data(log_message="Page data impressions", log_data=impressions)

            with pytest.allure.step('Сверяем собранные данные.'):
                with pytest.allure.step('Сравниваем параметр "page"'):
                    _page.CHECK.check_data_layer_section_exist('page', raise_error=True)
                    _page.CHECK.check_data_layer_value('page', 'ListingPage', raise_error=True)

                with pytest.allure.step('Сравниваем параметр "currencyCode"'):
                    _page.CHECK.check_data_layer_section_exist('ecommerce', raise_error=True)
                    _page.CHECK.check_data_layer_value('ecommerce.currencyCode', 'RUB', raise_error=True)

                with pytest.allure.step('Сравниваем параметр "impressions"'):
                    with pytest.allure.step('Сравнение общего количества.'):
                        _page.CHECK.check_data_layer_length_impressions_eq(value=len(impressions), raise_error=True)

                    with pytest.allure.step('Сравнение продуктов на странице с элементами dataLayer'):
                        _page.CHECK.check_page_data_impressions_with_data_layer_eq(value=impressions, raise_error=True)

                    with pytest.allure.step('Сравнение элементов dataLayer с продуктами на странице'):
                        _page.CHECK.check_data_layer_impressions_data_with_page_eq(value=impressions, raise_error=True)

        def step_open_product_page(_test: TestAutotest, _page, index, **kwargs):
            _page = _test.open_main_page()
            _page = _page.open_by_url('/designer/VQ', VMRDesignerPage)
            _index = random.randint(0, len(_page.get_products_elements())) if index is None else index
            _page = _page.go_to_product_by_index(index=_index)
            return _page, _index

        def step_check_product_page(_test: TestAutotest, _page: VMRProductPage, **kwargs):
            with pytest.allure.step('Получаем данные DataLayer'):
                _page.RECOMMENDED_PRODUCT.get_products_elements().assure(len_more, 0)

                if _page.RECOMMENDED_PRODUCT.get_next_recommended_button_element().exist():
                    _page.RECOMMENDED_PRODUCT.click_next_recommended_product_button()

                _page.init_data_layer()
                _test.log_json_data(log_message="DataLayer", log_data=_page.data_layer.data)

            with pytest.allure.step('Собираем данные со страницы'):

                elements = _page.BREADCRUMBS.get_breadcrumbs()
                names = [item.get_name() for item in elements]
                tkeys = [item.get_tkey() if item.get_tkey() else None for item in elements]

                start_index = None
                result = []

                for index, item in enumerate(zip(names, tkeys)):
                    if item[0] == _page.get_product_type_name():
                        start_index = index
                    if start_index and start_index <= index:
                        result.append(item)

                _detail_product = {"name": _page.get_full_name(),
                                   "price": _page.get_price(),
                                   "variant": _page.get_product_type_eng(),
                                   "tkey": _page.get_product_type_eng(),
                                   "id": _page.get_product_int_id(),
                                   "category_tkey": ">".join([item[1] for item in list(result)[1:]]),
                                   "category": "/".join([item[0] for item in list(result)])}
                _test.log_json_data(log_message="Данные продукта", log_data=_detail_product)

            with pytest.allure.step('Собираем данные рекомендованных продуктов'):
                elements = _page.RECOMMENDED_PRODUCT.get_products_elements().assure(len_more, 0)
                recommended_products = [ast.literal_eval(item.get_data()) for item in elements]
                _test.log_json_data(log_message="Данные рекомендованных продуктов", log_data=recommended_products)

            with pytest.allure.step('Сверяем собранные данные.'):
                with pytest.allure.step('Сравниваем параметр "page"'):
                    _page.CHECK.check_data_layer_section_exist('page', raise_error=True)
                    _page.CHECK.check_data_layer_value('page', 'ProductPage', raise_error=True)

                with pytest.allure.step('Сравниваем параметр "currencyCode" у секции продукта'):
                    _page.CHECK.check_data_layer_section_exist('ecommerce', section_sub_key='detail', raise_error=True)
                    _page.CHECK.check_data_layer_value('ecommerce.currencyCode', 'RUB', section_add_key='detail', raise_error=True)

                with pytest.allure.step('Сравниваем параметр "currencyCode" у секции рекомендованных продуктов'):
                    _page.CHECK.check_data_layer_section_exist('ecommerce', section_sub_key='impressions', raise_error=True)
                    _page.CHECK.check_data_layer_value('ecommerce.currencyCode', 'RUB', section_add_key='impressions', raise_error=True)

                with pytest.allure.step('Сравниваем данные продукта'):
                    data_layer_product_data = _page.data_layer.get_value('ecommerce.detail.products')[0]
                    _page.CHECK.check_product_data_data_layer_eq(_detail_product, data_layer_product_data)

                with pytest.allure.step('Сравниваем рекомендованные продукты'):
                    with pytest.allure.step('Сравнение общего количества.'):
                        _page.CHECK.check_data_layer_length_impressions_eq(value=len(recommended_products), raise_error=True)

                    with pytest.allure.step('Сравнение продуктов на странице с элементами dataLayer'):
                        _page.CHECK.check_page_data_impressions_with_data_layer_eq(value=recommended_products, raise_error=True)

                    with pytest.allure.step('Сравнение элементов dataLayer с продуктами на странице'):
                        _page.CHECK.check_data_layer_impressions_data_with_page_eq(value=recommended_products, raise_error=True)

            with pytest.allure.step('Добавляем продукт в корзину'):
                _page.set_random_size()
                _page.set_random_color()
                _page.add_product_to_cart()

            with pytest.allure.step('Получаем данные DataLayer'):
                _page.init_data_layer()
                _test.log_json_data(log_message="DataLayer", log_data=_page.data_layer.data)

            with pytest.allure.step('Сверяем собранные данные.'):
                with pytest.allure.step('Сравниваем параметр "currencyCode" у секции продукта'):
                    _page.CHECK.check_data_layer_section_exist('ecommerce', section_sub_key='add', raise_error=True)
                    _page.CHECK.check_data_layer_value('ecommerce.currencyCode', 'RUB', section_add_key='add', raise_error=True)
                    _page.CHECK.check_data_layer_value('event', 'addToCart', section_add_key='ecommerce', raise_error=True)

                with pytest.allure.step('Сравниваем данные продукта'):
                    data_layer_product_data = _page.data_layer.get_value('ecommerce.add.products')[0]
                    _page.CHECK.check_product_data_data_layer_eq(_detail_product, data_layer_product_data)

            _detail_product.update({"color": _page.get_color_key()})
            with pytest.allure.step('Переходим на страницу корзины'):
                return _page.go_to_cart(), _detail_product

        def clean_wait(_test, _page, **kwargs):
            time.sleep(5)

        def step_check_cart_page(_test: TestAutotest, _page: VMRCartPage, _detail_product: dict):
            with pytest.allure.step('Собираем данные со страницы'):
                recommended_products = [{"list": item.get_init_product_list(),
                                         "name": "{0} {1}".format(item.get_product_type(), item.get_name()),
                                         "price": int(item.get_price()),
                                         "variant": item.variant(),
                                         "id": int(item.get_init_product_id())}
                                        for item in _page.get_interesting_products()
                                        ]
                _test.log_json_data(log_message="Данные рекомендованных продуктов", log_data=recommended_products)

            with pytest.allure.step('Получаем данные DataLayer'):
                _page.init_data_layer()
                _test.log_json_data(log_message="DataLayer", log_data=_page.data_layer.data)

            with pytest.allure.step('Сравниваем параметр "page"'):
                _page.CHECK.check_data_layer_section_exist('page', raise_error=True)
                _page.CHECK.check_data_layer_value('page', 'BasketPage', raise_error=True)

            with pytest.allure.step('Сравниваем параметр "currencyCode" у секции рекомендованных продуктов'):
                _page.CHECK.check_data_layer_section_exist('ecommerce', section_sub_key='impressions', raise_error=True)
                _page.CHECK.check_data_layer_value('ecommerce.currencyCode', 'RUB', section_add_key='impressions', raise_error=True)

            with pytest.allure.step('Сравниваем рекомендованные продукты'):
                with pytest.allure.step('Сравнение общего количества.'):
                    _page.CHECK.check_data_layer_length_impressions_eq(value=len(recommended_products),
                                                                       raise_error=True)

                with pytest.allure.step('Сравнение продуктов на странице с элементами dataLayer'):
                    _page.CHECK.check_page_data_impressions_with_data_layer_eq(value=recommended_products,
                                                                               raise_error=True)

                with pytest.allure.step('Сравнение элементов dataLayer с продуктами на странице'):
                    _page.CHECK.check_data_layer_impressions_data_with_page_eq(value=recommended_products,
                                                                               raise_error=True)

            with pytest.allure.step('Сравниваем параметры страницы'):
                _page.CHECK.check_data_layer_value('ecommerce.checkout.actionField.step', 1, raise_error=True)
                _page.CHECK.check_data_layer_value('event', 'checkout', section_add_key='ecommerce', raise_error=True)

                with pytest.allure.step('Сверяем собранные данные.'):
                    with pytest.allure.step('Сравниваем данные продукта'):
                        data_layer_product_data = _page.data_layer.get_value('ecommerce.checkout.products')[0]
                        _detail_product.pop("category_tkey", None)
                        _page.CHECK.check_product_data_data_layer_eq(_detail_product, data_layer_product_data)

            with pytest.allure.step('Переходим к доставкам'):
                return _page.go_to_order_page()

        def clean_open_cart_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()
            return _page.HEADER.go_to_cart()

        def clean_open_order_page(_test: TestAutotest, **kwargs):
            _page = _test.open_main_page()

            if _page.HEADER.get_cart_count() != 1:
                raise OrderEmptyCartException("Поп-ап не появился, но корзина очистилась")

            _page = _page.HEADER.go_to_cart()
            return _page.go_to_order_page()

        def step_check_order_page(_test: TestAutotest, _page: VMROrderPage, _detail_product, **kwargs):
            time.sleep(3)  # Ждем пока сгенерируется DataLayer
            with pytest.allure.step('Получаем данные DataLayer'):
                _page.init_data_layer()
                _test.log_json_data(log_message="DataLayer", log_data=_page.data_layer.data)

            with pytest.allure.step('Сравниваем параметры страницы'):
                with pytest.allure.step('Сравниваем параметр step'):
                    _page.CHECK.check_data_layer_value('ecommerce.checkout.actionField.step', 2, raise_error=True)

                with pytest.allure.step('Сравниваем параметр event'):
                    _page.CHECK.check_data_layer_value('event', 'checkout', section_add_key='ecommerce', raise_error=True)

                with pytest.allure.step('Сравниваем параметр "page"'):
                    _page.CHECK.check_data_layer_section_exist('page', raise_error=True)
                    _page.CHECK.check_data_layer_value('page', 'BasketPage', raise_error=True)

                with pytest.allure.step('Сверяем собранные данные.'):
                    with pytest.allure.step('Сравниваем данные продукта checkout.products'):
                        data_layer_product_data = _page.data_layer.get_value('ecommerce.checkout.products')[0]

                        #  Убираем несравневаемые данные
                        _tmp_data = _detail_product.copy()
                        data_layer_product_data.pop("quantity", None)

                        _page.CHECK.check_product_data_data_layer_eq(_tmp_data, data_layer_product_data)
                        _page.CHECK.check_product_data_data_layer_eq(data_layer_product_data, _tmp_data)

                return _page

        def step_create_order(_test: TestAutotest, _page: VMROrderPage, **kwargs):
            with pytest.allure.step('Вводим данные'):
                _page.set_data(email=self.active_test.data.login,
                               phone=self.active_test.data.phone_number,
                               name=self.active_test.data.name,
                               comment=self.active_test.data.comment,
                               address=self.active_test.data.address,
                               city=self.active_test.data.result_city,
                               region=self.active_test.data.result_region
                               )

            with pytest.allure.step("Ждем появления способов доставки"):
                _page.get_all_delivery_elements(num_gte=1, timeout=20)

                _page.set_courier_delivery_data()

            with pytest.allure.step("Создаем заказ"):
                return _page.go_to_create_order()

        def clean_fail_test(_test: TestAutotest, **kwargs):
            pytest.fail("Ошибки при проверерке страницы успешного заказа")

        def step_check_successful_order_page(_test: TestAutotest, _page: VMRSuccessfulOrderPage, _detail_product):
            with pytest.allure.step('Получаем номер заказа.'):
                order_num = _page.get_order_num()
                _test.log_text('Номер заказа "{0}"'.format(order_num))
                _page.close_pop_up()

            with pytest.allure.step('Получаем данные DataLayer'):
                _page.init_data_layer()
                _test.log_json_data(log_message="DataLayer", log_data=_page.data_layer.data)

            with pytest.allure.step('Проверяем наличие секций в DataLayer'):
                _page.CHECK.check_data_layer_section_exist('page', raise_error=True)
                _page.CHECK.check_data_layer_section_exist('ecommerce', section_add_key="event", raise_error=True)

            with pytest.allure.step('Сравниваем параметр event'):
                _page.CHECK.check_data_layer_value('event', 'checkout', section_add_key='ecommerce', raise_error=True)

            with pytest.allure.step('Сравниваем параметр step'):
                _page.CHECK.check_data_layer_value('ecommerce.checkout.actionField.step', 3, raise_error=True)

            with pytest.allure.step('Сравниваем параметр affiliation'):
                _page.CHECK.check_data_layer_value('ecommerce.purchase.actionField.affiliation', "Интернет-магазин", raise_error=True)

            with pytest.allure.step('Сверяем собранные данные.'):
                with pytest.allure.step('Сравниваем данные продукта ecommerce.checkout'):
                    data_layer_product_data = _page.data_layer.get_value('ecommerce.checkout.products')[0]

                    #  Убираем несравневаемые данные
                    _tmp_data = _detail_product.copy()
                    _tmp_data.pop("color", None)
                    data_layer_product_data.pop("quantity", None)

                    _page.CHECK.check_product_data_data_layer_eq(_tmp_data, data_layer_product_data)
                    _page.CHECK.check_product_data_data_layer_eq(data_layer_product_data, _tmp_data)

                with pytest.allure.step('Сравниваем данные продукта ecommerce.purchase'):
                    data_layer_product_data = _page.data_layer.get_value('ecommerce.purchase.products')[0]

                    #  Убираем несравневаемые данные
                    _tmp_data = _detail_product
                    _tmp_data.pop("color", None)
                    data_layer_product_data.pop("quantity", None)

                    _page.CHECK.check_product_data_data_layer_eq(_tmp_data, data_layer_product_data)
                    _page.CHECK.check_product_data_data_layer_eq(data_layer_product_data, _tmp_data)

        def clean_empty(**kwargs):
            pass

        def step_check_del_cart(_test: TestAutotest, _page, _product_index, **kwargs):
            with pytest.allure.step('Открываем главную страницу'):
                _page = _test.open_main_page()
            with pytest.allure.step('Открываем страницу дизайнера'):
                _page = _page.open_by_url('/designer/VQ', VMRDesignerPage)
            with pytest.allure.step('Выбираем рандомный индекс продукта'):
                _index = random.randint(0, len(_page.get_products_elements())) if _product_index is None else _product_index
                _test.log_text("Индекс продукта = \"{0}\"" .format(_index))
            with pytest.allure.step('Переходим на странцу продукта'):
                _page = _page.go_to_product_by_index(index=_index)

            with pytest.allure.step('Собираем данные со страницы'):

                elements = _page.BREADCRUMBS.get_breadcrumbs()
                names = [item.get_name() for item in elements]
                tkeys = [item.get_tkey() if item.get_tkey() else None for item in elements]

                start_index = None
                result = []

                for index, item in enumerate(zip(names, tkeys)):
                    if item[0] == _page.get_product_type_name():
                        start_index = index
                    if start_index and start_index <= index:
                        result.append(item)

                _detail_product = {"name": _page.get_full_name(),
                                   "price": _page.get_price(),
                                   "variant": _page.get_product_type_eng(),
                                   "tkey": _page.get_product_type_eng(),
                                   "id": _page.get_product_int_id(),
                                   "category_tkey": ">".join([item[1] for item in list(result)[1:]]),
                                   "category": "/".join([item[0] for item in list(result)])}
                _test.log_json_data(log_message="Данные продукта", log_data=_detail_product)

            with pytest.allure.step('Устанавливаем случайный цвет и размер'):
                _page.set_random_color()
                _page.set_random_size()
                _detail_product.update({"color": _page.get_color_key()})

            with pytest.allure.step('Добавляем продукт в корзину'):
                _page.add_product_to_cart()

            with pytest.allure.step('Переходим на страницу корзины'):
                _page = _page.go_to_cart()

            with pytest.allure.step('Удалаяем продукт из корзины'):
                _page.delete_product_by_index(0)
                _page.get_product_elements().assure(len_equal, 0)
                time.sleep(5)  # Ждем для генерации datalayer

            with pytest.allure.step('Получаем данные DataLayer'):
                _page.init_data_layer()
                _test.log_json_data(log_message="DataLayer", log_data=_page.data_layer.data)

            with pytest.allure.step('Проверяем наличие секций в DataLayer'):
                _page.CHECK.check_data_layer_section_exist('ecommerce', section_sub_key="remove", raise_error=True)

            with pytest.allure.step('Сравниваем параметр event'):
                _page.CHECK.check_data_layer_value(key='event', value="removeFromCart",
                                                   section_key="ecommerce", section_sub_key="remove",
                                                   raise_error=True)

            with pytest.allure.step('Сверяем собранные данные.'):
                with pytest.allure.step('Сравниваем данные продукта ecommerce.remove'):
                    data_layer_product_data = _page.data_layer.get_value('ecommerce.remove.products')[0]

                    #  Убираем несравневаемые данные
                    _tmp_data = _detail_product.copy()
                    _tmp_data.pop("category_tkey", None)
                    data_layer_product_data.pop("quantity", None)

                    _page.CHECK.check_product_data_data_layer_eq(_tmp_data, data_layer_product_data)
                    _page.CHECK.check_product_data_data_layer_eq(data_layer_product_data, _tmp_data)

        with pytest.allure.step('Открываем главнцю страницу'):
            page = self.step(step_open_main_page, clean_wait, self, None)

        with pytest.allure.step('Проверяем главнцю страницу'):
            self.step(step_check_main_page, step_open_main_page, self, page)

        with pytest.allure.step('Выбираем случайный каталог'):
            catalog_index = random.randint(5, len(page.CATALOG_MENU.get_catalogs_elements())-1)
            self.log_text("Индекс каталога ='{0}'" .format(catalog_index))

        with pytest.allure.step('Переходим в каталог'):
            page = self.step(step_go_to_catalog, step_open_main_page, self, page, _catalog_index=catalog_index)
        with pytest.allure.step('Проверяем страницу каталога'):
            self.step(step_check_catalog_page, clean_catalog_page, self, page, _catalog_index=catalog_index)
        with pytest.allure.step('Переходим на страницу поиска'):
            page = self.step(step_go_to_search_page, step_open_main_page, self, self.open_main_page())
        with pytest.allure.step('Проверяем страницу поиска'):
            self.step(step_check_search_page, step_go_to_search_page, self, page)
        with pytest.allure.step('Переходим на страницу продукта'):
            page, product_index = self.step(step_open_product_page, step_open_main_page, self, None, index=None)
        with pytest.allure.step('Проверяем страницу продукта'):
            page, detail_product = self.step(step_check_product_page, step_open_product_page, self, page, index=product_index)
        with pytest.allure.step('Переходим на страницу корзины'):
            page = self.step(step_check_cart_page, clean_open_cart_page, self, page, _detail_product=detail_product)
        with pytest.allure.step('Проверяем страницу заказа'):
            page = self.step(step_check_order_page, clean_open_order_page, self, page, _detail_product=detail_product)
        with pytest.allure.step('Создаем заказа'):
            page = self.step(step_create_order, clean_open_order_page, self, page)
        with pytest.allure.step('Проверяем страницу успешного заказа'):
            self.step(step_check_successful_order_page, clean_fail_test, self, page, _detail_product=detail_product)
        with pytest.allure.step('Проверяем удаление продукта из корзины'):
            self.step(step_check_del_cart, clean_empty, self, None, _product_index=1)

    def test_vmr_opt_page(self):

        def step_open_main_page(_test: TestAutotest, _page: VMRMainPage):
            return _test.open_main_page()

        def clean_page(_test, _page):
            self.driver.refresh()

        def step_open_opt_page(_test: TestAutotest, _page: VMROptPage, **kwargs):
            _page = _test.init_page_by_url(VMROptPage, None, 'https://opt.vsemayki.ru/', 10, True)
            return _page

        def step_empty_input(_test: TestAutotest, _page: VMROptPage, **kwargs):
            _page.send_request(phone=None, name=None)

            _page.CHECK.check_value_text_error(expected_text = '*пожалуйста, введите контактный телефон',
                                               page_error_text=_page.get_phone_error_text_element(), raise_error=True)

            _page.CHECK.check_value_text_error(expected_text = '*пожалуйста, введите ваше имя',
                                               page_error_text=_page.get_name_error_text_element(), raise_error=True)

        def step_corrected_input(_test: TestAutotest, _page: VMROptPage, **kwargs):
            _page.send_request(phone=_test.active_test.data.phone_number,
                               name=_test.active_test.data.name, go_OptOrderSent=True)

        def step_empty_question(_test: TestAutotest, _page: VMROptPage, **kwargs):
            _page.send_question(phone=None, name=None, text=None)

            _page.CHECK.check_value_text_error(expected_text='*пожалуйста, введите контактный телефон',
                                               page_error_text=page.get_question_phone_error_text_element(), raise_error=True)

            _page.CHECK.check_value_text_error(expected_text='*пожалуйста, введите ваше имя',
                                               page_error_text=page.get_question_name_error_text_element(), raise_error=True)

            _page.CHECK.check_value_text_error(expected_text='*пожалуйста, введите ваш вопрос',
                                               page_error_text=page.get_question_text_error_text_element(), raise_error=True)

        def step_corrected_question(_test, _page: VMROptPage, **kwargs):
            _page.send_question(phone=_test.active_test.data.phone_number,
                                name=_test.active_test.data.name,
                                text=_test.active_test.data.text,
                                go_OptOrderSent=True)

        self.subtest_start(353)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)

        with pytest.allure.step('Открываем cтраницу опта'):
            page = self.step(step_open_opt_page, clean_page, step_test=self, step_page=page)

        with pytest.allure.step('Проверяем отправку пустой заявки '):
            self.step(step_empty_input, clean_page, step_test=self, step_page=page)
        self.subtest_pass(353)

        self.subtest_start(352)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)

        with pytest.allure.step('Открываем cтраницу опта'):
            page = self.step(step_open_opt_page, clean_page, step_test=self, step_page=page)

        with pytest.allure.step('Проверяем отправку заполненной заявки '):
            self.step(step_corrected_input, clean_page, step_test=self, step_page=page)
        self.subtest_pass(352)

        self.subtest_start(355)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)

        with pytest.allure.step('Открываем cтраницу опта'):
            page = self.step(step_open_opt_page, clean_page, step_test=self, step_page=page)

        with pytest.allure.step('Проверяем отправку пустого сообщения менеджеру '):
            self.step(step_empty_question, clean_page, step_test=self, step_page=page)
        self.subtest_pass(355)

        self.subtest_start(354)
        with pytest.allure.step('Открываем главную страницу'):
            page = self.step(step_open_main_page, clean_page, self, None)

        with pytest.allure.step('Открываем cтраницу опта'):
            page = self.step(step_open_opt_page, clean_page, step_test=self, step_page=page)

        with pytest.allure.step('Проверяем отправку сообщения менеджеру '):
            self.step(step_corrected_question, clean_page, step_test=self, step_page=page)
        self.subtest_pass(354)