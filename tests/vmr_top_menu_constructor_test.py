import pytest
import time

from module_projects.vmr.base_test import ProjectBaseTest
from module_projects.vmr.pages.MainPage.page import VMRMainPage

test_elements_number = 300


class TestAutotest(ProjectBaseTest):
    @pytest.yield_fixture(autouse=True, scope='class')
    def clean_cache(self, request, server_test_run_id):
        yield
        request.config.cache.set("elements_{0}".format(server_test_run_id), None)

    @pytest.mark.parametrize("element_number", range(0, test_elements_number))
    def test_vmr_monitoring_top_menu_constructor(self, request, element_number, server_test_run_id):

        def step_get_elements(_test: TestAutotest, _server_test_run_id, **kwargs):
            _elements = request.config.cache.get("elements_{0}".format(_server_test_run_id), None)

            if _elements is None:
                print('INIT MENU CACHE')
                page = self.open_main_page()
                _elements = page.HEADER.DROP_DOWN_MENU.get_menu_structure(type_filter=['constructor'])
                _elements = [item.to_json() for item in _elements]
                request.config.cache.set("elements_{0}".format(_server_test_run_id), _elements)

            if element_number > len(_elements) - 1:
                pytest.skip()

            if test_elements_number < len(_elements):
                pytest.fail('Требуется увеличить количество тестов в соответствии с количеством продуктов.')

            return _elements

        def clean_wait(**kwargs):
            time.sleep(5)

        def step_open_main_page(_test: TestAutotest, **kwargs):
            return self.open_main_page()

        def step_open_menu_element(_test: TestAutotest, _page: VMRMainPage, _element_data):
            _test.log_json_data(log_data=_element_data, log_message="Данные элемента")
            _test.log_screenshot()
            _page = _page.HEADER.DROP_DOWN_MENU.go_to_menu_item_by_name(name=_element_data['name'],
                                                                        main_name=_element_data['main_name'],
                                                                        sub_menu_name=_element_data['sub_menu_name'])
            _page.CHECK.check_price_not_eq(value=0, raise_error=True)

        elements = self.step(step_get_elements, clean_wait, self, None, _server_test_run_id=server_test_run_id)

        element = elements[element_number]

        if element['href'] is None:
            pytest.skip()

        self.log_text('Элемент - {0}, Главное меню - {1}, Сабменю - {2}'.format(element['name'], element['main_name'], element['sub_menu_name']))

        page = self.step(step_open_main_page, clean_wait, self, None)
        self.step(step_open_menu_element, step_open_main_page, self, page, _element_data=element)