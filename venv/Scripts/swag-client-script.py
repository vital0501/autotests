#!C:\Users\User\PycharmProjects\autotests\autotests\venv\Scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'swag-client==0.3.7','console_scripts','swag-client'
__requires__ = 'swag-client==0.3.7'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('swag-client==0.3.7', 'console_scripts', 'swag-client')()
    )
